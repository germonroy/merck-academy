<!--#include file="data/con_ma.inc" -->
<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US

Set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3 %>
<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/css/core-default.css" rel="stylesheet" type="text/css" />
<link href="assets/css/components.css" rel="stylesheet" type="text/css" />
<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="assets/css/pages.css" rel="stylesheet" type="text/css" />

<!-- / Google Analytics by Yoast -->
<script type='text/javascript' src='https://amazingcarousel.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://amazingcarousel.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://amazingcarousel.com/wp-content/uploads/amazingcarousel/sharedengine/amazingcarousel.js?ver=1.2'></script>
<div class="col-md-12">
    <div class="demo-slider">
        <link rel="stylesheet" type="text/css" media="all" href="https://amazingcarousel.com/wp-content/uploads/amazingcarousel/10/carouselengine/initcarousel.css">
        <div id="amazingcarousel-container-10">
            <div id="amazingcarousel-10" style="display: block; position: relative; width: 240px; margin: 0px auto; direction: ltr;">
                <div class="amazingcarousel-list-container" style="overflow: visible; position: relative; margin: 0px auto; height: 372px;">
                    <div class="amazingcarousel-list-wrapper" style="overflow: hidden; height: 100%; width: 240px;">
                        <ul class="amazingcarousel-list" style="display: block; position: relative; list-style-type: none; list-style-image: none; background-image: none; background-color: transparent; padding: 0px; margin: -1572.31px 0px 0px; height: 2340px;">
                            <li class="amazingcarousel-item" style="display: block; position: relative; background-image: none; background-color: transparent; margin: 0px; padding: 0px; width: 240px;">
                                <div class="amazingcarousel-item-container" style="position: relative; margin: 0px 0px 12px;">
                                    <div class="amazingcarousel-image">
                                        <a href="https://amazingcarousel.com/wp-content/uploads/amazingcarousel/10/images/lightbox/golden-wheat-field-lightbox.jpg" title="Golden Wheat Field" class="html5lightbox" data-group="amazingcarousel-10">
                                            <img src="https://amazingcarousel.com/wp-content/uploads/amazingcarousel/10/images/golden-wheat-field.jpg" alt="Golden Wheat Field"></a>
                                        <div class="amazingcarousel-text">
                                            <div class="amazingcarousel-text-bg"></div>
                                            <div class="amazingcarousel-title">Golden Wheat Field</div>
                                        </div>
                                        <div class="amazingcarousel-hover-effect" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; cursor: pointer; background-image: url(&quot;https://amazingcarousel.com/wp-content/uploads/amazingcarousel/10/carouselengine/skins/hoveroverlay-64-64-4.png&quot;); background-repeat: no-repeat; background-position: center center; display: none;"></div>
                                    </div>
                                </div>
                            </li>
                            <% claseNov = "active"
                                SQL = "select *, (select titulo from tbl_videos where id_video = tbl_top_contenido.id_tipo) as titulo from tbl_top_contenido where tipo = 'V' and localizacion = 'N' and activo = 'SI' order by newid()"
                                Cursor.Open SQL, StrConn, 1, 3
                                Do While Not Cursor.EOF %>

                            <li class="amazingcarousel-item" style="display: block; position: relative; background-image: none; background-color: transparent; margin: 0px; padding: 0px; width: 240px;">

                                <div class="item <%=claseNov %>" style="margin-right: 7px;">
                                    <div class="col-md-12 card-box text-left hvr-grow ">
                                        <figure class="effect-honey">
                                            <div class="col-md-6 novedades-img">
                                                <img src="images/videoteca/videos/<%=Cursor("id_tipo") %>.jpg" alt="<%=Cursor("id_tipo") %>" class="img-responsive articles-img" />
                                            </div>
                                            <div class="col-md-6">
                                                <h4 class="text-dark"><b>Videolecci�n:&nbsp;<%=Cursor("titulo") %></b></h4>
                                                <h5 class="text-dark"><%=Cursor("texto") %></h5>
                                                <a href="login.asp" class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">Ver m�s</a>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </li>

                            <% Cursor.MoveNext
                                claseNov = ""
                                Loop                                
                                Cursor.Close %>
                            <div style="clear: both;"></div>
                        </ul>
                    </div>
                </div>
                <div class="amazingcarousel-prev" style="overflow: hidden; position: absolute; cursor: pointer; width: 32px; height: 32px; background: url(&quot;https://amazingcarousel.com/wp-content/uploads/amazingcarousel/10/carouselengine/skins/arrows-32-32-4.png&quot;) left top no-repeat; display: block;"></div>
                <div class="amazingcarousel-next" style="overflow: hidden; position: absolute; cursor: pointer; width: 32px; height: 32px; background: url(&quot;https://amazingcarousel.com/wp-content/uploads/amazingcarousel/10/carouselengine/skins/arrows-32-32-4.png&quot;) right top no-repeat; display: block;"></div>
                <div class="amazingcarousel-nav"></div>
                <div class="amazingcarousel-engine" style="display: none;"><a href="https://amazingcarousel.com">JavaScript Image Scroller</a></div>
                <div class="amazingcarousel-engine" style="display: none;"><a href="https://amazingcarousel.com">WordPress Scroller</a></div>
            </div>
        </div>
        <script src="https://amazingcarousel.com/wp-content/uploads/amazingcarousel/10/carouselengine/initcarousel.js"></script>
    </div>
</div>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>