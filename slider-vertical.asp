<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>Vertical &amp; horizontal slider / Carousel - Bootsnipp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        window.alert = function () { };
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css) {
            if (css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="' + css + '" type="text/css" />');
            else $('head > link').filter(':first').replaceWith(defaultCSS);
        }
        $(document).ready(function () {
            var iframe_height = parseInt($('html').height());
            window.parent.postMessage(iframe_height, 'https://bootsnipp.com');
        });
    </script>
</head>
<style>
    .carousel-inner.vertical {
        height: 100%; /*Note: set specific height here if not, there will be some issues with IE browser*/
    }

        .carousel-inner.vertical > .item {
            -webkit-transition: .6s ease-in-out top;
            -o-transition: .6s ease-in-out top;
            transition: .6s ease-in-out top;
        }

    @media all and (transform-3d), (-webkit-transform-3d) {
        .carousel-inner.vertical > .item {
            -webkit-transition: -webkit-transform .6s ease-in-out;
            -o-transition: -o-transform .6s ease-in-out;
            transition: transform .6s ease-in-out;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-perspective: 1000;
            perspective: 1000;
        }

            .carousel-inner.vertical > .item.next,
            .carousel-inner.vertical > .item.active.right {
                -webkit-transform: translate3d(0, 33.33%, 0);
                transform: translate3d(0, 33.33%, 0);
                top: 0;
            }

            .carousel-inner.vertical > .item.prev,
            .carousel-inner.vertical > .item.active.left {
                -webkit-transform: translate3d(0, -33.33%, 0);
                transform: translate3d(0, -33.33%, 0);
                top: 0;
            }

                .carousel-inner.vertical > .item.next.left,
                .carousel-inner.vertical > .item.prev.right,
                .carousel-inner.vertical > .item.active {
                    -webkit-transform: translate3d(0, 0, 0);
                    transform: translate3d(0, 0, 0);
                    top: 0;
                }
    }

    .carousel-inner.vertical > .active {
        top: 0;
    }

    .carousel-inner.vertical > .next,
    .carousel-inner.vertical > .prev {
        top: 0;
        height: 100%;
        width: auto;
    }

    .carousel-inner.vertical > .next {
        left: 0;
        top: 33.33%;
        right: 0;
    }

    .carousel-inner.vertical > .prev {
        left: 0;
        top: -33.33%;
        right: 0;
    }

        .carousel-inner.vertical > .next.left,
        .carousel-inner.vertical > .prev.right {
            top: 0;
        }

    .carousel-inner.vertical > .active.left {
        left: 0;
        top: -33.33%;
        right: 0;
    }

    .carousel-inner.vertical > .active.right {
        left: 0;
        top: 33.33%;
        right: 0;
    }

    #carousel-pager .carousel-control.left {
        bottom: initial;
        width: 100%;
    }

    #carousel-pager .carousel-control.right {
        top: initial;
        width: 100%;
    }
</style>
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div id="carousel-pager" class="carousel slide " data-ride="carousel" data-interval="1000">
                <!-- Carousel items -->
                <div class="carousel-inner vertical">
                    <div class="active item">
                        <img src="http://placehold.it/600/f44336/000000&text=First+Slide" class="img-responsive" data-target="#carousel-main" data-slide-to="0">
                    </div>
                    <div class="item">
                        <img src="http://placehold.it/600/e91e63/000000&text=Second+Slide" class="img-responsive" data-target="#carousel-main" data-slide-to="1">
                    </div>
                    <div class="item">
                        <img src="http://placehold.it/600/9c27b0/000000&text=Third+Slide" class="img-responsive" data-target="#carousel-main" data-slide-to="2">
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-pager" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-pager" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    $('.carousel .vertical .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=1;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
</script>
