<script>
    $("#wrapper").toggleClass("enlarged");
    $("#wrapper").addClass("forced");
    $("body").removeClass("fixed-left").addClass("fixed-left-void");
    $("#botonbarramenulateral").addClass("btn-closed-menu");

    $("#btn-sidemenu").toggleClass("btn-closed-menu");
    $("#btn-sidemenu").addClass("btn-closed-menu");
    function btnSideMenu() {
        $("#btn-sidemenu").toggleClass("btn-closed-menu");
        $("#btn-sidemenu").addClass("btn-closed-menu");
    }
</script>
<script>
    function LiberarInformacion() {
        cat = 1;
        localStorage.clear();
        $.ajax({
            url: "jq/jq_logout.asp",
            type: "POST",
            data: { "cat": cat },
            async: false,
            success: function (opciones) {
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        })

        window.location = "default.asp";
        
    }
</script>

<style>
    @media only screen and (max-width: 992px) {
        .logo-legales {
            max-height: 10px;
        }
    }
</style>
<div style="height: 5vh;"></div>
<div class="cookie-pop text-center">
    <div class="form-inline">
        <div class="col-md-5 grad-aval">
        </div>
        <div class="col-md-2">
            <h2 class="text-center"><b>Avalado por:</b></h2>
        </div>
        <div class="col-md-5 grad-aval">
        </div>

    </div>

    <div class="row">
        <div id="adv_range_3_columns_carousel col-xs-12" class="hidden-lg hidden-md hidden-sm carousel slide three_shows_one_move range_slides_carousel_wrapper" data-ride="carousel" data-interval="3000" data-pause="hover">
            <div class="carousel-inner range_slides_carousel_inner" role="listbox">
                <div class="item active">
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/conamege.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/cmot.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/sccot.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                </div>
                <div class="item">
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/cncd.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/afeme.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/amig.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                </div>
                <div class="item">
                    <!-- <div class="col-xs-4 range_slides_item_image">
                        <img src="images/avales/avales/cmim.png" class="img-responsive center-block" style="width: 90%" />
                    </div>-->
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/amis.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/clemi.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/amig.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                </div>
                <div class="item">
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/amot.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/agot.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/AMETD-logo.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                </div>
                <div class="item">
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/shetd.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/anot.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/cmmg.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                </div>
                <div class="item">
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/ammf.png" class="img-responsive center-block" style="width: 90%" />
                    </div>
                    <div class="col-xs-4 range_slides_item_image">
                        <img src="assets/images/avales/avales/afeme.png" class="img-responsive center-block" style="width: 90%" />
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row hidden-xs">
        <div class="container-footer">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/conamege.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/cmot.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/sccot.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/cncd.png" class="img-responsive center-block" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/afeme.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/amig.png" class="img-responsive center-block" />
                    </div>
                    <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <img src="images/avales/avales/cmim.png" class="img-responsive center-block" />
                </div>-->
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/amis.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/cmmg.png" class="img-responsive center-block" />
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/ammf.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/clemi.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/amot.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/agot.png" class="img-responsive center-block" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/AMETD-logo.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/shetd.png" class="img-responsive center-block" />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="assets/images/avales/avales/anot.png" class="img-responsive center-block" />
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="row knockout-around">
        <div align="center" class="col-xs-12 hidden-xs">
            <a href="http://www.merck.com.mx/es/index.html" target="_blank">Conozca m&aacute;s sobre
                    <img src="assets/images/logos/logo_merck.png" style="width: 70px;" /></a>
            / <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-terminos-condiciones">T&eacute;rminos y condiciones </a>/ 
        <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-aviso-privacidad">Aviso de privacidad</a>
              / 
        <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-cookies">Pol&iacute;tica de cookies</a>
            <br />
            Informaci&oacute;n exclusiva para profesionales de la salud

            <div class="row num-aviso">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    N&uacute;mero de aviso 153300202C1679
                </div>
            </div>

        </div>
        <div align="center" class="hidden-lg hidden-md hidden-sm col-xs-12">
            <div class="row" id="footer-movil">
                <div class="col-xs-4 text-center center-block">
                    <a href="http://www.merck.com.mx/es/index.html" target="_blank">
                        <img src="assets/images/logos/merck.png" style="margin-bottom: 5px; width: 40%;" />
                    </a>
                </div>
                <div class="col-xs-4 text-center center-block">
                    <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-terminos-condiciones">
                        <img src="assets/images/logos/terminos2.png" style="margin-bottom: 5px; width: 130%;" />
                </div>
                <div class="col-xs-4 text-center center-block">
                    <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-aviso-privacidad">
                        <img src="assets/images/logos/legales.png" class="logo-legales" style="margin-bottom: 5px;" />
                    </a>
                </div>
            </div>
            <div class="row num-aviso">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    N&uacute;mero de aviso 153300202C1679
                </div>
            </div>
        </div>
    </div>
</div>
