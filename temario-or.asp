﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
    <link href="css/default.css" rel="stylesheet" />
    <link href="css/carousel_fade.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />
    <link href="css/gp_product_carousel_advance.css" rel="stylesheet" media="all">
    <link href="css/portfolio_carousel.css" rel="stylesheet" media="all">
    <link href="assets/plugins/bootstrap-sweetalerts2/sweetalert2.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>





    <style>
        .Tache {
            opacity: 10 !important;
        }
    </style>

</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->

    <!--#include file="top_temario.asp" -->
    <div class="row" style="margin-top: 90px;">
        <%
            for i = 2 to 9
        %>
        <div class="col-md-12">
            <img class="img-responsive" width="100%" src="images/temario/Ortopedia_0<%=i %>.png" />
        </div>
        <%
            next
        %>
        <%
            for i = 10 to 16
        %>
        <div class="col-md-12">
            <img class="img-responsive" width="100%" src="images/temario/Ortopedia_<%=i %>.png" />
        </div>
        <%
            next
        %>
    </div>


    <!--#include file="footer.asp" -->
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!--======= Touch Swipe =========-->
    <script src="js/jquery.touchSwipe.min.js"></script>

    <!--======= Customize =========-->
    <script src="js/responsive_bootstrap_carousel.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script src="js/MetroJs.js" type="text/javascript"></script>
    <script src="js/loader.js"></script>
    <script src="js/sticky-nav.js"></script>
    <script src="js/logout.js"></script>

    <!--Alerts-->
    <script src="assets/plugins/bootstrap-sweetalerts2/sweetalert2.js"></script>


</body>
</html>
