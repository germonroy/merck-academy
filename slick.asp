<html>
<head>
    <title>My Now Amazing Webpage</title>
    <link href="css/slick.css" rel="stylesheet" />
    <link href="css/slick-theme.css" rel="stylesheet" />
    <style>
        .slider {
            width: 650px;
            margin: 0 auto;
        }

        img {
            width: 200px;
            height: 200px;
        }
    </style>
</head>
<body>

    <div class="slider">
        <div>
            <img src="http://kenwheeler.github.io/slick/img/fonz1.png" />
        </div>
        <div>
            <img src="http://kenwheeler.github.io/slick/img/fonz2.png" />
        </div>
        <div>
            <img src="http://kenwheeler.github.io/slick/img/fonz3.png" />
        </div>
        <div>
            <img src="http://kenwheeler.github.io/slick/img/fonz1.png" />
        </div>
        <div>
            <img src="http://kenwheeler.github.io/slick/img/fonz2.png" />
        </div>
        <div>
            <img src="http://kenwheeler.github.io/slick/img/fonz3.png" />
        </div>
    </div>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/slick.min.js"></script>

    <script type="text/javascript">
        $('.slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1
        });
    </script>

</body>
</html>
			