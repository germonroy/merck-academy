﻿function likeVideo(usr, vid) {
    $.ajax({
        url: "../jq/jq_likes_dislikes.asp",
        type: "POST",
        data: { "idu": usr, "idv": vid, "lik": 1 },
        async: false,
        success: function (cont) {
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseText);
        }
    })
}

function dislikeVideo(usr, vid) {
    $.ajax({
        url: "../jq/jq_likes_dislikes.asp",
        type: "POST",
        data: { "idu": usr, "idv": vid, "lik": 0 },
        async: false,
        success: function (cont) {
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseText);
        }
    })
}

function actualizaLikes(vid) {
    $.ajax({
        url: "../jq/jq_actualiza_likes.asp",
        type: "POST",
        data: { "idv": vid },
        async: false,
        success: function (opciones) {
            var obj = JSON.parse(opciones);
            $(document.body).find(".countlikes").html(obj.likes);
            $(document.body).find(".countdislikes").html(obj.dislikes);
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseText);
        }
    })
}

function actualizarVistas(vid) {
    $.ajax({
        url: "../jq/jq_actualiza_vistas.asp",
        type: "POST",
        data: { "idv": vid },
        async: false,
        success: function (opciones) {
            var obj = JSON.parse(opciones);
            $(document.body).find(".numVistas").html(obj.vistas);
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseText);
        }
    })
}

function actualizaLikesParent(vid) {
    $.ajax({
        url: "../jq/jq_actualiza_likes.asp",
        type: "POST",
        data: { "idv": vid },
        async: false,
        success: function (opciones) {
            var obj = JSON.parse(opciones);
            $(window.parent.document).find(".countlikes").html(obj.likes);
            $(window.parent.document).find(".countdislikes").html(obj.dislikes);
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseText);
        }
    })
}

