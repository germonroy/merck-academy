﻿$(document).ready(function () {
    var stickyNavTop = $('.logo-dyn').offset().top;

    var stickyNav = function () {
        var scrollTop = $(window).scrollTop();

        if (scrollTop > stickyNavTop) {
            $('.logo-dyn').addClass('sticky');
            $("#chrome").hide();
        } else {
            $('.logo-dyn').removeClass('sticky');
            $("#chrome").show();
        }
    };

    stickyNav();

    $(window).scroll(function () {
        stickyNav();
    });
});