﻿var $this, modulo;

$(".btn-objetivos").click(function () {
     $this = $(this);
     modulo = $(this).data('modulo');
     $.ajax({
         async: false,
         type: 'POST',
         url: 'jq/jq_objetivos.asp',
         data: {
             id_modulo :modulo
         },
         encode: true
     }).done(function (data) {
         $("#text-obj").empty();
         $("#text-obj").append(data);
     });
     
});