﻿$(".switch-leftbar").click(function () {
    if (($("#switch").hasClass("open-switch")) && ($("#wrapper").hasClass("enlarged"))) {
        $("#switch").removeClass("open-switch");
        $("#switch").addClass("closed-switch");
        $("#swbtn").html(">");
    }
    else {
        $("#switch").removeClass("closed-switch");
        $("#switch").addClass("open-switch");
        $("#swbtn").html("<");
    }
});