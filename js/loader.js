﻿//$(document).ready(function () {
//    $(".live-tile,.flip-list").liveTile();
//    $(".appbar").applicationBar();
//    //         $(".live-tile").liveTile("goto", { index: "next", autoAniDirection: true });
//});

/* ==============================================
LOADER -->
=============================================== */

$(window).load(function () {
    $('#loader').delay(300).fadeOut('slow');
    $('#loader-container').delay(200).fadeOut('slow');
    $('body').delay(300).css({ 'overflow': 'visible' });
})