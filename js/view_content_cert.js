﻿$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});
$("[data-toggle='popover']").on('shown.bs.popover', function () {
    PopOverOn = false;
});

var PopOverOn;
$(document).ready(function () {
    $('#btnExam').popover({
        trigger: 'manual',
        placement: 'top',
        title: 'Evaluación Desbloqueada',
        html: true,
        content: 'Ha completado todas las videolecciones del Módulo ¡Ya tiene acceso  a la Evaluación Final! <a class="close" onclick="Pop();" style="position: absolute; top: 0; right: 6px;">&times;</a>'
    });
    $('#btnConstancia').popover({
        trigger: 'manual',
        placement: 'top',
        title: 'Constancia Desbloqueada',
        html: true,
        content: 'Ahora que has visto todas las videolecciones del módulo tienes la posibilidad de generar tu constancia <a class="close" onclick="Pop();" style="position: absolute; top: 0; right: 6px;">&times;</a>'
    });
    $('#btnExamMod').popover({
        trigger: 'manual',
        placement: 'top',
        title: 'Evaluación Desbloqueada',
        html: true,
        content: 'Ha completado todas las videolecciones del Módulo ¡Ya tiene acceso  a la Evaluación Final! <a class="close" onclick="Pop();" style="position: absolute; top: 0; right: 6px;">&times;</a>'
    });
    $('#btnConstanciaMod').popover({
        trigger: 'manual',
        placement: 'top',
        title: 'Constancia Desbloqueada',
        html: true,
        content: 'Ahora que has visto todas las videolecciones del módulo tienes la posibilidad de generar tu constancia <a class="close" onclick="Pop();" style="position: absolute; top: 0; right: 6px;">&times;</a>'
    });
});

function scrollIframe() {
    var element = document.getElementById("VCirugias");

    element.scrollIntoView();
}
function Pop() {
    $('#btnExam').popover('hide');
    $('#btnExamMod').popover('hide');
    $("#btnConstanciaMod").popover('hide');
    $('#btnConstancia').popover('hide');
}
$(document).click(function (e) {
    if (PopOverOn) {
        $('#btnExam').popover('hide');
        $('#btnConstancia').popover('hide');
        $("#btnConstanciaMod").popover('hide');
        $('#btnConstancia').popover('hide');
    }

});
