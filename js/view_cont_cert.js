﻿$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});
$(function () {
    $('[rel="popover"]').popover({
        container: 'body',
        html: true,
        placement: "left",
        content: function () {
            var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return clone;
        }
    }).click(function (e) {
        e.preventDefault();
    });
});

var slide = $('.slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    cssEase: 'linear',
    responsive: [
          {
              breakpoint: 1024,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  infinite: true,
                  dots: true
              }
          },
          {
              breakpoint: 600,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
    ]
});

$('#filters').on('click', 'a', function () {
    $('.slider').slick('slickUnfilter');
    var filterValue = $(this).attr('data-filter');
    $('.slider').slick('slickFilter', '' + filterValue + '');
});

$('.filters2').on('click', 'a', function () {
    $('.slider').slick('slickUnfilter');
    var filterValue = $(this).attr('data-filter');
    $('.slider').slick('slickFilter', '' + filterValue + '');
});

function enterCur(x) {
    $('#content-child').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $('#content-child').hide();
        $("#content-parent").animate({ width: "100%" }, "slow", function () {
            $("#content-parent").removeClass("col-md-8");
            $("#content-parent").addClass("col-md-11");
            $("#contenidoFrame").animate({ width: "100%" }, "slow");
            $("#contenidoFrame").width("100%");

            $(".btn-show").show();


        });
    });


}

function enterCurTopics(x) {
    $('#content-son-topics').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $("#contenidoRel").addClass("fullwidthframe");
        $('#content-son-topics').hide();
        $("#content-parent-topics").animate({ width: "100%" }, "slow", function () {
            $("#content-parent-topics").removeClass("col-md-8");
            $("#content-parent-topics").removeClass("col-sm-pull-4");
            $("#content-parent-topics").addClass("col-md-11");

            $(".btn-show-topics").show();
        });
  
    });
}

function enterCur2(x) {
    $("#content-parent").animate({ width: "66.66666667%" }, 1000, function () {
        $('#content-child').removeAttr('class');
        $("#content-child").addClass("col-md-4");
        $('#content-child').show();
        $(".btn-show").hide();
        $('#content-child').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $("#content-parent").removeClass("col-md-11");
            $("#content-parent").addClass("col-md-8");
        });
    });
}

function enterCur2Topics(x) {
    $("#content-parent-topics").animate({ width: "66.66666667%" }, 1000, function () {
        $("#contenidoRel").removeClass("fullwidthframe");
        $('#content-son-topics').removeAttr('class');
        $("#content-son-topics").addClass("col-md-4");
        $("#content-parent-topics").addClass("col-sm-pull-4");
        $("#content-son-topics").addClass("col-sm-push-8");
        $('#content-son-topics').show();
        $(".btn-show-topics").hide();
        $('#content-son-topics').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $("#content-parent-topics").removeClass("col-md-11");
            $("#content-parent-topics").addClass("col-md-8");
                  
        });
    });
}
$(".btn-hide").click(function () {
    enterCur("bounceOutRight");
});
$(".btn-show").click(function () {
    enterCur2("bounceInRight");
});

$(".btn-hide-topics").click(function () {
    enterCurTopics("bounceOutRight");
});
$(".btn-show-topics").click(function () {
    enterCur2Topics("bounceInRight");
});