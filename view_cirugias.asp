﻿<!--#include file="data/con_ma.inc" -->
<!--#include file="data/con_ma3.inc" -->
<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

SQL = "select source FROM tbl_videos WHERE id_video = " & Request("idv") & " and hash = '" & Request("hv") & "'"
Cursor.Open SQL, StrConn, 1, 3

idu= cint(Request("idu"))
idv = cint(Request("idv"))
idc = cint(Request("idc"))
idm = cint(Request("idm"))

%>
<!DOCTYPE html>
<html>
<head>
    <!-- For IE10 -->
    <meta http-equiv="X-UA-Compatible" content="requiresActiveX=true" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="title" content="22_CD" />
    <meta name="description" content="Online video presentation created with KnowledgeVision. Find out more at KnowledgeVision.com." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>22_CD</title>

    <style type="text/css">
        /* http://meyerweb.com/eric/tools/css/reset/ */
        /* v1.0 | 20080212 */

        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, font, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: baseline;
            /*background: transparent;*/
        }

        body {
            line-height: 1;
        }

        /* remember to define focus styles! */
        :focus {
            outline: 0;
        }

        .center {
            text-align: center;
        }

        body {
        }

        /* HTML5 Player styles */
        html, body, div {
            width: 100%;
            height: 100%;
            min-height: 100%;
        }

        * {
            line-height: 0;
        }

        .KnowledgeVisionEmbeddedContent {
            margin: 0 auto;
        }

        #contenido-modal-teaser {
            margin-top: 0px;
        }

        #compartir {
            z-index: 228;
            position: absolute;
            background-color: black;
            /*opacity: 0.5;*/
            color: aqua;
        }

        .align-center {
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
            border: 1px dashed deeppink;
        }

        .center-contenido {
            position: absolute;
            top: 45%;
        }

        .text-blanco {
            color: #fff;
        }
    </style>

    <script src="js/jquery-1.12.4.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js"></script>

    <link href="assets/css/icons.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
</head>

<body>

    <div id="compartir" class="row hidden hidden-xs" style="margin: 0px !important; padding: 0px !important;">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 center-block">
            <a class="btn btn-lg center-contenido text-blanco btn-like center-block" href="javascript:void(0);">
                <i class="fa fa-thumbs-o-up fa-2x pull-left"></i>Me gusta</a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 center-block">
            <a id="btnCompartir" class="btn btn-lg center-contenido text-blanco center-block" href="javascript:void(0);">
                <i class="fa fa-share-square-o fa-2x pull-left" data-toggle="modal" data-target="#modalCompartir"></i>Compartir</a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 center-block">
            <a id="btnRepetir" class="btn btn-lg center-contenido text-blanco center-block" href="javascript:void(0);">
                <i class="fa fa-repeat fa-2x pull-left"></i>Repetir</a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
    </div>

    <div id="contenido-modal-teaser" class="center">
        <%=Cursor("source") %>

        <script language="JavaScript" type="text/javascript">
            var myFunction = function (eventObject) {
                // console.log(eventObject);
                //TrackClose();
            };
            window.knowledgevisionLoader.addPlayerTrackListener("PlayerLoad", myFunction);
            window.knowledgevisionLoader.addPlayerTrackListener("PlaybackComplete", finVideo);

            function TrackClose() {
                $.ajax({
                    url: "jq/jq_savetrack.asp",
                    type: "POST",
                    data: { "idv": <%=idv%>, "idc": <%=idc%>, "idm": <%=idm%>, "idu": <%=idu%> },
                    async: false,
                    success: function (cont) {
                        console.log("Save success");
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                })
            }

            function finVideo() {
                document.getElementById("compartir").className = document.getElementById("compartir").className.replace(new RegExp('(?:^|\\s)' + 'hidden' + '(?:\\s|$)'), ' ');
                document.getElementById("compartir").className = document.getElementById("compartir").className.replace(new RegExp('(?:^|\\s)' + 'hidden' + '(?:\\s|$)'), ' ');
                TrackClose();
                //document.getElementById("compartir").className += " zoomIn";
                //testAnim("pulse");
            }
        </script>
        <script>            
            function testAnim(x) {
                //e.preventDefault();
                $('#compartir').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $(this).removeClass();
                });
            };
        </script>
        <script>
            $(function() {
                $('#btnCompartir').on('click', function(e) {
                    $(window.parent.document).find('#modalCompartir').modal({
                        appendTo: $(window.parent.document).find('body'),
                        overlayCss: { backgroundColor: "#333" }, // Optional overlay style
                        overlayClose: true,
                    });
                    // Set overlay's width
                    $(window.parent.document).find('#simplemodal-overlay').css('width', '100%');
                });
            })
        </script>
        <script src="js/likes.js"></script>
        <script>
            $(document.body).on('click', '.btn-like', function (e) {
                likeVideo(<%=Session("id_usuario") %>, <%=Request("idv") %>);
                actualizaLikesParent(<%=Request("idv") %>);
            });
        </script>
        <script>
            $(document.body).on('click', '.btn-dislike', function (e) {
                dislikeVideo(<%=Session("id_usuario") %>, <%=Request("idv") %>);
                actualizaLikesParent(<%=Request("idv") %>);
            });

            $(document.body).on('click', '#btnRepetir', function (e) {
                window.knowledgevisionLoader.addPlayerTrackListener("PlaybackStarted", playVid);
                $('#compartir').addClass("hidden");
            });

            function playVid() {
            };
        </script>
    </div>
</body>
</html>
