﻿<!--#include file="data/con_ma2.inc" -->
<%

Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

Usuario = Session("id_usuario")

'SQL = "SELECT     dbo.tbl_usuarios.*, dbo.cat_estados.estado, dbo.cat_paises.pais, dbo.cat_especialidades.especialidad, dbo.cat_zonas.zona, dbo.cat_zonas.cp AS codpos " 
'    SQL = SQL + "FROM         dbo.cat_especialidades RIGHT OUTER JOIN " 
'SQL = SQL + "dbo.tbl_usuarios ON dbo.cat_especialidades.id_especialidad = dbo.tbl_usuarios.id_especialidad LEFT OUTER JOIN " 
'SQL = SQL + "dbo.cat_paises ON dbo.tbl_usuarios.id_pais = dbo.cat_paises.id_pais LEFT OUTER JOIN " 
'SQL = SQL + "dbo.cat_zonas INNER JOIN " 
'SQL = SQL + "dbo.cat_estados ON dbo.cat_zonas.id_estado = dbo.cat_estados.id_estado ON dbo.tbl_usuarios.id_zona = dbo.cat_zonas.id_zona AND " 
'SQL = SQL + "dbo.tbl_usuarios.id_estado = dbo.cat_estados.id_estado AND dbo.tbl_usuarios.id_usuario = " & Session("id_usuario")

SQL= "select a.*,b.estado ,c.pais,d.especialidad,'' as zona,'' as codpos  from tbl_usuarios a,cat_estados b,cat_paises c,cat_especialidades d  where a.id_estado = b.id_estado  and a.id_pais = c.id_pais and a.id_especialidad = d.id_especialidad   and a.id_usuario = " & Usuario & ""
Cursor.Open SQL, StrConn, 1, 3

%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />
    <link href="css/mi-cuenta.css" rel="stylesheet" />
    <!-- form Uploads -->
    <link href="assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
    <style>
        .Tache {
            opacity: 10 !important;
        }

        .strong-look {
            font-weight: bold !important;
            color: #3266b1;
        }

        .cintillo {
            background-color: rgba(255,255,255, 0.3) !important;
        }

        hr {
            border-top: 2px solid #3266b1;
        }
    </style>

</head>

<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <form data-toggle="validator" role="form" id="registro" method="post" action="jq/jq_updateInfo.asp">

                        <div class="row">
                            <div class="col-md-4">
                                <%if Session("urlimagen") = "" then
                    if Session("cargo") = "Bienvenida " then %>
                                <img src="assets/images/users/Avatarmujer.jpg" class="imgCircle center-block img-responsive" style="width: 200px; height: 200px;" alt="<%=Session("TheName")%>">
                                <%else %>
                                <img src="assets/images/users/AvatarHombre.jpg" alt="<%=Session("TheName")%>" class="imgCircle center-block img-responsive" style="width: 200px; height: 200px;">
                                <%  end if
                else %>
                                <img src="/uploadfolder/<%=Session("urlimagen") %>" alt="<%=Session("TheName")%>" class="imgCircle center-block img-responsive" style="width: 200px; height: 200px;">
                                <%end if %>
                                <div class="row">
                                    <div class="btn-group center-block" style="margin-top: 50px;">
                                        <a data-toggle="modal" href="#FotoPerfil">
                                            <button class="btn dropdown-toggle btn-primary center-block">
                                                <i class="fa fa-cog"></i>&nbsp;Cambiar Foto
                                    <span class="caret"></span>
                                            </button>
                                        </a>
                                    </div>
                                    <!-- <%=SQL %>-->
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">
                                            *Nombre                                                                          
                                            <br />
                                            <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b></label>
                                        <input type="text" class="form-control form-input" id="inputName" name="inputName" value="<%=Cursor("nombre") %>" placeholder="Nombre" data-parsley-error-message="Complete your name. Thanks." required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="inputApaterno" class="control-label">
                                            *Apellido Paterno
                                                <br />
                                            <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b>
                                        </label>
                                        <input type="text" class="form-control form-input" id="inputApaterno" name="inputApaterno" value="<%=Cursor("apaterno") %>" placeholder="Apellido Paterno" data-parsley-error-message="Complete your first last name. Thanks." required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="inputAmaterno" class="control-label">
                                            *Apellido Materno
                                                <br />
                                            <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b>
                                        </label>
                                        <input type="text" class="form-control form-input" id="inputAmaterno" name="inputAmaterno" value="<%=Cursor("amaterno") %>" placeholder="Apellido Materno" data-parsley-error-message="Complete your second last name. Thanks." required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row cintillo">
                            <div class="col-md-12 ">
                                <h4 class="strong-look"><i class="fa fa-key"></i><span>&nbsp; DATOS PARA SU CUENTA</span></h4>
                                <hr />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail" class="control-label">*Correo electrónico</label>
                                    <input type="email" class="form-control" id="inputEmail" name="inputEmail" value="<%=Cursor("email") %>" placeholder="Email" data-parsley-error-message="Complete su e-mail. Gracias." data-parsley-type="email" required disabled>
                                </div>

                            </div>
                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                            <label for="inputPassword" class="control-label">*Contraseña</label>
                                    <input type="password" data-minlength="6" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password" data-parsley-min="6" data-parsley-error-message="Password. Thanks." required>
                                            <div class="help-block">Mínimo 6 caracteres</div>
                                </div>
                            </div>-->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label">*Contraseña</label>
                                    <input type="password" data-minlength="6" class="form-control" id="inputPassword" name="inputPassword" placeholder="Contraseña" data-parsley-min="6" data-parsley-error-message="Escriba su contraseña. Gracias." required>
                                    <div class="help-block">Mínimo 6 caracteres</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail" class="control-label">*Confirme su correo electrónico</label>
                                    <input type="email" class="form-control" id="inputEmailConfirm" name="inputEmailConfirm" value="<%=Cursor("email") %>" data-match="#inputEmail" data-parsley-error-message="Confirme su e-mail. Gracias." data-parsley-type="email" placeholder="Confirme su e-mail" required disabled>
                                </div>
                            </div>

                            <!--  <div class="col-md-6">
                                <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Confirm password</label>
                                    <input type="password" class="form-control" id="inputPasswordConfirm" name="inputPasswordConfirm" data-match="#inputPassword" data-parsley-error-message="Confirm password. Thanks. " data-parsley-min="6" placeholder="Confirm password" required>
                       
                                </div>
                            </div>
-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail" class="control-label">*Confirme su contraseña</label>
                                    <input type="password" class="form-control" id="inputPasswordConfirm" name="inputPasswordConfirm" data-match="#inputPassword" data-parsley-error-message="Confirme su contraseña. Gracias." data-parsley-min="6" placeholder="Confirmar" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row cintillo">
                            <div class="col-md-12">
                                <h4 class="strong-look"><i class="fa fa-university"></i><span>&nbsp; INFORMACIÓN ACADÉMICA Y LABORAL</span></h4>
                                <hr />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputSpeciality">*Especialidad</label>
                                    <select class="form-control form-control-sm" id="inputSpeciality" name="inputSpeciality" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione  su especialidad. Gracias. " required>
                                        <option value="<%=Cursor("id_especialidad") %>" selected><%=Cursor("especialidad") %></option>
                                        <%
                                                    set CursorEspecialidades = createobject("ADODB.Recordset")
                                                    CursorEspecialidades.CursorType =1 
                                                    CursorEspecialidades.LockType = 3

                                                    'SQLEspecialidades = "select * from cat_especialidades where activo = 'SI' order by id_especialidad  "
                                                     SQLEspecialidades = "select * from cat_especialidades where activo = 'SI' and  id_especialidad <> " & Cursor("id_especialidad")
                                                    CursorEspecialidades.Open SQLEspecialidades, StrConn, 1, 3

                                                    Do While not CursorEspecialidades.EOF 
                                        %>
                                        <option value="<%=CursorEspecialidades("id_especialidad") %>"><%=CursorEspecialidades("especialidad") %></option>
                                        <%
                                                    CursorEspecialidades.MoveNext
                                                    Loop
                                                  '  CursorEspecialidades.Close
                                                    CursorEspecialidades.MoveFirst    
                                        %>
                                        <option value="0">Otra</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputProfessionalID" class="control-label">*Cédula profesional</label>
                                    <input type="text" class="form-control" style="height: 40px;" id="inputProfessionalID" value="<%=Cursor("cedula") %>" name="inputProfessionalID" placeholder="Cédula profesional" data-parsley-error-message="Seleccione su especialidad. Gracias. " required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputSpeciality">*Tipo de práctica</label>
                                    <select class="form-control form-control-sm" id="inputPractice" name="inputPractice" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su tipo de práctica. Gracias." required>
                                        <% if Cursor("prac_priv") = "SI" and Cursor("prac_pub") = "SI" then%>
                                        <option value="Ambas" selected>Ambas</option>
                                        <option value="Pública">Publica</option>
                                        <option value="Privada">Privada</option>
                                        <% elseif Cursor("prac_priv") = "SI" then %>
                                        <option value="Privada" selected>Privada</option>
                                        <option value="Pública">Publica</option>
                                        <option value="Ambas">Ambas</option>
                                        <% else %>
                                        <option value="Pública" selected>Publica</option>
                                        <option value="Privada">Privada</option>
                                        <option value="Ambas">Ambas</option>
                                        <% end if %>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputProfessionalID" class="control-label">*Institución donde labora</label>
                                    <input type="text" class="form-control" style="height: 40px;" id="inputInstitution" value="<%=Cursor("institucion_labora") %>" name="inputInstitution" placeholder="Institución donde labaora" data-parsley-error-message="Complete su institución laboral. Gracias." required />
                                </div>
                            </div>
                        </div>
                        <div class="row cintillo">
                            <div class="col-md-12">
                                <h4 class="strong-look"><i class="fa fa-envelope"></i><span>&nbsp;DATOS DE CONTACTO</span></h4>
                                <hr />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputStreet" class="control-label">Calle</label>
                                <input type="text" class="form-control" id="inputStreet" value="<%=Cursor("calle") %>" name="inputStreet" placeholder="Calle" />
                            </div>
                            <div class="col-md-4">
                                <label for="inputNumber" class="control-label">Numero</label>
                                <input type="text" class="form-control" id="inputNumber" value="<%=Cursor("numero") %>" name="inputNumber" placeholder="Numero" />
                            </div>
                            <div class="col-md-4">
                                <label for="inputStreet2" class="control-label">Colonia</label>
                                <input type="text" class="form-control" id="inputStreet2" value="<%=Cursor("colonia") %>" name="inputStreet2" placeholder="Colonia" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <%
                                            set CursorPaises = createobject("ADODB.Recordset")
                                            CursorPaises.CursorType =1 
                                            CursorPaises.LockType = 3

                                            SQLPaises = "select * from cat_paises where activo = 'si'"
                                            CursorPaises.Open SQLPaises, StrConn, 1, 3

                                %>
                                <label for="inputCountry" class="control-label">País</label>
                                <select name="inputcountry" id="inputcountry" name="inputcountry" class="form-control form-control-sm" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su País. Gracias. " required>
                                    <option value="<%=Cursor("id_pais") %>" selected><%=Cursor("pais") %></option>
                                    <%
                                            Do While not CursorPaises.EOF 
                                    %>
                                    <option value="<%=CursorPaises("id_pais") %>"><%=CursorPaises("pais") %></option>
                                    <%
                                            CursorPaises.MoveNext
                                            Loop
                                          '  CursorPaises.Close
                                            CursorPaises.MoveFirst    
                                    %>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="inputState" class="control-label">*Estado</label>
                                <select class="form-control form-control-sm" id="inputState" name="inputState" style="font-size: 12px; height: 40px;" data-parsley-error-message="Select su estado. Gracias. " required>
                                    <option value="<%=Cursor("id_estado") %>" selected><%=Cursor("estado") %></option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="inputCity" class="control-label">*Ciudad</label>
                                <input type="text" class="form-control" id="inputCity" value="<%=Cursor("ciudad") %>" name="inputCity" style="font-size: 12px; height: 40px;" placeholder="Ciudad" data-parsley-error-message="Seleccion su ciudad. Thanks. ">
                            </div>
                            <div class="col-md-3">
                                <label for="inputCP" class="control-label">*Código Postal</label>
                                <select class="form-control form-control-sm" id="inputCP" name="inputCP" style="font-size: 12px; height: 40px;" data-parsley-error-message="Escriba su codigo postal. Gracias. ">
                                    <option value="<%=Cursor("id_zona") %>" selected><%=Cursor("cp") %></option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                                <div class="col-md-12">
                                    <label for="inputLadaTel" class="control-label">Teléfono</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="<%=Cursor("lada") %>" id="inputLadaTel" name="inputLadaTel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Lada">
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" value="<%=Cursor("telefono") %>" id="inputTelefono" name="inputTelefono" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Teléfono">
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                                <div class="col-md-12">
                                    <label for="inputLadaTel" class="control-label">Celular</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="<%=Cursor("lada_cel") %>" id="inputLadaCel" name="inputLadaCel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Lada">
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" value="<%=Cursor("celular") %>" id="inputCelular" name="inputCelular" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Celular">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-success" style="margin-top: 30px;">Actualizar Información</button>
                                <!--<button class="btn btn-info btn-lg custom-btn" id="submit" ><span class="glyphicon glyphicon-save"></span>Guardar</button>-->
                            </div>
                        </div>
                    </form>

                    <!--#include file="footer.asp" -->
                </div>
            </div>
        </div>
    </div>

    <div id="FotoPerfil" class="modal" tabindex="999999" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 40px;">
        <div class="modal-dialog">
            <div class="modal-content back-modal-pop" style="padding: 0px; border-color: none;">
                <!--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Foto de Perfil</h4>
                </div>-->
                <div class="panel-heading">
                    <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                        ×</button>
                    <h3 class="panel-title">Foto de Perfil</h3>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form name="file_upload" method="post" enctype="multipart/form-data" action="photoprofile/sendfiles.asp">
                            <%
hu = Session("hu")
gu = Session("idu")
                            %>
                            <input type="hidden" name="hu" value="<%=hu%>">
                            <input type="hidden" name="gu" value="<%=gu%>">
                            <input type="file" class="dropify" data-height="300" name="Image 1" id="Image 1" />
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info waves-effect waves-light">Actualizar</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="assets/plugins/fileuploads/js/dropify.min.js"></script>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!--Validator-->
    <script src="js/validator.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>

    <!-- file uploads js -->
    <script src="assets/plugins/fileuploads/js/dropify.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <!--    <script src="js/MetroJs.js" type="text/javascript"></script>-->
    <script src="js/loader.js" type="text/javascript"></script>

    <script>
    $('input[id=base-input]').change(function() {
        $('#fake-input').val($(this).val().replace("C:\\fakepath\\", ""));
    });


    $('input[id=main-input]').change(function() {
        console.log($(this).val());
        var mainValue = $(this).val();
        if(mainValue == ""){
            document.getElementById("fake-btn").innerHTML = "Choose File";
        }else{
            document.getElementById("fake-btn").innerHTML = mainValue.replace("C:\\fakepath\\", "");
        }
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.imgCircle')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

//    var checkme = document.getElementById('checker');
    var userImage = document.getElementById('image-input');
    var userName = document.getElementById('name');
    var userPhone = document.getElementById('phone');
    var userEmail = document.getElementById('email');
    var userPlace = document.getElementById('place');
    var UserSend = document.getElementById('submit');
    var editPic = document.getElementById('PicUpload');
//    checkme.onchange = function() {
//        UserSend.disabled = !this.checked;
//        userImage.disabled = !this.checked;
//        editPic.style.display = this.checked ? 'block' : 'none';
//    };
    </script>



    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Clic para buscar y adjuntar su archivo o Arrastrar y soltar un archivo',
                'replace': 'Clic para buscar y adjuntar su archivo o Arrastrar y soltar un archivo',
                'remove': 'Quitar',
                'error': 'Ooops, Algo salio mal.'
            },
            error: {
                'fileSize': 'El archivo es demasiado grande (1M max).'
            }
        });
    </script>
    <script src="js/logout.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //$(".live-tile,.flip-list").liveTile();
            //$(".appbar").applicationBar();

            function ActualizaCP() {
                $.ajax({
                    url: "jq/jq_obtener_codigos.asp",
                    type: "POST",
                    data: "estado=" + $("#inputState").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputCP").html(opciones);
                        $('#inputCP').prop("disabled", false);
                    }
                })
                $('#inputCP').prop("disabled", false);
            }

            function ActualizaEstados() {
                $.ajax({
                    url: "jq/jq_obtener_estados.asp",
                    type: "POST",
                    data: "inputcountry=" + $("#inputcountry").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputState").html(opciones);
                        if ($('#inputState option:selected').val() == 1) {
                            $('#inputState').prop("disabled", true);
                            $('#inputCity').prop("disabled", true);
                            $('#inputCP').prop("disabled", true);

                            $("#inputCP").html("<option value='N/A' selected>Doesn´t apply</option>");
                            $("#inputCity").val("Doesn´t apply");

                        } else {
                            $('#inputState').prop("disabled", false);
                            $('#inputCity').prop("disabled", false);
                            $("#inputCity").val("");
                        }
                    }
                })
            }

            function LimpiaCampos() {
                $('#inputState').removeClass("parsley-error");
                $('#inputCity').removeClass("parsley-error");
                $('#inputCP').removeClass("parsley-error");
            }

            $('#inputcountry').change(function () {
                LimpiaCampos();
                ActualizaEstados();
            });

            $('#inputState').change(function () {
                ActualizaCP();
            });
        });




    </script>


</body>
</html>
