<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--Modal de Contacto -->
<form data-toggle="validator" role="form" id="Contact" method="post" action="jq/jq_enviar_correo.asp">
    <div id="full-width-modal-Contact" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0 ">
                <div class="panel panel-color panel-primary back-modal-pop">
                    <div class="panel-heading modal-heading">
                        <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                            &times;</button>
                        <h3 class="panel-title">&nbsp;</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="jumbotron vertical-center" style="background-color: transparent;">
                                    <div class="container">
                                        <img src="assets/images/logos/1.png" class="img-responsive" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre Completo" required oninvalid="setCustomValidity('Debes de completar este campo')" oninput="setCustomValidity('')">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="txtMail" name="txtMail" placeholder="E-mail" required oninvalid="setCustomValidity('Debes de completar este campo')" oninput="setCustomValidity('')">
                                </div>
                                <div class="form-group no-margin">
                                    <textarea class="form-control autogrow" id="txtComentario" name="txtComentario" placeholder="Comentarios" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" required oninvalid="setCustomValidity('Debes de completar este campo')" oninput="setCustomValidity('')"></textarea>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <!--  <button type="submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" data-dismiss="modal" aria-hidden="true"><i class="fa fa-paper-plane m-l-5"></i>&nbsp;Enviar</button>-->
                                    <button type="submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5"><i class="fa fa-paper-plane m-l-5"></i>&nbsp;Enviar</button>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <button type="button" class="btn btn-danger btn-bordred waves-effect w-md waves-light m-b-5" data-dismiss="modal" aria-hidden="true"><i class="fa fa-ban m-l-5"></i>&nbsp;Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!--Modal deContacto -->

<!--Modal de Soporte -->
<form data-toggle="validator" role="form" id="soporte" method="post" action="jq/jq_enviar_correo_soporte.asp">
    <div id="full-width-modal-soporte" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0 ">
                <div class="panel panel-color panel-primary back-modal-pop">
                    <div class="panel-heading modal-heading">
                        <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                            &times;</button>
                        <h3 class="panel-title">&nbsp;</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="jumbotron vertical-center" style="background-color: transparent;">
                                    <div class="container">
                                        <img src="assets/images/logos/1.png" class="img-responsive" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="txtNombreDudaTecnica" name="txtNombreDudaTecnica" placeholder="Nombre Completo" required oninvalid="setCustomValidity('Debes de completar este campo')" oninput="setCustomValidity('')">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="txtEmailDudaTecnica" name="txtEmailDudaTecnica" placeholder="E-mail" required oninvalid="setCustomValidity('Debes de completar este campo')" oninput="setCustomValidity('')">
                                </div>
                                <div class="form-group no-margin">
                                    <textarea class="form-control autogrow" id="txtDudaTecnica" name="txtDudaTecnica" placeholder="Estamos muy contentos de ayudarle. Tenga en cuenta que las horas de servicio por este medio son: de lunes a viernes, de 9:00 am a 6:00 pm, hora de la Ciudad de M&eacute;xico. Su solicitud ser&aacute; contestada, a m&aacute;s tardar, 24 horas despu&eacute;s de enviar su pregunta. Si su solicitud es enviada en s&aacute;bado, domingo o d&iacute;a festivo, se contestar&aacute; el siguiente d&iacute;a h&aacute;bil" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 250px;" required oninvalid="setCustomValidity('Debes de completar este campo')" oninput="setCustomValidity('')"></textarea>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <button type="submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5"><i class="fa fa-paper-plane m-l-5"></i>&nbsp;Enviar</button>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <button type="button" class="btn btn-danger btn-bordred waves-effect w-md waves-light m-b-5" data-dismiss="modal" aria-hidden="true"><i class="fa fa-ban m-l-5"></i>&nbsp;Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal de Soporte -->
</form>
<!--Modal de Soporte -->

<!--Modal Avisos De Privacidad-->
<div id="full-width-modal-aviso-privacidad" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content p-0 b-0 ">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true"
                        style="color: white;">
                        ×</button>
                    <h3 class="panel-title">
                        &nbsp;</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 style="color: #3266b1;">
                                AVISO DE PRIVACIDAD</h4>
                            <div id="wordTerminos">
                                <ul>
                                    <li>
                                        <p align="JUSTIFY" style="margin-bottom: 0in">
                                            <font face=""><font size="2" style="">Merck, S.A. de C.V.
                                                (en lo sucesivo "MERCK") </font></font><font face=""><font size="2"
                                                    style="">una sociedad con domicilio en la </font></font><font face="">
                                                        <font size="2" style=""><span lang="es-ES">Calle 5, Número 7, Colonia
                                                            Industrial Alce Blanco, Naucalpan de Juárez, Estado de México, C.P. 53370 </span>
                                                        </font></font><font face=""><font size="2" style="">, </font>
                                                        </font><font face=""><font size="2" style="">compañía subsidiaria
                                                            y que forma parte de divisiones, compañías subsidiarias y/o filiales o grupos a
                                                            nivel global de Merck KGaA, con sede en Darmstadt, Alemania </font>
                                            </font><font face=""><font size="2" style="">, (en adelante
                                                " </font></font><font face=""><font size="2" style=""><b>
                                                    MERCK GROUP </b></font></font><font face=""><font size="2" style="">
                                                        "), con los fines previstos en la legislación aplicable sobre protección de datos,
                                                        es la compañía que controla el procesamiento de sus datos personales. </font>
                                            </font>
                                        </p>
                                </ul>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style=""><b>Merck, </b></font>
                                    </font><font face=""><font size="2" style="">es una empresa
                                        consciente de la responsabilidad y confianza que usted deposita en ella, es por
                                        esto que Merck, se encuentra comprometida con el buen tratamiento que dará a sus
                                        Datos Personales, por lo que en todo momento guardará la confidencialidad de la
                                        información que se le proporciona y actuará conforme a la regulación nacional e
                                        internacional aplicable en materia de Datos Personales. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Asimismo, MERCK agradece
                                        su interés en la compañía, razón por la que se pone a sus órdenes para cualquier
                                        duda o comentario que tenga sobre el mismo, en el contacto que se identifica al
                                        final de este documento. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-right: 0.08in; margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Para efectos de la
                                        Política de Privacidad de este Web Site </font></font><font face=""><font
                                            size="2" style=""><span lang="es-ES">será considerado Usuario-Titular
                                                como </span></font></font><font face=""><font size="2" style="">
                                                    aquella persona que utiliza un dispositivo o un ordenador y realiza múltiples operaciones
                                                    con distintos propósitos en la que deposita Datos Personales propios y/o de sus
                                                    pacientes. </font></font><font face=""><font size="2" style="">
                                                        <span lang="es-ES">Si el USUARIO-TITULAR no es mayor de edad o no cuenta con las autorizaciones
                                                            legales y capacidad requerida para acceder y usar los </span></font>
                                    </font><font face=""><font size="2" style=""><span lang="es-ES">
                                        datos, información </span></font></font><font face=""><font size="2"
                                            style=""><span lang="es-ES">y servicios, debe abstenerse de usarlo, abandonarlo,
                                                y no acceder a sus funcionalidades. El acceso al Web Site por parte de personas
                                                menores de edad, es de exclusiva responsabilidad de sus padres, representantes legales
                                                o personas mayores de edad que tengan a menores de edad bajo su cuidado. </span>
                                        </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-right: 0.08in; margin-bottom: 0in">
                                    <font face=""><font size="2" style=""><span lang="es-ES">
                                        El Web Site y MERCK se exime de toda responsabilidad de verificar o validar esta
                                        situación. </span></font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Por favor, tenga en
                                        cuenta la presente Política de Privacidad es exclusiva y se encuentra dirigida al
                                        USUARIO-TITULAR del Web Site que deposite Datos Personales propios, por lo que otras
                                        aplicaciones y sitios Web proporcionados por Merck, y otras entidades dentro Merck
                                        Group, no se rigen por el mismo. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">En cumplimiento a
                                        la Ley Federal de Protección de Datos Personales en Posesión de los particulares,
                                        se hace de su conocimiento la presente Política de Privacidad. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style=""><span lang="es-ES">
                                        Al utilizar el Web Site, usted consiente su sujeción a la Política de Privacidad
                                        y Cookies acuerda que utilizará el Web Site de conformidad con todas las leyes y
                                        normas aplicables. </span></font></font>
                                </p>
                                <p lang="es-MX" align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>RESPONSABLE. </b></font></font></font>
                                            </font>
                                        </p>
                                </ol>
                                <ul>
                                    <li>
                                        <p style="margin-bottom: 0in">
                                            <font face=""><font size="2" style="">El Responsable del
                                                Tratamiento de Datos Personales es Merck, S.A. de C.V. con domicilio en </font>
                                            </font><font face=""><font size="2" style=""><span lang="es-ES">
                                                Calle 5, Número 7, Colonia Industrial Alce Blanco, Naucalpan de Juárez, Estado de
                                                México, C.P. 53370 </span></font></font>
                                        </p>
                                </ul>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="2">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>FINALIDADES. </b></font></font></font>
                                            </font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Las finalidades por
                                        las que Merck, puede solicitar, obtener, almacenar y/o utilizar o transferir los
                                        Datos Personales son las siguientes: </font></font>
                                </p>
                                <ul>
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style="">Mantener una relación con usuarios, clientes,
                                                    pacientes, profesionales de la salud o cualquier interesado en los productos, servicios
                                                    e investigaciones que Merck, realiza. </font></font></font></font>
                                        </p>
                                        <li>
                                            <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                                <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                    <font size="2" style="">Para control estadístico y mercadológico del uso
                                                        de productos de Merck Group. </font></font></font></font>
                                            </p>
                                            <li>
                                                <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                                    <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                        <font size="2" style="">Atender a la solicitud de información, productos,
                                                            servicios, quejas, preguntas o comentarios. </font></font></font></font>
                                                </p>
                                                <li>
                                                    <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                                        <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                            <font size="2" style="">Creación de bases de datos para fines de investigación
                                                                y desarrollo de productos y/o servicios. </font></font></font></font>
                                                    </p>
                                                    <li>
                                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                                <font size="2" style="">Programar visitas de seguimiento de uso de productos
                                                                    y/o para la promoción de los productos. </font></font></font></font>
                                                        </p>
                                                        <li>
                                                            <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                                                <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                                    <font size="2" style="">Envío o recepción de información, revistas, noticias
                                                                        de la industria, comunicaciones y publicidad de sus productos. </font></font>
                                                                </font></font>
                                                            </p>
                                </ul>
                                <p lang="fr-CH" align="JUSTIFY" style="margin-left: 0.5in; margin-bottom: 0in; line-height: 100%">
                                </p>
                                <ol type="I" start="3">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>FORMA DE OBTENER LOS DATOS PERSONALES. </b>
                                                </font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Directamente del Web
                                        Site mediante el formulario electrónico contenido en la misma. El USUARIO-TITULAR
                                        deberá adicionalmente cumplir las obligaciones referentes al tratamiento de datos
                                        personales de sus pacientes. </font></font><font face=""><font size="2"
                                            style=""><span lang="es-ES">El USUARIO-TITULAR en caso de ser USUARIO
                                                MÉDICO deberá garantizar que cumple con las formalidades y requisitos conforme la
                                                Legislación en materia vigente aplicable para poder transmitir y depositar los Datos
                                                Personales de sus pacientes. </span></font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style=""><span lang="es-ES">
                                        Sin embargo, si el USUARIO MÉDICO comparte contenido de otras personas deberá garantizar
                                        que cuenta con el consentimiento formal y expreso de acuerdo a la Legislación aplicable
                                        en el país, de forma tal que de violar los derechos de otras personas, incluidos
                                        los derechos de privacidad, el USUARIO MÉDICO acepta que estará infringiendo estos
                                        términos e </span></font></font><font face=""><font size="2" style="">
                                            indemnizará, defenderá y mantendrá a MERCK, a sus respectivos funcionarios, empleados
                                            y proveedores a salvo contra cualquier reclamación o demanda, incluyendo honorarios
                                            razonables de abogados, hechos por algún tercero debido a o que supuestamente surjan
                                            del uso que el USUARIO MÉDICO haga del Web Site o por alguna contravención a este
                                            Contrato o violación a algún derecho de otra parte </font></font><font face="">
                                                <font size="2" style=""><span lang="es-ES">. </span></font>
                                    </font><font face=""><font size="2" style="">MERCK podrá
                                    </font></font><font face=""><font size="2" style=""><span
                                        lang="es-ES">eliminar su contenido del servicio en cualquier momento, si el USUARIO
                                        MÉDICO incumple estos términos </span></font></font><font face=""><font
                                            size="2" style=""><span lang="es-ES">y/o de otras personas, incluidos
                                                los derechos de privacidad, el USUARIO MÉDICO está infringiendo esta Política de
                                                Privacidad y deberá indemnizar completamente a Merck y a nuestras afiliadas con
                                                respecto a cualquier demanda de terceros que surja por su incumplimiento de estos
                                                Términos. </span></font></font><font face=""><font size="2" style="">
                                                    <b></b></font></font><font face=""><font size="2" style="">
                                                        <span lang="es-ES">El contenido continúa siendo de propiedad de sus pacientes y/o suya,
                                                            según corresponda. MERCK tampoco controla, verifica o aprueba el Contenido que se
                                                            le proporciona por medio del uso del Servicio. Si el USUARIO MÉDICO comparte los
                                                            Datos Personales al cual accede por medio del uso del Servicio en una forma que
                                                            viola los derechos de sus pacientes. </span></font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-left: 0.5in; margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="4">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>CONSENTIMIENTO PARA EL USO DE DATOS PERSONALES.
                                                </b></font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">El tratamiento de
                                        los Datos Personales estará siempre sujeto a que el USUARIO-TITULAR proporcione
                                        su consentimiento ya sea de manera expresa, manifestando su voluntad de manera verbal,
                                        por medios electrónicos, ópticos, por cualquier otra tecnología o por signos inequívocos,
                                        o bien de manera tacita, entendiendo por conocimiento tácito cuando habiéndose puesto
                                        a disposición del USUARIO-TITULAR el Aviso de Privacidad correspondiente, éste no
                                        manifieste oposición alguna. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Para ambos consentimientos
                                        el USUARIO-TITULAR de los Datos Personales cuenta con un plazo de 5 días para manifestar
                                        su oposición al tratamiento de dichos Datos Personales, en caso de que no se manifieste
                                        oposición se entenderá que se ha manifestado su consentimiento. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="5">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>DATOS PERSONALES SOLICITADOS. </b></font>
                                            </font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Merck, podrá solicitar
                                        Datos Personales por lo que previo a su recolección se pondrá a su disposición el
                                        documento de acuerdo país correspondiente para el tratamiento de los siguientes
                                        Datos Personales: </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                </p>
                                <ul>
                                    <li>
                                        <p align="JUSTIFY" style="margin-bottom: 0in">
                                            <font face=""><font size="2" style="">Nombre y apellidos.
                                            </font></font>
                                        </p>
                                        <li>
                                            <p align="JUSTIFY" style="margin-bottom: 0in">
                                                <font face=""><font size="2" style="">Teléfono(s) </font>
                                                </font>
                                            </p>
                                            <li>
                                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                                    <font face=""><font size="2" style="">Correo electrónico.
                                                    </font></font>
                                                </p>
                                                <li>
                                                    <p align="JUSTIFY" style="margin-bottom: 0in">
                                                        <font face=""><font size="2" style="">Cedula profesional
                                                            del profesional de la salud. </font></font>
                                                    </p>
                                </ul>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="6">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>ALMACENAMIENTO DE DATOS. </b></font></font>
                                            </font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Merck, almacena los
                                        Datos Personales durante el periodo requerido para realizar el servicio por el cual
                                        fueron recabados los datos y la finalidad que por la que se solicitaron. Asimismo,
                                        con el fin de salvaguardar y garantizar la seguridad de los datos personales recabados,
                                        Merck, podrá celebrar con terceros, contratos para el almacenaje o hosting de dichos
                                        datos. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="7">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>PROTECCION DE DATOS. </b></font></font>
                                            </font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Merck, en cumplimento
                                        a la regulación en materia de Datos Personales en Posesión de los Particulares ha
                                        adoptado las medidas de seguridad de protección de Datos Personales legalmente requeridas;
                                        y exige a los terceros a los que transfiere sus datos para su tratamiento o almacenamiento
                                        adopten las mismas. Asimismo, se les informa a los Titulares de los Datos Personales
                                        que Merck, cuenta con los medios y medidas técnicas adicionales para evitar la pérdida,
                                        mal uso, alteración, acceso no autorizado o robo de los Datos Personales del Usuario-Titular
                                        que le sean facilitados </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">No obstante lo anterior,
                                        y debido a que las medidas de seguridad pueden ser susceptibles de violaciones por
                                        actos fuera del control de Merck; éste, sin responsabilidad, se compromete a informar
                                        al Usuario-Titular en caso de que existan vulneraciones a sus medidas de seguridad,
                                        a fin de que esté pueda tomar las medidas correspondientes para la defensa de sus
                                        derechos. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="8">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>TRANSFERENCIA DE DATOS PERSONALES. </b>
                                                </font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Merck, podrá transferir
                                        los Datos Personales a fin de mantener una relación con el Usuario-Titular sobre
                                        los productos, servicios e investigaciones que realiza la sociedad; para control
                                        estadístico y mercadológico sobre el uso de productos de Merck Group; para poder
                                        desarrollar un adecuado control de calidad y una medición de los niveles de contacto
                                        con clientes y profesionales de la salud; para atender a la solicitud de información,
                                        productos, servicios, quejas, preguntas o comentarios, así como para la creación
                                        de bases de datos para fines de investigación y desarrollo de productos y/o servicios
                                        y/o programar visitas de seguimiento de uso de productos y/o para la promoción de
                                        los productos. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Además de las transferencias
                                        indicadas, Merck, no transferirá sus datos personales a terceros ajenos a Merck,
                                        sin consentimiento previo. Sin embargo, Merck, S.A. de C.V. podrá transferir sus
                                        datos personales cuando dicha transferencia esté prevista en la Ley. </font>
                                    </font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="9">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>DERECHOS </b></font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Los Usuarios-Titulares
                                        de los Datos Personales que se encuentran en posesión de Merck, en cualquier momento,
                                        podrá ejercer los derechos de acceso, cancelación, rectificación y oposición. </font>
                                    </font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Para poder hacer valer
                                        los derechos, es necesario que el Usuario-Titular de los Datos Personales realice
                                        una solicitud con los requisitos que se detallan a continuación: </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">(i) el nombre y domicilio
                                        o correo electrónico del Usuario-Titular para comunicarle la respuesta a su solicitud;
                                        (ii) los documentos que acrediten la identidad del Usuario-Titular, o en su caso,
                                        la representación legal de quien actúe en su nombre; (iii) la descripción clara
                                        y precisa de los Datos Personales respecto de los que el Usuario-Titular busca ejercer
                                        alguno de los derechos antes mencionados; (iv) cualquier otro elemento o documento
                                        que facilite la localización de los Datos Personales del Usuario-Titular; (v) especificar
                                        claramente si la solicitud es de acceso, rectificación, cancelación u oposición;
                                        (vi) el motivo de la solicitud; y (vii) las modificaciones a realizarse en caso
                                        de que la solicitud sea para la rectificación de Datos Personales. </font></font>
                                </p>
                                <p style="margin-bottom: 0in">
                                    <font face=""><font size="2" style=""><span lang="es-ES">
                                        Esta solicitud deberá de hacerla llegar a Merck S.A. de C.V. por cualquiera de los
                                        siguientes medios: </span></font></font>
                                </p>
                                <ul>
                                    <li>
                                        <p style="margin-bottom: 0in">
                                            <font face=""><font size="2" style=""><span lang="es-ES">
                                                Vía correo electrónico a la dirección </span></font></font><a href="mailto:Privacidad-Mexico@merckgroup.com">
                                                    <font color="#3666c8"><font face=""><font size="2" style="">
                                                        <span lang="es-ES">Privacidad-Mexico@merckgroup.com</span></font></font></font></a>
                                        </p>
                                        <li>
                                            <p style="margin-bottom: 0in">
                                                <font face=""><font size="2" style=""><span lang="es-ES">
                                                    Vía correo postal y/o personalmente en las oficinas de Merck S.A de C.V., ubicadas
                                                    en Calle 5, Número 7, Colonia Industrial Alce Blanco, Naucalpan de Juárez, Estado
                                                    de México, C.P. 53370 </span></font></font>
                                            </p>
                                </ul>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">El procedimiento que
                                        Merck adoptará una vez que haya recibido la solicitud consta en lo siguiente: </font>
                                    </font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Hará de su conocimiento
                                        en un plazo máximo de 20 (veinte) días naturales, contados a partir de la fecha
                                        en que haya recibido la solicitud, la determinación adoptada, y en caso de que la
                                        misma resulte procedente, se hará efectiva dentro de los 15 (quince) días naturales
                                        siguientes a la fecha en que se comunique su procedencia. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Se le informa que
                                        los plazos antes referidos, Merck podrá ampliarlos cuando las particularidades del
                                        caso así lo ameriten. Lo anterior se le notificará al USUARIO-TITULAR por el mismo
                                        medio en que fue realizada la solicitud. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Merck, podrá negar
                                        el acceso, rectificación, cancelación u oposición al tratamiento de los mismos,
                                        en los siguientes supuestos: </font></font>
                                </p>
                                <ol>
                                    <li>
                                        <p align="JUSTIFY" style="margin-bottom: 0in">
                                            <font face=""><font size="2" style="">Cuando el solicitante
                                                no sea el USUARIO-TITULAR de los datos personales, o el representante legal no esté
                                                debidamente acreditado para ello; </font></font>
                                        </p>
                                        <li>
                                            <p align="JUSTIFY" style="margin-bottom: 0in">
                                                <font face=""><font size="2" style="">Cuando en su base
                                                    de datos no se encuentren los datos personales del solicitante; </font></font>
                                            </p>
                                            <li>
                                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                                    <font face=""><font size="2" style="">Cuando se lesionen
                                                        los derechos de un tercero; </font></font>
                                                </p>
                                                <li>
                                                    <p align="JUSTIFY" style="margin-bottom: 0in">
                                                        <font face=""><font size="2" style="">Cuando exista un impedimento
                                                            legal, o la resolución de una autoridad competente que restrinja el acceso a los
                                                            datos personales o que no permita la rectificación, cancelación u oposición de los
                                                            mismos; </font></font>
                                                    </p>
                                                    <li>
                                                        <p align="JUSTIFY" style="margin-bottom: 0in">
                                                            <font face=""><font size="2" style="">Cuando la rectificación,
                                                                cancelación u oposición haya sido previamente realizada. </font></font>
                                                        </p>
                                                        <li>
                                                            <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in; line-height: 100%">
                                                                <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                                    <font size="2" style="">En todo caso, usted podrá consultar nuestra Política
                                                                        de Protección de Datos Personales, en nuestra página web: http://www.merck.com.co/country..
                                                                    
                                                                    </font></font></font></font>
                                                            </p>
                                </ol>
                                <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="10">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>RECOPILACION DE INFORMACION POR PARTE DE TERCEROS. </b>
                                                </font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">La Web Site , puede contener enlaces
                                        a otros sitios cuyas prácticas de información pueden ser diferentes al mismo, por
                                        lo que los servicios, contenidos, información y/o administración de dichos sitios
                                        no son responsabilidad de Merck, por lo que Merck, no será responsable de la protección
                                        y Tratamiento de los Datos Personales y Datos Personales Sensibles que el USUARIO-TITULAR
                                        proporcione a terceros con los que Merck, tiene celebrado algún convenio de colaboración
                                        en beneficio de sus Titulares. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Por lo tanto, es responsabilidad
                                        del USUARIO-TITULAR consultar las condiciones de privacidad de los terceros responsables
                                        sobre el Tratamiento que éstos le darán a los Datos Personales. Asimismo, Merck,
                                        en ningún caso y bajo ninguna circunstancia será responsable por el Tratamiento
                                        que otros sitios distintos al presente Web Site, den a los Datos Personales del
                                        USUARIO-TITULAR. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="11">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>RECOPILACIÓN DE DATOS DE PACIENTES </b></font></font>
                                            </font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">El presente Web Site tiene como
                                        finalidad fungir como una herramienta auxiliar para el manejo de pacientes con diagnóstico
                                        de talla baja (se entiende por talla baja: **); para ello es necesario introducir
                                        a la aplicación información relacionada al estado de salud del paciente. Se le informa
                                        que dicha información es almacenada de forma encriptada por el proveedor de servicios
                                        Livemed, S. de R.L. de C.V. con domicilio en Av. Constituyentes 908, Col. Lomas
                                        Altas, Del. Miguel Hidalgo, Ciudad de Mexico, CP. 11950, quien tiene implementados
                                        controles de seguridad hasta el alcance de su responsabilidad, que le permiten proteger
                                        dicha información, contra cualquier daño, pérdida, alteración, uso o acceso no autorizado
                                        de manera interna y con terceros. Merck, no almacena, accede, explota, utiliza o
                                        trata de manera alguna a dicha información. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="12">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>ACTUALIZACION DEL AVISO. </b></font></font></font>
                                            </font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Merck, se reserva el derecho a modificar
                                        o complementar el presente Aviso de Privacidad para adaptarlo a novedades legislativas
                                        o jurisprudenciales, así como a prácticas de la industria, pero siempre en cumplimiento
                                        de la legislación y reglamentación aplicable. En dichos supuestos, Merck, anunciará
                                        en el Portal los cambios introducidos con razonable antelación a su puesta en práctica
                                        y/o hará del conocimiento de los titulares del cambio, de la misma forma en que
                                        se obtuvieron sus datos. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="13">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>CONTACTO. </b></font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">En caso de que tener preguntas o
                                        sugerencias, diríjalas nuestro Oficial de Datos Personales, quienes han sido designados
                                        especialmente por Merck, para atender las solicitudes que se realicen relacionadas
                                        con el tratamiento de Datos Personales. </font></font>
                                </p>
                                <p style="margin-bottom: 0in">
                                    <font face=""><font size="2" style=""><b>Oficial de Datos Personales de
                                        Merck, S.A. de C.V.: </b></font></font><font face=""><font size="2"
                                            style="">
                                            <br>
                                            nombre: Rogelio Luna Campuzano<br>
                                        </font></font><font color="#003300"><font face=""><font size="2" style="">
                                            Correo electrónico: </font></font></font><a href="mailto:Rogelio.a.luna@merckgroup.com">
                                                <font face=""><font size="2" style="">Rogelio.a.luna@merckgroup.com</font></font></a><font
                                                    face=""><font size="2" style=""><br>
                                                    </font></font>
                                    <br>
                                </p>
                                <p lang="es-CL" align="JUSTIFY" style="margin-bottom: 0in; background: #ffffff">
                                    <br>
                                </p>
                                <ol type="I" start="14">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font color="#4bacc6"></font><font face=""><font size="2" style="font-size: 11pt">
                                                <font face=""><font size="2" style=""><b>OBLIGACIONES DEL USUARIO-TITULAR.
                                                </b></font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Por el hecho de registrarse, usar
                                        o navegar por este Web Site, el USUARIO-TITULAR consiente expresamente el tratamiento
                                        de sus datos personales por parte de MERCK. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">El USUARIO-TITULAR se obliga a mantener
                                        indemne a MERCK y a cualquier persona que reciba información y datos personales
                                        de parte de MERCK ante cualquier posible reclamación, multa o sanción que pueda
                                        venir obligada a soportar como consecuencia del incumplimiento por parte del USUARIO-TITULAR
                                        de la presente Política de Privacidad. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">MERCK se reserva el derecho a limitar,
                                        suspender o terminar el acceso al Web Site en caso de el USUARIO-TITULAR incumpla
                                        el contenido de la presente Política de Privacidad, las Condiciones Generales o
                                        de cualesquiera otros términos o condiciones particulares recogidos en el Web Site.
                                        Dicho derecho podrá ser también ejercido en el caso de que MERCK tenga una sospecha
                                        razonable de que el USUARIO-TITULAR está vulnerando cualquiera de los contenidos
                                        mencionados anteriormente. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="15">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>COMUNICACIONES DE DATOS EFECTUADAS POR EL PROPIO USUARIO-TITULAR.
                                                </b></font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">El USUARIO-TITULAR es plenamente
                                        consciente de que al utilizar las distintas secciones que componen el Web Site o
                                        al publicar o autorizar la publicación de cualquier tipo de material e información
                                        (datos, contenidos, mensajes, dibujos, archivos de sonido e imagen, fotografías,
                                        software, etc.) en el Sitio Web, estará permitiendo que terceras personas puedan
                                        acceder a datos personales suyos o incluso de terceros. En este sentido, MERCK no
                                        se responsabiliza de las posibles consecuencias o perjuicios que el USUARIO-TITULAR
                                        o un tercero puedan sufrir con motivo de su libre decisión de compartir sus datos
                                        personales. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Antes de facilitar en el Web Site,
                                        datos personales relativos a terceras personas, el USUARIO-TITULAR deberá obtener
                                        su previo y expreso consentimiento, habiéndoles informado de los términos contenidos
                                        en esta Política de Privacidad. En caso de facilitar datos personales de menores
                                        de edad el USUARIO-TITULAR deberá tener la patria potestad del menor o su tutela
                                        legal, o bien haber obtenido previamente el consentimiento de quien ostente su patria
                                        potestad o tutela. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">El USUARIO-TITULAR se obliga a mantener
                                        indemne a MERCK ante cualquier posible reclamación, multa o sanción que pueda venir
                                        obligada a soportar como consecuencia del incumplimiento por parte del USUARIO-TITULAR
                                        del deber descrito en este párrafo. </font></font>
                                </p>
                                <ol type="I" start="16">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>SEGURIDAD. </b></font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">MERCK mantiene los niveles de seguridad
                                        de protección de datos personales conforme a la normativa actual. </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="17">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><b>NULIDAD E INEFICACIA DE LAS CLÁUSULAS </b></font></font>
                                            </font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <font face=""><font size="2" style="">Si cualquier cláusula incluida en
                                        esta Política de Privacidad fuese declarada total o parcialmente nula o ineficaz,
                                        tal nulidad o ineficacia tan sólo afectará a dicha disposición o a la parte de la
                                        misma que resulte nula o ineficaz, subsistiendo la presente Política de Privacidad
                                        en todo lo demás y considerándose tal disposición total o parcialmente por no incluida.
                                    </font></font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <ol type="I" start="18">
                                    <li>
                                        <p lang="fr-CH" align="JUSTIFY" style="margin-bottom: 0.14in; background: #ffffff;
                                            line-height: 115%">
                                            <font face=""><font size="2" style="font-size: 11pt"><font face="">
                                                <font size="2" style=""><span lang="es-CL"><b>MODIFICACIÓN DE LA POLÍTICA DE PRIVACIDAD.
                                                </b></span></font></font></font></font>
                                        </p>
                                </ol>
                                <p align="JUSTIFY" style="margin-bottom: 0in; background: #ffffff">
                                    <font face=""><font size="2" style=""><span lang="es-CL">Merck puede cambiar
                                        esta Política de Privacidad en cualquier momento mediante la publicación de la Política
                                        de privacidad cambiada o modificada en el Web Site. Su visión continua o el uso
                                        de este Web Site después de la publicación de cualquier cambio a esta Política de
                                        Privacidad significarán su aceptación de tales cambios o modificaciones. </span>
                                    </font></font>
                                </p>
                                <p lang="es-CL" align="JUSTIFY" style="margin-bottom: 0in; background: #ffffff">
                                    <br>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <a name="_GoBack"></a><font color="#4bacc6"><font face=""><font size="2"
                                        style=""><b>Fecha de última actualización: 16-noviembre-2017 </b></font></font>
                                    </font>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                                <p align="JUSTIFY" style="margin-bottom: 0in">
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal Términos y Condiciones-->
<div id="full-width-modal-terminos-condiciones" class="modal fade" tabindex="-1"
    role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content p-0 b-0 ">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true"
                        style="color: white;">
                        ×</button>
                    <h3 class="panel-title">
                        &nbsp;</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 style="color: #3266b1;">
                                TÉRMINOS Y CONDICIONES</h4>
                           
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            El presente Web Site ("Web
            Site") es provisto por Merck
        </FONT><FONT SIZE=2 STYLE="">
            <U>
                S.A.
                DE C.V.
            </U>
        </FONT><FONT SIZE=2 STYLE="">
            (en lo
            sucesivo "MERCK"), compañía subsidiaria y que forma parte de
            divisiones, compañías subsidiarias y/o filiales o grupos a nivel
            global de Merck KGaA, con sede en Darmstadt, Alemania, (en adelante
            el "Grupo Merck")
        </FONT><FONT SIZE=2 STYLE=""><SPAN LANG="es-ES">.</SPAN></FONT><FONT SIZE=2 STYLE="">
            Dado que los usos, productos, requisitos legales y de carácter
            regulatorio pueden variar significativamente en cada país todo el
            contenido del Web Site se encuentra dirigido única y exclusivamente
            a los residentes de los ESTADOS UNIDOS MEXICANOS que cuentan con la
            mayoría de edad conforme la legislación de éste país.
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            Para efectos de los Términos y
            Condiciones de este Web Site
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                será
                considerado Usuario Médico como
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            aquella
            persona que ostentándose como Profesional de la Salud utiliza un
            dispositivo o un ordenador y realiza múltiples operaciones con
            distintos propósitos.
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Si el USUARIO MÉDICO no es mayor de edad o no cuenta con las
                autorizaciones legales y capacidad requerida para acceder y usar los
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                datos,
                información (conjuntamente el "Contenido")
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                y servicios, debe abstenerse de usarlo, abandonarlo, y no acceder a
                sus funcionalidades. El acceso a los servicios y el Contenido por
                parte de personas menores de edad, es de exclusiva responsabilidad de
                sus padres, representantes legales o personas mayores de edad que
                tengan a menores de edad bajo su custodia y cuidado.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El  Web Site y
                MERCK se eximen de toda responsabilidad de verificar o validar esta
                situación.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            La información contenida en el
            Web Site se provee exclusivamente a modo de orientación general y de
            ningún modo constituye una recomendación médica ni sustituye
            consulta profesional alguna. MERCK recomienda a todos los usuarios
            que ante cualquier duda consulten a su médico. MERCK,  no es
            responsable por ninguna acción que pueda tomar cualquier persona u
            organización en función o consideración de la información
            provista en el presente Web Site.
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                ES OBLIGACIÓN
                DEL USUARIO MÉDICO LEER LOS TÉRMINOS Y CONDICIONES AQUÍ
                ESTABLECIDAS, PREVIAMENTE A LA DESCARGA DEL WEB SITE, POR TANTO,
                ENTIENDE QUE CUENTA CON EL CONOCIMIENTO INTEGRAL DE ESTE DOCUMENTO
                POR LO QUE
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            AL HACER
            CLIC EN EL BOTÓN "ESTOY DE ACUERDO", SIGNIFICA QUE ACEPTA EL
            USUARIO MÉDICO OBLIGARSE POR TODOS LOS TÉRMINOS Y CONDICIONES DE
            ESTE WEB SITE; EL NO ACEPTARLOS, IMPLICA QUE NO PODRÁ UTILIZAR LA
            WEB SITE. SI ELIGE NO ACEPTAR TODOS LOS TÉRMINOS Y CONDICIONES
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                DEBE
                ABSTENERSE DE HACER USO DEL CONTENIDO Y
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            SALIR
            DE ESTA PANTALLA.
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">DESCRIPCIÓN DEL  WEB SITE. </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            Estos Términos y Condiciones
            rigen el uso que haga el USUARIO MÉDICO de este  Web Site,
            incluyendo, de forma enunciativa mas no limitativa, el uso de todo el
            contenido tal como el texto, información, imágenes y audio,
            integradas, software, bases de datos, herramientas de ayuda y otros
            artículos relacionados
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El Web Site,
                el software, la información, las funciones y los servicios
                disponibles en o a través del Web Site se denominan colectivamente
                en el presente como el "Servicio" o los "Servicios". A menos
                que se establezca explícitamente lo contrario, todas las nuevas
                funciones o servicios que aumenten o mejoren el Servicio en el futuro
                se considerarán parte del Servicio y estarán sujetos a los
                presentes términos.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El
                Web Site le proporciona acceso y Servicio dirigido a profesionales de
                la salud, únicamente a aquellos debidamente autorizados para ejercer
                en el país de uso. El acceso al Web Site se encuentra protegido por
                contraseña.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=2>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">IDIOMA.</FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Es posible que
                partes del Contenido no existan en el idioma del USUARIO MÉDICO, sin
                embargo, el desarrollo de texto de estos Términos y Condiciones son
                en idioma español,  la interpretación del mismo deberá atenderse a
                la literalidad de la letra del mismo contenido.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=3>
        <LI>
            <P STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">
                    <SPAN LANG="es-ES">
                        ACCESO A
                        INTERNET NECESARIO
                    </SPAN>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Para acceder
                al servicio por medio de una conexión de datos celular en un
                dispositivo portátil, necesitará un plan de datos de su proveedor
                de servicio inalámbrico. Para acceder al servicio mediante una
                conexión de WiFi o Internet, deberá contar con un dispositivo,
                software y buscador compatible y acceso a Internet. Verifique con su
                proveedor si esto tiene algún costo. El USUARIO MÉDICO es
                exclusivamente responsable por los costos o gastos en los que incurra
                para acceder al Servicio a través de cualquier servicio inalámbrico
                u otro servicio de comunicación.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Los
                contenidos y servicios de la Web Site requieren software que se puede
                descargar, este software podrá actualizarse automáticamente en su
                equipo siempre que haya versiones o funciones nuevas disponibles.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            Siempre
                            y cuando cumpla con estas condiciones, MERCK concede una licencia
                            limitada, no exclusiva, no transferible y revocable para usar el
                            software exclusivamente para acceder a los contenidos y Servicios.
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            No
                            podrá copiar, modificar, distribuir, vender ni prestar ninguna parte
                            de los Servicios ni del software incluido ni podrá aplicar técnicas
                            de ingeniería inversa ni intentar extraer el código fuente de dicho
                            software.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            Los
                            contenidos y Servicios prestados por la  Web Site no pueden ser
                            utilizados por el USUARIO MÉDICO de forma inadecuada, con fines o
                            efectos ilícitos, contrarios a lo establecido en la presente
                            licencia, en la condiciones del servicios, en la ley, en la moral y
                            el orden público, incluyendo, sin constituir limitación alguna, las
                            restricciones aplicables relativas a los derechos de autor y otros
                            derechos de propiedad intelectual. Sólo podrá usar los contenidos y
                            Servicios si es autorizado por su titular o si está permitido por la
                            ley.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
        <A NAME="_Hlk500101453"></A>
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El contenido
                continúa siendo de propiedad de sus pacientes y/o suya, según
                corresponda. MERCK tampoco controla, verifica o aprueba el Contenido
                que se le proporciona por medio del uso del Servicio. Si el USUARIO
                MÉDICO comparte el Contenido al cual accede por medio del uso del
                Servicio en una forma que viola los derechos de sus pacientes
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                <I>
                    <B>
                        resultará
                        responsable de manera solidaria ante cualquier reclamo, denuncia,
                        queja o bien sanción que resulte de la Ley en materia vigente
                        aplicable debiendo sacar en paz y a salvo cualquier reclamo que
                        derive de tal violación en contra de MERCK.
                    </B>
                </I>
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            Estas
                            condiciones no le otorgan el derecho a vender, copiar el software
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            &nbsp;
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            que
                            se licencia ni usar las marcas y logotipos utilizados en los
                            contenidos y Servicios que reconoce el USUARIO MÉDICO son de
                            propiedad de MERCK.
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO se obliga a no utilizar los Servicios y contenidos de
                            cualquier forma que puedan dañar, inutilizar, sobrecargar o
                            deteriorar la  Web Site. El uso de los contenidos y Servicios de la
                            Web Site no lo convierte en titular de ninguno de los derechos de
                            propiedad intelectual ni del contenido al que acceda.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Si se le
                proporciona un código de identificación de USUARIO MÉDICO, una
                contraseña u otra información como parte de los procedimientos de
                seguridad, el USUARIO MÉDICO debe tratar dicha información de forma
                confidencial y no debe divulgarla a ningún tercero. MERCK tiene en
                todo momento derecho a inhabilitar cualquier código de
                identificación de usuario o contraseña, ya sea escogida por el
                USUARIO MÉDICO o asignada por MERCK, en cualquier momento, si en
                nuestra opinión el USUARIO MÉDICO no cumplió con alguna de las
                disposiciones de estos Términos. El USUARIO MÉDICO es responsable
                de realizar todos los arreglos necesarios para poder tener acceso al
                Web Site.  El USUARIO MÉDICO debe mantener las credenciales de su
                cuenta de forma confidencial y no debe autorizar a ninguna otra
                persona a que acceda o utilice el Servicio en su nombre. El USUARIO
                MÉDICO es responsable de todas las actividades que tengan lugar en
                su cuenta de Servicio.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            Con el propósito de utilizar
            esta Web Site, es posible que necesite acceder a la World Wide Web
            (internet), ya sea directamente o a través de dispositivos que
            tengan acceso al contenido en la Web, y que pague cuotas por Servicio
            relacionadas con dicho acceso. Asimismo, deberá proporcionar el
            equipo necesario para hacer dicha conexión a la World Wide Web.
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">MERCK no reivindica </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                la
                propiedad de datos, información que el USUARIO MÉDICO proporcione
                en el Servicio. Su contenido continúa siendo de la propiedad del
                USUARIO MÉDICO. Tampoco
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                controla,
                verifica o aprueba el Contenido que el USUARIO MÉDICO y otros pongan
                a disposición en el Servicio.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El USUARIO
                MÉDICO controla quién puede acceder a su contenido, junto con
                cualesquiera co-custodios.  El USUARIO MÉDICO acepta que se obliga a
                dar observancia a las Políticas de Privacidad de este Web Site. Sin
                embargo, si el USUARIO MÉDICO comparte contenido de otras personas
                deberá garantizar que cuenta con el consentimiento formal y expreso
                de acuerdo a la Legislación aplicable en el país, de forma tal que
                de violar los derechos de otras personas, incluidos los derechos de
                privacidad, el USUARIO MÉDICO acepta que estará infringiendo estos
                términos e
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            indemnizará,
            defenderá y mantendrá a MERCK, a sus respectivos funcionarios,
            empleados y proveedores a salvo contra cualquier reclamación o
            demanda, incluyendo honorarios razonables de abogados, hechos por
            algún tercero debido a o que supuestamente surjan del uso que el
            USUARIO MÉDICO haga de la  Web Site o por alguna contravención a
            este Contrato o violación a algún derecho de otra parte
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                .
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK podrá</FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                eliminar su contenido del Servicio en cualquier momento, si el
                USUARIO MÉDICO incumple estos términos.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                y/o
                de otras personas, incluidos los derechos de privacidad, el USUARIO
                MÉDICO está infringiendo estos Términos y deberá indemnizar
                completamente a Merck y a nuestras afiliadas con respecto a cualquier
                demanda de terceros que surja por su incumplimiento de estos
                Términos. A criterio MERCK podrá eliminar el Contenido del Servicio
                en cualquier momento si el USUARIO MÉDICO incumple estos Términos.
                El USUARIO MÉDICO tiene conocimiento de que MERCK podrá rescindir
                el Servicio en cualquier momento. A criterio de MERCK podrá eliminar
                de forma permanente el Contenido de los servidores. MERCK no tendrá
                ninguna obligación de poner a su disposición el Contenido después
                de la cancelación del Servicio, podrá almacenar de forma permanente
                el Contenido y/o podrá eliminarlo después de cierto
                período
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                <I>
                    <B>
                        [¿cuánto
                        tiempo?]
                    </B>
                </I>
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                .
                <!-- La legislación en diversas materias establece 10 años. -->En
                caso de ser eliminado, el Contenido puede ser irrecuperable. Todas
                las copias digitales y/o impresas que el USUARIO MÉDICO realice del
                Contenido puesto a disposición mediante este Servicio deberán ser
                realizadas en su propio sistema y/o con su propio equipamiento y
                quedan bajo su propia responsabilidad.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El USUARIO
                MÉDICO tiene conocimiento de que
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
            podrá
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                rescindir el Servicio en cualquier momento.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
            podrá
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                eliminar de forma permanente su contenido de sus servidores. Salvo
                medie mandato judicial o de autoridad competente del país
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                no
                tiene ninguna obligación de devolver al USUARIO MÉDICO el contenido
                después de la cancelación del Servicio.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                No
                almacena de forma permanente el contenido que el USUARIO MÉDICO
                carga y puede eliminarlo después de un período de tiempo. El
                USUARIO MÉDICO es consiente que el Contenido eliminado puede ser
                irrecuperable. En todo caso, antes de proceder a la eliminación
                total de cualquier contenido que el USUARIO MÉDICO haya ingresado,
                MERCK le informará con treinta (30) días de anticipación,
                permitiendo a el USUARIO MÉDICO, si así lo quisiera, imprimir una
                copia del reporte de sus pacientes que esté disponible en el
                sistema. Esta posibilidad de imprimir el reporte de sus pacientes
                tendrá una vigencia única de treinta (30) días.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Si el USUARIO
                MÉDICO decide incluir en sus registros cualquier contenido puesto a
                disposición mediante este Servicio, debe guardar una copia en su
                propio sistema.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=4>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">
                    <SPAN LANG="es-CO">
                        DESCARGA DE
                        SOFTWARE
                    </SPAN>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CO">
                El USUARIO
                MÉDICO debe asegurarse de contar con un anti-virus apropiado y con
                los requisitos informáticos necesarios para acceder y/o descargar
                cualquier contenido del Web Site.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-left: 0.75in; margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=5>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT FACE="Roboto Slab, serif">
                    <FONT SIZE=2 STYLE="font-size: 10pt">
                        <FONT FACE="Calibri, serif">
                            <FONT SIZE=2 STYLE="">
                                <SPAN LANG="es-ES">
                                    VIRUS,
                                    PIRATERÍA INFORMÁTICA Y OTRAS OFENSAS:
                                </SPAN>
                            </FONT>
                        </FONT>
                    </FONT>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO No debe hacer un mal uso del Servicio mediante la
                            introducción de virus, troyanos, gusanos informáticos, bombas
                            lógicas u otro material que sea malicioso o tecnológicamente
                            nocivo. El USUARIO MÉDICO No debe intentar obtener acceso no
                            autorizado al Web Site de
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif"><FONT SIZE=2 STYLE="">MERCK</FONT></FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            ,
                            el servidor en el que el Web Site se encuentra almacenado o cualquier
                            servidor, computadora o base de datos conectada al Web Site.  No debe
                            atacar el Web Site con un ataque de tipo denegación de servicio.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Ninguna parte
                de la  Web Site podrá ser reproducida o transmitida o almacenada en
                otro Web Site o en otra forma de sistema de recuperación
                electrónico.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            Al
                            violar esta disposición, el USUARIO MÉDICO estará cometiendo un
                            delito, reservándose
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        MERCK
                        el ejercicio de acción en contra de quien resulte responsable
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            .
                            En el caso de una violación de dicho tipo, su derecho de utilizar el
                            Servicio cesará inmediatamente.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        MERCK
                        no es
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            responsable
                            de ninguna pérdida o daño causado por un ataque de denegación del
                            Servicio, virus u otro material tecnológicamente nocivo que pueda
                            infectar su computadora, programas informáticos, datos u otros
                            materiales privados debido a su uso del Servicio o a la descarga de
                            cualquier material publicado en el mismo o en cualquier Web Site
                            vinculado a él.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=6>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">
                    <SPAN LANG="es-CO">
                        CONTENIDOS DE
                        TERCEROS
                    </SPAN>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CO">
                El Web Site
                pueden contener información propiedad de un tercero (por ejemplo
                artículos, cuadros comparativos, etcétera) así como también
                hipervínculos a sitios de terceros. En tal caso, proveerá esos
                contenidos a modo de cortesía para los USUARIO MÉDICOs. MERCK No
                tiene ningún tipo de control sobre sitios o contenidos de terceros,
                por lo que no los patrocina, representa, ni acepta ninguna
                responsabilidad de las que le corresponda al tercero. La navegación
                por fuera del presente Web Site, propiedad de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK</FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CO">
                ,
                es al propio riesgo de cada USUARIO MÉDICO.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Los Contenidos
                de terceros están sujetas a los términos y condiciones del titular
                tercero relevante.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                no
                tiene control sobre, ni es responsable por ningún asunto relacionado
                con tus tratos o transacciones con tales terceros ni sobre ningún
                contenido, contenido distribuido,  Web Site, sitio, anuncio, enlace,
                política de privacidad o práctica de terceros a los que se pueda
                acceder a través de la  Web Site de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK</FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                .
                Es responsabilidad y obligación del USUARIO MÉDICO consultar las
                políticas de privacidad y los términos y condiciones de servicio y
                uso de todos los sitios de terceros que visite antes de descargar o
                usar tales Contenidos de terceros.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">MERCK </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                se reserva el derecho a realizar un seguimiento e informar, de forma
                anónima, a los proveedores de servicios estadísticos de terceros,
                de la actividad del USUARIO MÉDICO en la  Web Site de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
            y/o "Grupo Merck".
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO autoriza la revisión del
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            &nbsp;
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            contenido
                            para determinar si es ilegal o infringe nuestras políticas, y
                            eliminarlo o negarnos a publicarlo si MERCK tiene razones suficientes
                            para considerar que infringe nuestras políticas o la ley. Sin
                            embargo, esta posibilidad no implica necesariamente que el contenido
                            sea revisado por
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif"><FONT SIZE=2 STYLE="">MERCK</FONT></FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            ,
                            asimismo
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        MERCK
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            no
                            tiene la obligación de examinar los contenidos registrados y/o
                            transmitidos por los USUARIO MÉDICOs de los servicios y contenidos
                            y, por tanto, no puede garantizar la legalidad, veracidad,
                            honestidad, rigurosidad, lealtad, integridad y/o calidad de los
                            mismos, ni asume ninguna responsabilidad derivada del incumplimiento
                            por parte de los usuarios de los presentes términos de uso.
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P LANG="es-CO" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=7>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">
                    <SPAN LANG="es-CO">
                        CAMBIOS EN EL
                        WEB SITE
                    </SPAN>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">MERCK se reserva </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CO">
                el
                derecho de cambiar cualquier parte del Web Site o de esta
                notificación legal en cualquier momento y sin necesidad de aviso
                previo a ninguna persona física o jurídica. Las modificaciones
                tendrán vigencia inmediata. Asimismo, y en concordancia con lo
                mencionado anteriormente,
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CO">
                no se
                obliga a mantener el Web Site actualizado.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                A exclusivo
                criterio de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                podrá modificar los Términos sin enviarle una notificación. Por lo
                tanto, sírvase continuar revisando los Términos cada vez que
                utilice el Web Site. No podrá utilizar el Web Site en ninguna forma
                no autorizada que pudiera interferir con el uso del mismo por otra
                persona ni obtener acceso no autorizado a ningún servicio, dato,
                cuenta o red.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Al continuar
                con el acceso y uso del Web Site después de la modificación de los
                Términos, el USUARIO MÉDICO acepta dichas modificaciones. Asimismo,
                cuando utiliza los servicios o funciones particulares de Merck en el
                Web Site, estará sujeto a cualesquiera directrices, normas, términos
                y condiciones publicadas aplicables a dichos servicios o funciones
                que puedan publicarse periódicamente.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            MERCK
                            puede cambiar y/o mejorar sus servicios. Por ello, es posible que
                            añada o elimine algunas funciones o características, o que suspenda
                            o cancele un servicio temporalmente o por completo con o sin
                            notificación previa.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            MERCK
                            puede suspender o cancelar el uso que haga de los contenidos o
                            servicios si no cumple con estas condiciones o si usa los servicios
                            de un modo que pueda ocasionar obligaciones legales a MERCK, que
                            afecte los servicios o afecte el uso de los servicios por parte de
                            otras personas.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO puede dejar de usar los servicios en cualquier
                            momento.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            MERCK
                            puede dejar de proporcionarle los servicios o añadir o crear nuevas
                            limitaciones en cualquier momento.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            Si
                            MERCK interrumpe un servicio, en los casos en los que sea razonable,
                            le informará con suficiente antelación y se le permitirá extraer
                            la información del servicio.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Ya sea que se
                reconozca específicamente o no, las marcas comerciales, las marcas
                de servicio y los logos visualizados en esta  Web Site pertenece a
                MERCK, sus socios promocionales u otros terceros.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; font-weight: normal; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                <SPAN STYLE="font-weight: normal">
                    MERCK
                </SPAN>
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                no
                interfiere, no toma decisiones, ni garantiza las relaciones que los
                USUARIO MÉDICOs lleguen a sostener o las vinculaciones con terceros
                que pauten y/o promocionen sus productos y servicios. Estas marcas de
                terceros se utilizan solamente para identificar los productos y
                servicios de sus respectivos propietarios y el patrocinio o el aval
                por parte de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                <SPAN STYLE="font-weight: normal">
                    MERCK
                </SPAN>
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                no
                se deben inferir con el uso de estas marcas comerciales.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=8>
        <LI>
            <P STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">
                    <SPAN LANG="es-ES">
                        SIN
                        RESPONSABILIDAD DE MERCK
                    </SPAN>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El USUARIO
                MÉDICO acuerda que ni
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK</FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            y/o Merck Group, ni
            ninguna de sus subsidiarias, divisiones, filiales o grupos a nivel
            global, ni ninguno de sus empleados será responsable por cualquier
            daño, reclamo o queja relacionada con el presente Web Site (ya sea
            por su uso, contenido o por cualquier evento que pueda tener relación
            alguna con el Web Site).
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Esta protección cubre las demandas basadas en la garantía,
                contrato, agravio, responsabilidad rigurosa y cualquier otra teoría
                legal. Esta protección cubre a
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK</FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                ,
                sus filiales y a los funcionarios, directores, empleados,
                representantes, y proveedores de la casa matriz y terceros de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK</FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                y sus filiales. Esta protección cubre todas las pérdidas incluidas,
                a modo no taxativo, los daños directos o indirectos, especiales,
                fortuitos, consiguientes, ejemplares y punitorios, lesión
                personal/muerte por negligencia, pérdida de ganancias o daños que
                resulten de la pérdida de datos o interrupción de la actividad
                comercial.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; background: #ffffff; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CL">
                La información
                provista en este Web Site es sólo para propósitos de información
                general.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CL">
                hace
                lo posible por proveer sólo información actualizada y completa en
                este Web Site, sin embargo no está en condiciones de verificar
                exactitud, integridad o vigencia de los mismos. Ya que dichos errores
                pueden ocurrir,
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CL">
                NO
                OTORGA GARANTÍAS RELACIONADAS CON LA EXACTITUD, CONFIABILIDAD,
                AJUSTE A CUALQUIER PROPÓSITO, O INTEGRIDAD DE CUALQUIER INFORMACIÓN
                PUBLICADA EN ESTE WEB SITE.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-CL">
                PROVEE
                LA INFORMACIÓN "COMO ESTÁ Y DONDE ESTÁ" Y, POR EL SOLO HECHO
                DE ACEPTAR ESTAS CONDICIONES, EL USUARIO MÉDICO RENUNCIA A TODAS LAS
                GARANTÍAS (EXPRESAS O IMPLÍCITAS) QUE PUDIERA INVOCAR.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El Contenido
                se proporciona a efectos informativos únicamente. A pesar de que se
                han tomado las precauciones necesarias en su compilación y
                mantenimiento,
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
            no es
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                responsable
                de ninguna acción iniciada por ninguna persona u organización,
                dondequiera que se encuentren, directa o indirectamente como
                resultado del servicio contenido o al que se accede a través del Web
                Site. Nada en este servicio debe interpretarse como un ofrecimiento
                de asesoramiento médico ni como una recomendación.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">MERCK </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                NI
                NINGUNA DE SUS FILIALES PRACTICAN LA MEDICINA NI OFRECEN SERVICIOS O
                ASESORAMIENTO MÉDICO.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El Servicio en este Web Site no pretende sustituir el asesoramiento
                médico informado.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                EL
                USO DEL SERVICIO OBTENIDO O DESCARGADO DE ESTE WEB SITE ES A CRITERIO
                Y RIESGO EXCLUSIVO DEL USUARIO MÉDICO.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                La disponibilidad y contenido del servicio varía según el país.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">MERCK  no asume </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                responsabilidad
                alguna con respecto al retraso, interrupción, rendimiento,
                interrupción por incumplimiento, infección o corrupción de
                cualquier dato u otra información transmitida con relación al uso
                de este Web Site y el servicio. Mediante el uso de este Web Site el
                USUARIO MÉDICO reconoce que la información será transmitida a
                través de intercambio local, intercambio entre centrales y líneas
                transportadoras de troncales de internet y a través de routers,
                conmutadores y otros dispositivos de propiedad, mantenidos y
                atendidos por empresas de comunicaciones locales de terceros y
                empresas de comunicaciones de larga distancia, empresas de servicios,
                proveedores de servicios de internet y otros, todos los que se
                encuentran fuera del control y de la jurisdicción de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                y sus
                respectivas filiales y proveedores.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            Cualquier
                            violación de su parte de estos términos de uso, o cualquier queja o
                            información que MERCK reciba de terceros sobre el incumplimiento,
                            abuso o mal uso de estos, podrá ser investigada por MERCK, quien
                            podrá tomar todas las medidas e iniciar todas las acciones legales y
                            extra legales, para obtener la cesación de las conductas o los
                            remedios e indemnizaciones a que haya lugar bajo la ley aplicable. La
                            violación de estos términos de uso puede resultar en
                            responsabilidad civil o penal. Si no está seguro de que sus acciones
                            respecto del acceso y uso de los servicios y contenidos constituyen
                            una violación o abuso de estos, por favor no dude en consultarnos
                            previamente. MERCK atenderá su consulta.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=9>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">
                    <SPAN LANG="es-ES">
                        RESPONSABILIDADES
                        DEL USUARIO MÉDICO:
                    </SPAN>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El USUARIO
                MÉDICO acepta que el acceso a los servicios y a los contenidos se
                realiza de forma autónoma, bajo su exclusiva responsabilidad y en
                equipos y conexiones seguras.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            Al acceder al Web
            Site el USUARIO MÉDICO acepta que toda la información que obtenga o
            descargue por medio del mismo es a su propia discreción y riesgo.
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El USUARIO
                MÉDICO es el único responsable del acceso y uso que con o sin
                intención, conocimiento o consentimiento, haga de este Web Site, los
                contenidos y la información contenida en el Web Site.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El Web Site se
                encuentra disponible únicamente para uso personal y no para uso
                comercial del USUARIO MÉDICO. El USUARIO MÉDICO reconoce que el
                ingreso de su información personal, y los datos que contiene el Web
                Site  a su disposición, lo realizan de manera voluntaria.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                <I>
                    Por
                    tratarse de una  Web Site  que puede ser utilizada en diferentes
                    países, el USUARIO MÉDICO es responsable del cumplimiento de las
                    leyes del país en que se encuentre, obligándose a cumplir en
                    especial con las normas vigentes de manejo de información digital en
                    salud de su país residente, liberando de cualquier responsabilidad a
                    MERCK. Es responsabilidad del USUARIO MÉDICO verificar si el uso de
                    la  Web Site no contraria ninguna disposición legal en el país de
                    residencia.
                </I>
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El USUARIO
                MÉDICO se obliga a usar la  Web Site  y los contenidos encontrados
                en ella de una manera diligente, correcta, lícita y en especial, se
                compromete a NO realizar las conductas descritas a continuación:
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                (a) Utilizar
                los contenidos de forma, con fines o efectos contrarios a la ley, a
                la moral y a las buenas costumbres generalmente aceptadas o al orden
                público;
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                (b)
                Reproducir, copiar, representar, utilizar, distribuir, transformar o
                modificar los contenidos de la  Web Site, por cualquier procedimiento
                o sobre cualquier soporte, total o parcial, o permitir el acceso del
                público a través de cualquier modalidad de comunicación pública;
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                (c) Utilizar
                los contenidos de cualquier manera que entrañen un riesgo de daño o
                inutilización de la  Web Site o de los contenidos;
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                (d) Suprimir,
                eludir o manipular el derecho de autor y demás datos identificativos
                de los derechos de autor incorporados a los contenidos, así como los
                dispositivos técnicos de protección, o cualesquiera mecanismos de
                información que pudieren tener los contenido;
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                (e) Emplear
                los contenidos y, en particular, la información de cualquier clase
                obtenida a través de la  Web Site  para distribuir, transmitir,
                remitir, modificar, rehusar o reportar la publicidad o los contenidos
                de esta con fines de venta directa o con cualquier otra clase de
                finalidad comercial, mensajes no solicitados dirigidos a una
                pluralidad de personas con independencia de su finalidad, así como
                comercializar o divulgar de cualquier modo dicha información,
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            rentar, arrendar,
            vender, sublicenciar, ceder, realizar ingeniería inversa,
            desensamblar, modificar, prestar, distribuir, exportar ni de otra
            forma transferir, ni permitir que otros utilicen la  Web Site,
            tecnología u otra información, incluyendo cualquier material
            impreso al respecto, tampoco puede crear trabajos derivados de ni de
            otra forma modificarlos, utilizar, descargar, ni exportar la  Web
            Site cuando lo anterior contravenga alguna ley o reglamento
            aplicable.
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                (f) No
                permitir que terceros ajenos a el USUARIO MÉDICO usen la  Web Site
                móvil con su clave;
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                (g) Utilizar
                la  Web Site  y los contenidos con fines lícitos y/o ilícitos,
                contrarios a lo establecido en estos Términos y Condiciones, o al
                uso mismo de la  Web Site, que sean lesivos de los derechos e
                intereses de terceros, o que de cualquier forma puedan dañar,
                inutilizar, sobrecargar o deteriorar la  Web Site  y los contenidos o
                impedir la normal utilización o disfrute de esta y de los contenidos
                por parte de los usuarios.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE=""><SPAN LANG="es-ES">(h) </SPAN></FONT><FONT SIZE=2 STYLE="">
            USOS
            PROHIBIDOS DE LA APLICACIÓN. El USUARIO MÉDICO acuerda no revender
            la Aplicación ni uso de ésta ni acceso a la Aplicación. El USUARIO
            MÉDICO acuerda no realizar ingeniería inversa, descompilar,
            desensamblar ni de otra forma intentar discernir el código fuente de
            los componentes del Web Site
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                COMO
                PROFESIONAL DE LA SALUD, EL USUARIO MÉDICO TIENE LA RESPONSABILIDAD
                TOTAL Y FINAL DEL ANÁLISIS, REVISIÓN Y EVALUACIÓN DE TODO
                CONTENIDO MOSTRADO EN EL SERVICIO.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Los
                USUARIOS MÉDICOS deben verificar en este Web Site cuál es la
                información adecuada para su país.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO acepta que la información ingresada, a través de la
                            Web Site  es de su total responsabilidad y/o de la persona que ella
                            autorice, que los datos allí consignados son fidedignos. Asume la
                            responsabilidad de actualizar oportunamente la información básica
                            correspondiente a cambio de dirección, teléfono, empresa, e-mail y
                            demás datos que se requieran.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO es responsable de su conducta y sus archivos. Los
                            contenidos y servicios podrán estar protegido por los derechos de
                            propiedad intelectual de otras personas. No copie, cargue, descargue
                            ni comparta contenido, a menos que tenga derecho de hacerlo.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO reconoce y acepta que la Web Site  requiere y utiliza
                            el servicio de teléfono, de acceso de datos o la capacidad de enviar
                            mensajes a la  Web Site no se responsabiliza de a disponibilidad o
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            &nbsp;
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            falta
                            de disponibilidad de dicha información. En consecuencia, la
                            prestadora de servicios de telefonía celular, podrá cobrar por el
                            uso del servicio de teléfono, los datos o el envió de mensajes de
                            texto, y el USUARIO MÉDICO es responsable de pagar por esos
                            servicios.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO deberá observar todas las leyes y regulaciones en
                            cuanto al uso de teléfonos móviles y otros dispositivos, vigentes y
                            aplicables en las ciudades o sitios que pretenda utilizarlos.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MÉDICO tiene prohibido eliminar, ocultar o alterar los
                            avisos legales que se muestren en los servicios o contenidos a que
                            tiene acceso.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            La
                            Web Site  podrá analizar su conducta y su contenido para verificar
                            el cumplimiento con estas condiciones. Sin embargo, no tiene
                            obligación de hacerlo.
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        MERCK
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            no es responsable del contenido que las personas publican y comparten
                            a través de los servicios.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            Por
                            lo anterior, ni
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        MERCK
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            ni sus autores se responsabilizan del entendimiento, interpretación
                            y/o uso de este contenido por parte de sus usuarios y/o usuarios
                            médicos, razón por la cual, su uso es de su exclusiva
                            responsabilidad y de quien la consulta.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Los links de
                Facebook®, Instagram®, twitter® en esta  Web Site  pueden mostrar
                contenido que no están bajo el control de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">MERCK.</FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                Aunque esta
                Web Site  trata de suministrar links solamente a sitios y
                aplicaciones de terceros que cumplan con las leyes y regulaciones
                aplicables y las normas, el USUARIO MÉDICO debe entender que no
                tiene control sobre la naturaleza y el contenido de esos sitios y no
                está recomendando estos sitios, la información que contienen ni los
                productos o servicios de terceros.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                <SPAN STYLE="font-weight: normal">
                    MERCK
                </SPAN>
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                no
                acepta responsabilidad por el contenido del sitio de un tercero con
                el cual existe un link de hipertexto y no ofrece garantía (explícita
                o implícita) en cuanto al contenido de la información en esos
                sitios.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                El USUARIO
                MÉDICO debe verificar las secciones de política legal y de
                privacidad de algunos otros sitios de
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                <SPAN STYLE="font-weight: normal">
                    MERCK
                </SPAN>
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                o
                de un tercero con los cuales se enlaza.
            </SPAN>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE=""><SPAN LANG="es-ES"><SPAN STYLE="font-weight: normal">MERCK</SPAN></SPAN></FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                no asume ninguna responsabilidad por pérdida directa, indirecta o
                consecuencial por el uso de un sitio de un tercero.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=10>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT FACE="Roboto Slab, serif">
                    <FONT SIZE=2 STYLE="font-size: 10pt">
                        <FONT FACE="Calibri, serif">
                            <FONT SIZE=2>
                                <SPAN LANG="es-ES">
                                    RESPONSABILIDADES
                                    ESPECIALES DEL USUARIO MEDICO
                                </SPAN>
                            </FONT>
                        </FONT>
                    </FONT>
                </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MEDICO es responsable del uso adecuado de su
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            &nbsp;
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            cuenta
                            y del ejercicio y riesgos
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            &nbsp;
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            propios
                            de la profesión, Así mismo del cumplimiento de las normas
                            inherentes al código de ética y demás protocolos, reglamentos, y
                            directivas establecidos por las autoridades y entes competentes para
                            la prestación de los servicios de la salud, especialmente los
                            reglamentos o normas contemplados en el país en que ejerce sus
                            profesión.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MEDICO tiene la obligación de hacer buen uso de la cuenta
                            que posee, la autorización o culpa que permita el acceso a su cuenta
                            de servicio y/o divulgación de contenidos en áreas públicas
                            implica la aceptación de responsabilidad por los daños que la
                            divulgación de dicho contenido pueda causar a terceros. Lo anterior,
                            sin perjuicio de la facultad que MERCK de eliminar o restringir el
                            acceso o visualización de la información que considere vulnera los
                            derechos de otros usuarios o de terceros.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MEDICO es responsable por los daños y perjuicios que por
                            motivo de su negligencia o descuido se causen a la persona o bienes
                            de los clientes y/o pacientes, de MERCK, y/o sus dependientes o de
                            terceros.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MEDICO se compromete al cumplimiento de las demás
                            obligaciones que se deriven de la naturaleza del contrato y asumirá
                            el costo de las sanciones a las que se puede ver expuesto
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            &nbsp;
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        MERCK
                    </FONT>
                </FONT><FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            con ocasión del incumplimiento de los términos de uso y/o de normas
                            o reglamentos que en ejercicio de su profesión, como también el
                            pago mismo de cualquier otro perjuicio que se genere para
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif"><FONT SIZE=2 STYLE="">MERCK.</FONT></FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MEDICO garantiza que es un profesional especializado
                            habilitado para el desarrollo del objeto de este contrato.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MEDICO deberá garantizar el pago de pólizas de
                            responsabilidad civil profesional para el ejercicio de la medicina.
                            El USUARIO MEDICO debe abstenerse de usar contenidos o servicios que
                            estén en contravía de las normas legales o reglamentos de la
                            profesión.
                        </SPAN>
                    </FONT>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Roboto Slab, serif">
            <FONT SIZE=2 STYLE="font-size: 10pt">
                <FONT FACE="Calibri, serif">
                    <FONT SIZE=2 STYLE="">
                        <SPAN LANG="es-ES">
                            El
                            USUARIO MEDICO cumplirá los protocolos de seguridad informática
                            establecidos por
                        </SPAN>
                    </FONT>
                </FONT><FONT FACE="Calibri, serif"><FONT SIZE=2 STYLE="">MERCK</FONT></FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-left: 0.75in; margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=11>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">PROPIEDAD INTELECTUAL</FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            El USUARIO MÉDICO reconoce que
            (a) la  Web Site contiene información propiedad y confidencial que
            está protegida por las leyes de propiedad intelectual y otras y que
            (b)
        </FONT><FONT SIZE=2 STYLE="">MERCK</FONT><FONT SIZE=2 STYLE="">
            <U>
            </U>
        </FONT><FONT SIZE=2 STYLE="">
            y/o Merck Group, ni
            ninguna de sus subsidiarias, divisiones, filiales o grupos a nivel
            global, ni ninguno de sus empleados (en adelante el "Grupo Merck")
        </FONT><FONT SIZE=2 STYLE=""><SPAN LANG="es-ES">.</SPAN></FONT><FONT SIZE=2 STYLE="">
            <U>
            </U>
        </FONT><FONT SIZE=2 STYLE="">
            y/o terceros poseen
            todo el derecho, título e interés en y para la  Web Site y software
            proporcionado a través de o junto con la  Web Site, incluyendo en
            forma enunciativa mas no limitativa, todos los Derechos de Propiedad
            Intelectual pertinentes. "Derechos de Propiedad Intelectual"
            significa todos y cada uno de los derechos que existan ocasionalmente
            en conformidad con la ley de patentes, ley de derechos de autor, ley
            de secreto comercial, ley de marcas registradas, ley de competencia
            injusta y cualquier otro derecho propietario, así como todas y cada
            una de las aplicaciones, renovaciones, extensiones y restauraciones
            de dicha  Web Site, que ahora o en adelante entren en vigencia en
            todo el mundo. El USUARIO MÉDICO está de acuerdo en que el USUARIO
            MÉDICO no lo hará ni permitirá que ningún tercero, (i) copie,
            venda, otorgue licencia, distribuya, transfiera, modifique, adapte,
            traduzca, prepare trabajos derivados de, descompile, realice
            ingeniería inversa, desensamble ni que de otra forma intente derivar
            códigos fuente de la  Web Site, a menos que esté permitido de otra
            forma, (ii) tome ninguna acción para evitar ni rechazar la
            seguridad, ni las reglas de uso del contenido que se proporcionen,
            desplieguen o hagan cumplir para cualquier funcionalidad (incluyendo
            en forma enunciativa mas no limitativa funcionalidad de
            administración de derechos digitales) incluidos en la  Web Site
            (iii) utilice la  Web Site para acceder, copiar, transferir,
            transcodificar ni retransmitir el contenido contraviniendo alguna ley
            o derechos de un tercero, ni (iv) eliminará, obscurecerá ni
            modificará las notificaciones de derechos de autor de Merck, S.A. de
            C.V. ni de ningún tercero, marcas registradas ni otras
            notificaciones de derechos propietarios fijos o incluidos dentro de o
            a los que se tenga acceso junto con o a través de la  Web Site.
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=12>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">CESIÓN. </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            El USUARIO MÉDICO no puede ceder
            ni transferir sus derechos contraídos por este documento sin el
            consentimiento previo por escrito de MERCK. MERCK puede ceder todos
            los derechos y responsabilidades contraídas por este Contrato a una
            subsidiaria, afiliada o sucesor, de todo o de una parte substancial
            de su negocio y activos sin su consentimiento. Sujeto a lo anterior,
            este Contrato pasará a beneficio de y será obligatorio para los
            sucesores y cesionarios permitidos de las partes.
        </FONT>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT FACE="Calibri, serif"><SPAN LANG="es-ES"><B>Jurisdicción.</B></SPAN></FONT><FONT FACE="Calibri, serif">
            <SPAN LANG="es-ES">
            </SPAN>
        </FONT><FONT FACE="Calibri, serif">
            Para todo lo relativo al
            cumplimiento e interpretación del presente Contrato, así como para
            cualquier controversia que surgiera del mismo, las partes se someten
            expresamente a la jurisdicción de los tribunales competentes del
            Distrito Federal, México, renunciando expresamente a cualquier otra
            jurisdicción que pudiere corresponderles por sus domicilios
            presentes o futuros, o por cualquier otra causa.
        </FONT>
    </P>
    <OL TYPE=I START=13>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">RENUNCIA Y NOTIFICACIÓN. </FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            La información que contiene esta
            Web Site, incluyendo, de forma enunciativa mas no limitativa, a
            referencias clínicas, imágenes, herramientas y otros puntos
            relacionados, tienen el propósito de ser utilizados como un recurso
            de referencia y no como una referencia completa. Si bien se ha tenido
            cuidado de confirmar la precisión de la información presentada y de
            describir las prácticas generalmente aceptadas, MERCK y sus
            respectivos cedentes de licencias, autores, correctores, revisores,
            contribuyentes y editores no son responsables de errores ni omisiones
            ni de ninguna consecuencia de la  Web Site de la información aquí
            incluida y no declaran expresa ni implícitamente, con respecto a la
            actualidad, integridad ni precisión del contenido de la  Web Site.
            Es responsabilidad del licenciatario aplicar esta información en una
            situación en particular. MERCK no avala y no es responsable de la
            precisión del contenido de fuentes ajenas a MERCK ni prácticas o
            normas de fuentes ajenas a MERCK.
        </FONT>
    </P>
    <P LANG="es-CL" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <OL TYPE=I START=14>
        <LI>
            <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                <FONT SIZE=2 STYLE="">VIGENCIA Y TERMINACIÓN</FONT>
            </P>
    </OL>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            Estos términos y condiciones
            estarán vigentes por
        </FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                tiempo
                indefinido hasta que Merck decida suspender o interrumpir la campaña.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            MERCK en todo momento podrá dar por terminados los presentes. A la
            terminación, el USUARIO MÉDICO dejará de utilizarla y eliminará o
            desinstalará permanentemente todas las copias de la  Web Site en su
            totalidad. Si el USUARIO MÉDICO no cumple con algún término de
            esta licencia, los derechos que haya el USUARIO MÉDICO adquirido con
            esta licencia terminarán automáticamente sin notificación de parte
            MERCK.
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                MERCK informa
                que los datos estadísticos recogidos se mantendrán hasta cinco (5)
                años después de la eventual desactivación del Web Site.
            </SPAN>
        </FONT>
    </P>
    <P LANG="es-ES" ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <BR>
    </P>
    <P ALIGN=JUSTIFY STYLE="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
        <FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                En el Evento
                en que un USUARIO MÉDICO incumpla estos Términos y Condiciones, o
                cualesquiera otras disposiciones que resulten de  Web Site,
            </SPAN>
        </FONT><FONT SIZE=2 STYLE=""><SPAN LANG="es-ES"><SPAN STYLE="font-weight: normal">MERCK</SPAN></SPAN></FONT><FONT SIZE=2 STYLE="">
            <SPAN LANG="es-ES">
                podrá suspender su acceso a la  Web Site.
            </SPAN>
        </FONT><FONT SIZE=2 STYLE="">
            El
            USUARIO MÉDICO está de acuerdo en acceder y utilizar la  Web Site
            en conformidad con todas las leyes y reglamentos aplicables,
            incluyendo, en forma enunciativa mas no limitativa, leyes y
            reglamentos estatales y federales. El USUARIO MÉDICO está de
            acuerdo en no utilizar la  Web Site  para ningún propósito ilegal
            ni contrario a la ley.
        
                                
                                </font>
                            </p>
                            <p align="JUSTIFY" style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                <font size="2" style="">Este documento constituye el convenio y acuerdo definitivo,
                                    exclusivo y completo de las partes, con respecto al asunto del mismo y reemplaza
                                    todos los acuerdos y convenios anteriores y contemporáneos, orales o escritos entre
                                    las partes. </font>
                            </p>
                            <p align="JUSTIFY" style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                <font size="2" style="">La falta de MERCK de ejercer o hacer cumplir algún derecho o
                                    estipulación de este documento, no constituirá una renuncia de dicho contrato o
                                    estipulación. </font>
                            </p>
                            <p align="JUSTIFY" style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                <br>
                            </p>
                            <ol type="I" start="15">
                                <li>
                                    <p align="JUSTIFY" style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                        <font size="2" style="">ACUERDO DEL USUARIO MÉDICO. </font>
                                    </p>
                            </ol>
                            <p align="JUSTIFY" style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                <font size="2" style="">El USUARIO MÉDICO declara y garantiza por medio de este instrumento
                                    que quedará legalmente obligado por este documento una vez que descargue, instale
                                    y/o utilice la Web Site. </font>
                            </p>
                            <p lang="es-ES" style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                <br>
                            </p>
                            <p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                <br>
                            </p>
                            <p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%">
                                <br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal Olvidar Contrase&ntilde;a-->
<form data-toggle="validator" role="form" id="Recover" method="post" action="jq/jq_recoverpassword.asp">
    <div id="RecoverPassword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0 ">
                <div class="panel panel-color panel-primary back-modal-pop">
                    <div class="panel-heading modal-heading">
                        <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                            ×</button>
                        <h3 class="panel-title">&nbsp;</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="jumbotron vertical-center" style="background-color: transparent;">
                                    <div class="container">
                                        <img src="assets/images/logos/1.png" class="img-responsive" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ingrese su e-mail</label>

                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="CorreoRecuperacion" name="CorreoRecuperacion" placeholder="Ingrese su  e-mail" required oninvalid="setCustomValidity('Debes de completar este campo')" oninput="setCustomValidity('')">
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <button type="submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5"><i class="fa fa-paper-plane m-l-5"></i>&nbsp;Enviar</button>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <button type="button" class="btn btn-danger btn-bordred waves-effect w-md waves-light m-b-5" data-dismiss="modal" aria-hidden="true"><i class="fa fa-ban m-l-5"></i>&nbsp;Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!--Modal Olvidar Contrase&ntilde;a-->



<!--Modal Cookies -->
<div id="full-width-modal-cookies" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content p-0 b-0 ">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true"
                        style="color: white;">
                        ×</button>
                    <h3 class="panel-title">
                        &nbsp;</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <h4 style="color: #3266b1;">
                                    <u>POLÍTICA DE COOKIES</u>
                                </h4>
                                <p>
                                    MERCK utiliza cookies de terceros con la finalidad de medir y hacer seguimiento
                                    de la actividad del sitio web y sus subdominios (el " <strong>Sitio Web</strong>").
                                </p>
                                <p>
                                    Al hacer clic en cualquier botón u oprimir el mismo de forma digital, casilla de
                                    verificación o enlace contenido en el Sitio Web, rellenar un formulario o hacer
                                    <em>login</em>, descargar cualquier contenido o hacer <em>scroll</em>, el Usuario
                                    manifiesta su aceptación al uso de cookies que hace el Sitio Web y la instalación
                                    de las mismas en el ordenador usado (incluyendo, los dispositivos móviles y los
                                    navegadores) por éste para acceder y navegar a través del mismo, así como la recogida
                                    y el tratamiento de los datos de carácter personal del Usuario de la manera y finalidades
                                    descritas en esta Política de Cookies.
                                </p>
                                <p>
                                    En caso de no estar acuerdo con esta Política de Cookies, el Usuario debe abstenerse
                                    de usar el Sitio Web y abandonar el mismo de forma inmediata.
                                </p>
                                <p>
                                    <strong>1. ¿Qué es una <em>cookie</em>?.</strong>
                                </p>
                                <p>
                                    <strong></strong>
                                </p>
                                <p>
                                    Una cookie<em> </em>es un fichero que se descarga en el terminal (por ejemplo, un
                                    ordenador, un teléfono móvil, una <em>tablet</em>, etc.) del Usuario cuando éste
                                    accede a determinadas páginas web, como por ejemplo, ésta. Las cookies<em> </em>
                                    permiten a dichas páginas web, entre otras cosas, monitorear, almacenar y recuperar
                                    información sobre los hábitos de navegación del Usuario (por ejemplo, preferencias,
                                    perfil, claves de acceso, etc.) así como obtener información técnica relacionada
                                    con la navegabilidad de las páginas web. Las cookies también contribuyen a la funcionalidad,
                                    uso e accesibilidad de las páginas web.
                                </p>
                                <p>
                                    Dependiendo de la función y finalidad de las cookies, existen varios tipos:
                                    <br />
                                    <br />
                                </p>
                                <ul>
                                    <li><strong>Analíticas</strong> .- Estas cookies recogen información con la finalidad
                                        de permitir a los sitios web realizar una evaluación del uso que hace los usuarios
                                        de los mismos y su actividad general, así como recopilar datos estadísticos. Las
                                        cookies analíticas miden y recogen datos del sitio web (visitas, parámetros de tráfico,
                                        clicks, páginas vistas, etc.) para entender y optimizar la página web. </li>
                                    <li><strong>Sociales</strong> .- Son necesarias para las redes sociales externas (Facebook,
                                        Google, Twitter, Pinterest, etc.). Su función es controlar la interacción con los
                                        widgets sociales dentro de una página web. </li>
                                </ul>
                                <p>
                                    <strong></strong>
                                </p>
                                <ul>
                                    <li><strong>Cookies de publicidad y comportamentales</strong> .- Recogen información
                                        sobre los hábitos de navegación del Usuario y su comportamiento con la finalidad
                                        de identificar las preferencias, gustos y hábitos dentro de un sitio web específico.
                                        Estas cookies permiten adecuar el contenido de la publicidad en base al análisis
                                        realizado de los hábitos de navegación del usuario y deduciendo las características
                                        del mismo como localización, grupo de edad, género. </li>
                                </ul>
                                <p>
                                    <strong></strong>
                                </p>
                                <ul>
                                    <li><strong>Técnicas</strong> .- Son las estrictamente necesarias para la funcionalidad
                                        y la navegabilidad de una página web, permiten la navegación del Usuario a través
                                        de dicha página o aplicación y la utilización de sus diferentes opciones o servicios.
                                    </li>
                                </ul>
                                <p>
                                    <strong></strong>
                                </p>
                                <ul>
                                    <li><strong>Cookies de personalización</strong> .- Son aquéllas que permiten a los sitios
                                        web conservar determinadas preferencias (por ejemplo, idioma, país, configuración
                                        regional, etc.) predefinidas por el usuario en su primera visita (y posteriores)
                                        en el sitio web. </li>
                                    <li><strong>Cookies de terceros:</strong> Las cookies de terceros son titularidad de
                                        terceros usadas por los sitios web a objeto de gestionar y mejorar el contenido
                                        y los servicios ofrecidos. </li>
                                </ul>
                                <p>
                                    Para obtener más información acerca de las cookies, el Usuario puede visitar la
                                    página web All About Cookies.org, accesible a través del siguiente enlace <a href="http://www.allaboutcookies.org/es">
                                        http://www.allaboutcookies.org/es </a>.
                                </p>
                                <p>
                                    <strong>2. ¿Qué tipos de <em>cookies </em>utiliza este sitio web?.</strong>
                                </p>
                                <p>
                                    <strong></strong>
                                </p>
                                <p>
                                    <strong><u>Cookies analíticas</u></strong>
                                </p>
                                <p>
                                    <strong>Google Analytics</strong>
                                </p>
                                <p>
                                    <strong></strong>
                                </p>
                                <p>
                                    Nuestra web utiliza Google Analytics, un servicio analítico web prestado por Google,
                                    Inc. (en adelante, “<strong>Google</strong>”), una empresa ubicada en los Estados
                                    Unidos de América. Por tanto, los datos recogidos y tratados por Google Analytics
                                    (incluyendo la dirección IP del Usuario) podrán ser transferidos a los Estados Unidos
                                    de América y almacenados en los servidores de Google ubicados en dicho país.
                                </p>
                                <p>
                                    Por medio de las Google Analytics, MERCK analiza la interacción del Usuario con
                                    el Sitio Web, hace seguimiento de los hábitos de navegación del mismo dentro de
                                    la misma, recoge datos de su actividad para que MERCK pueda medir el rendimiento
                                    de ésta.
                                </p>
                                <p>
                                    El Usuario puede gestionar y bloquear el tratamiento de sus datos de carácter personal
                                    a través de Google Analytics mediante la configuración del navegador usado para
                                    acceder al Sitio Web.
                                </p>
                                <p>
                                    El Usuario puede ampliar la información aquí contenida sobre Google Analytics haciendo
                                    clic <a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage">
                                        aquí </a>.
                                </p>
                                <p>
                                    Por otro lado, el Usuario puede desactivar Google Analytics mediante la instalación
                                    de un complemento de inhabilitación. El Usuario puede obtener más información sobre
                                    dicho complemento haciendo clic <a href="https://support.google.com/analytics/answer/181881?hl=es">
                                        aquí. </a>
                                </p>
                                <p>
                                    <strong><u>Cookies de publicidad y comportamentales</u></strong>
                                </p>
                                <p>
                                    <strong>Google Adwords Conversion</strong>
                                </p>
                                <p>
                                    <strong></strong>
                                </p>
                                <p>
                                    El Sitio Web utiliza Google Adwords Conversion, un servicio de seguimiento de conversiones
                                    de Adwords prestado por Google, con la finalidad de ayudar a MERCK a realizar un
                                    seguimiento de los Usuarios que visualizan los anuncios de MERCK y hacen clic en
                                    los mismos.
                                </p>
                                <p>
                                    El Usuario puede ampliar la información aquí contenida sobre la cookie Google Adwords
                                    Conversion haciendo clic <a href="http://www.google.es/intl/es/policies/technologies/ads/">
                                        aquí.</a>
                                </p>
                                <p>
                                    <strong>Google Dynamic Remarketing</strong>
                                </p>
                                <p>
                                    El Sitio Web utiliza Google Dynamic Remarketing, un servicio de publicidad de web
                                    prestado por Google, con la finalidad de ayudar MERCK a la gestión de anuncios y
                                    su publicación en el Sitio Web.
                                </p>
                                <p>
                                    <strong></strong>
                                </p>
                                <p>
                                    El Usuario puede ampliar la información aquí contenida sobre la cookie Google Dynamic
                                    Remarketing haciendo clic <a href="https://support.google.com/adwords/answer/2454000?hl=es&amp;ref_topic=3122875">
                                        aquí. </a>
                                </p>
                                <p>
                                    <strong>Google Tag Manager </strong>
                                </p>
                                <p>
                                    El Sitio Web utiliza Google Tag Manager, un servicio de publicidad de web prestado
                                    por Google, con la finalidad de ayudar MERCK a la gestión de anuncios y su publicación
                                    en el Sitio Web.
                                </p>
                                <p>
                                    <strong></strong>
                                </p>
                                <p>
                                    El Usuario puede ampliar la información aquí contenida sobre la cookie Google Tag
                                    Manager haciendo clic <a href="https://www.google.com/intl/es/tagmanager/">aquí.</a>
                                </p>
                                <p>
                                    <strong>3. ¿Cómo puedo desactivar o eliminar las cookies que utiliza este sitio web?.
                                    </strong>
                                </p>
                                <p>
                                    <strong></strong>
                                </p>
                                <p>
                                    El Usuario puede gestionar la actividad de las cookies, bloquearlas o eliminarlas
                                    mediante la selección de la configuración apropiada del navegador empleado para
                                    navegar el Sitio Web. En caso contrario, la navegación por el Sitio Web por parte
                                    del Usuario implica su aceptación del uso de las mismas conforme lo aquí expuesto.
                                </p>
                                <p>
                                    <strong>En navegadores PC:</strong>
                                </p>
                                <p>
                                    · Mozilla Firefox: En caso de utilizar el navegador Mozilla Firefox, el Usuario
                                    puede obtener información sobre cómo bloquear el uso de cookies y eliminarlas haciendo
                                    clic <a href="https://support.mozilla.org/es/kb/impedir-que-los-sitios-web-guarden-sus-preferencia">
                                        aquí </a>.
                                </p>
                                <p>
                                    · Internet Explorer: En caso de utilizar el navegador Internet Explorer, el Usuario
                                    puede obtener información sobre cómo bloquear el uso de cookies y eliminarlas haciendo
                                    clic <a href="http://windows.microsoft.com/es-es/windows-vista/block-or-allow-cookies">
                                        aquí </a>.
                                </p>
                                <p>
                                    · Google Chrome: En caso de utilizar el navegador Google Chrome, el Usuario puede
                                    obtener información sobre cómo bloquear el uso de cookies y eliminarlas haciendo
                                    clic <a href="https://support.google.com/chrome/answer/95647?hl=es">aquí</a>.
                                </p>
                                <p>
                                    · Apple Safari: En caso de utilizar el navegador Apple Safari, el Usuario puede
                                    obtener información sobre cómo bloquear el uso de cookies y eliminarlas en la versión
                                    Safari 6/7 (Mavericks) haciendo clic <a href="https://support.apple.com/kb/PH17191?locale=es_ES">
                                        aquí</a> y en la versión Safari 8 (Yosemite) <a href="https://support.apple.com/kb/PH19214?locale=es_ES&amp;viewlocale=es_ES">
                                            aquí </a>.
                                </p>
                                <p>
                                    · Opera: En caso de utilizar el navegador Opera, el Usuario puede obtener información
                                    sobre cómo bloquear el uso de cookies y eliminarlas haciendo clic <a href="http://help.opera.com/Windows/11.50/es-ES/cookies.html">
                                        aquí</a>.
                                </p>
                                <p>
                                    <strong>En navegadores de dispositivos móviles: </strong>
                                </p>
                                <p>
                                    · IOS: El Usuario puede obtener información sobre cómo bloquear el uso de cookies
                                    y eliminarlas en Opera haciendo clic <a href="http://www.opera.com/es-419/help/mini/ios">aquí</a>,
                                    en Safari <a href="https://support.apple.com/es-es/HT201265">aquí</a> y en Google
                                    Chrome <a href="https://support.google.com/chrome/answer/2392709?hl=es">aquí</a>.
                                </p>
                                <p>
                                    · Android: El Usuario puede obtener información sobre cómo bloquear el uso de cookies
                                    y eliminarlas en Mozilla Firefox haciendo clic <a href="https://support.mozilla.org/es/kb/habilitar-o-deshabilitar-cookies-en-firefox-para-android">
                                        aquí </a>y en Google Chrome <a href="https://support.google.com/chrome/answer/2392709?hl=es">
                                            aquí</a>.
                                </p>
                                <p>
                                    · Windows Phone 7: El Usuario puede obtener información sobre cómo bloquear el uso
                                    de cookies y eliminarlas haciendo clic <a href="https://privacy.microsoft.com/es-es/privacystatement">
                                        aquí </a>.
                                </p>
                                <p>
                                    · Windows Phone 8: El Usuario puede obtener información sobre cómo bloquear el uso
                                    de cookies y eliminarlas haciendo clic <a href="https://www.windowsphone.com/es-ES/legal/wp8/windows-phone-privacy-statement">
                                        aquí </a>.
                                </p>
                                <p>
                                    · Windows Phone 10: El Usuario puede obtener información sobre cómo bloquear el
                                    uso de cookies y eliminarlas haciendo clic <a href="http://windows.microsoft.com/es-es/windows-10/windows-privacy-faq">
                                        aquí </a>.
                                </p>
                                <p>
                                    El bloqueo, la deshabilitación o la eliminación de las cookies utilizadas por MERCK
                                    en el Sitio Web puede afectar a la plena funcionalidad del mismo; haciendo que algunos
                                    servicios o funciones del Sitio Web no estén disponibles.
                                </p>
                                <p>
                                    <strong>Última fecha de actualización:16-noviembre-2017</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>