<!--#include file="data/con_ma.inc" -->
<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
    <style>
        html {
            background-color: white;
            background: url(images/fondos/fondointerior3.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            font-family: Verdana, Geneva, sans-serif;
            margin: 0;
            color: #797979;
        }

        body {
            background-color: transparent;
        }

        #bg {
            background-image: url(images/fondos/plecaBlanca.png);
            background-size: 100% 100%;
        }

        .grad {
            background: #008bff; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(#008bff, #003d90); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#008bff, #003d90); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#008bff, #003d90); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#008bff, #003d90); /* Standard syntax */
            color: white;
            box-shadow: 0px 5px 5px #888888;
            padding: 0.2em;
            margin-top: 4em;
            margin-bottom: 4em;
        }

            .grad h4 {
                color: white;
                font-weight: bold;
                margin-left: 1.5em;
            }

        .form-control {
            border: 3px solid #E3E3E3;
        }

        /* ESTILOS NORMALES (para móviles en vertical) */
        #div-registro {
            margin-top: 5em;
            margin-bottom: 5em;
        }

        /* RESPONSIVE */

        /* Móviles en horizontal o tablets en vertical*/
        @media (min-width: 768px) {
        }

        /* Tablets en horizonal y escritorios normales */
        @media (min-width: 1024px) {
            #div-registro {
                margin-top: 3em;
                margin-bottom: 5em;
            }
        }

        /*Escritorios muy anchos*/
        @media (min-width: 1200px) {
        }
    </style>
    <script>
         function RefreshImage(valImageId) {
            var objImage = document.images[valImageId];
            if (objImage == undefined) {
                return;
            }
            var now = new Date();
            objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
        }
    </script>
</head>

<body class="fixed-left">
    <!--#include file="top_home.asp" -->
    <main class="page-content">
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row hidden-xs">
                        <div class="col-md-12">
                            <div align="center">
                                <img src="images/logo/logoMerck.png" class="img-responsive" style="margin-top: 18px; margin-bottom: 18px;" />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="bg">
                        <form data-toggle="validator" role="form" id="registro" method="post" action="jq/jq_registro.asp">
                            <div class="form-group col-lg-12 col-md-12" id="div-registro">
                                <div class="col-md-4">
                                    <h1>REGISTRO</h1>
                                </div>
                                <div class="col-md-8">
                                    <p>Por favor, ingrese la informaci&oacute;n que se le solicita a continuaci&oacute;n. Es importante que seleccione su especialidad correctamente para asegurar que la informaci&oacute;n que le mostremos sea la adecuada a su perfil. Las casillas marcadas con * son obligatorias.</p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-user"></i><span>&nbsp;DATOS PERSONALES</span></h4>
                                    </div>
                                </div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">
                                                *Nombre
                                                <br />
                                                <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b></label>
                                            <input type="text" class="form-control" id="inputName" placeholder="Nombre" data-parsley-error-message="Complete su nombre. Gracias." required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputApaterno" class="control-label">
                                                *Apellido Paterno
                                                <br />
                                                <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b>
                                            </label>
                                            <input type="text" class="form-control" id="inputApaterno" placeholder="Apellido Paterno" data-parsley-error-message="Complete su apellido paterno. Gracias." required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputAmaterno" class="control-label">
                                                *Apellido Materno
                                                <br />
                                                <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b>
                                            </label>
                                            <input type="text" class="form-control" id="inputAmaterno" placeholder="Apellido Materno" data-parsley-error-message="Complete su apellido materno. Gracias." required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSex">Sexo</label>
                                            <select class="form-control form-control-sm" style="font-size: 12px; height: 40px;" id="inputSex">
                                                <option>Seleccione</option>
                                                <option value="F">Femenino</option>
                                                <option value="M">Masculino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputBirth">*Fecha de nacimiento</label>
                                            <input class="form-control" type="date" style="font-size: 12px; height: 40px;" id="inputBirth" data-parsley-error-message="Complete su fecha de nacimiento. Gracias." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-key"></i><span>&nbsp;DATOS PARA SU CUENTA</span></h4>
                                    </div>
                                </div>


                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Correo electrónico</label>
                                            <input type="email" class="form-control" id="inputEmail" placeholder="Email" data-parsley-error-message="Complete su correo electrónico. Gracias." data-parsley-type="email" required>
                                            <div class="help-block with-errors"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label">*Contraseña</label>
                                            <input type="password" data-minlength="6" class="form-control" id="inputPassword" placeholder="Contraseña" data-parsley-min="6" data-parsley-error-message="Escriba su contraseña. Gracias." required>
                                            <div class="help-block">Mínimo 6 caracteres</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Confirme su correo electrónico</label>
                                            <input type="email" class="form-control" id="inputEmailConfirm" data-match="#inputEmail" data-parsley-error-message="Confirme su correo electrónico. Gracias." data-parsley-type="email" placeholder="Confirmar correo" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Confirme su contraseña</label>
                                            <input type="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-parsley-error-message="Confirme su contraseña. Gracias." data-parsley-min="6" placeholder="Confirmar" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-12"></div>

                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-university"></i><span>&nbsp;INFORMACIÓN ACADÉMICA Y LABORAL</span></h4>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSpeciality">*Especialidad</label>
                                            <select class="form-control form-control-sm" id="inputSpeciality" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su especialidad. Gracias." required>
                                                <option value="">Seleccione</option>
                                                <option value="Medicina General">Medicina General</option>
                                                <option value="Medicina Interna">Medicina Interna</option>
                                                <!--<option value="Odontología">Odontología</option>-->
                                                <option value="Ortopedia">Ortopedia</option>
                                                <option value="Otra">Otra</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputProfessionalID" class="control-label">*Cédula profesional</label>
                                            <input type="text" class="form-control" style="height: 40px;" id="inputProfessionalID" placeholder="Cédula profesional" data-parsley-error-message="Complete su cédula profesional. Gracias." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>
                                <br />
                                <div class="col-md-12"></div>
                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSpeciality">*Tipo de práctica</label>
                                            <select class="form-control form-control-sm" id="inputPractice" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su tipo de práctica. Gracias." required>
                                                <option value="">Seleccione</option>
                                                <option value="Pública">Pública</option>
                                                <option value="Privada">Privada</option>
                                                <option value="Ambas">Ambas</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputProfessionalID" class="control-label">*Institución donde labora</label>
                                            <input type="text" class="form-control" style="height: 40px;" id="inputInstitution" placeholder="Institución" data-parsley-error-message="Complete su institución laboral. Gracias." required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>

                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-envelope"></i><span>&nbsp;DATOS DE CONTACTO</span></h4>
                                    </div>
                                </div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-4">
                                        <label for="inputStreet" class="control-label">Calle</label>
                                        <input type="text" class="form-control" id="inputStreet" placeholder="Calle" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputNumber" class="control-label">Número</label>
                                        <input type="text" class="form-control" id="inputNumber" placeholder="Número" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputStreet2" class="control-label">Colonia</label>
                                        <input type="text" class="form-control" id="inputStreet2" placeholder="Calle" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-3">
                                        <%
                                            set CursorPaises = createobject("ADODB.Recordset")
                                            CursorPaises.CursorType =1 
                                            CursorPaises.LockType = 3

                                            SQLPaises = "select * from cat_paises where activo = 'si'"
                                            CursorPaises.Open SQLPaises, StrConn, 1, 3

                                        %>
                                        <label for="inputCountry" class="control-label">País</label>
                                        <select name="inputcountry" id="inputcountry" class="form-control form-control-sm" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su país. Gracias." required>
                                            <option value="">*Seleccione</option>
                                            <%
                                            Do While not CursorPaises.EOF 
                                            %>
                                            <option value="<%=CursorPaises("id_pais") %>"><%=CursorPaises("pais") %></option>
                                            <%
                                            CursorPaises.MoveNext
                                            Loop
                                          '  CursorPaises.Close
                                            CursorPaises.MoveFirst    
                                            %>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputState" class="control-label">*Estado</label>
                                        <select class="form-control form-control-sm" id="inputState" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su estado. Gracias." required disabled>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputCity" class="control-label">*Ciudad</label>
                                        <input type="text" class="form-control" id="inputCity" style="font-size: 12px; height: 40px;" placeholder="Ciudad" data-parsley-error-message="Complete su ciudad. Gracias." required disabled>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputCP" class="control-label">*Código Postal</label>
                                        <select class="form-control form-control-sm" id="inputCP" style="font-size: 12px; height: 40px;" data-parsley-error-message="Escriba su código postal. Gracias." required disabled>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6" style="padding-left:0px; padding-right:0px;">
                                        <div class="col-md-12">
                                        <label for="inputLadaTel" class="control-label">Teléfono</label></div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="inputLadaTel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Lada">
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="inputTelefono" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Teléfono">
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-left:0px; padding-right:0px;">
                                        <div class="col-md-12">
                                        <label for="inputLadaTel" class="control-label">Celular</label></div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="inputLadaCel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Lada">
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="inputCelular" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Teléfono">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            *Ingrese el c&oacute;digo de seguridad:
                                        <img src="captcha.asp" hspace="2" id="imgCaptcha" class="img-responsive" onclick="RefreshImage('imgCaptcha')" />
                                        </div>
                                        <div class="col-md-8">
                                            <span>
                                                <input type="text" class="form-control" id="capchacode" style="font-size: 12px; height: 40px;" placeholder="Código" data-parsley-error-message="Escriba el código de seguridad. Gracias." required>
                                            </span>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox1" type="checkbox">
                                                <label for="checkbox1">
                                                    *Acepto los terminos y condiciones <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-terminos-condiciones">(consultar)</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox2" type="checkbox">
                                                <label for="checkbox2">
                                                    *He le&iacute;do el aviso de privacidad <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-aviso-privacidad">(consultar)</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Registrarse</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <!--<script src="assets/js/waves.js"></script>-->
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!--Validator-->
    <script src="js/validator.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>

    <script src="js/MetroJs.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
            $(".appbar").applicationBar();

            function ActualizaCP() {
                $.ajax({
                    url: "jq/jq_obtener_codigos.asp",
                    type: "POST",
                    data: "estado=" + $("#inputState").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputCP").html(opciones);
                        $('#inputCP').prop("disabled", false);
                    }
                })
                $('#inputCP').prop("disabled", false);
            }

            function ActualizaEstados() {
                $.ajax({
                    url: "jq/jq_obtener_estados.asp",
                    type: "POST",
                    data: "inputcountry=" + $("#inputcountry").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputState").html(opciones);
                        if ($('#inputState option:selected').val() == 1) {
                            $('#inputState').prop("disabled", true);
                            $('#inputCity').prop("disabled", true);
                            $('#inputCP').prop("disabled", true);

                            $("#inputCP").html("<option value='N/A' selected>No aplica</option>");
                            $("#inputCity").val("No aplica");

                        } else {
                            $('#inputState').prop("disabled", false);
                            $('#inputCity').prop("disabled", false);
                            $("#inputCity").val("");
                        }
                    }
                })
            }

            function LimpiaCampos() {
                $('#inputState').removeClass("parsley-error");
                $('#inputCity').removeClass("parsley-error");
                $('#inputCP').removeClass("parsley-error");
            }

            $('#inputcountry').change(function () {
                LimpiaCampos();
                ActualizaEstados();
            });

            $('#inputState').change(function () {
                ActualizaCP();
            });
        });
    </script>
</body>
</html>
