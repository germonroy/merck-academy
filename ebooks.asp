<!--#include file="data/con_ma.inc" -->


<%

    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

         set Cursor = createobject("ADODB.Recordset")
  Cursor.CursorType =1 
  Cursor.LockType = 3


         set Cursor2 = createobject("ADODB.Recordset")
  Cursor2.CursorType =1 
  Cursor2.LockType = 3

    set Cursor3 = createobject("ADODB.Recordset")
  Cursor3.CursorType =1 
  Cursor3.LockType = 3


    SQL = "select *, (select max(fecha) from tbl_libros_revistas where activo='SI') as lastDate from tbl_libros_revistas where activo = 'SI' ORDER by orden"
    Cursor.Open SQL, StrConn, 1, 3
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
</head>


<body class="fixed-left">

    <!--#include file="loader.asp" -->
    <!-- Begin page -->
    <div id="wrapper">

        <!--#include file="top_int.asp" -->


        <!--#include file="sidebar.asp" -->


        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6">
                            <img src="images/escudos/escudo_ebooks.png" class="img-responsive" style="float: right; padding-bottom: 2em;" />
                        </div>
                        <div class="col-lg-6 col-lg-pull-6">
                            <div class="row">
                                <div class="portfolioFilter">
                                    <div class="row text-center" style="margin-top: 30px;">
                                        <div class="col-xs-12 col-md-6" style="padding-bottom: 2em;">
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <div class="btn-group">
                                                    <button type="button" style="padding-top: 0.8em; padding-bottom: 0.8em; margin-bottom: 0.3em;" class="btn btn-primary-cross dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><span class="icon-algos" style="margin-right: 5px;"></span><span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <%SQL3 = "select distinct anio from tbl_libros_revistas where activo = 'SI' order by anio"
                                                  Cursor3.Open SQL3, StrConn, 1, 3
                                                 Do While not Cursor3.EOF %>
                                                        <li><a href="#" data-filter=".mi<%=Cursor3("anio") %>" type="button"><%=Cursor3("anio") %></a></li>
                                                        <%Cursor3.MoveNext
                                                Loop
                                                '  Cursor.Close
                                                Cursor3.MoveFirst    
                                                        %>
                                                    </ul>
                                                </div>
                                                <br />
                                                ALGOS
                                            </div>
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".mg" type="button" class="btn btn-warning-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-tpm"></i></a>
                                                <br />
                                                TPM
                                            </div>

                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".orto" type="button" class="btn btn-danger-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-vitaminas"></i></a>
                                                <br />
                                                Vitaminas
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6" style="padding-bottom: 2em;">
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".new" type="button" class="btn btn-purple-cross btn-lg waves-effect waves-light m-b-5"><i class="fa fa-star"></i></a>
                                                <br />
                                                Nuevos
                                            </div>
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter="*" type="button" class="btn btn-inverse-cross btn-lg waves-effect waves-light m-b-5"><i class="fa fa-th-large"></i></a>
                                                <br />
                                                Todos
                                            </div>
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row port m-b-20">
                        <div class="portfolioContainer">
                            <% 
    txtLabes = ""
    txtBotones = ""
    visibility= ""
    A = 1
    counter = 0
    Do While not Cursor.EOF 
      If Instr(Cursor("id_publicacion"), "1") then
       txtLabes = txtLabes & " " & "mi" & Cursor("anio")
      txtBotones = txtBotones & "<button style='margin-left=10px;' class='btn btn-icon waves-effect waves-light btn-primary-cross btn-lg m-b-5'> <i class='icon-medicinaInterna'></i> </button> "
      end if

      If Instr(Cursor("id_publicacion"), "2") then
       txtLabes = txtLabes & " " & "mg"
        txtBotones = txtBotones & "<button style='margin-left=10px;' class='btn btn-icon waves-effect waves-light btn-warning-cross btn-lg m-b-5'> <i class='icon-medicinaGeneral'></i> </button> "
      end if

      If Instr(Cursor("id_publicacion"), "3") then
       txtLabes = txtLabes & " " & "orto"
        txtBotones = txtBotones & "<button style='margin-left=10px;' class='btn btn-icon waves-effect waves-light btn-danger-cross btn-lg m-b-5'> <i class='icon-ortopedia'></i> </button> "
      end if    
                    
      If Cursor("fecha") = Cursor("lastDate") then
       txtLabes = txtLabes & " " & "new"
      end if
                            %>
                            <div class="col-sm-6 col-lg-3 col-md-4 <%=txtLabes %>">
                                <div class="gal-detail thumb">
                                    <a href="javascript:void(0);" onclick="window.EbooksFrame.location='ebooks/<%=Cursor("id_libro") %>/?idf=<%=Cursor("id_libro") %>'; document.getElementById('tituloEbook').innerHTML = '<%=Cursor("titulo") %>';" disabled data-toggle="modal" data-target="#full-width-modalEbooks">
                                        <img src="images/videoteca/ebooks/<%=Cursor("id_libro") %>.png" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="work-thumbnail" />
                                        <h4><%=Cursor("titulo") %></h4>
                                    </a>
                                </div>
                            </div>

                            <% visibility=""
                                counter = counter + 1
                                If counter Mod 4 <> 0 then
                                visibility="hide"
                                end if
                            %>

                            <div class="col-sm-12 col-lg-12 col-md-12 <%=visibility %>">
                                <img src="images/linea.png" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="work-thumbnail" />
                            </div>



                            <%Cursor.MoveNext
    txtBotones = ""
    txtLabes = ""
  A = A + 1
Loop
  '  Cursor.Close
    Cursor.MoveFirst    
                            %>
                        </div>
                        <!-- end portfoliocontainer-->
                    </div>
                    <!-- End row -->

                    <div id="full-width-modalEbooks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-full">
                            <div class="modal-content">
                                <div class="modal-header hidden-xs">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.EbooksFrame.location='blank.htm';">×</button>
                                    <img style="width: 100%" src="images/header_revisiones.png" class="hidden-xs" />
                                </div>
                                <h4 class="modal-title hidden-xs" style="z-index: 3; color: #fff; position: absolute; top: 60px; right: 35px;" id="full-width-modalLabel"><span id="tituloEbook"></span></h4>
                                <iframe style="width: 100%; height: 70vh;" src="#" id="EbooksFrame" name="EbooksFrame" frameborder="0" allowfullscreen></iframe>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="window.EbooksFrame.location='blank.htm';">Cerrar</button>

                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
                <!-- container -->
            </div>
            <!--#include file="footer.asp" -->
        </div>
        <!-- content -->
    </div>
    <!-- END wrapper -->



    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- isotope filter plugin -->
    <script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Magnific popup -->
    <script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>
    <script type="text/javascript">
        $(window).load(function () {
            var $container = $('.portfolioContainer');
            $container.isotope({
                filter: '*',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            $('.portfolioFilter a').click(function () {
                $('.portfolioFilter .current').removeClass('current');
                $(this).addClass('current');

                var selector = $(this).attr('data-filter');
                $container.isotope({
                    filter: selector,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
                return false;
            });
        });
        $(document).ready(function () {
            $('.image-popup').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-fade',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                }
            });
        });

        /* ==============================================
        LOADER -->
        =============================================== */

        $(window).load(function () {
            $('#loader').delay(300).fadeOut('slow');
            $('#loader-container').delay(200).fadeOut('slow');
            $('body').delay(300).css({ 'overflow': 'visible' });
        })
    </script>

    <script type="text/javascript">
        function scrollIframe() {
            var element = document.getElementById("EbooksFrame");

            element.scrollIntoView();
        }
    </script>
</body>
</html>
