<!--#include file="includes/config.asp" -->
<!--#include file="data/con_ma2.inc" -->

<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

    set Cursor = createobject("ADODB.Recordset")
    Cursor.CursorType =1 
    Cursor.LockType = 3
        
    SQL = "SELECT porcentaje_vl FROM progresos where id_usuario = " & Session("id_usuario")
    Cursor.Open SQL, StrConn, 1, 3

    if Cursor.RecordCount > 0 then
        if Request("vs") <> 1 then
            Response.Redirect "certificados.asp?idcurso=" & Request("idcurso")
        end if
    end if

    color = ""
    if Request("idcurso") = 3 then
        color = "#feca0d"
        imagen= "mgeneral"
        btnclase = "btn-warning"
        iconclase = "icon-medicinaGeneral"
    elseif Request("idcurso") = 2 then
        color = "#0066b2"
        imagen= "minterna"
        btnclase = "btn-primary"
        iconclase = "icon-medicinaInterna"
    elseif Request("idcurso") = 4 then
        color = "#ec1a3b"
        imagen= "ortopedia"
        btnclase = "btn-danger"
        iconclase = "icon-ortopedia"
    else
        color = "#009a5a"
        imagen= "odontologia"
        btnclase = "btn-success"
        iconclase = "icon-odontologia"
    end if
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />
    <style>
        h2 {
            color: <%=color %>;
            text-align: center;
        }

        b, h4 {
            color: <%=color %>;
        }
    </style>
    <link href="css/reglas.css" rel="stylesheet" />
    <%if Request("vw") = 1 then %>
    <style>
        .content-page .content {
            padding: 0px 5px;
            margin-top: 0px !important;
        }

        .content-page {
            margin-left: 0px;
            overflow: hidden;
        }

        #mainContainer {
            padding: 0px 0px 0px !important;
        }

        #wrapper.enlarged .content-page {
            margin-left: 0px !important;
        }

        html {
            background: transparent !important;
        }
    </style>
    <%end if %>
</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <%if Request("vw") <> 1 then
            miclase= "style='margin-top: 5em;'"
            clasrewr = "margin-left: 70px;"
        %>
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->

        <%end if %>
        <div class="content-page">
            <div class="container containerAcercaDe" <%=miclase %>>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" align="center">
                        <div class="row">
                            <div class="col-md-12">
                                <img src="images/escudos/<%=imagen %>.png" class="img-responsive" style="margin-top: 40px; margin-bottom: 25px; width: 30%;" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="bg">
                        <p id="texto-reglas" style="text-align: justify">
                            Antes de iniciar es necesario, <b>lea y comprenda</b> la dinámica del progreso.
                            <h4 style="text-align: justify">Sobre el contenido del curso</h4>
                            <ul style="text-align: justify">
                                <li>Para obtener el 100% de progreso de cada módulo, es necesario que consulte todas y cada una de las video-lecciones, cápsulas, revisiones del mes, ejercicios de reforzamiento y/o video-cirugías que contiene el módulo, material que puede ser visualizado o resuelto las veces que considere necesario.</li>
                                <li>Puede revisar su avance dando clic en el botón "Progreso".</li>
                                <li>Para identificar cuáles son los temas o actividades que no ha consultado, utilice el botón "Lecciones pendientes".</li>
                                <li>Al obtener el 100% de cada módulo, se le otorgará un diploma de Merck Academy con valor curricular (para el proceso detallado, continuar leyendo).</li>
                            </ul>
                            <h4 style="text-align: justify">Para obtener certificación</h4>
                            <ul style="text-align: justify">
                                <li>Es necesario que consulte el 100% del contenido de cada módulo (las veces que considere necesario antes de iniciar la evaluación).</li>
                                <li>Al obtener el 100% de la evaluación, tendrá el derecho a certificarse.</li>
                                <li>La opción de certificación solo se habilitará por módulo al 100%.</li>
                                <li>Una vez iniciado el proceso de evaluación el contenido no podrá ser consultado.</li>
                                <li>El curso podrá consultarse nuevamente al término del proceso (independientemente de su resultado).</li>
                                <li>El proceso de certificación consiste en:
                                    <ul style="text-align: justify">
                                        <li>Resolver la evaluación que consta de al menos 50 preguntas de opción múltiple (las instituciones que lo avalan definen la cantidad de preguntas).</li>
                                        <li>Tiene como límite 30 días naturales a partir de que decide iniciar el proceso de evaluación para terminarlo.</li>
                                        <li>Para acreditar y obtener su constancia con valor curricular, debe obtener 80% de respuestas correctas (el porcentaje puede variar por módulo, dependiendo de la institución que lo avala).</li>
                                        <li>Tiene dos oportunidades para aprobar la evaluación, sino deberá repetir el Módulo.</li>
                                        <li>Al obtener el porcentaje requerido le será posible descargar e imprimir su constancia con valor curricular.</li>
                                        <li>El proceso de certificación puede ser completado en varias sesiones durante el periodo de los 7 días naturales.</li>
                                    </ul>
                                </li>
                                <li>Para información de las instituciones que avalan los cursos, consulte la sección <b>"Avales".</b></li>
                            </ul>
                        </p>
                        <%if Request("vw") <> 1 then %>
                        <div align="center" id="boton-aceptar">
                            <a href="certificados.asp?idcurso=<%=Request("idcurso") %>">
                                <button type="button" class="btn <%=btnclase %> waves-effect waves-light w-lg m-b-5"><i class="<%=iconclase %>"></i><span>&nbsp;ACEPTAR E INICIAR</span> </button>
                            </a>
                        </div>
                        <%else %>
                        <div style="height:80px;"></div>
                        <%end if %>
                    </div>
                </div>
            </div>
            <%if Request("vw") <> 1 then %>
            <!--#include file="footer.asp" -->
            <%end if %>
        </div>
    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>
    <script src="js/MetroJs.js" type="text/javascript"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="js/logout.js"></script>
</body>
</html>
