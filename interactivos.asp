<!--#include file="data/con_ma.inc" -->
<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

         set Cursor = createobject("ADODB.Recordset")
    Cursor.CursorType =1 
  Cursor.LockType = 3


         set Cursor2 = createobject("ADODB.Recordset")
  Cursor2.CursorType =1 
  Cursor2.LockType = 3


    SQL = "select (select max(id_dragable) from tbl_dragables) as lastOne, * from tbl_dragables where activo = 'SI' order by id_dragable"
    Cursor.Open SQL, StrConn, 1, 3
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/flipper-journals.css" rel="stylesheet">
    <style>
        .flipper {
            margin-bottom: 10px;
        }
    </style>
</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 hidden-lg">
                            <div class="col-lg-4 col-md-3" style="float: right;">
                                <img src="images/escudos/escudo_interactivos.png" class="img-responsive" style="height: auto; margin-bottom: 30px; width: 150%" />
                            </div>
                        </div>
                        <div class="col-lg-4" style="float: left;">
                        </div>
                        <div class="col-lg-8 hidden-md hidden-sm hidden-xs">
                            <div class="col-lg-4 col-md-3" style="float: right;">
                                <img src="images/escudos/escudo_interactivos.png" class="img-responsive" style="height: auto; margin-bottom: 30px; width: 150%" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="margin-bottom: 5px; padding-bottom: 5px;">
                            <h4 style="color: #243e9f; margin-bottom: 5px; padding-bottom: 5px;"><strong><i class="fa fa-info-circle"></i>&nbsp;La ciencia se encuentra con la tecnología en esta sección, donde lo último en los descubrimientos del dolor se combina con la más avanzada tecnología.</strong></h4>
                        </div>
                    </div>
                    <div class="row">
                        <% 
        Do While not Cursor.EOF
                        %>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div id="touch<%=Cursor("id_dragable") %>" class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <div class="flipper">
                                    <div class="front">
                                        <img src="images/videoteca/interactivos/<%=Cursor("id_dragable") %>.jpg" class="img-responsive center-block" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor("titulo") %>" />
                                    </div>
                                    <div class="back">
                                        <img src="images/videoteca/interactivos/vuelta_<%=Cursor("id_dragable") %>.jpg" class="img-responsive center-block" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor("titulo") %>" />
                                        <input type="hidden" name="idi" value="<%=Cursor("id_dragable") %>" />
                                        <a href="javascript:void(0);" class="btnInter" onclick="window.contenidoFrame.location='dragables/<%=Cursor("id_dragable") %>/?iddrag=<%=Cursor("id_dragable") %>'; document.getElementById('tituloInteractivo').innerHTML = '<%=Cursor("titulo") %>';" disabled data-toggle="modal" data-target="#full-width-modalInteractivos">
                                            <button type="button" class="btn btn-primary center-block text-center" style="bottom: 5px; position: absolute; left: 39%; z-index: 3;">Ver</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%
                        Cursor.MoveNext
        Loop
        Cursor.MoveFirst    
                        %>
                    </div>
                    <div id="full-width-modalInteractivos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-full">
                            <div class="modal-content">
                                <div class="modal-header hidden-xs">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.contenidoFrame.location='blank.htm';">×</button>
                                    <img style="width: 100%" src="images/header_revisiones.png" class="hidden-xs" />
                                </div>
                                <h4 class="modal-title hidden-xs" style="z-index: 3; color: #fff; position: absolute; top: 60px; right: 35px;" id="full-width-modalLabel"><span id="tituloInteractivo"></span></h4>
                                <iframe style="width: 100%; height: 65vh;" src="#" id="contenidoFrame" name="contenidoFrame" frameborder="0" allowfullscreen></iframe>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="window.contenidoFrame.location='blank.htm';">Cerrar</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--#include file="footer.asp" -->
        </div>
    </div>

    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- isotope filter plugin -->
    <script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Magnific popup -->
    <script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/portfolio-filters.js"></script>
    <script src="js/logout.js"></script>
    <script src="js/loader.js"></script>
    <script>
        var touch = 'ontouchstart' in document.documentElement
            || (navigator.MaxTouchPoints > 0)
            || (navigator.msMaxTouchPoints > 0);

            if (touch) { // remove all :hover stylesheets
                try { // prevent exception on browsers not supporting DOM styleSheets properly
                    for (var si in document.styleSheets) {
                        var styleSheet = document.styleSheets[si];
                        if (!styleSheet.rules) continue;

                        for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
                            if (!styleSheet.rules[ri].selectorText) continue;

                            if (styleSheet.rules[ri].selectorText.match(':hover')) {
                                styleSheet.deleteRule(ri);
                            }
                        }
                    }
                } catch (ex) {}
            }

        <%
            for i = 1 to Cursor("lastOne")
                idtouch = "touch" & i
        %>
            $("#<%=idtouch %>").click(function(){

                if ($("#<%=idtouch %>").hasClass("flip-touch")) {
                    $("#<%=idtouch %>").removeClass("flip-touch"); 
                }
                else {              
                   $(".flip-touch").removeClass("flip-touch"); 
                   $("#<%=idtouch %>").delay(200).addClass("flip-touch");
                }
            });
        <%
            next
        %>
    </script>
    <script type="text/javascript">
        function scrollIframe() {
            var element = document.getElementById("contenidoFrame");

            element.scrollIntoView();
        }

        $(document.body).on('click', '.btnInter', function (e) {
            var idi = $(this).parent().find('input:hidden[name=idi]').val();
            $.ajax({
                url: "jq/jq_savetrack_generico_dragables.asp",
                type: "POST",
                data: { "idi": idi },
                async: false,
                success: function (cont) {
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            })
        });
    </script>

</body>
</html>
