<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-60818105-1', 'auto');
    ga('send', 'pageview');
    ga('set', 'forceSSL', true); 
</script>
<script type="text/javascript"> 
 <!-- SessionCam Client Integration v6.0 -->
 //<![CDATA[ 
 var scRec=document.createElement('SCRIPT'); 
 scRec.type='text/javascript'; 
 scRec.src="//d2oh4tlt9mrke9.cloudfront.net/Record/js/sessioncam.recorder.js";
 document.getElementsByTagName('head')[0].appendChild(scRec); 
 //]]>
 <!-- End SessionCam -->
</script>
<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0064/8688.js" async="async"></script>
