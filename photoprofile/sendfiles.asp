<%
Session.LCID = 1034

 %>

<!--#include file="../includes/config.asp" -->
<!--#include file="../data/con_ma2.inc" -->
<!--#include file="../generadores/md5.asp" -->
<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
%>


<%
'Option explicit
'Stores only files with size less than MaxFileSize


Const maxFileSize = 90000000000 'limit of size per file
Const imageExts = ".gif,.jpg,.png, jpge"

Dim DestinationPath
'DestinationPath = Server.mapPath("..\nmanagerpro\database")
'DestinationPath = Server.mapPath("uploadfolder")
    DestinationPath = Server.mapPath("/uploadfolder")


Dim Form: Set Form = New ASPForm %><!--#INCLUDE FILE="_upload.asp"--><% 

Server.ScriptTimeout = 20000
Form.SizeLimit = 10*1000000 'limit of size per whole form

'{b}Set the upload ID for this form.
'Progress bar window will receive the same ID.
if len(Request.QueryString("UploadID"))>0 then
  Form.UploadID = Request.QueryString("UploadID")'{/b}
end if
'was the Form successfully received?
Const fsCompletted  = 0

If Form.State = fsCompletted Then 'Completted
  Dim CustomerID
  CustomerID = Form("CustomerID")

  'Do something with upload - save, enumerate, ...
  response.write "<br><b>Upload result: Form was accepted.</b>" 
  response.write "<br>Number of file fields:" & Form.Files.Count
  response.write "<br>Request total bytes:" & Request.TotalBytes

  'PRocess files and create HTML report
  Dim OutHTML:  OutHTML = do_Files (Form)

  'Send the report by email
  SendReport "to@yourdomain.com", OutHTML

  'Write the report to a client
  response.write OutHTML

ElseIf Form.State > 10 then
  Const fsSizeLimit = &HD
  Select case Form.State
    case fsSizeLimit: response.write  "<br><Font Color=red>Source form size (" & Form.TotalBytes & "B) exceeds form limit (" & Form.SizeLimit & "B)</Font><br>"
    case else response.write "<br><Font Color=red>Some form error.</Font><br>"
  end Select
End If'Form.State = 0 then




Function do_Files (Form)
  Dim HTML


  '1. Process main upload fields - CustomerID, Description
  Dim UploadID, Uploads, CustomerID
  CustomerID = Form("CustomerID")
  if len(CustomerID)=0 then CustomerID = -1

  'DB contains two tables:
  ' - Uploads with UploadID (primary key), Description, and CustomerID
  ' - UploadsFiles with UploadID (foreign key), Description, DestFileName, DataSize and SourceFileName
  'Open table with list of uploads
  
'  Set Uploads = OpenUploadRS("Uploads")
 ' Uploads.AddNew
 '   Uploads("Description") = Form("Description")
 '   Uploads("CustomerID") = CustomerID
 ' Uploads.Update
 ' UploadID = Uploads("UploadID")

  HTML = HTML & "<br>UploadID:" & UploadID
  HTML = HTML & "<br>CustomerID:" & Form("CustomerID")

  '2. Process form files
  Dim File
  For Each File In Form.Files.Items
    If Len(File.FileName) > 0 Then
      
      'Open recordset to store uploaded files
      'Dim UploadsFiles: Set UploadsFiles = OpenUploadRS("UploadsFiles")

      HTML = HTML &  "<br>File:" & File.FileName & ", size :" & (File.Length \ 1024 +1) & "kB"
      HTML = HTML &  ", Is image:" & IsImage(File)
      if File.Length = 0 then
        HTML = HTML &  "<Font Color=red> exceeds the size limit  (" & maxFileSize & ").</font>" 
      elseif not IsImage(File) Then 
        HTML = HTML &  "<Font Color=red> is not an image type (" & imageExts & ").</font>" 
      else
        Dim DestFileName


  set Cursor = createobject("ADODB.Recordset")
  Cursor.CursorType =1 
  Cursor.LockType = 3  
  
  SQL = "SELECT id_usuario, photo FROM tbl_usuarios WHERE hash = '" & Form("hu") & "'"
  Cursor.Open SQL, StrConn, 1, 3

  StrH = Session("idu") & "_" & File.FileName & "_" & now()
  NombreArchivoF = md5(StrH)

        'hu = Form("hu") & right(File.FileName,4)
        
        hu = NombreArchivoF & right(File.FileName,4)
        'DestFileName = GetUniqueFileName(File.FileName, DestinationPath)
        'DestFileName = GetUniqueFileName(hu, DestinationPath)
        DestFileName = hu
        File.SaveAs DestinationPath & "\" & Replace(DestFileName,"jpeg",".jpg")

        'Store extra info about upload to database
        'UploadsFiles.AddNew
'         UploadsFiles("UploadID") = UploadID
         'UploadsFiles("SourceFileName") = left(File.FilePath,255)
         'UploadsFiles("DestFileName") = left(DestFileName, 255)
         'UploadsFiles("DataSize") = File.Length
         '...


  Cursor("photo") = Replace(hu,"jpeg",".jpg")
  'Session("urlphoto") = "https://labs.ma.webtube.com.mx/photoprofile/uploadfolder/" & Replace(hu,"jpeg",".jpg")
  Session("urlimagen") = Cursor("photo")
  Cursor.Update
  Cursor.Close
  StrConn.Close
  Set Cursor = Nothing
  Set StrConn = nothing

        
        
 	     Nom =left(DestFileName, 255) 	
      end if
    end if'if len(File.FileName)=0 then

  Next
  
'  Form.Files.Save DestinationPath 
  HTML = HTML &  "<br>Files was saved to " & DestinationPath & " folder."
  do_Files = HTML
    If Form("lang") = "" then
     Response.Redirect "../mi-cuenta.asp"
    else
      If Form("lang") = "en" then
       Response.Redirect "../mi-cuenta.asp"
      end if
    end if
End Function







'This function checks filename and CONTENTS of a field
'to recognize images
Function IsImage(Field)
  IsImage = True 'I'm sorry, PureASP upload does not have HexString property.
  Exit Function
  if instr(1, imageExts & ",", Field.FileExt & ",", 1)>0 _
    or Left(Field.ContentType, 5) = "image" Then 
    
    ' FFD8FF = JFIF
    ' 49492A00 = TIF
    if Field.HexString (0,3)="FFD8FF" or Field.HexString (0,4)="49492A00" _
    or Field.String(,6,4)="JFIF" or Field.String(,0,3)="GIF" _
    or Field.String(,1,3)="PNG"  or Field.String(,0,2)="BM" then
      IsImage = True
    end if
  end if
End Function





Dim gFS
'creates an unique filename
'in filename.ext, filename-1.ext, filename-2.ext, filename-3.ext, ... schema
Function GetUniqueFileName(FileName, DestPath)
  if isempty(gFS) then Set gFS = CreateObject("Scripting.FileSystemObject")
  Dim DotPos: DotPos = InStrRev(FileName,".")
  if DotPos = 0 then DotPos = len(FileName)+1
  Dim Counter, FullPath, NewFileName
  Counter = 1
  NewFileName = FileName
  if gFS.FileExists(DestPath & "\" & NewFileName) then
    Do
      Counter = Counter + 1
      NewFileName = Left(FileName, DotPos-1) & "-" & Counter _
        & Mid(FileName, DotPos)
    Loop while gFS.FileExists(DestPath & "\" & NewFileName)
  end if
  GetUniqueFileName = NewFileName
End Function


%>  



<SCRIPT>
//Open window with progress bar.
//pair upload window and progress window (using UploadID).
function ProgressBar(form){
 //check file sizes.
 
 if (checkFileSize()) {
   alert('Upload size is over limit. Please check selected files.')
   return false;
 };

  
 //ASP script handling progress window
 var ProgressScript
 ProgressScript = 'progress.asp'
 

 //Progress window parameters
 var pp = 'toolbar=no,location=no,directories=no,status=no,menubar=no'
 pp+=',scrollbars=no,resizable=yes,width=350,height=200';
  
 //1. Get unique UploadID
 var UploadID
 UploadID = Math.round(Math.random() * 0x7FFFFFF0)
  
 //2. Add upload ID to form action URL
 var action = form.action;
 if ('' == action) action = ''+document.location;
 action = AddToQuery(action, 'UploadID', UploadID); 
 form.action = action


 

 //3. Open progress window with the same UploadID
 var ProgressURL
 ProgressURL = ProgressScript + '?UploadID=' + UploadID 

 var v = window.open(ProgressURL,'_blank',pp)
 
 return true;
};

//Adds value and its name to querystring
function AddToQuery(q, valname, val){
 if (q.indexOf('?')<0) {
   q += '?'
 } else {
 var pv = q.indexOf(valname+'=');
 if (pv >= 0){
  var amp = q.indexOf('&', pv);
  if (amp<0) {
   q = q.substr(0, pv) 
  } else {
   q = q.substr(0, pv) + q.substr(amp+1) + '&'
  }
 } else {
  if (q.substr(q.length-1)!='?') q += '&'
 };
 };
 q += valname + '=' + val
 return q
};
</SCRIPT> 


<Script>
//Additional function - dynamic form to add new files at a client side.

var nfiles = 3;
//Add two files for upload
//Expand();
function Expand(){
  
  //get an HTML code of a first upload element
  var adh = dfile1.outerHTML;

  //replace '1' to nfiles (2, 3, ...)
  adh = adh.replace(/1/g,++nfiles)
  
  //insert the code of a new element before end of div files
  files.insertAdjacentHTML('BeforeEnd',adh);

  //clear mask and real value of the element nfiles
  //document.getElementById('maskfile'+nfiles).value=''
  return false;
};

</Script>


<Script>
//Huge-asp upload preview sample
//http://www.motobit.com

function isImage(file){
  //Get a file extension
  var ext = file.substr(file.lastIndexOf('.')).toLowerCase()

  //Check extension to image types.
  
  return '<%=imageExts%>,'.indexOf(ext+',') >= 0
};

var lastfieldname = ''
var filenamechecked = ''

function preview(n) {
  //get current input preview
  var htmlfile = document.getElementById('Image '+n);
  var file = htmlfile.value

  //set the size field.
  var himg = document.getElementById('himg'+n);
  if (file.length>0) himg.src = 'file://' + file;
  else { 
    himg.src = ''; 
    document.getElementById('size'+n).innerHTML=''
  };
  
  if (file.length<=0) return;

  //or get get preview for one of form field
  //var file = file_upload.SourceFile1.value
//  var ipreview = document.getElementById('ipreview');
  
  //do not check the file more than one.
  if (filenamechecked != htmlfile.value) {
    filenamechecked = htmlfile.value
  } else {
    return true;
  };

  if (isImage(file)) {

    //Show preview for the image.
  //  ipreview.src = 'file://' + file
    //alert(file);
    //alert(ipreview.src);
    //ipreview.title = 'Image ' + file 
    //if (ipreview.width != 300) ipreview.width = 300;

    //ImageName.innerHTML = 'Image preview<br>(' + htmlfile.name + ')'
  } else {
    //some default image for preview
    //ipreview.src = 'res://shdoclc.dll/warning.gif'

    alert('Solo se permiten archivos: (<%=imageExts%>)');
    window.close();
  };
  
  lastfieldname = htmlfile.name
}

function himgLoaded(n){
  checkFileSize();
};


//this function gets a sizes of images,
//write the sizes to HTML form
//counts total size and checks file sizes against a limit
var maxFileSize = <%=maxFileSize%>
var FormSizeLimit = <%=Form.SizeLimit%>
function checkFileSize() {
  var totalSize = 0;
  var htmlSize;
  var overLimit = false;
  for (j = 1; j <= nfiles; j++) {
    var himg = document.getElementById('himg'+j);
    var size = document.getElementById('size'+j);
    var fileSize = himg.fileSize ;

    fileSize = parseInt(fileSize);
    
    if (fileSize < 0) {
      size.innerHTML = '';
    } else {
      htmlSize = formatSize(fileSize);
      if ( fileSize>maxFileSize ) {
        htmlSize += ' (over limit, ' + formatSize(maxFileSize) + ' max)'
        size.style.color = 'red';
        overLimit = true;
      } else {
        size.style.color = '';
      };
      size.innerHTML = htmlSize;
      totalSize += fileSize;
    };//if (fileSize < 0) {
  };//for (j = 1; j <= nfiles; j++)


  var htotalSize = document.getElementById('totalSize');
  
  if (totalSize > 0){
    htmlSize = formatSize(totalSize);
  } else {
    htmlSize = '';
  };
  if (totalSize > FormSizeLimit) {
    htotalSize.style.color = 'red';
    htmlSize += ' (over limit, ' + formatSize(FormSizeLimit) + ' max)'
    overLimit = true;
  } else {
    htmlSize += ' (of ' + formatSize(FormSizeLimit) + ' max)'
  };
  htotalSize.innerHTML = htmlSize
  return overLimit;
};

function formatSize(size) {
  if (size < 0x100000) {// < 1 MB
    return Math.round(size / 0x400)+"&nbsp;kB"; 
  } else { // > 1 MB
    return (Math.round((size / 0x100000)*10)/10)+"&nbsp;MB";
  }
}


//window.onerror = donotmsgboxes;
function donotmsgboxes(msg,url,line)
{  // we do not need error messages
  return true
}

</Script>
