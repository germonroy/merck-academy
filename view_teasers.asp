﻿<!--#include file="data/con_ma.inc" -->
<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

SQL = "select source FROM tbl_carrusel_home WHERE id_carrusel = " & Request("idv") & ""
Cursor.Open SQL, StrConn, 1, 3
%>
<!DOCTYPE html>
<html>
<head>
    <!-- For IE10 -->
    <meta http-equiv="X-UA-Compatible" content="requiresActiveX=true" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="title" content="22_CD" />
    <meta name="description" content="Online video presentation created with KnowledgeVision. Find out more at KnowledgeVision.com." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>22_CD</title>

    <style type="text/css">
        /* http://meyerweb.com/eric/tools/css/reset/ */
        /* v1.0 | 20080212 */

        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, font, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: baseline;
            /*background: transparent;*/
        }

        body {
            line-height: 1;
        }

        /* remember to define focus styles! */
        :focus {
            outline: 0;
        }

        .center {
            text-align: center;
        }

        body {
            background:;
        }

        /* HTML5 Player styles */
        html, body, div {
            width: 100%;
            height: 100%;
            min-height: 100%;
        }

        * {
            line-height: 0;
        }

        .KnowledgeVisionEmbeddedContent {
            margin: 0 auto;
        }

        #contenido-modal-teaser {
            margin-top: 0px;
        }

        .combo-player-slides {
            margin-top: -7vh !important;
        }

        .combo-player-video {
            margin-top: -7vh !important;
        }
    </style>

    <script src="js/jquery-1.12.4.min.js"></script>

</head>

<body>

    <div id="contenido-modal-teaser" class="center">
        <%=Cursor("source") %>
    </div>

    <script language="JavaScript" type="text/javascript">
        var myFunction = function (eventObject) {
            // console.log(eventObject);
            TrackClose();
        };
        window.knowledgevisionLoader.addPlayerTrackListener("PlaybackStarted", playVid);
        window.knowledgevisionLoader.addPlayerTrackListener("PlaybackPaused", pauseVid);

        function playVid() {
            anim = "bounceOutRight";
            $(window.parent.document).find('#miniRegistro').removeClass('bounceInRight');
            $(window.parent.document).find('#miniRegistro').removeClass('animate');
            $(window.parent.document).find('#miniRegistro').removeClass('animated');
            enterCur(anim);
        }

        function pauseVid() {
            anim = "bounceInRight";
            $(window.parent.document).find('#miniRegistro').removeClass('bounceOutRight');
            $(window.parent.document).find('#miniRegistro').removeClass('animate');
            saleCur(anim);
        }

        function enterCur(x) {
            $(window.parent.document).find('#miniRegistro').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            });
        };

        function saleCur(x) {
            $(window.parent.document).find('#miniRegistro').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            });
        };
                
    </script>

</body>
</html>


<!-- Checar sources  y quitar queris de las paginas -->
