<%Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID = 1033
%>
<!--#include file="data/con_ma3.inc" -->
<!--#include file="data/con_ma.inc" -->

<%
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3
set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

SQL = "select * from tbl_cursos where id_curso = " & Request("idcurso")
Cursor.Open SQL, StrConn, 1, 3
If Cursor.RecordCount > 0 then
ActiveCons = Cursor("constancia")
End If
Cursor.Close

    if Request("idcurso") = 2 then
        SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & Request("idcurso") & " or id_curso2 = " & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ") ORDER by secuencia2"
    elseif Request("idcurso") = 4 then
        SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & Request("idcurso") & " or id_curso2 = " & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ") ORDER by secuencia3"
    else
        SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & Request("idcurso") & " or id_curso2 = " & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ") ORDER by secuencia"
    end if
    
    Cursor.Open SQL, StrConn, 1, 3

    color = ""
    if Request("idcurso") = 3 then
        colorTop = "#feca0d"
        color = "#0066b2"
        imagen= "mgeneral"
        btnclase = "btn-primary"
        iconclase = "icon-medicinaGeneral"
        claseGeneral = "primary"
    elseif Request("idcurso") = 2 then
        colorTop = "#0066b2"
        color = "#0066b2"
        imagen= "minterna"
        btnclase = "btn-primary"
        iconclase = "icon-medicinaInterna"
        claseGeneral = "primary"
    elseif Request("idcurso") = 4 then
        colorTop = "#ec1a3b"
        color = "#0066b2"
        imagen= "ortopedia"
        btnclase = "btn-primary"
        iconclase = "icon-ortopedia"
        claseGeneral = "primary"
    else
        colorTop = "#009a5a"
        color = "#0066b2"
        imagen= "odontologia"
        btnclase = "btn-primary"
        iconclase = "icon-odontologia"
        claseGeneral = "primary"
    end if

    set Cursor2 = createobject("ADODB.Recordset")
    Cursor2.CursorType =1 
    Cursor2.LockType = 3

    SQL2 = "SELECT * FROM tbl_modulos WHERE id_modulo in (1,2,3) order by id_modulo"
    Cursor2.Open SQL2, StrConn, 1, 3

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="es">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="icono-lec/style.css" rel="stylesheet" />
    <style>
        .certificate-top {
            background-color: <%=colorTop %>;
        }

        .certificate-row {
            background-color: <%=color %>;
        }

        .btn-objetivos {
            background-color: #797979 !important;
            border-color: #797979 !important;
        }

            .btn-objetivos:hover {
                background-color: #5a5555 !important;
                border-color: #5a5555 !important;
            }

            .btn-objetivos:focus {
                background-color: #5a5555 !important;
                border-color: #5a5555 !important;
            }
    </style>
    <link href="css/certificados.css" rel="stylesheet" />
    <style>
        hr {
            border-top: 2px solid #3266b1;
        }

        .strong-look {
            font-weight: bold !important;
            font-size: 32px !important;
            color: #1a6cb0;
        }

        .cintillo {
            background-color: rgba(255,255,255, 0.3) !important;
        }
    </style>
</head>

<body class="fixed-left">

    <!--#include file="loader.asp" -->

    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->

        <div class="content-page">

            <div class="content">
                <div class="container">
                    <div class="row text-center card-box">
                        <div class="col-lg-4 vertical-center" style="padding-top: 1.6em;">
                            <img src="images/escudos/escudo_<%=imagen %>.png" class="img-responsive center-block" style="vertical-align: middle;" />
                        </div>

                        <div class="col-lg-8" style="margin-bottom: 1em;">
                            <h2 style="color: #9a9896; font-weight: bold; text-align: left;">Progreso </h2>
                            <div class="progress" id="statusGeneral">
                                <% set CursorPorGral = createobject("ADODB.Recordset")
                                      CursorPorGral.CursorType =1 
                                      CursorPorGral.LockType = 3
                                     SQLPorGeneral =  "select [ma_15].[dbo].[tbl_usuarios].id_usuario, (select COUNT(*) from  [demo_merck_17].[dbo].tbl_modulos  where (id_curso = " & Request("idcurso") & " or id_curso2 = " & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ") and activo = 'SI') as num_modulos, (select coalesce(SUM([ma_15].[dbo].[progresos].porcentaje_vl),0) as sumatotal from [ma_15].[dbo].[progresos] where id_usuario = " & Session("id_usuario") & " and id_modulo  in (select distinct id_modulo from [demo_merck_17].[dbo].[tbl_modulos] where (id_curso = " & Request("idcurso") & " or id_curso2 = " & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ") and activo = 'SI') ) as total_curso from  [ma_15].[dbo].[tbl_usuarios] where id_usuario = " & Session("id_usuario") & " "
                                    'Response.Write(SQLPorGeneral)
                                    CursorPorGral.Open SQLPorGeneral, StrConn2, 1, 3 

                                    if CursorPorGral.RecordCount > 0 then

                                    porcentaje = cint((CursorPorGral("total_curso")/CursorPorGral("num_modulos")))

                                    if porcentaje > 100 then
                                        porcentaje = 100
                                    end if

                                    Do While not CursorPorGral.EOF

                                    
                                %>



                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                    aria-valuenow="<%=porcentaje %>" aria-valuemin="0" aria-valuemax="100" style="width: <%=porcentaje %>%; padding-top: 0.3em;">
                                    <%=porcentaje %>%
                                </div>

                                <%
                                    CursorPorGral.MoveNext
                                    Loop

                                    else
                                %>

                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; padding-top: 0.3em;">
                                </div>

                                <%end if %>
                                <%CursorPorGral.Close %>

                                <%SQL= "select SUM(calificacion) as suma from log_evaluacion_eval  a where  id_evaluacion in (select top 1 id_evaluacion from log_evaluacion_eval b where a.id_evaluacion = b.id_evaluacion  order by intento )and id_modulo in(select id_modulo from [demo_merck_17].[dbo].tbl_modulos c where (c.id_curso = " & Request("idcurso") & " or id_curso2 =" & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ")) and a.id_usuario = " &  Session("id_usuario")  &" " %>
                                <%   CursorPorGral.Open SQL, StrConn2, 1, 3  %>
                                <%Promedio =  CursorPorGral("suma") %>
                                <%CursorPorGral.Close %>


                                <%SQL= "select SUM(calificacion) as suma from log_evaluacion_eval  a where  id_evaluacion in (select top 1 id_evaluacion from log_evaluacion_eval b where a.id_evaluacion = b.id_evaluacion  order by intento )and id_modulo in(select id_modulo from [demo_merck_17].[dbo].tbl_modulos c where (c.id_curso = " & Request("idcurso") & " or id_curso2 =" & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ")) and a.id_usuario = " &  Session("id_usuario")  &" " %>
                                <%'Response.Write(SQL)   
                                CursorPorGral.Open SQL, StrConn2, 1, 3  %>
                                <%If isNull(CursorPorGral("suma")) then
                                    Promedio = 0
                                    Else
                                    Promedio =  CursorPorGral("suma")
                                    End If    
                                %>
                                <%CursorPorGral.Close %>



                                <% SQL= "select * from tbl_modulos c where evaluacion_activa = 'SI' and (c.id_curso = " & Request("idcurso") & " or id_curso2 =" & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ") and c.activo ='SI'" %>
                                <%   CursorPorGral.Open SQL, StrConn, 1, 3  %>
                                <%TotalEvalActivos =  CursorPorGral.RecordCount %>
                                <%CursorPorGral.Close %>
                            </div>


                            <!-- <button type="button" class="btn btn-<%=claseGeneral %> btn-trans waves-effect w-xs waves-info m-b-5" onclick="location.href='reglas.asp?idcurso=<%=Request("idcurso") %>&vs=1';">Regulations</button>-->
                            <a href="/reglas.asp?idcurso=<%=Request("idcurso") %>&vs=1" class="btn btn-<%=claseGeneral %> btn-trans waves-effect w-xs waves-info m-b-5"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;Reglamento</a>
                            <%If ActiveCons = "SI" then %>
                            <%If porcentaje >= 100 then %>
                            <%If cint(Promedio) > 79 then  %>
                            <div id="divAdvice" style="display: inline-table;" data-toggle='popover' title='¡Aviso!' data-content='Usted aprob&oacute; el curso satisfactoriamente. ¡Ya puede descargar su constancia!'>
                                <button type="button" class="btn btn-success btn-trans waves-effect w-xs waves-info m-b-5" onclick="window.open('makepdf/?tlpu=<%=Session("id_user") %>&hu=<%=Session("hu") %>&idc=<%=Request("idcurso") %>');"><i class="fa fa-certificate" aria-hidden="true"></i>&nbsp;Constancia</button>
                            </div>
                            <%Else %>
                            <div id="divAdvice" style="display: inline-table;" data-toggle='popover' title='¡Aviso!' data-content='Usted a&uacute;n no tiene el porcentaje ideal para generar su constancia, el m&iacute;nimo requerido es 80%. Su porcentaje actual es: <%=cint(Promedio) %>%'>
                                <button type="button" class="btn btn-success btn-trans waves-effect w-xs waves-info m-b-5" onclick="window.open('makepdf/?tlpu=<%=Session("id_user") %>&hu=<%=Session("hu") %>&idc=<%=Request("idcurso") %>');"><i class="fa fa-certificate" aria-hidden="true"></i>&nbsp;Constancia</button>
                            </div>
                            <%End IF %>
                            <%Else   %>
                            <div id="divAdvice" style="display: inline-table;" data-toggle='popover' title='¡Aviso!' data-content='Usted tiene que ver todas las videolecciones y obtener m&aacute;s del 80% como promedio general  para poder generar su constancia'>
                                <button type="button" class="btn btn-success btn-trans waves-effect w-xs waves-info m-b-5" disabled><i class="fa fa-certificate" aria-hidden="true"></i>&nbsp;Constancia</button>
                            </div>
                            <%End If  %>
                            <%End If %>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 cintillo">
                            <h2 class="strong-look"><i class="icon-cursoscertificacion"></i><span>&nbsp;INTRODUCCIÓN TRONCO COMÚN </span></h2>
                            <hr />
                        </div>
                    </div>

                    <div class="row port m-b-20">
                        <div class="portfolioContainer">
                            <% 
                            Do While not Cursor2.EOF   
                            %>

                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="curso-<%=Cursor("id_modulo") %>">
                                <div class="card-box" style="min-height: 447px">
                                    <div class="media m-b-10">
                                        <div class="gal-detail thumb">
                                            <a href="view_cont_cert.asp?tlpmod=<%=Cursor2("id_modulo") %>&idcurso=1&retcurso=<%=Request("idcurso")%>">
                                                <img src="images/videoteca/modulos/tronco0<%=Cursor2("id_modulo") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor2("titulo") %>">
                                            </a>
                                            <div class="progress">
                                                <%
                                                    set CursorProg1 = createobject("ADODB.Recordset")
                                                    CursorProg1.CursorType =1 
                                                    CursorProg1.LockType = 3

                                                    asas="Herodoto 63"

                                                    SQL2 = "SELECT porcentaje_vl FROM [ma_15].[dbo].progresos where id_modulo = " & Cursor2("id_modulo") & " and id_usuario = " & Session("id_usuario")
                                                    CursorProg1.Open SQL2, StrConn, 1, 3
                                                    
                                                    If CursorProg1.RecordCount > 0 then
                                                    Do While not CursorProg1.EOF  
                                                        colorInterior = "#fff"
                                                        if cint(CursorProg1("porcentaje_vl")) < 20 then
                                                            colorInterior = "#343434"
                                                        end if
                                                        if cint(CursorProg1("porcentaje_vl")) < 50 then    
                                                            claseBarra = "danger"
                                                        elseif cint(CursorProg1("porcentaje_vl")) < 85 then
                                                            claseBarra = "warning"
                                                        else
                                                            claseBarra = "success"
                                                        end if
                                                %>
                                                <div class="progress-bar progress-bar-<%=claseBarra %> progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="<%=cint(CursorProg1("porcentaje_vl")) %>" aria-valuemin="0" aria-valuemax="100" style="width: <%=cint(CursorProg1("porcentaje_vl")) %>%; color: <%=colorInterior%>;">
                                                    <%=cint(CursorProg1("porcentaje_vl")) %> %
                                                </div>
                                                <%
                                                CursorProg1.MoveNext
                                                Loop
                                                    else
                                                %>
                                                <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; color: #343434;">
                                                    0%
                                                </div>
                                                <%end if %>
                                            </div>
                                            <div class="row text-center" style="min-height: 55px">
                                                <% if Cursor2("total_videos") <> 0 then  %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-videoCirugias iconos-contenido"></span>&nbsp;<%=Cursor2("total_videos") %></b><br />
                                                    <small>Videos</small>
                                                </div>
                                                <% end if
                                                   if Cursor2("total_interactivos") <> 0 then  %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-interactivos iconos-contenido"></span>&nbsp;<%=Cursor2("total_interactivos") %></b><br />
                                                    <small>Interactivos</small>
                                                </div>
                                                <% end if
                                                   if Cursor2("total_rv") <> 0 then 
                                                    
                                                %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-revisioDelMes iconos-contenido"></span>&nbsp;<%=Cursor2("total_rv") %></b><br />
                                                    <small>Revisión del mes</small>
                                                </div>
                                                <% end if %>
                                                <%If Cursor2("total_lec") >  0 then %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-lecturaguiada-02 iconos-contenido"></span>&nbsp;<%=Cursor2("total_lec") %></b><br />
                                                    <small>Lecturas Guiadas</small>
                                                </div>
                                                <%End If %>
                                                <%If Cursor2("total_tareas") >  0 then %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-tarea iconos-contenido"></span>&nbsp;<%=Cursor2("total_tareas") %></b><br />
                                                    <small>Tareas</small>
                                                </div>
                                                <%End If %>
                                            </div>
                                            <div class="row center-block text-center">
                                                <div class="row">
                                                    <div class="form-grop col-md-12">
                                                        <div class=" col-md-6">
                                                            <a class="btn btn-block  <%=btnClase %> btn-objetivos " href="javascript:void(0)" data-modulo="<%=Cursor2("id_modulo") %>" data-toggle="modal" data-target="#objetivos"><i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                                <br />
                                                                Objetivos</a>
                                                        </div>
                                                        <div class=" col-md-6">
                                                            <a class="btn btn-block  <%=btnClase %>" href="view_cont_cert.asp?tlpmod=<%=Cursor2("id_modulo") %>&idcurso=1&retcurso=<%=Request("idcurso")%>"><i class="<%=iconclase %>"></i>&nbsp;Ver<br />
                                                                Contenido</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%
                            Cursor2.MoveNext
                            Loop
                            'Cursor.Close
                            Cursor2.MoveFirst    
                            %>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 cintillo">
                            <h2 class="strong-look"><i class="icon-cursoscertificacion"></i><span>&nbsp;MÓDULOS  </span></h2>
                            <hr />
                        </div>
                    </div>
                    <div class="row port m-b-20">
                        <div class="portfolioContainer">
                            <% 
                            Do While not Cursor.EOF   
                            %>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="curso-<%=Cursor("id_modulo") %>">
                                <div class="card-box responsive-height">
                                    <div class="media m-b-10">
                                        <div class="gal-detail thumb">
                                            <a href="view_cont_cert.asp?tlpmod=<%=Cursor("id_modulo") %>&idcurso=<%=Request("idcurso") %>&retcurso=<%=Request("idcurso")%>">
                                                <img src="images/videoteca/modulos/<%=Cursor("id_modulo") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor("titulo") %>">
                                            </a>

                                            <div class="progress">
                                                <%
                                                    set CursorProg1 = createobject("ADODB.Recordset")
                                                    CursorProg1.CursorType =1 
                                                    CursorProg1.LockType = 3

                                                    SQL2 = "SELECT  top 1 porcentaje_vl FROM [ma_15].[dbo].progresos where id_modulo = " & Cursor("id_modulo") & " and id_usuario = " & Session("id_usuario")
                                                    CursorProg1.Open SQL2, StrConn, 1, 3
                                                    
                                                    If CursorProg1.RecordCount > 0 then
                                                    Do While not CursorProg1.EOF  
                                                        colorInterior = "#fff"
                                                        if cint(CursorProg1("porcentaje_vl")) < 20 then
                                                            colorInterior = "#343434"
                                                        end if
                                                        if cint(CursorProg1("porcentaje_vl")) < 50 then    
                                                            claseBarra = "danger"
                                                        elseif cint(CursorProg1("porcentaje_vl")) < 85 then
                                                            claseBarra = "warning"
                                                        else
                                                            claseBarra = "success"
                                                        end if
                                                %>
                                                <div class="progress-bar progress-bar-<%=claseBarra %> progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="<%=cint(CursorProg1("porcentaje_vl")) %>" aria-valuemin="0" aria-valuemax="100" style="width: <%=cint(CursorProg1("porcentaje_vl")) %>%; color: <%=colorInterior%>;">
                                                    <%=cint(CursorProg1("porcentaje_vl")) %>%
                                                </div>
                                                <%
                                                CursorProg1.MoveNext
                                                Loop
                                                    else
                                                %>
                                                <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; color: #343434;">
                                                    0%
                                                </div>
                                                <%end if %>
                                            </div>
                                            <div class="row text-center" style="min-height: 55px">
                                                <% if Cursor("total_videos") <> 0 then  %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-videoCirugias iconos-contenido"></span>&nbsp;<%=Cursor("total_videos") %></b><br />
                                                    <small>Videos</small>
                                                </div>
                                                <% end if %>
                                                <%
                                                   if Cursor("total_rv") <> 0 then  %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-revisioDelMes iconos-contenido"></span>&nbsp;<%=Cursor("total_rv") %></b><br />
                                                    <small>Revisión del mes</small>
                                                </div>
                                                <% end if %>
                                                <%If Cursor("total_lec") <>  0 then %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-lecturaguiada-02 iconos-contenido"></span>&nbsp;<%=Cursor("total_lec") %></b><br />
                                                    <small>Lecturas Guiadas</small>
                                                </div>
                                                <%End If %>
                                                <%If Cursor("total_tareas") <>  0 then %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-tarea iconos-contenido"></span>&nbsp;<%=Cursor("total_tareas") %></b><br />
                                                    <small>Tareas</small>
                                                </div>
                                                <%End If %>
                                                <% if Cursor("total_interactivos") <> 0 then  %>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                    <b><span class="icon-interactivos iconos-contenido"></span>&nbsp;<%=Cursor("total_interactivos") %></b><br />
                                                    <small>Interactivo</small>
                                                </div>
                                                <% end if %>
                                            </div>
                                            <div class="row center-block text-center">
                                                <div class="row">
                                                    <div class="form-grop col-md-12">
                                                        <div class="col-md-6">
                                                            <a class="btn btn-block  <%=btnClase %> btn-objetivos " href="javascript:void(0)" data-modulo="<%=Cursor("id_modulo") %>" data-toggle="modal" data-target="#objetivos"><i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                                <br />
                                                                Objetivos</a>
                                                        </div>
                                                        <div class=" col-md-6">
                                                            <a class="btn btn-block  <%=btnClase %>" href="view_cont_cert.asp?tlpmod=<%=Cursor("id_modulo") %>&idcurso=<%=Request("idcurso") %>&retcurso=<%=Request("idcurso")%>"><i class="<%=iconclase %>"></i>&nbsp;Ver
                                                                <br />
                                                                Contenido</a>
                                                        </div>
                                                    </div>





                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%
                            Cursor.MoveNext
                            Loop
                            'Cursor.Close
                            Cursor.MoveFirst    
                            %>
                        </div>
                    </div>
                </div>
                <!--#include file="footer.asp" -->
            </div>
        </div>
    </div>
    <div id="objetivos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0 ">
                <div class="panel panel-color panel-primary back-modal-pop">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                            ×</button>
                        <h3 class="panel-title">&nbsp;</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" id="text-obj">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/logout.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="js/certificados.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#divAdvice").hover(function () {
                $('#divAdvice').popover('toggle');
            }, function () {
                $('#divAdvice').popover('hide');
            });
        });
    </script>

</body>
</html>
