
<%If Request.Form ("save") = "SI" then %>
<!--#include file="../data/conexion_icss_va.inc" -->
<%

Function PwdAleatorio ( Longitud, Repetir )
Dim vPass(), I, J ' nuestro vector y dos contadores
Dim vNumeros()	  ' vector para guardar lo que llevamos
Dim n, bRep		  
Dim vCaracteres	  ' vector donde est�n los posibles caract.

vCaracteres = Array("a","b", "c", "d", "e", "f", "g", "h", _
"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", _
"u", "v", "w", "x", "y", "z", "A","B", "C", "D", "E", "F", _ 
"G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", _
"S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", _
"5", "6", "7", "8", "9", "0")

'Establezco la longitud del vector
Redim vPass(Longitud-1)
'Y del vector auxiliar que guarda los caracteres ya escogidos
Redim vNumeros(Longitud-1)
I = 0
'Inicializo los n�s aleatorios
Randomize
'Hasta que encuentre todos los caracteres
do until I = Longitud
	'Hallo un n�mero aleatorio entre 0 y el m�ximo indice
	' del vector de caracteres.
	n = int(rnd*Ubound(vCaracteres))
	'Si no puedo repetir...
	if not Repetir then
		bRep = False
		'Busco el numero entre los ya elegidos
		for J = 0 to UBound(vNumeros)
			if n = vNumeros(J) then
			'Si esta, indico que ya estaba
				bRep = True
			end if
		next
		'Si ya estaba, tengo que repetir este caracter
		'as� que resto 1 a I y volvemos sobre la misma
		'posici�n.
		if bRep then 
			I = I - 1
		else
			vNumeros(I) = n
			vPass(I) = vCaracteres(n)	
		end if
	else
	'Me da igual que est� o no repetido
		vNumeros(I) = n
		vPass(I) = vCaracteres(n)
	end if
'Siguiente car�cter!
I = I + 1
loop

'Devuelvo la cadena. Join une los elementos de un vector
'utilizando como separador el segundo par�metro: en este
'caso, nada -&gt; "".
PwdAleatorio = Join(vPass, "")

End Function 'PwdAleatorio

HSession = PwdAleatorio(8,2)    



 set Cursor = createobject("ADODB.Recordset")
 Cursor.CursorType =1 
 Cursor.LockType = 3 
 
 set Cursor2 = createobject("ADODB.Recordset")
 Cursor2.CursorType =1 
 Cursor2.LockType = 3 
 
  set Cursor3 = createobject("ADODB.Recordset")
 Cursor3.CursorType =1 
 Cursor3.LockType = 3 


 u = Request.Form ("u")
 q = Request.Form ("q")
 opc = Request.Form ("opc")
 id_cuestionario = Request.Form ("id_cuestionario")
 id_modulo = Request.Form ("id_modulo")
 id_video = Request.Form("idv")
 mostrar = cint(Request.Form ("mostrar"))
 
 A = 1
 P = 0 
 Porcentaje = 0
 
 Do While A <= mostrar
   Campo = "IDP_" & A
   id_pregunta = Request.Form (Campo)
   Campo2 = "RP_" & A
   id_respuesta = Request.Form (Campo2)
   
   SQL = "SELECT porcentaje FROM tbl_preguntas WHERE id_pregunta = " & id_pregunta
   Cursor2.Open SQL, StrConn, 1, 3
   
   If Cursor2.RecordCount > 0 then
    P = Cursor2("porcentaje")
   end if
   Cursor2.Close
   
   
   SQL = "SELECT id_respuesta, correcta FROM tbl_respuestas WHERE id_pregunta = " & id_pregunta  & " and id_respuesta = " & id_respuesta
   Cursor2.Open SQL, StrConn, 1, 3
 
   If Cursor2.RecordCount > 0 then
    If Cursor2("correcta") = "SI" then
      Porcentaje = Porcentaje + P
      Correcta = "SI"
    else
      Correcta = "NO"
    end if    
   end if
   Cursor2.Close

  SQL = "SELECT * FROM log_respuestas WHERE id = 0"
  Cursor2.Open SQL, StrConn, 1, 3
  
  Cursor2.Addnew
  Cursor2("id_pregunta") = id_pregunta
  Cursor2("id_usuario") = u
  Cursor2("id_video") = id_video
  Cursor2("correcta") = Correcta
  Cursor2("hash") = HSession
  Cursor2("fecha") = date()
  Cursor2("hora") = time()
  Cursor2("orden") = A
  Cursor2.Update
  Cursor2.Close 
   
  A = A + 1
  P = 0
Loop
     
SQL = "SELECT * FROM log_evaluaciones WHERE id_cuestionario = " & id_cuestionario & " and id_video = " & id_video & " and id_usuario = " & u
Cursor2.Open SQL, StrConn, 1, 3

If Cursor2.RecordCount = 0 then
  Cursor2.AddNew
  Cursor2("id_cuestionario") = id_cuestionario
  'Cursor2("id_modulo") = id_modulo
  Cursor2("id_video") = id_video
  Cursor2("id_usuario") = u
  Cursor2("porcentaje") = Porcentaje
  Porcentaje2 = Porcentaje
  Cursor2("porcentaje_progreso") = Porcentaje2
  Cursor2("fecha") = date()
  Cursor2("hora") = time()
  Cursor2("hash_intento1") = HSession
  Cursor2("intentos") = 1
  Intentos = 1
else  
  Intentos = Cursor2("intentos") + 1
  Cursor2("porcentaje") = Porcentaje
  Cursor2("intentos") = Intentos

  Porcentaje2 = Porcentaje
  Cursor2("porcentaje_progreso") = Porcentaje2
  
  If Intentos = 2 then
    Cursor2("hash_intento2") = HSession
  else
    If Intentos = 3 then
      Cursor2("hash_intento3") = HSession
    end if
  end if
  Cursor2("fecha") = date()
  Cursor2("hora") = time()        
end if
Cursor2.Update
Cursor2.Close
     
SQL = "SELECT * FROM tbl_cuestionarios WHERE id_modulo = " & id_modulo & " and activo = 'SI'"
Cursor3.Open SQL, StrConn, 1, 3

SQL = "SELECT * FROM progresos WHERE id_modulo = " & id_modulo & " and id_usuario = " & u
Cursor2.Open SQL, StrConn, 1, 3

total_cuestionarios = Cursor3.RecordCount
Cursor3.Close

PV = 100 / total_cuestionarios
B = (Porcentaje*PV)/100 

If Cursor2.RecordCount > 0 then
  Porcentaje2 = Porcentaje
  Cursor2("porcentaje_evaluacion") = Porcentaje2
  Cursor2("fecha_actualizacion") = date()
  Cursor2("hora_actualizacion") = time()
 ' Cursor2("porcentaje_vl") = 0
  'Cursor("videos_vistos") = 0
  Cursor2("porcentaje_global") = Cursor2("porcentaje_global") + B
  
else
  Cursor2.AddNew
  Porcentaje2 = Porcentaje
   Cursor2("id_usuario") = u
   Cursor2("id_modulo") = id_modulo
   'Cursor2("id_video") = id_video
   'Cursor2("porcentaje_evaluacion") = Porcentaje2
   'Cursor2("porcentaje_actividades1") = 0
   'Cursor2("porcentaje_actividades2") = 0
   'Cursor2("porcentaje_actividades3") = 0
   'Cursor2("porcentaje_actividades4") = 0
   'Cursor2("porcentaje_actividades5") = 0  
    Cursor2("porcentaje_vl") = 0
   Cursor2("videos_vistos") = 0   
   Cursor2("fecha_actualizacion") = date()
   Cursor2("hora_actualizacion") = time()   
   
   Cursor2("porcentaje_global") = Cursor2("porcentaje_global") + B
end if
Cursor2.Update
Cursor2.Close

'SQL = "SELECT * FROM log_time_evaluaciones WHERE id_usuario = " & u & " and id_modulo = " & id_modulo & " and intento = " & Intentos
'Cursor2.Open SQL, StrConn, 1, 3

'If Cursor2.RecordCount > 0 then
  ' Cursor2("concluido") = "SI"
 '  Cursor2.Update
'end if

'Cursor2.Close 

     
SQL = "SELECT * FROM log_respuestas WHERE id_usuario = " & u & " and hash = '" & HSession & "' ORDER By orden"
Cursor.Open SQL, StrConn, 1, 3  

URL =  "savequizok.asp?u=" & u & "&hs=" & HSession  & "&q=" & q & "&opc=" & opc & "&id_modulo=" & id_modulo & "&id_cuestionario=" & id_cuestionario & "&mostrar=" & mostrar & "&p=" & Porcentaje & "&i=" & Intentos & "&id_video=" & id_video
Response.Redirect (URL)
 
 
end if %>