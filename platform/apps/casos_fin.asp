<!--#include file="../data/conexion_icss_va.inc" -->

<html>
<head>
<title>Vacunas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--#include file="../includes/css.asp" -->

</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="0">

<%
 set Cursor = createobject("ADODB.Recordset")
 Cursor.CursorType =1 
 Cursor.LockType = 3
 
 set Cursor2 = createobject("ADODB.Recordset")
 Cursor2.CursorType =1 
 Cursor2.LockType = 3 
 
 tlp = Request("tlp")
 odc3 = Request("odc3")
 idcp = Request("idcp")
 
 SQL = "SELECT id_modulo FROM casos WHERE id_caso = " & idcp
 Cursor.Open SQL, StrConn, 1, 3
 
 If Cursor.RecordCount > 0 then
   idm = Cursor("id_modulo")
 end if
 Cursor.Close
 
 SQL = "SELECT * FROM progresos WHERE id_modulo = " & idm & " and id_usuario = " & tlp
 Cursor.Open SQL, StrConn, 1, 3
 
 If Cursor.RecordCount = 0 then
   Cursor.AddNew
   Cursor("id_usuario") = tlp
   Cursor("id_modulo") = idm
   Cursor("porcentaje_evaluacion") = 0
   Cursor("porcentaje_actividades1") = 0
  Select Case (idcp)
   Case 1
    Cursor("porcentaje_actividades2") = 2.5
    Cursor("porcentaje_actividades3") = 0
    Cursor("porcentaje_actividades4") = 0
    Cursor("porcentaje_actividades5") = 0
   Case 2
    Cursor("porcentaje_actividades2") = 0
    Cursor("porcentaje_actividades3") = 2.5
    Cursor("porcentaje_actividades4") = 0
    Cursor("porcentaje_actividades5") = 0
   Case 3
    Cursor("porcentaje_actividades2") = 0
    Cursor("porcentaje_actividades3") = 0
    Cursor("porcentaje_actividades4") = 2.5
    Cursor("porcentaje_actividades5") = 0
   Case 4 
     Cursor("porcentaje_actividades1") = 10  
    Cursor("porcentaje_actividades2") = 0
    Cursor("porcentaje_actividades3") = 0
    Cursor("porcentaje_actividades4") = 0
    Cursor("porcentaje_actividades5") = 0     
  End Select   
   
   Cursor("porcentaje_vl") = 0
   Cursor("videos_vistos") = 0   
   Cursor("fecha_actualizacion") = date()
   Cursor("hora_actualizacion") = time()   
   Cursor("encuesta") = 0    
   Cursor("porcentaje_global") = Cursor("porcentaje_vl") +  Cursor("porcentaje_evaluacion") + Cursor("porcentaje_actividades1") + Cursor("porcentaje_actividades2") + Cursor("porcentaje_actividades3") +  Cursor("porcentaje_actividades4") +  Cursor("porcentaje_actividades5")  
   
 else
 
  Select Case (idcp)
   Case 1
    Cursor("porcentaje_actividades2") = 2.5
    'Cursor("porcentaje_actividades3") = 0
    'Cursor("porcentaje_actividades4") = 0
    'Cursor("porcentaje_actividades5") = 0
   Case 2
    'Cursor("porcentaje_actividades2") = 0
    Cursor("porcentaje_actividades3") = 2.5
    'Cursor("porcentaje_actividades4") = 0
    'Cursor("porcentaje_actividades5") = 0
   Case 3
    'Cursor("porcentaje_actividades2") = 0
    'Cursor("porcentaje_actividades3") = 0
    Cursor("porcentaje_actividades4") = 2.5
    'Cursor("porcentaje_actividades5") = 0
  Case 4   
   Cursor("porcentaje_actividades1") = 10
   
  End Select   

  Cursor("fecha_actualizacion") = date()
  Cursor("hora_actualizacion") = time()

  Cursor("porcentaje_global") =  Cursor("porcentaje_vl") + Cursor("porcentaje_evaluacion") + Cursor("porcentaje_actividades1") + Cursor("porcentaje_actividades2") + Cursor("porcentaje_actividades3") +  Cursor("porcentaje_actividades4") +  Cursor("porcentaje_actividades5")  
end if

Cursor.Update
Cursor.Close 

 'If Request("c") = "" then
   'Correctas = 0
' else
 '  Correctas = Request("c")
' end if   
 
 SQL = "SELECT * FROM casos WHERE id_caso = " & idcp
 Cursor.Open SQL, StrConn, 1, 3
 
 If Cursor.RecordCount > 0 then
  TitC = Cursor("titulo")
  CantidadPreg = Cursor("cantidad_preg")
  Presentacion = Cursor("presentacion")
 end if
 Cursor.Close
 If Request("secuencia") = "" then
   secuencia = 1
 else
   secuencia = Request("secuencia") + 1  
 end if
 
 SQL  = "SELECT TOP 1 * FROM preguntas_casos WHERE id_caso = " & idcp & " and secuencia = " & secuencia & " ORDER by secuencia"
 Cursor.Open SQL, StrConn, 1, 3
 
 
 %>
 
 
<table width="720" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      
    <td bgcolor="07759a" height="33"> 
      <div align="center" class="tquiz">Actividades pr&aacute;cticas</div><br>
       
      </td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      
    <td height="88"> 
     <div align="left" class="rquiz"><b><%=TitC %>:</b> <%=Presentacion %></div>
      <form name="form1" method="post" action="casos_feed.asp?idcp=<%=idcp %>&tlp=<%=Request("tlp") %>&odc3=<%=Request("odc3") %>&c=<%=Request("c") %>">
      </form>
      <table class="rquiz" width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td colspan="2" height="66">Lo invitamos a continuar con el progreso 
            dentro del Diplomado Latinoamericano de Vacunolog&iacute;a.<br>
            Tome en cuenta sus resultados para repasar los contenidos que considere 
            necesarios.<br>
          </td>
        </tr>
        <tr> 
          <td width="27%" height="55"><b>Respuestas correctas: </b></td>
          <td width="73%"><b><font color="#FF0000"><%=Request("c")%></font></b></td>
        </tr>
      </table>
    </td>
    </tr>
  </table>

</body>
</html>
