<!--#include file="../includes/config2.asp" -->
<!--#include file="../data/conexion_icss_va.inc" -->

<%




SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI'"
 set Cursor = createobject("ADODB.Recordset")
 Cursor.CursorType =1 
 Cursor.LockType = 3 
 
 set Cursor2 = createobject("ADODB.Recordset")
 Cursor2.CursorType =1 
 Cursor2.LockType = 3  
 
 Cursor.Open SQL, StrConn, 1, 3
 %>
<html>
<head>
<title>Vacunas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--#include file="../includes/css.asp" -->

</head>

<body bgcolor="#FFFFFF" text="#000000">

  
<table width="720" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td bgcolor="07759a" height="33"> 
      <div align="center" class="tquiz">Material de apoyo</div>
    </td>
  </tr>
  <tr>
    <td height="10">&nbsp;</td>
  </tr>
  <tr> 
    <td class="materiales2" height="33"><font color="#333333"><b>Art&iacute;culos 
      M&oacute;dulo 1 y 2</b></font></td>
  </tr>
  <tr> 
    <td height="88" valign="top"> 
      <table width="696" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="07759a">
        <tr class="materiales2"> 
          <td width="374" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">Titulo</font></b></div>
          </td>
          <td width="229" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">Especialidad</font></b></div>
          </td>
          <td width="85" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">PDF</font></b></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td width="374"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="materiales">Consenso para el diagn&oacute;stico cl&iacute;nico y microbiol&oacute;gico y la prevenci&oacute;n de la infecci&oacute;n por Bordetella pertussis</td>
              </tr>
            </table>
          </td>
          <td width="229"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Infectolog&iacute;a</td>
            </tr>
          </table></td>
          <td width="85" height="44"> 
            <div align="center"><a href="../pdfs/articulos/4.pdf" target="_blank"><img src="../images/btn_descargar.png" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="<%=Color %>"> 
          <td width="374"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="materiales">En personas inmunocompetentes que han recibido tres dosis de vacuna contra la hepatitis B no parece necesaria la administraci&oacute;n de una dosis de recuerdo</td>
              </tr>
            </table>
          </td>
          <td width="229"><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Infectolog&iacute;a</td>
            </tr>
          </table></td>
          <td width="85" height="44">
            <div align="center"><a href="../pdfs/articulos/5.pdf" target="_blank"><img src="../images/btn_descargar.png" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Respuesta inmune r&aacute;pida contra la hepatitis B mediante esquema acelerado de vacunaci&oacute;n intrad&eacute;rmica</td>
            </tr>
          </table></td>
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Infectolog&iacute;a</td>
            </tr>
          </table></td>
          <td height="44"> 
            <div align="center"><a href="../pdfs/articulos/7.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="<%=Color %>">
          <td><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Investigaci&oacute;n cl&iacute;nica I. Dise&ntilde;os de investigaci&oacute;n</td>
            </tr>
          </table></td>
          <td><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Investigaci&oacute;n</td>
            </tr>
          </table></td>
          <td height="44">
            <div align="center"><a href="../pdfs/articulos/11.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Francisco Xavier Balmis y las juntas de vacuna, un ejemplo pionero para implementar la vacunaci&oacute;n</td>
            </tr>
          </table></td>
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Pediatr&iacute;a</td>
            </tr>
          </table></td>
          <td height="44"> 
            <div align="center"><a href="../pdfs/articulos/13.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td class="materiales2" height="33"><font color="#333333"><b>Art&iacute;culos 
      M&oacute;dulo 2 y 3</b></font></td>
  </tr>
  <tr> 
    <td height="88" valign="top"> 
      <table width="696" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="07759a">
        <tr class="materiales2"> 
          <td width="374" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">Titulo</font></b></div>
          </td>
          <td width="229" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">Especialidad</font></b></div>
          </td>
          <td width="85" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">PDF</font></b></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td width="374"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="materiales">Mortalidad por enfermedad diarreica en menores, antes y despu&eacute;s de la introducci&oacute;n de la vacuna contra el rotavirus.</td>
              </tr>
            </table>
          </td>
          <td width="229"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
              
                <td class="materiales">Enfermedades Infecciosas</td>
              </tr>
            </table>
          </td>
          <td width="85" height="44"> 
            <div align="center"><a href="../pdfs/articulos/1.pdf" target="_blank"><img src="../images/btn_descargar.png" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="<%=Color %>"> 
          <td width="374"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="materiales">Diferencias regionales en la mortalidad por c&aacute;ncer de mama y c&eacute;rvix en M&eacute;xico entre 1979 y 2006</td>
              </tr>
            </table>
          </td>
          <td width="229"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
              
                <td class="materiales">Gineco  Obstetricia</td>
              </tr>
            </table>
          </td>
          <td width="85" height="44">
            <div align="center"><a href="../pdfs/articulos/2.pdf" target="_blank"><img src="../images/btn_descargar.png" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Vacunolog&iacute;a Cl&aacute;sica y Nuevas Tecnolog&iacute;as en el Dise&ntilde;o de Vacunas</td>
            </tr>
          </table></td>
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Inmunolog&iacute;a</td>
            </tr>
          </table></td>
          <td height="44"> 
            <div align="center"><a href="../pdfs/articulos/10.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="<%=Color %>">
          <td><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Seguridad de las vacunas combinadas hexavalentes y principio de precauci&oacute;n</td>
            </tr>
          </table></td>
          <td><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Pediatr&iacute;a</td>
            </tr>
          </table></td>
          <td height="44">
            <div align="center"><a href="../pdfs/articulos/14.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Desarrollo y autorizaci&oacute;n de vacunas pand&eacute;micas de la gripe A (H1N1) 2009</td>
            </tr>
          </table></td>
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Vacunolog&iacute;a</td>
            </tr>
          </table></td>
          <td height="44"> 
            <div align="center"><a href="../pdfs/articulos/15.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td class="materiales2" height="33"><font color="#333333"><b>Art&iacute;culos 
      M&oacute;dulo 3 y 4</b></font></td>
  </tr>
  <tr> 
    <td height="88" valign="top"> 
      <table width="696" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="07759a">
        <tr class="materiales2"> 
          <td width="374" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">Titulo</font></b></div>
          </td>
          <td width="229" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">Especialidad</font></b></div>
          </td>
          <td width="85" bgcolor="07759a"> 
            <div align="center"><b><font color="#FFFFFF">PDF</font></b></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td width="374"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="materiales">Afecta la vacunaci&oacute;n a la prescripci&oacute;n de antibi&oacute;ticos</td>
              </tr>
            </table>
          </td>
          <td width="229"> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
              
                <td class="materiales">Infectolog&iacute;a</td>
              </tr>
            </table>
          </td>
          <td width="85" height="44"> 
            <div align="center"><a href="../pdfs/articulos/3.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="<%=Color %>">
          <td width="374"><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Glicoprote&iacute;na (Gp-G) del virus r&aacute;bico  Estructura, inmunogenicidad y rol en la patogenia</td>
            </tr>
          </table></td>
          <td width="229"><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Infectolog&iacute;a</td>
            </tr>
          </table></td>
          <td width="85" height="44">
            <div align="center"><a href="../pdfs/articulos/6.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Vacuna anti-varicela</td>
            </tr>
          </table></td>
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Infectolog&iacute;a</td>
            </tr>
          </table></td>
          <td height="44"> 
            <div align="center"><a href="../pdfs/articulos/8.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="<%=Color %>">
          <td><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Vacunas de ADN inducci&oacute;n de la respuesta inmunitaria</td>
            </tr>
          </table></td>
          <td><table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Inmunolog&iacute;a</td>
            </tr>
          </table></td>
          <td height="44">
            <div align="center"><a href="../pdfs/articulos/9.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
        <tr bgcolor="F4FDFF"> 
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="materiales">Adherencia a las inmunizaciones en ni&ntilde;os nacidos con menos de 1 500 gr de peso &oacute; antes de 32 semanas de gestaci&oacute;n, en dos centros chilenos.</td>
            </tr>
          </table></td>
          <td> 
            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              
              <td class="materiales">Pediatr&iacute;a</td>
            </tr>
          </table></td>
          <td height="44"> 
            <div align="center"><a href="../pdfs/articulos/12.pdf" target="_blank"><img src="../images/btn_descargar.png" alt="" width="75" height="22" border="0"></a></div>
          </td>
        </tr>
      </table></td>
  </tr>
  
<%Do While not Cursor.EOF %>  
  <tr> 
    <td valign="middle" class="materiales2" height="33"><font color="#333333"><b>Presentaciones 
      Video-lecciones: <%=Cursor("titulo") %>, <%=Cursor("tip") %></b></font></td>
  </tr>
  <tr> 
    <td height="88" valign="top"> 
      <div align="left" class="rquiz"> 
        <table width="700" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="07759a">
          <tr class="materiales2"> 
            <td width="374" bgcolor="07759a"> 
              <div align="center"><b><font color="#FFFFFF">Titulo</font></b></div>
            </td>
            <td width="229" bgcolor="07759a"> 
              <div align="center"><b><font color="#FFFFFF">Profesor</font></b></div>
            </td>
            <td width="89" bgcolor="07759a"> 
              <div align="center"><b><font color="#FFFFFF">PDF</font></b></div>
            </td>
          </tr>
          <%
Color = "#F4FDFF"
SQL = " SELECT tbl_videos.id_video, tbl_videos.titulo, cat_ponentes.nombre, cat_ponentes.titulo AS titulo2 FROM tbl_videos "
SQL = SQL & " INNER JOIN cat_ponentes ON tbl_videos.id_ponente = cat_ponentes.id_ponente "
SQL = SQL & " WHERE ppt = 'SI' and tbl_videos.id_modulo = " & Cursor("id_modulo")
SQL = SQL & " ORDER by tbl_videos.id_video "
Cursor2.Open SQL, StrConn, 1, 3

Do While not Cursor2.EOF 
  
  
  If Color = "#F4FDFF" then
    Color = "#FFFFFF"
  else
    If Color = "#FFFFFF" then
      Color = "#F4FDFF"
    end if
  end if    
  
  If Cursor2("id_video") <= 17 then 
    Archivo = "../downloads/pdf/" 
    If Cursor2("id_video") < 10 then
     Archivo = Archivo & "0" & Cursor2("id_video") & ".pdf"
    else
     Archivo = Archivo & Cursor2("id_video") & ".pdf"
    end if
  else
      Archivo = "../downloads/pdf/" 
      Archivo = Archivo & Cursor2("id_video") & "_slides.pdf"
  end if
%>
          <tr bgcolor="<%=Color %>"> 
            <td width="374"> 
              <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="materiales"><%=Cursor2("titulo") %></td>
                </tr>
              </table>
            </td>
            <td width="229"> 
              <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <%TheName = Cursor2("titulo2") & " " & Cursor2("nombre") %>
                  <td class="materiales"><%=TheName %></td>
                </tr>
              </table>
            </td>
            <td width="89" height="44"> 
              <div align="center"><a href="<%=Archivo%>" target="_blank"><img src="../images/btn_descargar.png" width="75" height="22" border="0"></a></div>
            </td>
          </tr>
          <%Cursor2.MoveNext
Archivo = ""
Loop
Cursor2.Close
'StrConn.Close
'Set Cursor = Nothing
'Set StrConn = Nothing

 %>
        </table>
      </div>
    </td>
  </tr>
<%Cursor.MoveNext
Loop

Cursor.Close %>  
</table>

</body>
</html>
