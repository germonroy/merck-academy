<!--#include file="../includes/config2.asp" -->
<!--#include file="../data/conexion_icss_va.inc" -->

<%
 set Cursor = createobject("ADODB.Recordset")
 Cursor.CursorType =1 
 Cursor.LockType = 3 
 
  SQL = "SELECT id_modulo, titulo FROM tbl_modulos WHERE hash ='" & Request("opc") & "'"
  Cursor.Open SQL, StrConn, 1, 3
  
  id_modulo = Cursor("id_modulo")
  Titulo = Cursor("titulo")
  Cursor.Close
  
  ''' Checa intento 1
  
  SQL = "SELECT * FROM log_time_evaluaciones WHERE id_modulo = " & id_modulo & " and id_usuario = " & Request("tlp") & " and intento = 1"
  Cursor.Open SQL, StrConn, 1, 3
  
  If Cursor.RecordCount > 0 then
    
    If Cursor("concluido") = "SI" then
      Intento1 = 1
    else
      If date() > Cursor("fecha") then
        Intento1 = 1
      else
        Intento1 = 2
      end if  
    end if
    uIntento = 1
  else
   Intento1 = 0  
  end if
  
Cursor.Close
  ''' Checa intento 2
  
  SQL = "SELECT * FROM log_time_evaluaciones WHERE id_modulo = " & id_modulo & " and id_usuario = " & Request("tlp") & " and intento = 2"
  Cursor.Open SQL, StrConn, 1, 3
  
  If Cursor.RecordCount > 0 then
    
    If Cursor("concluido") = "SI" then
      Intento2 = 1
    else
      If date() > Cursor("fecha") then
        Intento2 = 1
      else
        Intento2 = 2
      end if  
    end if
    
    uIntento = 2 
  else
   Intento2 = 0    
  end if


  
  
'  If Cursor.RecordCount > 0 then
   
 '  If date() > Cursor("fecha") then
    ' Response.Redirect "overtime.asp?opc=" & Request("opc") & "&odc3=" & Request("odc3") & "&tlp=" & Request("tlp")
   'else  
   '  If Cursor("fecha") = date() and Cursor("hora_completa_termina") > time() then
    '  Response.Redirect "overtime.asp?opc=" & Request("opc") & "&odc3=" & Request("odc3") & "&tlp=" & Request("tlp")
'     else
  If Intento1 = 2 or Intento2 = 2 then
      Response.Redirect "quiz.asp?opc=" & Request("opc") & "&odc3=" & Request("odc3") & "&tlp=" & Request("tlp") & "&uintento=" & uIntento     
  end if
  
  If Intento1 = 1 and Intento2 = 1 then
    Response.Redirect "quiz.asp?opc=" & Request("opc") & "&odc3=" & Request("odc3") & "&tlp=" & Request("tlp") & "&uintento=" & uIntento     
  end if
  Cursor.Close

 %>

<html>
<head>
<title>Medix</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--#include file="../includes/css.asp" -->
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="0">
<table width="720" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="tquiz"><img src="images/evaluacion.jpg" width="720" height="80"> 
    </td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
  <tr> 
    <td height="250" class="rquiz"><br>
      <b>Estimado Usuario: </b> 
      <p>Esta usted a punto de comenzar su evaluaci&oacute;n correspondiente al<b> 
        <%=Titulo %></b> de su <b>Diplomado en Sobrepeso y Obesidad</b>; por 
        lo que es <b>importante</b> que conozca el funcionamiento de la misma:</p>
      <ol>
        <li>S&oacute;lo se tienen <b>dos oportunidades </b>para realizarlo.<br>
          <br>
        </li>
        <li><b>Cuenta con 2 horas por cada </b>oportunidad, por lo que si decide 
          iniciarlo deber&aacute; concluirlo; es decir de principio a fin, ya 
          que el sistema no guarda los resultados de forma parcial.<br>
          <br>
        </li>
        <li>La calificaci&oacute;n aprobatoria deber&aacute; ser <b>80 o superior</b>.<br>
          <br>
        </li>
        <li>Es importante que en su<b> primer oportunidad no deje pasar el tiempo 
          limite de 2 hrs</b> ya que de lo contrario <b>no tendr&aacute; acceso 
          a la segunda.</b></li>
      </ol>
      <p><br>
        Si usted est&aacute; seguro de comenzar su evaluaci&oacute;n de clic en 
        el bot&oacute;n<b> &#147;Comenzar ahora&#148;</b> o si prefiere consultar 
        nuevamente las videolecci&oacute;n cierre esta ventana.<br>
      </p>
      <p align="center"><a href="generate_quiz.asp?opc=<%=Request("opc")%>&odc3=<%=Request("odc3")%>&tlp=<%=Request("tlp")%>"><img src="images/btn_comenzar_ahora.jpg" border="0"></a> 
      </p>
    </td>
  </tr>
</table>

</body>
</html>
