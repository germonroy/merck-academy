<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
     %>
<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left ">
        <a href="./" class=" img-responsive visible-xs visible-sm visible-md visible-lg">
            <img src="images/logo_top.png" style="margin-bottom: 0px; margin-top: 5px;" /></a>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default navigation-menu" role="navigation">
        <div class="container" style="margin-top: 10px;">

            <!-- Right(Notification and Searchbox -->
            <ul class="nav navbar-nav navbar-right">

                <li style="margin-right: 10px;">
                    <h4><a href="index.html"><i class="fa fa-envelope-o "></i><span class="hidden-xs">Contacto </span></a></h4>
                </li>
                <li class="hidden-xs" style="margin-right: 10px;">
                    <h4><a href="javasctipt:void(0);" style="cursor: none;"><span style="cursor: default;">/ </span></a></h4>
                </li>

                <li>
                    <h4><a href="index.html"><i class="fa fa-question"></i><span class="hidden-xs">Soporte </span></a></h4>
                </li>

                <li class="hidden-xs" style="margin-right: 10px; margin-left: 10px;">
                    <h4><a href="javasctipt:void(0);" style="cursor: default;"><span style="cursor: default;">/ </span></a></h4>
                </li>
                <li style="margin-right: 10px;" class="hidden-xs">
                    <h4><a href="javasctipt:void(0);" style="cursor: default;">Siguenos: </a></h4>
                </li>

                <li style="margin-right: 10px;" class="hidden-xs">
                    <h4><a href="#"><i class="fa fa-facebook-official"></i></a></h4>
                </li>
                <li style="margin-right: 10px;" class="hidden-xs">
                    <h4><a href="#"><i class="fa fa-youtube-play "></i></a></h4>
                </li>

            </ul>

        </div>
        <!-- end container -->

    </div>
    <!-- end navbar -->
</div>
<!-- Top Bar End -->
