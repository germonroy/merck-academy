<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"
    color = ""
    if Request("idcurso") = 3 then
        color = "#feca0d"
        imagen= "mgeneral"
        btnclase = "btn-warning"
        iconclase = "icon-medicinaGeneral"
    elseif Request("idcurso") = 2 then
        color = "#0066b2"
        imagen= "minterna"
        btnclase = "btn-primary"
        iconclase = "icon-medicinaInterna"
    elseif Request("idcurso") = 4 then
        color = "#ec1a3b"
        imagen= "ortopedia"
        btnclase = "btn-danger"
        iconclase = "icon-ortopedia"
    else
        color = "#009a5a"
        imagen= "odontologia"
        btnclase = "btn-success"
        iconclase = "icon-odontologia"
    end if
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />
    <link href="css/bienvenida.css" rel="stylesheet" />
</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="container containerAcercaDe" style="margin-top: 5em;">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" align="center">
                        <div class="row">
                            <div class="col-md-12">
                                <div align="center">
                                    <img src="images/logo/logoMerck.png" class="img-responsive" style="margin-top: 18px; margin-bottom: 18px;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <div class="row" style="background-image: url(../images/fondos/plecaBlanca.png); background-size: 100% 100%;">
                    <div class="col-md-12" id="bg">
                        <p id="texto-reglas" style="text-align: justify">
                            <h2 style="text-align: center; margin-bottom: 1em;">WELCOME</h2>
                            El área médica de <b>MERCK y los expertos en dolor</b> le dan la más cordial bienvenida a <b>MERCK ACADEMY,</b> una plataforma virtual con lo más actual en contenidos médicos relacionados con su práctica diaria. Todo con un amplio rigor científico.<br />
                            En <b>MERCK</b> sabemos la importanccia de la actualización médica y con el afán de contribuir en el crecimiento profesional de los especialistas en tratar el dolor de la población ponemos a su disposición una serie de contenidos académicos en un sólo lugar.
                            <br />
                            Al ser miembro de <b>MERCK ACADEMY</b> podrá acceder a diversos materiales que van desde la lectura de un artículo escrito por un líder de opinión hasta ser elegido para cursar un programa completo de <b>actualización médica</b> con validez oficial. <b>Todos los contenidos están segmentados de acuerdo a su perfil profesional</b>, de este modo tendrá acceso inmediato a lo que usted necesita.<br />
                            Acceda ahora mismo a todos los contenidos que <b>MERCK ACADEMY</b> tiene para usted:
                            <ul>
                                <li>News</li>
                                <li>Articles</li>
                                <li>eBooks</li>
                                <li>Videos</li>
                                <li>Video conferences</li>
                                <li>Invitaciones a talleres presenciales</li>
                                <li>Online Courses</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
            <!--#include file="footer.asp" -->
        </div>
    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/MetroJs.min.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="js/logout.js"></script>

</body>
</html>
