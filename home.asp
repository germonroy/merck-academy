﻿<!--#include file="data/con_ma.inc" -->
<!--#include file="data/con_ma3.inc" -->
<!--#include file="includes/config.asp" -->
<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US

Set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3
    
'Porcentaje Medicina General
SQL =  "select [ma_15].[dbo].[tbl_usuarios].id_usuario, (select COUNT(*) from  [demo_merck_17].[dbo].tbl_modulos  where (id_curso =  3 " &_
     " or id_curso2 = 3 or id_curso3 = 3) and activo = 'SI') as num_modulos, (select coalesce(SUM([ma_15].[dbo].[progresos].porcentaje_vl)"&_
    ",0) as sumatotal from [ma_15].[dbo].[progresos] where id_usuario = " & Session("id_usuario") & " and id_modulo  in (select distinct" &_
    " id_modulo from [demo_merck_17].[dbo].[tbl_modulos] where (id_curso = 3 or id_curso2 = 3 or id_curso3 = 3) and activo = 'SI') ) as " &_
    "total_curso from  [ma_15].[dbo].[tbl_usuarios] where id_usuario = " & Session("id_usuario")
Cursor.Open SQL, StrConn2, 1, 3 
if Cursor.RecordCount > 0 then
    porcentajeMG = cint((Cursor("total_curso")/Cursor("num_modulos")))
    if porcentajeMG > 100 then
        porcentajeMG = 100
    end if
else
    porcentajeMG = 0
end if
Cursor.Close

'Porcentaje Medicina Interna
SQL =  "select [ma_15].[dbo].[tbl_usuarios].id_usuario, (select COUNT(*) from  [demo_merck_17].[dbo].tbl_modulos  where (id_curso =  2 " &_
     " or id_curso2 = 2 or id_curso3 = 2) and activo = 'SI') as num_modulos, (select coalesce(SUM([ma_15].[dbo].[progresos].porcentaje_vl)"&_
    ",0) as sumatotal from [ma_15].[dbo].[progresos] where id_usuario = " & Session("id_usuario") & " and id_modulo  in (select distinct" &_
    " id_modulo from [demo_merck_17].[dbo].[tbl_modulos] where (id_curso = 2 or id_curso2 = 2 or id_curso3 = 2) and activo = 'SI') ) as " &_
    "total_curso from  [ma_15].[dbo].[tbl_usuarios] where id_usuario = " & Session("id_usuario")
Cursor.Open SQL, StrConn2, 1, 3 
if Cursor.RecordCount > 0 then
    porcentajeMI = cint((Cursor("total_curso")/Cursor("num_modulos")))
    if porcentajeMI > 100 then
        porcentajeMI = 100
    end if
else
    porcentajeMI = 0
end if
Cursor.Close

'Porcentaje Ortopedia
SQL =  "select [ma_15].[dbo].[tbl_usuarios].id_usuario, (select COUNT(*) from  [demo_merck_17].[dbo].tbl_modulos  where (id_curso =  4 " &_
     " or id_curso2 = 4 or id_curso3 = 4) and activo = 'SI') as num_modulos, (select coalesce(SUM([ma_15].[dbo].[progresos].porcentaje_vl)"&_
    ",0) as sumatotal from [ma_15].[dbo].[progresos] where id_usuario = " & Session("id_usuario") & " and id_modulo  in (select distinct" &_
    " id_modulo from [demo_merck_17].[dbo].[tbl_modulos] where (id_curso = 4 or id_curso2 = 4 or id_curso3 = 4) and activo = 'SI') ) as " &_
    "total_curso from  [ma_15].[dbo].[tbl_usuarios] where id_usuario = " & Session("id_usuario")
Cursor.Open SQL, StrConn2, 1, 3 
if Cursor.RecordCount > 0 then
    porcentajeOR = cint((Cursor("total_curso")/Cursor("num_modulos")))
    if porcentajeOR > 100 then
        porcentajeOR = 100
    end if
else
    porcentajeOR = 0
end if
Cursor.Close

'Porcentaje Odontologia
SQL =  "select [ma_15].[dbo].[tbl_usuarios].id_usuario, (select COUNT(*) from  [demo_merck_17].[dbo].tbl_modulos  where (id_curso =  1 " &_
     " or id_curso2 = 1 or id_curso3 = 1) and activo = 'SI') as num_modulos, (select coalesce(SUM([ma_15].[dbo].[progresos].porcentaje_vl)"&_
    ",0) as sumatotal from [ma_15].[dbo].[progresos] where id_usuario = " & Session("id_usuario") & " and id_modulo  in (select distinct" &_
    " id_modulo from [demo_merck_17].[dbo].[tbl_modulos] where (id_curso = 1 or id_curso2 = 1 or id_curso3 = 1) and activo = 'SI') ) as " &_
    "total_curso from  [ma_15].[dbo].[tbl_usuarios] where id_usuario = " & Session("id_usuario")
Cursor.Open SQL, StrConn2, 1, 3 
if Cursor.RecordCount > 0 then
    porcentajeOD = cint((Cursor("total_curso")/Cursor("num_modulos")))
    if porcentajeOD > 100 then
        porcentajeOD = 100
    end if
else
    porcentajeOD = 0
end if
Cursor.Close

%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <meta name="author" content="Inkuba Project México" />
    <title>Merck Academy</title>
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#e74c3c">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core-default.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/menu-default.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/special-classes.css" rel="stylesheet" />
    <link href="assets/iconos-merck/style.css" rel="stylesheet" />
    <link href="assets/css/flipper.css" rel="stylesheet" />
    
    <!--#include file="includes/google_track.asp" -->

    <link href="assets/css/text-align.css" rel="stylesheet" />
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>

    <link href="assets/css/top_int.css" rel="stylesheet" />
    <link href="assets/css/sidebar.css" rel="stylesheet" />
    <link href="css/scrollbars.css" rel="stylesheet" />
    <link href="css/home-cus1.css" rel="stylesheet" />
    <link href="css/figure_set2.css" rel="stylesheet" />
    <link href="css/home-cus2.css" rel="stylesheet" />
    <link href="css/hover-min.css" rel="stylesheet" />
    <style>
        .frameHeight {
            min-height: 37vw;
        }

        .hugebtn {
            padding: 10px 60px !important;
            font-size: 80px;
        }
    </style>
    <link href="css/home.css" rel="stylesheet" />
    <style>
        .odon-text-color {
            color: #6ed4ff !important;
        }

        .ort-text-color:hover, .ort-text-color:active {
            background-color: #f10031 !important;
            color: white !important;
        }

        .mi-text-color:hover {
            background-color: #ffc83d !important;
            color: white !important;
        }

        .nav-tabs > li.active > a.ort-text-color {
            background-color: #f10031 !important;
            color: white !important;
        }

        @media only screen and (max-width : 992px) {
            .rec-img {
                opacity: 0.5 !important;
                margin-top: 0px;
            }

            .cajasvuelta {
                margin-bottom: 2vh;
            }

            .h2-rec {
                margin-top: -20px !important;
                font-size: 17px;
                line-height: 20px;
            }

            .h2-recomendac {
                font-size: 28px !important;
            }

            .btn-warning-cross, btn-warning-cross:hover, btn-warning-cross:active {
                background: linear-gradient(45deg, transparent 8px, #f9c851 0) bottom left;
            }

            .btn-primary-cross, btn-primary-cross:hover, btn-primary-cross:active {
                background: linear-gradient(45deg, transparent 8px, #188ae2 0) bottom left;
            }

            .btn-danger-cross, btn-danger-cross:hover, btn-danger-cross:active {
                background: linear-gradient(45deg, transparent 8px, #ff5b5b 0) bottom left;
            }

            .btn-info-cross, btn-info-cross:hover, btn-info-cross:active {
                background: linear-gradient(45deg, transparent 8px, #6ed4ff 0) bottom left;
            }

            .cursos-home {
                margin-top: -40px;
            }

            .logo-legales {
                max-height: 10px;
            }

            #frameVid {
                max-height: 55vw !important;
            }
        }

        hr {
            border-top: 2px solid #3266b1;
        }

        .hg-card {
            /*min-height: 50vw;*/
            background-color: rgba(255,255,255, 0.3) !important;
            padding: 10px;
        }
    </style>
</head>
<body>
    <div id="wrapper">
        <!--#include file="modal.asp" -->
        <!--#include file="header-home.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="content">
                <div class="container" style="width: auto">
                    <div class="row">
                        <div class="row text-center hidden-lg hidden-md" style="margin-top: 30px;">
                            <div class="col-xs-12 col-md-6" style="padding-bottom: 2em;">
                                <div class="col-xs-3 col-md-3 text-center center-block">
                                    <a id="tabCIsm" href="#" data-filter=".odon" type="button" class="btn btn-info-cross waves-effect waves-light m-b-5 tabcursos"><i class="icon-atomo"></i></a>
                                    <br />
                                    Curso Introductorio
                                </div>
                                <div class="col-xs-3 col-md-3 text-center center-block">
                                    <a id="tabMGsm" href="#" data-filter=".mg" type="button" class="btn btn-warning-cross waves-effect waves-light m-b-5 tabcursos"><i class="icon-medicinaGeneral"></i></a>
                                    <br />
                                    Medicina General
                                           
                                </div>
                                <div class="col-xs-3 col-md-3 text-center center-block">
                                    <a id="tabMIsm" href="#1" data-toggle="tab" type="button" class="btn btn-primary-cross waves-effect waves-light m-b-5 tabcursos"><i class="icon-medicinaInterna"></i></a>
                                    <br />
                                    Medicina Interna
                                </div>
                                <div class="col-xs-3 col-md-3 text-center center-block">
                                    <a id="tabORsm" href="#" data-filter=".orto" type="button" class="btn btn-danger-cross waves-effect waves-light m-b-5 tabcursos"><i class="icon-ortopedia"></i></a>
                                    <br />
                                    Ortopedia
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 pd-right-0">
                            <ul class="nav nav-tabs hidden-xs hidden-sm">
                                <li class="active tabs-cursos">
                                    <a id="tabCI" href="#1" data-toggle="tab" class="odon-text-color tabcursos"><i class="icon-cursoscertificacion" aria-hidden="true"></i>&nbsp;Curso Introductorio</a>
                                </li>
                                <li>
                                    <a id="tabMG" href="#1" data-toggle="tab" class="mi-text-color tabcursos"><i class="icon-medicinaGeneral" aria-hidden="true"></i>&nbsp;Medicina General</a>
                                </li>
                                <li><a id="tabMI" href="#1" data-toggle="tab" class="md-text-color tabcursos"><i class="icon-medicinaInterna" aria-hidden="true"></i>&nbsp;Medicina Interna</a>
                                </li>
                                <li><a id="tabOR" href="#1" data-toggle="tab" class="ort-text-color tabcursos"><i class="icon-ortopedia" aria-hidden="true"></i>&nbsp;Ortopedia</a>
                                </li>
                            </ul>
                            <div class="tab-content ">
                                <!--Videos-->
                                <div class="tab-pane active" id="1">
                                    <div class="row">
                                        <div class="col-md-12" id="frameVideos" style="cursor: pointer;">
                                            <img class="col-md-12 img-responsive" id="posterintro" src="images/posters/cIntroductorio.jpg" />
                                            <iframe id="frameVid" src="" class="frameHeight" width="100%" height="600px" frameborder="0"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="5">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <iframe src="avales.asp?cur=2" class="frameHeight" width="100%" height="600px" frameborder="0"></iframe>
                                            <!--<iframe src="" class="frameHeight" width="100%" height="600px" frameborder="0"></iframe>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!--<iframe src="temario.asp?cur=2" id="frameTemario" class="frameHeight" width="100%" height="600px" frameborder="0"></iframe>-->
                                            <iframe src="" id="frameTemario" class="frameHeight" width="100%" height="600px" frameborder="0"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="7">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!--<iframe src="profesores.asp?cur=2" id="frameProf" class="frameHeight" width="100%" height="600px" frameborder="0"></iframe>-->
                                            <iframe src="" id="frameProf" class="frameHeight" width="100%" height="600px" frameborder="0"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2 pd-left-0 hidden-sm hidden-xs" style="min-height: 49px;"></div>
                        <div class="col-md-2 pd-left-0 menu-merck">
                            <div class="col-md-12 col-sm-2 col-xs-2 mini-box pd-right-0 strong-apollo tabcursos menuTemario">
                                <a href="#6" data-toggle="tab">
                                    <h1 class="h1-boxes"><i class="icon-cursos"></i></h1>
                                    <h4 class="h4-boxes">&nbsp;Temario</h4>
                                </a>
                            </div>
                            <div class="col-md-12 col-sm-2 col-xs-2 mini-box pd-right-0 strong-apollo tabcursos menuProfesores">
                                <a href="#7" data-toggle="tab" class="btnProf">
                                    <h1 class="h1-boxes"><i class="icon-profesores"></i></h1>
                                    <h4 class="h4-boxes">&nbsp;Profesores</h4>
                                </a>
                            </div>
                            <div class="col-md-12 col-sm-2 col-xs-2 mini-box pd-right-0 strong-apollo tabcursos">
                                <a href="#5" data-toggle="tab">
                                    <h1 class="h1-boxes"><i class="icon-avales h1-boxes"></i></h1>
                                    <h4 class="h4-boxes">&nbsp;Avales</h4>
                                </a>
                            </div>
                            <div id="miProgreso" class="col-md-12 col-sm-2 col-xs-2 mini-box pd-right-0">
                            </div>
                            <div class="col-md-12 col-sm-2 col-xs-2 mini-box pd-right-0">
                                <h1 class="h1-boxes" id="numHoras">16</h1>
                                <h4 class="h4-boxes">&nbsp;Horas</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 2vw;">
                        <div class="col-md-6 hg-card">
                            <div class="col-md-12">
                                <h2><i class="fa icon-cursoscertificacion strong-look" aria-hidden="true"></i><span class="strong-look">&nbsp;MIS CURSOS</span></h2>
                                <hr />
                            </div>
                            <%
                            numeroRec = 0
                            For i = 1 To 4
                            select case i
                                case 3 
                                    clase = "warning"
                                case 2      
                                    clase = "default"
                                case 4
                                    clase = "danger"
                                case 5
                                    clase = "success"             
                            end select

                            'SQL =  "select [ma_15].[dbo].[tbl_usuarios].id_usuario, (select COUNT(*) from  [demo_merck_17].[dbo].tbl_modulos  where (id_curso = " & i & " or id_curso2 = " & i & " or id_curso3 = " & i & ") and activo = 'SI') as num_modulos, (select coalesce(SUM([ma_15].[dbo].[progresos].porcentaje_vl),0) as sumatotal from [ma_15].[dbo].[progresos] where id_usuario = " & Session("id_usuario") & " and id_modulo  in (select distinct id_modulo from [demo_merck_17].[dbo].[tbl_modulos] where (id_curso = " & i & " or id_curso2 = " & i & " or id_curso3 = " & i & ") and activo = 'SI') ) as total_curso from [ma_15].[dbo].[tbl_usuarios] where id_usuario = " & Session("id_usuario")

                            SQL = "select top 1 * from progresos where id_usuario = " & Session("id_usuario") & " and id_curso = "& i &" and id_modulo not in (1,2,3) order by fecha_actualizacion desc, hora_actualizacion desc"
                            Cursor.Open SQL, StrConn2, 1, 3 
                            if Cursor.RecordCount = 0 then
                                progreso = "no"
                            else
                                ultimoModulo = Cursor("id_modulo")
                                progreso = "si"
                            end if
                            Cursor.Close

                            if progreso = "si" then
                                SQL = "SELECT porcentaje_vl FROM [ma_15].[dbo].progresos where id_modulo = " & ultimoModulo & " and id_usuario = " & Session("id_usuario") 
                                Cursor.Open SQL, StrConn2, 1, 3 
                                If Cursor.RecordCount > 0 then
                                    porcentaje  = cint(Cursor("porcentaje_vl"))
                                Else
                                    porcentaje = 0
                                End If
                                Cursor.Close

                                numeroRec = numeroRec + 1
                                SQL = "select * from tbl_modulos where id_modulo = " & ultimoModulo
                                Cursor.Open SQL, StrConn, 1, 3 
                                    nombreModulo = Cursor("titulo")
                            %>
                            <div class="col-md-12 card-box text-center">
                                <figure class="effect-honey">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5 col-xs-12 text-sm-center text-xs-center crop-cursos">
                                            <img src="images/videoteca/modulos/<%=ultimoModulo %>.jpg" class="img-responsive cursos-home" />
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-12">
                                            <h3 class="text-left">Progreso</h3>
                                            <h4 class="text-left"><b>Módulo: </b><%=nombreModulo %>.</h4>
                                            <% if Cursor("total_videos") <> 0 then  %>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                <b><span class="icon-videoCirugias iconos-contenido"></span>&nbsp;<%=Cursor("total_videos") %></b><br />
                                                <small>Videos</small>
                                            </div>
                                            <% end if
                                        if Cursor("total_interactivos") <> 0 then  %>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                <b><span class="icon-interactivos iconos-contenido"></span>&nbsp;<%=Cursor("total_interactivos") %></b><br />
                                                <small>Interactivos</small>
                                            </div>
                                            <% end if
                                        if Cursor("total_rv") <> 0 then 
                                            %>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                <b><span class="icon-revisioDelMes iconos-contenido"></span>&nbsp;<%=Cursor("total_rv") %></b><br />
                                                <small>Revisiones</small>
                                            </div>
                                            <% end if %>
                                            <%If Cursor("total_lec") >  0 then %>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                <b><span class="icon-lecturaguiada-02 iconos-contenido"></span>&nbsp;<%=Cursor("total_lec") %></b><br />
                                                <small>Lecturas</small>
                                            </div>
                                            <%End If %>
                                            <%If Cursor("total_tareas") >  0 then %>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 iconosCardbox">
                                                <b><span class="icon-tarea iconos-contenido"></span>&nbsp;<%=Cursor("total_tareas") %></b><br />
                                                <small>Tareas</small>
                                            </div>
                                            <%End If %>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-<%=clase %> progress-bar-striped active" role="progressbar" aria-valuenow="<%=porcentaje%>" aria-valuemin="0" aria-valuemax="100" style="width: <%=porcentaje%>%">
                                                            <%=porcentaje%>%
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 hidden-xs">
                                            <a href="view_cont_cert.asp?tlpmod=<%=ultimoModulo %>&idcurso=<%=i%>&retcurso=<%=i%>">
                                                <img src="assets/images/btn/<%=i%>.jpg" class="img-responsive continuar-btn" />
                                            </a>
                                        </div>
                                        <div class="col-xs-12 hidden-lg hidden-md hidden-sm m-t-10">
                                            <a href="view_cont_cert.asp?tlpmod=<%=ultimoModulo %>&idcurso=<%=i%>&retcurso=<%=i%>" class="btn btn-block btn-danger">Continuar</a>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                            <% Cursor.Close
                            end if
                            Next %>
                        </div>
                        <!--<div class="col-md-1"></div>-->
                        <div class="col-md-6 hg-card">
                            <div class="col-md-12">
                                <h2 class="h2-recomendac"><i class="fa fa-list strong-look" aria-hidden="true"></i><span class="strong-look">&nbsp;RECOMENDACIONES</span></h2>
                                <hr />
                            </div>
                            <%  j = 1
                                 SQL = "select top "&numeroRec&" *, (select titulo from tbl_videos where id_video = tbl_top_contenido.id_tipo) as titulo, " &_
                                "(select duracion from tbl_videos where id_video = tbl_top_contenido.id_tipo) as duracion, " &_
                                "(select id_modulo from tbl_videos where id_video = tbl_top_contenido.id_tipo) as modulo, " &_
                                "(select id_curso from tbl_videos where id_video = tbl_top_contenido.id_tipo) as curso, " &_
                                "(select (titulo + ' ' + nombre) from cat_ponentes where id_ponente in (select top 1 id_ponente from " &_
                                " tbl_videos where tbl_videos.id_video = tbl_top_contenido.id_tipo)) as ponente " &_
                                "from tbl_top_contenido where tipo = 'V' and localizacion = 'C' and activo = 'SI' order by newid()"
                            Cursor.Open SQL, StrConn, 1, 3
                            Do While Not Cursor.EOF %>
                            <div class="col-md-12 card-box text-center">
                                <div class="grid">
                                    <figure class="effect-julia rec-box">
                                        <%select case Cursor("tipo")
                                            case "V"
                                                textoNov = "Videolección"
                                                imagenNov = "<img src='images/videoteca/videos/"&Cursor("id_tipo")&".jpg' class='rec-img' />"
                                            case "R"
                                                textoNov = "Revisión"
                                                imagenNov = "<img src='images/videoteca/revisiones/"&Cursor("id_tipo")&".png' class='rec-img' />"
                                            case "L"
                                                textoNov = "Lectura guiada"
                                                imagenNov = "<img src='images/videoteca/revisiones/"&Cursor("id_tipo")&".png' class='rec-img' />"
                                            end select %>
                                        <%=imagenNov %>
                                        <figcaption class="splat">
                                            <h2 class="h2-rec" style="text-transform: none; letter-spacing: 1px; font-size: 24px;"><strong>&nbsp;<%=textoNov %>:&nbsp;</strong><%=Cursor("titulo") %></h2>
                                            <!--<div>
                                                <a><%=Cursor("texto") %></a>
                                            </div>-->
                                            <a href="view_cont_cert.asp?tlpmod=<%=Cursor("modulo") %>&idcurso=<%=Cursor("curso") %>&retcurso=<%=Cursor("curso") %>">Ver más</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <%Cursor.MoveNext
                                j = j + 1
                              Loop
                              Cursor.Close %>
                        </div>
                    </div>
                    <div class="row hidden" id="boxes-con">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch9" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/1.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/1_vuelta.jpg" alt="4">
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-acercade" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch10" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/2.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/2_vuelta.jpg" alt="4">
                                        <a href="avales.asp" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch1" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/3.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/3_vuelta.jpg" alt="4">
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-cursosEspecialidad" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch3" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/4.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/4_vuelta.jpg" alt="4">
                                        <a href="vid_cirugias.asp" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch8" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/5.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/5_vuelta.jpg" alt="4">
                                        <a href="conf_esp.asp" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch2" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/6.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/6_vuelta.jpg" alt="4">
                                        <a href="rev_mes.asp" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch4" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/7.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/7_vuelta.jpg" alt="4">
                                        <a href="ebooks.asp" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 cajasvuelta">
                            <div id="touch6" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="assets/images/contenido/8.jpg" alt="3">
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="assets/images/contenido/8_vuelta.jpg" alt="4">
                                        <a href="profesores.asp" class="btn btn-default btn-show">Mostrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--#include file="footer-home.asp" -->

                    <div id="full-width-modal-cursosEspecialidad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content p-0 b-0">
                                <div class="panel panel-color panel-primary">
                                    <div class="heading" style="background: #3266b1; padding: 10px;">
                                        <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                                            ×</button>
                                        <h3 class="panel-title">&nbsp;Seleccionar Especialidad</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row center-block text-center">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=3" data-filter=".mg" type="button" class="btn hugebtn btn-warning-cross btnlgmg btn-lg waves-effect waves-light m-b-5"><i class="icon-medicinaGeneral"></i></a>
                                                <br />
                                                Medicina General
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=2" data-filter=".mi" type="button" class="btn hugebtn btn-primary-cross btnlgmi btn-lg waves-effect waves-light m-b-5"><i class="icon-medicinaInterna"></i></a>
                                                <br />pro
                                                Medicina Interna
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=4" data-filter=".orto" type="button" class="btn hugebtn btn-danger-cross btnlgor btn-lg waves-effect waves-light m-b-5"><i class="icon-ortopedia"></i></a>
                                                <br />
                                                Ortopedia
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=5" data-filter=".odon" type="button" class="btn hugebtn btn-success-cross btnlgod btn-lg waves-effect waves-light m-b-5"><i class="icon-odontologia"></i></a>
                                                <br />
                                                Odontología
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="full-width-modal-acercade" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content p-0 b-0">
                                <div class="panel panel-color panel-primary">
                                    <div class="heading" style="background: #3266b1; padding: 10px;">
                                        <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                                            ×</button>
                                        <h3 class="panel-title">&nbsp;Acerca de Merck Academy</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row center-block text-center">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="mision.asp">
                                                    <img src="images/iconos/mision.png" class="img-responsive center-block text-center" />
                                                </a>
                                                Misión
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="vision.asp">
                                                    <img src="images/iconos/vision.png" class="img-responsive center-block text-center" />
                                                </a>
                                                Visión
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="valores.asp">
                                                    <img src="images/iconos/valores.png" class="img-responsive center-block text-center" />
                                                </a>
                                                Valores
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>



    <script>
            var resizefunc = [];
    </script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- KNOB JS -->
    <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>


    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#frameVid').hide();
            $('[data-toggle="popover"]').popover();
            <%if porcentajeMG = 0 then%>
            $("#miProgreso").html("<h4 class='h4-boxes h1-color-success'>&nbsp;No iniciado</h4>");
            <%else%>
            $("#miProgreso").html("<h1 class='mi-text-color h1-boxes h1-color-success'><%=porcentajeOD%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
            <%end if%>
        });
        $(function () {
            $('[rel="popover"]').popover({
                container: 'body',
                html: true,
                placement: "left",
                content: function () {
                    var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
                    return clone;
                }
            }).click(function (e) {
                e.preventDefault();
            });
        });

        $(document.body).on('click', '.tabcursos', function (e) {
            $('#posterintro').show();
            $('#frameVid').attr('src', "blank.htm");
            $('#frameVid').hide();
            var tabsel = $(this).attr("id");
            changeVid(tabsel);
        });

        function changeVid(tabsel) {
            if ((tabsel == "tabMG")|| (tabsel == "tabMGsm")) {
                $('#posterintro').attr('src', "images/posters/mGeneral.jpg");
                $('#frameTemario').attr('src', "temario.asp?cur=3");
                $('#frameProf').attr('src', "profesores.asp?cur=3");
                <%if porcentajeMG = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-warning'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-boxes h1-color-warning'><%=porcentajeMG%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("16")
            }
            if ((tabsel == "tabMI")|| (tabsel == "tabMIsm")) {
                $('#posterintro').attr('src', "images/posters/mInterna.jpg");
                $('#frameTemario').attr('src', "temario.asp?cur=2");
                $('#frameProf').attr('src', "profesores.asp?cur=2");
                <%if porcentajeMI = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-default'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-boxes h1-color-default'><%=porcentajeMI%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("18")
            }
            if ((tabsel == "tabOR")|| (tabsel == "tabORsm")) {
                $('#posterintro').attr('src', "images/posters/Ortopedia.jpg");
                $('#frameTemario').attr('src', "temario.asp?cur=4");
                $('#frameProf').attr('src', "profesores.asp?cur=4");
                <%if porcentajeOR = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-danger'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-boxes h1-color-danger'><%=porcentajeOR%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("31")
            }
            if ((tabsel == "tabCI")|| (tabsel == "tabCIsm")) {
                $('#posterintro').attr('src', "images/posters/cIntroductorio.jpg");
                $('#frameTemario').attr('src', "temario.asp?cur=1");
                $('#frameProf').attr('src', "profesores.asp?cur=5");
                <%if porcentajeOD = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-success'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-color-success h1-boxes'><%=porcentajeOD%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("2")
            }
        }

        $(document.body).on('click', '#posterintro', function (e) {
            if ($('#posterintro').attr('src') == 'images/posters/mGeneral.jpg'){
                $('#frameVid').attr('src', "https://view.knowledgevision.com/presentation/2033c43d2a94453594a3db7f64997060");                
            }
            if ($('#posterintro').attr('src') == 'images/posters/mInterna.jpg'){
                $('#frameVid').attr('src', "https://view.knowledgevision.com/presentation/def3152558974d30ae8effc2ad4a50b7");                
            }
            if ($('#posterintro').attr('src') == 'images/posters/Ortopedia.jpg'){
                $('#frameVid').attr('src', "https://view.knowledgevision.com/presentation/220ead2f0d2d4a0999cae93da63c4428");
            }
            if ($('#posterintro').attr('src') == 'images/posters/cIntroductorio.jpg'){
                $('#frameVid').attr('src', "https://view.knowledgevision.com/presentation/443d2a78583949e1899e255220c1d79b");
            }
            $('#posterintro').hide();
            $('#frameVid').show();
        });

        $(document.body).on('click', '.menuTemario', function (e) {
            var sn = $('#frameTemario').attr('src');
            if ((sn == null) || (sn == ""))  {
                //alert("null");
                $('#frameTemario').attr('src', "temario.asp?cur=1");
            }
        });

        $(document.body).on('click', '.menuProfesores', function (e) {
            //alert("Rev");
            var sn = $('#frameProf').attr('src');
            if ((sn == null) || (sn == ""))  {
                //alert("null");
                $('#frameProf').attr('src', "profesores.asp?cur=1");
            }
        });

        function playVid(tabsel) {
            if ((tabsel == "tabMG")|| (tabsel == "tabMGsm")) {
                contenido="<video id='vidMI' width='100%' controls='controls' onclick='this.play();' poster='images/posters/mGeneral.jpg' preload><source src='videos/Promo_Medicina_General_musica_C2.mp4' type='video/mp4'><source src='videos/Promo_Medicina_General_musica_C2.webm' type='video/webm'><source src='videos/Promo_Medicina_General_musica_C2.ogv' type='video/ogv'>Your browser does not support HTML5 video.</video>"
                $('#frameVideos').html(contenido);
                var vid = document.getElementById("vidMG");
                $('#frameTemario').attr('src', "temario.asp?cur=3");
                $('#frameProf').attr('src', "profesores.asp?cur=3");
                <%if porcentajeMG = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-warning'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-boxes h1-color-warning'><%=porcentajeMG%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("16")
                }
            if ((tabsel == "tabMI") || (tabsel == "tabMIsm")) {
                contenido="<video id='vidMI' width='100%' controls='controls' onclick='this.play();' poster='images/posters/mInterna.jpg' preload><source src='videos/Promo_Medicina_Interna_musica_C4.mp4' type='video/mp4'><source src='videos/Promo_Medicina_Interna_musica_C4.webm' type='video/webm'><source src='videos/Promo_Medicina_Interna_musica_C4.ogv' type='video/ogv'>Your browser does not support HTML5 video.</video>"
                $('#frameVideos').html(contenido);
                var vid = document.getElementById("vidMI");
                $('#frameTemario').attr('src', "temario.asp?cur=2");
                $('#frameProf').attr('src', "profesores.asp?cur=2");
                <%if porcentajeMI = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-default'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-boxes h1-color-default'><%=porcentajeMI%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("18")
                }
            if ((tabsel == "tabOR") || (tabsel == "tabORsm")) {
                contenido="<video id='vidOR' width='100%' controls='controls' onclick='this.play();' poster='images/posters/Ortopedia.jpg' preload><source src='videos/Promo_Ortopedia_musica_C2.mp4' type='video/mp4'><source src='videos/Promo_Ortopedia_musica_C2.webm' type='video/webm'><source src='videos/Promo_Ortopedia_musica_C2.ogv' type='video/ogv'>Your browser does not support HTML5 video.</video>"
                $('#frameVideos').html(contenido);
                var vid = document.getElementById("vidOR");
                $('#frameTemario').attr('src', "temario.asp?cur=4");
                $('#frameProf').attr('src', "profesores.asp?cur=4");
                <%if porcentajeOR = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-danger'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-boxes h1-color-danger'><%=porcentajeOR%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("31")
                }
            if ((tabsel == "tabCI") || (tabsel == "tabCIsm")) {
                contenido="<video id='vidMG' width='100%' controls='controls' onclick='this.play();' poster='images/posters/cIntroductorio.jpg' preload><source src='videos/Promo_Introductorio_musica.mp4' type='video/mp4'><source src='videos/Promo_Introductorio_musica.webm' type='video/webm'><source src='videos/Promo_Introductorio_musica.ogv' type='video/ogv'>Your browser does not support HTML5 video.</video>"
                $('#frameVideos').html(contenido);
                var vid = document.getElementById("vidOD");
                $('#frameTemario').attr('src', "temario.asp?cur=5");
                $('#frameProf').attr('src', "profesores.asp?cur=5");
                <%if porcentajeOD = 0 then%>
                $("#miProgreso").html("<h4 class='h4-boxes h1-color-success'>&nbsp;No iniciado</h4>");
                <%else%>
                $("#miProgreso").html("<h1 class='mi-text-color h1-color-success h1-boxes'><%=porcentajeOD%>%</h1><h4 class='h4-boxes'>&nbsp;Mi Progreso</h4>");
                <%end if%>
                $("#numHoras").html("2")
                }
            //vid.play();
        }

    </script>
</body>
</html>
