<!--#include file="data/con_ma.inc" -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--======= Helping Plug-in StyleSheets =========-->
    <!--<link href="css/bootstrap.min.css" rel="stylesheet" media="all">-->
    <!--<link href="assets/css/bootstrap-carousel.min.css" rel="stylesheet" />-->
    <link href="assets/css/bootstrap-carousel.min.css" rel="stylesheet" />
    <!--======= Responsive Bootstrap Carousel StyleSheets =========-->
    <link href="css/range_slides_carousel_relacionado.css" rel="stylesheet" />
    <style>
        .range_slides_carousel_wrapper {
            padding: 0px;
        }

        .range_slides_carousel_wrapper {
            background: #fff;
        }

        .lec-left {
            position: absolute;
            color: #007dea;
            margin-left: -4%;
        }

        .lec-right {
            position: absolute;
            left: 92%;
            color: #007dea;
        }

        .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next {
            margin-right: 0px;
        }

        @media only screen and (width : 992px) {
            .flecha-izq {
                margin-left: -70px;
            }

            /*.carousel-inner > .item {
                margin-left: 10px !important;
            }

            .carousel-inner > .item:active {
                margin-left: 10px !important;
            }*/

            .col-md-3 {
                width: 31.333333%;
            }

            .thumb-img {
                margin-left: 0px;
                margin-right: 0px;
            }
        }
    </style>
</head>
<%
Session.LCID = 1033   

set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

filtro = Request("filtro")
activeAUX = "active"
%>
<body>
    <%
if filtro = "" then
'Response.Write("entro")
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images  from tbl_images where id_imagen = "&Request("ID")&""
Cursor.Open SQL, StrConn, 1, 3
    %>
    <div id="adv_range_4_columns_carousel" class="carousel slide four_shows_one_move range_slides_carousel_wrapper" data-ride="carousel" data-interval="false" data-pause="hover">
        <div class="carousel-inner range_slides_carousel_inner" role="listbox">
            <div class="carousel-inner">
                <!--Revisiones del mes-->
                <% if isNull(Cursor("ids_fasciculos")) then
                else
                SQL2 = "select top 5 * from tbl_fasciculos where id_fasciculos in (" & Cursor("ids_fasciculos") &") and activo='SI' and disponible= 'SI' order by newid()"
                Cursor2.Open SQL2, StrConn, 1, 3
                if Cursor2.RecordCount > 0 then
                Do While Not Cursor2.EOF
                %>
                <div class="item fasifil <%=activeAUX %>" data-category="fasifil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop btn-revision" onclick="parent.parent.InteractivosFrame.location='fasciculos/<%=Cursor2("id_fasciculos") %>/?idf=<%=Cursor2("id_fasciculos") %>';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>';">
                                <img src="images/videoteca/revisiones/<%=Cursor2("id_fasciculos") %>.png" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="work-thumbnail">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
                activeAUX = ""
                Loop
                end if
                Cursor2.Close
            end if%>

                <!--Ebooks-->
                <% if isNull(Cursor("ids_libros")) then
                else
                SQL2="select top 5 * from tbl_libros_revistas where id_libro in (" & Cursor("ids_libros") &") and activo='SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
Do While Not Cursor2.EOF
                %>
                <div class="item librosfil <%=activeAUX %>" data-category="librosfil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop" onclick="parent.parent.InteractivosFrame.location='ebooks/<%=Cursor2("id_libro") %>/?idf=<%=Cursor2("id_libro") %>';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                                <img src="images/videoteca/ebooks/<%=Cursor2("id_libro") %>.png" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="work-thumbnail">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
activeAUX = ""
Loop
Cursor2.Close
end if%>

                <!--Dragables-->
                <% if isNull(Cursor("ids_dragables")) then
                else
                SQL2="select top 5 * from tbl_dragables where id_dragable in (" & Cursor("ids_dragables") &") and activo='SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
Do While Not Cursor2.EOF
                %>
                <div class="item drafil <%=activeAUX %>" data-category="drafil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop" onclick="parent.parent.InteractivosFrame.location='dragables/<%=Cursor2("id_dragable") %>/?iddrag=<%=Cursor2("id_dragable") %>';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                                <img src="images/videoteca/interactivos/<%=Cursor2("id_dragable") %>.jpg" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="<%=Cursor2("id_dragable") %>">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
activeAUX = ""
Loop
Cursor2.Close
end if%>

                <!--Imagenes-->
                <%  if isNull(Cursor("ids_images")) then
                else
                SQL2 = "select top 5 * from tbl_images where id_imagen in (" & Cursor("ids_images") &") and activo='SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
Do While Not Cursor2.EOF
                %>
                <div class="item imagefil <%=activeAUX %>" data-category="imagefil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop" onclick="parent.parent.InteractivosFrame.location='images/videoteca/bancoimagenes/fullimage/<%=Cursor2("id_imagen") %>.jpg';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                                <img src="images/videoteca/bancoimagenes/<%=Cursor2("id_imagen") %>.jpg" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="<%=Cursor2("id_imagen") %>">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
activeAUX = ""
Loop
Cursor2.Close
end if%>
            </div>
            <!-- Left and right controls -->
            <a class="lec-left carousel-control flecha-izq" href="#adv_range_4_columns_carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="lec-right carousel-control" href="#adv_range_4_columns_carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <% Cursor.Close
end if 'Si no hay filtro %>

        <%if filtro = "revision" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images  from tbl_images where id_imagen = "&Request("ID")&""
Cursor.Open SQL, StrConn, 1, 3
        %>
        <div id="adv_range_4_columns_carousel" class="carousel slide four_shows_one_move range_slides_carousel_wrapper" data-ride="carousel" data-interval="false" data-pause="hover">
            <div class="carousel-inner range_slides_carousel_inner" role="listbox">
                <!--Revisiones del mes-->
                <% if isNull(Cursor("ids_fasciculos")) then
                else
                SQL2 = "select top 5 * from tbl_fasciculos where id_fasciculos in (" & Cursor("ids_fasciculos") &") and activo='SI' and disponible= 'SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
if Cursor2.RecordCount > 0 then
Do While Not Cursor2.EOF
                %>
                <div class="item fasifil <%=activeAUX %>" data-category="fasifil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop" onclick="parent.parent.InteractivosFrame.location='fasciculos/<%=Cursor2("id_fasciculos") %>/?idf=<%=Cursor2("id_fasciculos") %>';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                                <img src="images/videoteca/revisiones/<%=Cursor2("id_fasciculos") %>.png" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="work-thumbnail">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
activeAUX = ""
Loop
end if
Cursor2.Close
end if%>
            </div>

            <!-- Left and right controls -->
            <a class="lec-left carousel-control flecha-izq" href="#adv_range_4_columns_carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="lec-right carousel-control flecha-der" href="#adv_range_4_columns_carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <% Cursor.Close
end if %>


        <%if filtro = "ebooks" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images  from tbl_images where id_imagen = "&Request("ID")&""
Cursor.Open SQL, StrConn, 1, 3
        %>
        <div id="adv_range_4_columns_carousel" class="carousel slide four_shows_one_move range_slides_carousel_wrapper" data-ride="carousel" data-interval="false" data-pause="hover">
            <div class="carousel-inner range_slides_carousel_inner" role="listbox">

                <!--Ebooks-->
                <% if isNull(Cursor("ids_libros")) then
                else
                SQL2="select top 5 * from tbl_libros_revistas where id_libro in (" & Cursor("ids_libros") &") and activo='SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
Do While Not Cursor2.EOF
                %>
                <div class="item librosfil <%=activeAUX %>" data-category="librosfil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop" onclick="parent.parent.InteractivosFrame.location='ebooks/<%=Cursor2("id_libro") %>/?idf=<%=Cursor2("id_libro") %>';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                                <img src="images/videoteca/ebooks/<%=Cursor2("id_libro") %>.png" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="work-thumbnail">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
activeAUX = ""
Loop
Cursor2.Close
end if%>
            </div>

            <!-- Left and right controls -->
            <a class="lec-left carousel-control flecha-izq" href="#adv_range_4_columns_carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="lec-right carousel-control flecha-der" href="#adv_range_4_columns_carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <% Cursor.Close
end if %>

        <%if filtro = "interactivos" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images  from tbl_images where id_imagen = "&Request("ID")&""
Cursor.Open SQL, StrConn, 1, 3
        %>
        <div id="adv_range_4_columns_carousel" class="carousel slide four_shows_one_move range_slides_carousel_wrapper" data-ride="carousel" data-interval="false" data-pause="hover">
            <div class="carousel-inner range_slides_carousel_inner" role="listbox">

                <!--Dragables-->
                <% if isNull(Cursor("ids_dragables")) then
                else
                SQL2="select top 5 * from tbl_dragables where id_dragable in (" & Cursor("ids_dragables") &") and activo='SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
Do While Not Cursor2.EOF
                %>
                <div class="item drafil <%=activeAUX %>" data-category="drafil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop" onclick="parent.parent.InteractivosFrame.location='dragables/<%=Cursor2("id_dragable") %>/?iddrag=<%=Cursor2("id_dragable") %>';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                                <img src="images/videoteca/interactivos/<%=Cursor2("id_dragable") %>.jpg" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="<%=Cursor2("id_dragable") %>">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
activeAUX = ""
Loop
Cursor2.Close
end if%>
            </div>
            <!-- Left and right controls -->
            <a class="lec-left carousel-control flecha-izq" href="#adv_range_4_columns_carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="lec-right carousel-control" href="#adv_range_4_columns_carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <% Cursor.Close
end if %>

        <%if filtro = "imagenes" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images  from tbl_images where id_imagen = "&Request("ID")&""
Cursor.Open SQL, StrConn, 1, 3
        %>
        <div id="adv_range_4_columns_carousel" class="carousel slide four_shows_one_move range_slides_carousel_wrapper" data-ride="carousel" data-interval="false" data-pause="hover">
            <div class="carousel-inner range_slides_carousel_inner" role="listbox">
                <!--Imagenes-->
                <%  if isNull(Cursor("ids_images")) then
                else
                SQL2 = "select top 5 * from tbl_images where id_imagen in (" & Cursor("ids_images") &") and activo='SI' order by newid()"
                Cursor2.Open SQL2, StrConn, 1, 3
                Do While Not Cursor2.EOF
                %>
                <div class="item imagefil <%=activeAUX %>" data-category="imagefil">
                    <figure class="effect-honey">
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="btnscrolltop btn-hide" onclick="parent.parent.InteractivosFrame.location='images/videoteca/bancoimagenes/fullimage/<%=Cursor2("id_imagen") %>.jpg';parent.document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>';">
                                <img src="images/videoteca/bancoimagenes/<%=Cursor2("id_imagen") %>.jpg" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="<%=Cursor2("id_imagen") %>">
                            </a>
                        </div>
                    </figure>
                </div>
                <% Cursor2.MoveNext
            activeAUX = ""
            Loop
            Cursor2.Close
            end if%>
            </div>
            <!-- Left and right controls -->
            <a class="lec-left carousel-control flecha-izq" href="#adv_range_4_columns_carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="lec-right carousel-control" href="#adv_range_4_columns_carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <% Cursor.Close
end if %>
        <!--======= jQuery =========-->
        <script src="js/jquery-1.12.4.min.js"></script>
        <!--======= Bootstrap =========-->
        <script src="js/bootstrap.min.js"></script>
        <!--======= Touch Swipe =========-->
        <script src="js/jquery.touchSwipe.min.js"></script>
        <!--======= Customize =========-->
        <script src="js/responsive_bootstrap_carousel.js"></script>

        <script>
            $(document.body).on('click', '.btnscrolltop', function (e) {
                $("html, body",window.parent.document).animate({ scrollTop: 0 }, 600);
                $(".yt-look-alike",window.parent.document).hide();
                return false;
            }); 
        </script>
</body>
</html>
