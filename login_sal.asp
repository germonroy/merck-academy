﻿<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
     
       <style>
           html {
               background-color: white;
               background: url(images/fondos/fondointerior3.jpg) no-repeat center center fixed;
               -webkit-background-size: cover;
               -moz-background-size: cover;
               -o-background-size: cover;
               background-size: cover;
               font-family: Verdana, Geneva, sans-serif;
               margin: 0;
               color: #797979;
           }

           body {
               background-color: transparent;
           }

           #bg {
               background-image: url(images/fondos/plecaBlanca.png);
               background-size: 100% 100%;
           }
       </style>
</head>
<body class="fixed-left">
    <!--#include file="top_home.asp" -->
    <main class="page-content">
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row hidden-xs">
                        <div class="col-md-12">
                            <div align="center">
                                <img src="images/logo/logoMerck.png" class="img-responsive" style="margin-top: 18px; margin-bottom: 18px;" />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="bg">
                        <form data-toggle="validator" role="form" id="registro" method="post" action="jq/jq_login.asp">
                            <div class="col-md-4"></div>
                            <div class="form-group col-md-4" style="margin-top: 4em; margin-bottom: 4em;">
                                <div class="row">
                                    <div class="col-md-1" align="center"><i class="fa fa-user fa-3x" aria-hidden="true" style="color: #3266b1;"></i></div>
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Ingrese su usuario o correo electr&oacute;nico" data-parsley-error-message="Complete su correo electrónico. Gracias." data-parsley-type="email" required>
                                            <div class="help-block with-errors"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1" align="center"><i class="fa fa-lock fa-3x" aria-hidden="true" style="color: #3266b1;"></i></div>
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Ingrese su contrase&ntilde;a" data-parsley-error-message="Escriba su contraseña. Gracias." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-11"><a href="ui-widgets.html">¿Olvid&oacute; su contrase&ntilde;a?</a></div>
                                </div>
                                <br />
                                <br />
                                <div class="row">
                                    <div class="col-md-6 col-centered">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox1" type="checkbox">
                                                <label for="checkbox1">
                                                    Recordarme
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-centered" align="center">
                                        <a href="home.asp">
                                            <!--<button type="button" class="btn btn-primary btn-lg">Ingresar</button>-->
                                            <button type="submit" class="btn btn-primary btn-lg">Ingresar</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" align="center"><a href="ui-widgets.html">¿No es usuario?</a></div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <a href="registro.asp">
                                            <button type="button" class="btn btn-success">Reg&iacute;strese ahora</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <!--</div>-->

    <!-- content -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Videos -->
    <!-- ============================================================== -->

    <!-- END wrapper -->

    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- KNOB JS -->
    <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!--Morris Chart-->
    <script src="assets/plugins/morris/morris.min.js"></script>
    <script src="assets/plugins/raphael/raphael-min.js"></script>

    <!-- Dashboard init -->
    <script src="assets/pages/jquery.dashboard.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <!--======= JavaScript =========-->

    <!--======= Touch Swipe =========-->
    <script src="js/jquery.touchSwipe.min.js"></script>

    <!--======= Customize =========-->
    <script src="js/responsive_bootstrap_carousel.js"></script>

    <!--Validator-->
    <script src="js/validator.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>

</body>
</html>
