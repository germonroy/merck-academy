<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Croppie - a simple javascript image cropper - Foliotek</title>

    <meta name="description" content="Croppie is an easy to use javascript image cropper.">

    <meta property="og:title" content="Croppie - a javascript image cropper">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://foliotek.github.io/Croppie">
    <meta property="og:description" content="Croppie is an easy to use javascript image cropper.">
    <meta property="og:image" content="https://foliotek.github.io/Croppie/demo/hero.png">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700" rel="stylesheet" type="text/css">
    <link rel="Stylesheet" type="text/css" href="demo/prism.css">
    <link rel="Stylesheet" type="text/css" href="bower_components/sweetalert/dist/sweetalert.css">
    <link href="css/croppie.css" rel="stylesheet" />
    <link href="css/demo-croppie.css" rel="stylesheet" />
    <link rel="icon" href="//foliotek.github.io/favico-64.png">
</head>
<body>
    <section>
        <a id="demos" name="demos"></a>
        <div class="section-header">
            <h2>Demos</h2>
        </div>
        <div class="demo-wrap">
            <div class="container">
                <div class="grid">
                    <div class="col-1-2">
                        <strong>Basic Example</strong>
                        <pre class=" language-javascript"><code class=" language-javascript">
<span class="token keyword">var</span> basic <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">'#demo-basic'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">croppie</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    viewport<span class="token punctuation">:</span> <span class="token punctuation">{</span>
        width<span class="token punctuation">:</span> <span class="token number">150</span><span class="token punctuation">,</span>
        height<span class="token punctuation">:</span> <span class="token number">200</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
basic<span class="token punctuation">.</span><span class="token function">croppie</span><span class="token punctuation">(</span><span class="token string">'bind'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    url<span class="token punctuation">:</span> <span class="token string">'demo/cat.jpg'</span><span class="token punctuation">,</span>
    points<span class="token punctuation">:</span> <span class="token punctuation">[</span><span class="token number">77</span><span class="token punctuation">,</span><span class="token number">469</span><span class="token punctuation">,</span><span class="token number">280</span><span class="token punctuation">,</span><span class="token number">739</span><span class="token punctuation">]</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment" spellcheck="true">//on button click</span>
basic<span class="token punctuation">.</span><span class="token function">croppie</span><span class="token punctuation">(</span><span class="token string">'result'</span><span class="token punctuation">,</span> <span class="token string">'html'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token keyword">function</span><span class="token punctuation">(</span>html<span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token comment" spellcheck="true">// html is div (overflow hidden)</span>
    <span class="token comment" spellcheck="true">// with img positioned inside.</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
                        <div class="actions">
                            <button class="basic-result">Result</button>
                            <input type="number" class="basic-width" placeholder="width">
                            x
                            <input type="number" class="basic-height" placeholder="height">
                        </div>
                    </div>
                    <div class="col-1-2">
                        <div id="demo-basic" class="croppie-container">
                            <div class="cr-boundary" style="width: 300px; height: 300px;">
                                <img class="cr-image" src="demo/cat.jpg" style="opacity: 1; transform: translate3d(-131.215px, -225.047px, 0px) scale(0.2667); transform-origin: 281.215px 375.047px 0px;"><div class="cr-viewport cr-vp-square" tabindex="0" style="width: 150px; height: 200px;"></div>
                                <div class="cr-overlay" style="width: 200.025px; height: 200.025px; top: 49.9751px; left: 74.9999px;"></div>
                            </div>
                            <div class="cr-slider-wrap">
                                <input class="cr-slider" type="range" step="0.0001" min="0.2667" max="1.5000">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="demo-wrap">
            <div class="container">
                <div class="grid">
                    <div class="col-1-2">
                        <div id="vanilla-demo" class="croppie-container">
                            <div class="cr-boundary" style="width: 300px; height: 300px;">
                                <canvas class="cr-image" width="3072" height="2306" style="transform: translate3d(-1386px, -1003px, 0px) scale(0.0434); transform-origin: 1536px 1153px 0px; opacity: 1;"></canvas>
                                <div class="cr-viewport cr-vp-square" tabindex="0" style="width: 100px; height: 100px;"></div>
                                <div class="cr-overlay" style="width: 133.325px; height: 100.08px; top: 99.96px; left: 83.3376px;"></div>
                            </div>
                            <div class="cr-slider-wrap">
                                <input class="cr-slider" type="range" step="0.0001" min="0.0434" max="1.5000" style="display: none;">
                            </div>
                        </div>
                    </div>
                    <div class="col-1-2">
                        <strong>Vanilla Example</strong>
                        <pre class=" language-javascript"><code class=" language-javascript">
<span class="token keyword">var</span> el <span class="token operator">=</span> document<span class="token punctuation">.</span><span class="token function">getElementById</span><span class="token punctuation">(</span><span class="token string">'vanilla-demo'</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token keyword">var</span> vanilla <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Croppie</span><span class="token punctuation">(</span>el<span class="token punctuation">,</span> <span class="token punctuation">{</span>
    viewport<span class="token punctuation">:</span> <span class="token punctuation">{</span> width<span class="token punctuation">:</span> <span class="token number">100</span><span class="token punctuation">,</span> height<span class="token punctuation">:</span> <span class="token number">100</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    boundary<span class="token punctuation">:</span> <span class="token punctuation">{</span> width<span class="token punctuation">:</span> <span class="token number">300</span><span class="token punctuation">,</span> height<span class="token punctuation">:</span> <span class="token number">300</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    showZoomer<span class="token punctuation">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
    enableOrientation<span class="token punctuation">:</span> <span class="token boolean">true</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
vanilla<span class="token punctuation">.</span><span class="token function">bind</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    url<span class="token punctuation">:</span> <span class="token string">'demo/demo-2.jpg'</span><span class="token punctuation">,</span>
    orientation<span class="token punctuation">:</span> <span class="token number">4</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment" spellcheck="true">//on button click</span>
vanilla<span class="token punctuation">.</span><span class="token function">result</span><span class="token punctuation">(</span><span class="token string">'blob'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token keyword">function</span><span class="token punctuation">(</span>blob<span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token comment" spellcheck="true">// do something with cropped blob</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
                        <div class="actions">
                            <button class="vanilla-result">Result</button>
                            <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                            <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="demo-wrap upload-demo">
            <div class="container">
                <div class="grid">
                    <div class="col-1-2">
                        <strong>Upload Example (with exif orientation compatability)</strong>
                        <pre class=" language-javascript"><code class=" language-javascript">
$uploadCrop <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">'#upload-demo'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">croppie</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    enableExif<span class="token punctuation">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    viewport<span class="token punctuation">:</span> <span class="token punctuation">{</span>
        width<span class="token punctuation">:</span> <span class="token number">200</span><span class="token punctuation">,</span>
        height<span class="token punctuation">:</span> <span class="token number">200</span><span class="token punctuation">,</span>
        type<span class="token punctuation">:</span> <span class="token string">'circle'</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    boundary<span class="token punctuation">:</span> <span class="token punctuation">{</span>
        width<span class="token punctuation">:</span> <span class="token number">300</span><span class="token punctuation">,</span>
        height<span class="token punctuation">:</span> <span class="token number">300</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
                        <div class="actions">
                            <a class="btn file-btn">
                                <span>Upload</span>
                                <input type="file" id="upload" value="Choose a file" accept="image/*">
                            </a>
                            <button class="upload-result">Result</button>
                        </div>
                    </div>
                    <div class="col-1-2">
                        <div class="upload-msg">
                            Upload a file to start cropping
                        </div>
                        <div class="upload-demo-wrap">
                            <div id="upload-demo" class="croppie-container">
                                <div class="cr-boundary">
                                    <canvas class="cr-image"></canvas>
                                    <div class="cr-viewport cr-vp-circle" tabindex="0" style="width: 100px; height: 100px;"></div>
                                    <div class="cr-overlay"></div>
                                </div>
                                <div class="cr-slider-wrap">
                                    <input class="cr-slider" type="range" step="0.0001">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="demo-wrap hidden-demo">
            <div class="container">
                <div class="grid">
                    <div class="col-1-2">
                        <strong>Hidden Example</strong>
                        <p>When binding a croppie element that isn't visible, i.e., in a modal - you'll need to call bind again on your croppie element, to indicate to croppie that the position has changed and it needs to recalculate its points.</p>

                        <pre class=" language-javascript"><code class=" language-javascript">
<span class="token function">$</span><span class="token punctuation">(</span><span class="token string">'#hidden-demo'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">croppie</span><span class="token punctuation">(</span><span class="token string">'bind'</span><span class="token punctuation">)</span></code></pre>
                        <div class="actions">
                            <button class="show-hidden">Toggle Croppie</button>
                        </div>
                    </div>
                    <div class="col-1-2">
                        <div id="hidden-demo" style="display: none;" class="croppie-container">
                            <div class="cr-boundary" style="width: 200px; height: 200px;">
                                <img class="cr-image" src="demo/demo-3.jpg" style="opacity: 0;"><div class="cr-viewport cr-vp-circle" tabindex="0" style="width: 175px; height: 175px;"></div>
                                <div class="cr-overlay"></div>
                            </div>
                            <div class="cr-slider-wrap">
                                <input class="cr-slider" type="range" step="0.0001">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="demo"></div>
    <script>
$('.demo').croppie({
    url: 'demo/demo-1.jpg',
});
    </script>
    <!-- or even simpler -->
    <img class="my-image" src="demo/demo-1.jpg" />
    <script>
$('.my-image').croppie();
    </script>
    <script src="js/croppie.js"></script>
    <script src="js/demo-croppie.js"></script>
    <script>
        function demoUpload() {
		var $uploadCrop;

		function readFile(input) {
 			if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            
	            reader.onload = function (e) {
					$('.upload-demo').addClass('ready');
	            	$uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	}).then(function(){
	            		console.log('jQuery bind complete');
	            	});
	            	
	            }
	            
	            reader.readAsDataURL(input.files[0]);
	        }
	        else {
		        swal("Sorry - you're browser doesn't support the FileReader API");
		    }
		}

		$uploadCrop = $('#upload-demo').croppie({
			viewport: {
				width: 100,
				height: 100,
				type: 'circle'
			},
			enableExif: true
		});

		$('#upload').on('click', function () { readFile(this); });
		$('.upload-result').on('click', function (ev) {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
				popupResult({
					src: resp
				});
			});
		});
	}

    </script>

</body>
</html>
