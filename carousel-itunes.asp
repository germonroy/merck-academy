﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>jQuery.Coverflow</title>
    <script src="js/jquery4.js"></script>
    <script src="js/jquery-ui.js"></script>
    <!-- Plugin -->
    <script src="js/jquery.coverflow.js"></script>
    <!-- Optionals -->
    <script src="js/jquery.interpolate.min.js"></script>
    <script src="js/jquery.touchSwipe.min.js"></script>
    <script src="js/reflection.js"></script>
    <style>
        #preview {
            padding-bottom: 100px;
        }

        #preview-coverflow .cover {
            cursor: pointer;
            width: 600px;
            height: 400px;
        }
    </style>
    <!--#include file="includes/google_track.asp" -->
</head>
<body>
    <div id="preview">
        <div id="preview-coverflow">
            <img class="cover" src="images/banners/38.jpg" alt="Banner14" />
            <img class="cover" src="images/banners/39.jpg" alt="Banner14" />
            <img class="cover" src="images/banners/7.jpg" alt="Banner14" />
            <img class="cover" src="images/banners/40.jpg" alt="Banner14" />
            <img class="cover" src="images/banners/41.jpg" alt="Banner14" />

        </div>
        <script>
                $(".cover").click(function(){
                    if ($('.cover').hasClass('current')){
                        window.top.location.href = "registro.asp"; 
                    }
                });
                        
				$(function() {
           
					if ($.fn.reflect) {
						$('#preview-coverflow .cover').reflect();	// only possible in very specific situations
					}

					$('#preview-coverflow').coverflow({
                       
						index:			2,
						density:		2,
						innerOffset:	50,
						innerScale:		.7,
						animateStep:	function(event, cover, offset, isVisible, isMiddle, sin, cos) {
							if (isVisible) {
								if (isMiddle) {
									$(cover).css({
										'filter':			'none',
										'-webkit-filter':	'none'
									});
								} else {
									var brightness	= 1 + Math.abs(sin),
										contrast	= 1 - Math.abs(sin),
										filter		= 'contrast('+contrast+') brightness('+brightness+')';
									$(cover).css({
										'filter':			filter,
										'-webkit-filter':	filter
									});
								}
							}
						}
					});
				});
        </script>
    </div>
</body>
</html>
