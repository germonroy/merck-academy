<!--#include file="data/con_ma.inc" -->
<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

set CursorCursos = createobject("ADODB.Recordset")
CursorCursos.CursorType =1 
CursorCursos.LockType = 3

SQLCurso = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and titulo like '%" & Request("word") & "%'"
CursorCursos.Open SQLCurso, StrConn, 1, 3

set CursorVideos = createobject("ADODB.Recordset")
CursorVideos.CursorType =1 
CursorVideos.LockType = 3

SQLVideos = "select A.titulo as titulovideo, A.hash as hashvideo, A.*, B.* from tbl_videos A, cat_ponentes B where  A.activo = 'SI' and A.id_ponente = B.id_ponente and A.titulo like '%" & Request("word") & "%'  ORDER by A.id_video ASC"
CursorVideos.Open SQLVideos, StrConn, 1, 3

set CursorRevisiones = createobject("ADODB.Recordset")
CursorRevisiones.CursorType =1 
CursorRevisiones.LockType = 3

SQLRevisiones = "select *, (select max(fecha) from tbl_fasciculos where disponible='SI') as lastDate from tbl_fasciculos where activo = 'SI' and disponible = 'SI' and titulo like '%" & Request("word") & "%' ORDER by fecha DESC"
CursorRevisiones.Open SQLRevisiones, StrConn, 1, 3


set CursorVideosTotal = createobject("ADODB.Recordset")
CursorVideosTotal.CursorType =1 
CursorVideosTotal.LockType = 3

SQLVideosTot = "select count(*) as totalVideos from tbl_videos A, cat_ponentes B where A.activo = 'SI'   and A.id_ponente = B.id_ponente and A.titulo like '%" & Request("word") & "%'"
CursorVideosTotal.Open SQLVideosTot, StrConn, 1, 3


set CursorCursosTot = createobject("ADODB.Recordset")
CursorCursosTot.CursorType =1 
CursorCursosTot.LockType = 3

SQLCursoTot = "SELECT count(*) as totalCursos FROM tbl_modulos WHERE activo = 'SI' and titulo like '%" & Request("word") & "%'"
CursorCursosTot.Open SQLCursoTot, StrConn, 1, 3

totalBusqueda = CursorCursosTot("totalCursos") + CursorVideosTotal("totalVideos")


set CursorRevisionesTot = createobject("ADODB.Recordset")
CursorRevisionesTot.CursorType =1 
CursorRevisionesTot.LockType = 3

SQLRevisionesTot = "select count(*) as totalRevisiones from tbl_fasciculos where activo = 'SI' and disponible = 'SI' and titulo like '%" & Request("word") & "%'"
CursorRevisionesTot.Open SQLRevisionesTot, StrConn, 1, 3

    totalBusqueda = CursorCursosTot("totalCursos") + CursorVideosTotal("totalVideos") +CursorRevisionesTot ("totalRevisiones")

%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />
    <style>
        .Tache {
            opacity: 10 !important;
        }

        .nav-tabs a {
            color: #000;
            background-color: #dae6ec;
        }

        .nav > li > a:focus, .nav > li > a:hover {
            text-decoration: none;
            background-color: #188ae2;
            color: #fff;
        }

        .nav .open > a, .nav .open > a:focus, .nav .open > a:hover {
            border-color: #a7effc;
            background-color: #188ae2;
            /*color: #3266b1;*/
            color: #fff;
        }
    </style>
</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <h1 style="color: #243e9f;">RESULTADOS DE LA BÚSQUEDA
                        </h1>
                        <h4 style="color: #243e9f; margin-bottom: 5px; padding-bottom: 5px;"><strong><i class="fa fa-search"></i>&nbsp;Se encontraron un total de <%=totalBusqueda %> elementos con: "<%=Request("word") %>" </strong></h4>
                        <div class="col-md-12">
                            <ul class="nav nav-tabs">
                                <%
                                    CursosActiva = ""
                                    VideosActiva = ""
                                    RevisionesActiva = ""

                                    CursosActivaTab = ""
                                    VideosActivaTab =  ""
                                    RevisionesActivaTab = ""                               
                                %>
                                <%If CursorVideosTotal("totalVideos") > 0 then 
                                     VideosActiva = "active "
                                    VideosActivaTab = "active in"

                                %>
                                <li role="presentation" class="<% =VideosActiva %>">
                                    <a href="#tab-videos" role="tab" data-toggle="tab" aria-expanded="true">Videos (<%=CursorVideosTotal("totalVideos") %>)</a>
                                </li>
                                <% end if %>

                                <%If CursorCursosTot("totalCursos") > 0 then
                                     if VideosActiva = "" then
                                        CursosActiva = "active"
                                       CursosActivaTab = "active in"
                                     end if 
                                %>
                                <li role="presentation" class="<% =CursosActiva %>">
                                    <a href="#tab-cursos" role="tab" data-toggle="tab" aria-expanded="false">Módulos (<%=CursorCursosTot("totalCursos") %>)</a>
                                </li>
                                <% end if %>

                                <%If CursorRevisionesTot("totalRevisiones") > 0 then
                                     if (CursosActiva = "" and VideosActiva = "" ) then
                                            RevisionesActiva = "active"
                                           RevisionesActivaTab = "active in"
                                     end if 
                                     
                                %>
                                <li role="presentation" class="<% =RevisionesActiva %>">
                                    <a href="#tab-revisiones" role="tab" data-toggle="tab" aria-expanded="true">Revisiones (<%=CursorRevisionesTot("totalRevisiones") %>)</a>
                                </li>
                                <% end if %>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade <% =CursosActivaTab%> " id="tab-cursos">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <%Do While not CursorCursos.EOF %>
                                            <div class="card-box widget-user">
                                                <div>
                                                    <img src="images/videoteca/modulos/<%=CursorCursos("id_modulo") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="<%=CursorCursos("titulo") %>">
                                                    <div class="wid-u-info">
                                                        <h4 class="m-t-0 m-b-5 font-600"><%=CursorCursos("titulo") %></h4>
                                                        <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-cursosEspecialidad">
                                                            <button type="button" class="btn btn-primary boton-contenido-iphone">Ver más</button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <%
                                                CursorCursos.MoveNext
                                                Loop
                                            %>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade <% =VideosActivaTab%> " id="tab-videos">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <%Do While not CursorVideos.EOF %>
                                            <div class="card-box widget-user">
                                                <!-- <a href="javascript:void(0);" onclick="window.VCirugias.location='view_cirugias.asp?idv=<%=CursorVideos("id_video") %>&hv=<%=CursorVideos("hashvideo") %>&idu=<%=Session("id_usuario") %>'; document.getElementById('tituloVideo').innerHTML = '<%=CursorVideos("titulovideo") %>';" disabled data-toggle="modal" data-target="#full-width-modalCirugias">-->
                                                <!-- <a href="javascript:void(0);" onclick="window.VCirugias.location='view_cirugias.asp?idv=<%=CursorVideos("id_video") %>&hv=<%=CursorVideos("hashvideo") %>&idu=<%=Session("id_usuario") %>&idc=0; document.getElementById('tituloVideo').innerHTML = '<%=CursorVideos("titulo") %>';" disabled data-toggle="modal" data-target="#full-width-modalCirugias">-->
                                                <a href="javascript:void(0);" onclick="window.VCirugias.location='view_cirugias.asp?idv=<%=CursorVideos("id_video") %>&hv=<%=CursorVideos("hashvideo") %>&idu=<%=Session("id_usuario") %>&idc=0'; document.getElementById('tituloVideo').innerHTML = '<%=CursorVideos("titulovideo") %>';" disabled data-toggle="modal" data-target="#full-width-modalCirugias">
                                                    <div>
                                                        <!--                                                      -->
                                                        <img src="images/videoteca/videos/<%=CursorVideos("id_video") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="<%=CursorVideos("titulovideo") %>">
                                                        <div class="wid-u-info">
                                                            <h4 class="m-t-0 m-b-5 font-600"><%=CursorVideos("titulovideo") %></h4>
                                                            <p class="text-muted m-b-5 font-13"><%=CursorVideos("titulo") %>&nbsp;<%=CursorVideos("nombre") %></p>
                                                            <small class="text-info"><b>Duración: <%=CursorVideos("duracion") %></b></small>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <%
                                                CursorVideos.MoveNext
                                                Loop
                                            %>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade <% =RevisionesActivaTab%> " id="tab-revisiones">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <%Do While not CursorRevisiones.EOF
                                                if CursorRevisiones("disponible") = "SI" then
                                                    imagenVP = CursorRevisiones("id_fasciculos")
                                                else
                                                    imagenVP = "proximamente"
                                                end if
                                            %>
                                            <div class="card-box widget-user">
                                                <!--<a href="javascript:void(0);" onclick="window.RevisionFrame.location='fasciculos/<%=CursorRevisiones("id_fasciculos") %>/?idf=<%=CursorRevisiones("id_fasciculos") %>&labs=1'; document.getElementById('tituloRevision').innerHTML = '<%=CursorRevisiones("titulo") %>';" disabled data-toggle="modal" data-target="#full-width-modalRevisiones">-->
                                                <a href="javascript:void(0);" onclick="window.RevisionFrame.location='fasciculos/<%=CursorRevisiones("id_fasciculos") %>/?idf=<%=CursorRevisiones("id_fasciculos") %>&labs=1'; document.getElementById('tituloRevision').innerHTML = '<%=CursorRevisiones("titulo") %>';" disabled data-toggle="modal" data-target="#full-width-modalRevisiones">
                                                    <div>
                                                        <img src="images/videoteca/revisiones/<%=imagenVP %>.png" class="img-responsive" style="max-width: 100%;" class="thumb-img">
                                                        <div class="wid-u-info">
                                                            <h4 class="m-t-0 m-b-5 font-600"><%=CursorRevisiones("titulo") %></h4>
                                                            <a href="javascript:void(0);" onclick="window.RevisionFrame.location='fasciculos/<%=CursorRevisiones("id_fasciculos") %>/?idf=<%=CursorRevisiones("id_fasciculos") %>&labs=1'; document.getElementById('tituloRevision').innerHTML = '<%=CursorRevisiones("titulo") %>';" disabled data-toggle="modal" data-target="#full-width-modalRevisiones">
                                                                <button type="button" class="btn btn-primary boton-contenido-iphone">Ver más</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <%
                                                CursorRevisiones.MoveNext
                                                Loop
                                            %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="full-width-modalCirugias" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header hidden-xs">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.VCirugias.location='blank.htm';">×</button>
                                            <img style="width: 100%" src="images/header_revisiones.png" class="hidden-xs" />
                                        </div>
                                        <h4 class="modal-title hidden-xs" style="z-index: 3; color: #fff; position: absolute; top: 60px; right: 35px;" id="full-width-modalLabel"><span id="tituloVideo"></span></h4>
                                        <iframe style="width: 100%; height: 65vh;" src="#" id="VCirugias" name="VCirugias" frameborder="0" allowfullscreen></iframe>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="window.VCirugias.location='blank.htm';">Cerrar </button>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div id="full-width-modal-cursosEspecialidad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0 b-0">
                                        <div class="panel panel-color panel-primary">
                                            <div class="heading" style="background: #3266b1; padding: 10px;">
                                                <button type="button" class="close m-t-5 Tache" data-dismiss="modal" aria-hidden="true" style="color: white;">
                                                    ×</button>
                                                <h3 class="panel-title">&nbsp;Seleccionar Especialidad</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row center-block text-center">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                        <a href="reglas.asp?idcurso=2" data-filter=".mi" type="button" class="btn btn-primary-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-medicinaInterna"></i></a>
                                                        <br />
                                                        Medicina Interna
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                        <a href="reglas.asp?idcurso=3" data-filter=".mg" type="button" class="btn btn-warning-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-medicinaGeneral"></i></a>
                                                        <br />
                                                        Medicina General

                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                        <a href="reglas.asp?idcurso=4" data-filter=".orto" type="button" class="btn btn-danger-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-ortopedia"></i></a>
                                                        <br />
                                                        Ortopedia
                                                    </div>
                                                    <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                        <a href="reglas.asp?idcurso=5" data-filter=".odon" type="button" class="btn btn-success-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-odontologia"></i></a>
                                                        <br />
                                                        Odontología
                                                    </div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="full-width-modalRevisiones" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header hidden-xs">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.RevisionFrame.location='blank.htm';">×</button>
                                            <img style="width: 100%" src="images/header_revisiones.png" class="hidden-xs" />
                                        </div>
                                        <h4 class="modal-title hidden-xs" style="z-index: 3; color: #fff; position: absolute; top: 60px; right: 35px;" id="full-width-modalLabel"><span id="tituloRevision"></span></h4>
                                        <iframe style="width: 100%; height: 70vh;" src="#" id="RevisionFrame" name="RevisionFrame" frameborder="0" allowfullscreen></iframe>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="window.EbooksFrame.location='blank.htm';">Cerrar</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--#include file="footer.asp" -->
                </div>
            </div>
        </div>
    </div>
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>
    <script src="js/MetroJs.js" type="text/javascript"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="js/logout.js"></script>
</body>
</html>
