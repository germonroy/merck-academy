<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>All Modules</title>
    <link href="MetroJs.css" rel="stylesheet" type="text/css" />
</head><body>

<!-- Apply blue theme as default for all tiles, but use the tiles class to allow the theme to be swapped -->


    <!-- Red Flip Tile that flips every 4 seconds -->
    <div class="red live-tile" data-delay="8000" data-bounce="true " data-mode="flip" data-direction="horizontal" data-front-israndom ="true" style="width:114px;height:150px;">            
        <div>
            <img class="full" src="escudo_cursosCertificacion.png" alt="3" />
            
        </div>
        <div>
            <img class="full" src="escudo_journalsElsevier.png" alt="4" />
            
        </div>
    </div>

    <div class="red live-tile" data-delay="11000"  data-mode="carousel" data-direction="vertical" data-bounce="true" data-front-israndom ="true" style="width:114px;height:150px;">            
        <div>
            <img class="full" src="escudo_glosario.png" alt="3" />
            
        </div>
        <div>
            <img class="full" src="escudo_interactivos.png" alt="4" />
            
        </div>
    </div>
        <script src="jquery-1.7.1.min.js" type="text/javascript"></script>
        <script src="MetroJs.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".live-tile,.flip-list").liveTile();
                $(".appbar").applicationBar();
                
            });
	    </script>
</body>
</html>