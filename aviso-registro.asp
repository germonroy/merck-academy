﻿<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
     <link href="css/aviso-registro.css" rel="stylesheet" />
</head>
<body class="fixed-left">
    <!--#include file="top_home.asp" -->
    <main class="page-content">
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row hidden-xs">
                        <div class="col-md-12">
                            <div align="center">
                                <img src="images/logo/logoMerck.png" class="img-responsive" style="margin-top: 18px; margin-bottom: 18px;" />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="bg">
                        <div class="col-md-2 hidden-xs hidden-md hidden-sm"></div>
                        <div class="col-md-8" id="texto-reglas">
                            <h2 style="text-align: center; margin-bottom: 0.7em; color: #0066b2; text-align: center; font-weight: bold;">REGISTRO</h2>
                            <p style="text-align: justify; line-height: 2em; margin-bottom: 2em;">
                                AVISO IMPORTANTE
                                <br />
                                <br />
                                <b>Estimado(a) Dr(a).:</b><br />
                                Es un gusto contar con su registro, sin embargo todos los usuarios que desean inscribirse sin un código de invitación tienen que pasar por un proceso de validación de datos de nuestro staff técnico, por lo que en las próximas horas recibirá noticias nuestras, y de resultar exitoso, recibirá sus datos de acceso a <b>MERCK ACADEMY.</b><br />
                                Gracias por la espera.
                            </p>
                            <p style="text-align: center; line-height: 2em; color: #0066b2; margin-bottom: 2em;">
                                Atentamente<br />
                                Administración Merck Academy
                            </p>
                        </div>
                        <div class="col-md-2 hidden-xs hidden-md hidden-sm"></div>
                    </div>
                    <div class="col-md-3 hidden-xs hidden-md hidden-sm"></div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!--Morris Chart-->
    <script src="assets/plugins/morris/morris.min.js"></script>
    <script src="assets/plugins/raphael/raphael-min.js"></script>

    <!-- Dashboard init -->
    <script src="assets/pages/jquery.dashboard.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    
    <!--======= Touch Swipe =========-->
    <script src="js/jquery.touchSwipe.min.js"></script>

    <!--======= Customize =========-->
    <script src="js/responsive_bootstrap_carousel.js"></script>
    <script src="js/logout.js"></script>

</body>
</html>
