<!--#include file="data/con_ma.inc" -->
<%
        Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

         set Cursor = createobject("ADODB.Recordset")
  Cursor.CursorType =1 
  Cursor.LockType = 3


         set Cursor2 = createobject("ADODB.Recordset")
  Cursor2.CursorType =1 
  Cursor2.LockType = 3

        set Cursor3 = createobject("ADODB.Recordset")
  Cursor3.CursorType =1 
  Cursor3.LockType = 3

    SQL = "select A.titulo as titulovideo, A.hash as hashvideo, A.*, B.* from tbl_videos A, cat_ponentes B where A.disponible = 'SI' and A.disponible is not null and A.cirugia = 1 and A.id_ponente = B.id_ponente  and A.activo = 'SI' ORDER by A.id_video ASC"
    Cursor.Open SQL, StrConn, 1, 3
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <style>
        @media only screen and (min-width : 320px) {
            #filtros {
                font-size: 10px;
            }
        }
        /* Extra Small Devices, Phones */
        @media only screen and (min-width : 480px) {
            #filtros {
                font-size: 12px;
            }
        }
    </style>
    <link href="icono-lec/style.css" rel="stylesheet" />
</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6">
                            <img src="images/escudos/escudo_videoCirugias.png" class="img-responsive" style="float: right; padding-bottom: 2em;" />
                        </div>
                        <div class="col-lg-6 col-lg-pull-6">
                            <div class="row">
                                <div class="portfolioFilter">
                                    <div class="row text-center" style="margin-top: 30px;">
                                        <div class="col-xs-12 col-md-6" style="padding-bottom: 2em;">
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".R" type="button" class="btn btn-primary-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-rodilla"></i></a>
                                                <br />
                                                Rodilla
                                            </div>
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".CA" type="button" class="btn btn-primary-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-cadera"></i></a>
                                                <br />
                                                Cadera
                                            </div>
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".CO" type="button" class="btn btn-primary-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-columna-02"></i></a>
                                                <br />
                                                Columna
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6" style="padding-bottom: 2em;">
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".TO" type="button" class="btn btn-primary-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-pie"></i></a>
                                                <br />
                                                Tobillo
                                            </div>
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter=".HO" type="button" class="btn btn-primary-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-hombro"></i></a>
                                                <br />
                                                Hombro
                                            </div>
                                            <div class="col-xs-4 col-md-4 text-center center-block">
                                                <a href="#" data-filter="*" type="button" class="btn btn-inverse-cross btn-lg waves-effect waves-light m-b-5"><i class="fa fa-th-large"></i></a>
                                                <br />
                                                Todos
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row port m-b-20">
                        <div class="portfolioContainer">

                            <% 
                            txtLabes = ""
                            txtBotones = ""
                            visibility= ""
                            A = 1
                            counter = 0
                            Do While not Cursor.EOF 
                            If Cursor("division") = "R"  then
                            txtLabes = txtLabes & " " & "mi"
                            txtBotones = txtBotones & "<button style='margin-left=10px;' class='btn btn-icon waves-effect waves-light btn-primary-cross btn-lg m-b-5'> <i class='icon-medicinaInterna'></i> </button> "
                            elseif Cursor("division") = "C" then
                            txtLabes = txtLabes & " " & "mg"
                            txtBotones = txtBotones & "<button style='margin-left=10px;' class='btn btn-icon waves-effect waves-light btn-warning-cross btn-lg m-b-5'> <i class='icon-medicinaGeneral'></i> </button> "
                            elseif Cursor("division") = "Ca" then
                            txtLabes = txtLabes & " " & "orto"
                            txtBotones = txtBotones & "<button style='margin-left=10px;' class='btn btn-icon waves-effect waves-light btn-warning-cross btn-lg m-b-5'> <i class='icon-medicinaGeneral'></i> </button> "
                            elseif Cursor("division") = "T" then
                            txtLabes = txtLabes & " " & "odon"
                            txtBotones = txtBotones & "<button style='margin-left=10px;' class='btn btn-icon waves-effect waves-light btn-warning-cross btn-lg m-b-5'> <i class='icon-medicinaGeneral'></i> </button> "
                            end if

                            if Cursor("disponible") = "SI" then
                            imagenVP = Cursor("id_video")
                            else
                            imagenVP = "proximamente"
                            end if
    
                            %>
                            <div class="col-sm-6 col-lg-3 col-md-4 <%=Cursor("division") %>">
                                <div class="gal-detail thumb">
                                    <a href="javascript:void(0);" onclick="window.VCirugias.location='view_cirugias_con.asp?idv=<%=Cursor("id_video") %>&hv=<%=Cursor("hashvideo") %>&idu=<%=Session("id_usuario") %>&idc=0'; document.getElementById('tituloVideo').innerHTML = '<%=Cursor("titulovideo") %>';" disabled data-toggle="modal" data-target="#full-width-modalCirugias">
                                        <img src="images/videoteca/videocirugias/<%=imagenVP %>.jpg" class="img-responsive center-block" style="max-width: 100%;" class="thumb-img" alt="work-thumbnail">
                                        <!--<h4><%=Cursor("titulovideo") %></h4>-->
                                        <h5><%=Cursor("titulo") %>&nbsp;<%=Cursor("nombre") %></h5>
                                        <h6>Duración: <%=Cursor("duracion") %></h6>
                                    </a>
                                </div>
                            </div>

                            <% visibility=""
                                counter = counter + 1
                                If counter Mod 4 <> 0 then
                                visibility="hide"
                                end if
                            %>

                            <div class="col-sm-12 col-lg-12 col-md-12 <%=visibility %>">
                                <img src="images/linea.png" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="work-thumbnail" />
                            </div>



                            <%Cursor.MoveNext
    txtBotones = ""
    txtLabes = ""
  A = A + 1
Loop    
                            %>
                        </div>
                    </div>
                    <div id="full-width-modalCirugias" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-full">
                            <div class="modal-content">
                                <div class="modal-header hidden-xs">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.VCirugias.location='cargador.htm';">×</button>
                                    <img style="width: 100%" src="images/header_revisiones.png" class="hidden-xs" />
                                </div>
                                <h4 class="modal-title hidden-xs" style="z-index: 3; color: #fff; position: absolute; top: 60px; right: 35px;" id="full-width-modalLabel"><span id="tituloVideo"></span></h4>
                                <style>
                                    .holds-the-iframe {
                                        background: url(images/preloader.gif) center center no-repeat;
                                    }
                                </style>
                                <div class="holds-the-iframe">
                                    <iframe style="width: 100%; height: 65vh;" src="cargador.htm" id="VCirugias" name="VCirugias" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="window.VCirugias.location='cargador.htm';">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--#include file="footer.asp" -->
        </div>
    </div>
    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>

    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- isotope filter plugin -->
    <script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Magnific popup -->
    <script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script src="js/portfolio-filters.js" type="text/javascript"></script>

    <script src="js/logout.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script type="text/javascript">
        function scrollIframe() {
            var element = document.getElementById("VCirugias");

            element.scrollIntoView();
        }
    </script>
</body>
</html>
