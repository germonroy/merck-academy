﻿<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID = 1034
%>

<!--#include file="data/con_ma3.inc" -->
<!--#include file="data/con_ma.inc" -->
<%
Response.Buffer = True
Server.ScriptTimeout=300
%>


<%
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

SQL = "select * from tbl_tareas where id_tarea = " & Request("idt") &""
Cursor.Open SQL, StrConn, 1, 3

If Cursor.RecordCount > 0 then
    Title = Cursor("titulo")
end if
Cursor.Close

Select case Request("idc")
    Case 1
        icono =""
    Case 2
        icono = "medicinaInterna"
    Case 3
        icono = "medicinaGeneral"
    Case 4
        icono = "ortopedia"
    Case 5
        icono = "revisioDelMes"

  End Select  
%>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css" />
    <style>
        .certificate-top {
            background-color: <%=colorTop %>;
        }

        .certificate-row {
            background-color: <%=color %>;
        }

        #panelLec > h2 {
            color: white;
            font-weight: 800;
            display: inline-block;
        }

        #panelLec > h3 {
            color: white;
            display: inline-block;
        }

        .panel-primary {
            border-radius: 20px 20px 20px 20px;
            -moz-border-radius: 20px 20px 20px 20px;
            -webkit-border-radius: 20px 20px 20px 20px;
            border: 0px solid #000000;
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.3);
        }

        .panel-heading {
            border-radius: 20px 20px 0px 0px;
            -moz-border-radius: 20px 20px 0px 0px;
            -webkit-border-radius: 20px 20px 0px 0px;
            border: 0px solid #000000;
        }

        .panel-footer {
            border-radius: 20px 20px 20px 20px;
            -moz-border-radius: 20px 20px 20px 20px;
            -webkit-border-radius: 20px 20px 20px 20px;
            border: 0px solid #000000;
        }
        /*==========  Mobile First Method  ==========*/

        /* Custom, iPhone Retina */
        @media only screen and (min-width : 320px) {
            #mainContainer {
                padding: 0px 0px 0px;
            }
        }

        /* Extra Small Devices, Phones */
        @media only screen and (min-width : 480px) {
            #mainContainer {
                padding: 47px 48px 0px;
            }
        }

        /* Small Devices, Tablets */
        @media only screen and (min-width : 768px) {
            #mainContainer {
                padding: 47px 80px 0px;
            }
        }

        /* Medium Devices, Desktops */
        @media only screen and (min-width : 992px) {
            #mainContainer {
                padding: 47px 80px 0px;
            }
        }

        /* Large Devices, Wide Screens */
        @media only screen and (min-width : 1200px) {
            #mainContainer {
                padding: 47px 80px 0px;
            }
        }

        .panel-group .panel .panel-heading a[data-toggle=collapse]:before {
            content: '\f056';
            display: block;
            float: right;
            font-family: 'FontAwesome';
            font-size: 40px;
            text-align: right;
            width: 25px;
            color: white;
        }

        .correct-answer {
            border-color: rgb(55, 221, 26) !important;
            border-width: 10px !important;
            border-style: solid !important;
        }
    </style>
    <style>
        /* Popover Header */
        .popover-title {
            background-color: #3266b1 !important;
            color: #FFFFFF !important;
            font-size: 28px;
            text-align: center;
        }
        /* Popover Body */
        .popover-content {
            background-color: #ffffff;
            color: #004ba4;
            padding: 25px;
        }

        .close {
            float: right;
            font-size: 21px;
            font-weight: 700;
            line-height: 1;
            color: white;
            text-shadow: 0 1px 0 #fff;
            filter: alpha(opacity=20);
            opacity: .2;
        }

        .content-page .content {
            padding: 0px 5px;
            margin-top: 0px !important;
        }

        .content-page {
            margin-left: 0px;
            overflow: hidden;
        }

        #mainContainer {
            padding: 0px 0px 0px !important;
        }

        #wrapper.enlarged .content-page {
            margin-left: 0px !important;
        }

        .h3, h3 {
            font-size: 16px;
        }

        html {
            background-color: white;
            background: white;
        }

        .panel-primary {
            border-radius: 20px 20px 20px 20px;
            -moz-border-radius: 20px 20px 20px 20px;
            -webkit-border-radius: 20px 20px 20px 20px;
            border: none;
            box-shadow: none;
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
        }
    </style>
    <link href="css/certificados.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.css" rel="stylesheet" />
    <link href="css/scrollbars.css" rel="stylesheet" />
</head>

<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <div class="content-page">
            <div class="content">
                <div class="container" id="mainContainer">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="background-color: #3266b1 !important; padding: 0px; background: #cccccc" id="panelLec">
                            <h2 style="font-size: 16px; margin-left: 10px;"><span class="icon-<%=icono %>" style="margin-right: 5px; margin-top: 5px; color: white; font-size: 40px; vertical-align: middle;"></span>&nbsp;Tarea:</h2>
                            <h3 style="font-size: 14px;">&nbsp;<%=Title %></h3>
                        </div>
                        <%SQL = "select * from tbl_tareas where id_tarea = " & Request("idt") &"" %>
                        <%Cursor.Open SQL, StrConn, 1, 3%>

                        <%If Cursor.RecordCount > 0 then %>
                        <%Do While not Cursor.EOF      %>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #cccccc !important; padding: 15px 30px; background: #cccccc">
                                        <h4 class="panel-title" style="color: black; font-size: 12px;">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="font-size: 1.5em">Escenario del Problema</a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <h3 style="text-align: justify"><%=Cursor("escenario") %></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #cccccc !important; padding: 15px 30px; background: #cccccc">
                                        <h4 class="panel-title" style="color: black; font-size: 12px;">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" style="font-size: 1.5em">Objetivos del Aprendizaje</a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <ul style="color: #3266b1; font-size: 25px">
                                                <%Objetivos=Cursor("objetivos") 
                                            a=Split(Objetivos,"|")
                                            for each x in a
                                                %>
                                                <li>
                                                    <h3 style="color: black"><%=x %></h3>
                                                </li>
                                                <%
                                            next    
                                                %>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #cccccc !important; padding: 15px 30px; background: #cccccc">
                                        <h4 class="panel-title" style="color: black; font-size: 12px;">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" style="font-size: 1.5em">Preguntas para estimular la discusión</a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <form id="registerPreg" name="registerPreg" action="jq/jq_save_hw_questions_cont.asp" method="post">
                                                <ul style="color: #3266b1; font-size: 25px">
                                                    <%
                                                        SQL = "select * from tbl_tareas_preguntas where id_tarea = "&Request("idt")&" and activo = 'SI'"
                                                        Cursor2.Open SQL, StrConn, 1, 3
                                                    %>

                                                    <%Contador =  1 %>
                                                    <%If Cursor2.RecordCount > 0 then %>
                                                    <%TotalPreguntas =  Cursor2.RecordCount  %>
                                                    <%Do while not Cursor2.EOF %>

                                                    <li>
                                                        <h3 style="color: black"><%=Cursor2("pregunta") %></h3>
                                                        <%If Cursor2("palabras_extra_activo") = "SI" then
                                                            Code = "onkeyup='ValidWord(this);'"
                                                        %>
                                                        <textarea id="PREG<%=Contador %>" name="PREG<%=Contador %>" class="form-control" rows="5" data-pregunta="<%=Cursor2("id_pregunta") %>" <%=Code %> data-opciones="<%=Replace(Cursor2("palabras_extra_texto"),"|"," y ") %>"></textarea>
                                                        <%
                                                          Else
                                                            Code = ""
                                                        %>
                                                        <textarea id="PREG<%=Contador %>" name="PREG<%=Contador %>" class="form-control" rows="5" data-pregunta="<%=Cursor2("id_pregunta") %>" <%=Code %>></textarea>
                                                        <%
                                                          End If    
                                                        %>
                                                       
                                                    </li>

                                                    <input type="hidden" id="IDPREG<%=Contador %>" name="IDPREG<%=Contador %>" value="<%=Cursor2("id_pregunta") %>" />
                                                    <%Cursor2.MoveNext %>
                                                    <%Contador = Contador +1 %>
                                                    <%Loop %>
                                                    <%End If %>
                                                    <%Cursor2.Close %>
                                                </ul>
                                                <input type="hidden" id="NUMPREGUNTAS" name="NUMPREGUNTAS" value="<%=TotalPreguntas %>" />
                                                <input type="hidden" id="IDTAREA" name="IDTAREA" value="<%=Request("idt") %>" />
                                                <input type="hidden" id="IDC" name="IDC" value="<%=Request("idc") %>" />
                                                <div class="panel-footer" style="text-align: -webkit-right; background-color: white" id="footer">
                                                    <button class="btn btn-info btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" id="btnRegistro"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Enviar Preguntas</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: -webkit-right; background-color: white" id="footer">
                            <button class="btn btn-success btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" onclick="Back();"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Regresar</button>
                        </div>

                        <%
                        Cursor.MoveNext
                        Loop
                        %>
                        <%End If %>
                        <%Cursor.Close %>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/logout.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>

    <script src="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.js"></script>
    <script type="text/javascript">
        var PopOverOn;
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();   
        });
        $("[data-toggle='popover']").on('shown.bs.popover', function(){
            PopOverOn = false;
        });
    </script>


    <script type="text/javascript">
        var objPadre;
        $("#btnRedirect").click(function () {
            swal({
                title: 'Aviso',
                text: "¿Realmente desea seguir a la evaluación?",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Si',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> No'
            }).then(function () {
                window.location = '../lec-guid-exam.asp?idr=<%=Request("idr")%>&idc=<%=Request("idc")%>';
            });
        });

        function ValidWord(obj)
        {
            objPadre= obj;
            word = $(obj).val();
            if(word != "")
            {
                $(obj).popover({
                    trigger: 'manual',
                    placement: 'top',
                    title:'¡Correcto!',
                    html:true,
                    content:'Ha ingresado la opción correcta. Las respuestas son: '+$(obj).attr("data-opciones")+' <a class="close" onclick="Pop(this);" style="position: absolute; top: 0; right: 6px;">&times;</a>'
                });    
                if(word.length >= 4 )
                {
                    
                    var formData = {
                        'word': $(obj).val(),
                        'id_pregunta':$(obj).attr("data-pregunta")

                    };

                    $.ajax({
                        async: false,
                        type: 'POST',
                        url: "jq/jq_validte_hw_word.asp", 
                        data: formData, 
                        encode: true
                    }).done(function (data) {
                        var words = data.split("|")
                        for(var i = 0; i< words.length;i++)
                        {
                            if($(obj).val() == words[i])
                            {
                                                          
                                $(obj).addClass("correct-answer");
                                PopOverOn = true;
                                $(obj).popover('toggle');
                                $(obj).on('shown.bs.popover', function () {
                                    var $pop = $(this);
                                    setTimeout(function () {
                                        $pop.popover('hide');
                                    }, 15000);
                                });
                                break;


                            }else{
                                $(obj).removeClass("correct-answer");
                            }
                        }


                    });
                }

               
            }
            else{
                $(obj).removeClass("correct-answer");
            }
        }
        function Pop(obj)
        {
            $(objPadre).popover('hide');
        }

        $(document).click(function (e) {
            if(PopOverOn)
            {
                $(objPadre).popover('hide');
            }
           
        });
    </script>

    <script src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">

        $("#btnRegistro").click(function () {

            $("#registerPreg").validate({
                // Specify validation rules
                rules: {
                     <%For i = 1 to  TotalPreguntas %>
                    PREG<%=i%>:{
                        required:true
                    },
                    <%Next%>

                     

                },
                    // Specify validation error messages
                    messages: {
                         <%For i = 1 to  TotalPreguntas %>
                            PREG<%=i%>:{
                                required:"Por favor complete esta respuesta",
                            },
                            <%Next%>


                },
                        // Make sure the form is submitted to the destination defined
                        // in the "action" attribute of the form when valid
                        submitHandler: function (form) {
                            form.submit();
                        }

                    });


        });

        function Back()
        {
            window.parent.location = '../certificados.asp?idcurso=<%=Request("idc")%>'
        }
    </script>

    <%If Request("res") = 1 then  %>
    <script type="text/javascript">
        $(window).load(function () {
            swal({
                title: "¡Éxito!",
                text: "Las respuestas de tu tarea han sido guardas con éxito!",
                type: "success",
                showCancelButton: true,
                confirmButtonClass: 'btn-info waves-effect waves-light',
                confirmButtonText: 'Ok',
                cancelButtonText : 'Regresar',
                cancelButtonClass: 'btn-danger waves-effect waves-light',
                confirmButtonText: 'OK'
            }).then(function () {

            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
                if (dismiss === 'cancel') {
                    Back();
                }
            });
        });
    </script>
    <%End If %>
</body>
</html>
