<!--#include file="data/con_ma.inc" -->
<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

Set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3 %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
    <link href="css/registro.css" rel="stylesheet" />
    <script>
         function RefreshImage(valImageId) {
            var objImage = document.images[valImageId];
            if (objImage == undefined) {
                return;
            }
            var now = new Date();
            objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
        }
    </script>
    <script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1029986&mt_adid=157482&v1=&v2=&v3=&s1=&s2=&s3='></script>
    <!-- CONVERSION TAG -->
    <script type="text/javascript" src="https://cstatic.weborama.fr/js/advertiserv2/adperf_conversion.js"></script>
    <script type="text/javascript">
        var adperftrackobj = {
            client: ""      /* set your client id here */
        , amount: "0.0"  /* set the total price here */
        , invoice_id: "" /* set your invoice id here */
        , quantity: 0    /* set the number of items purchased */
        , is_client: 0   /* set to 1 if the client is a new client */
        , optional_parameters: {
            "N1": "0" /* to set */
            , "N2": "0" /* to set */
            , "N3": "0" /* to set */
            /* to set free parameter follow this pattern : */
            /*      ,"customer_type" : "abc" */
        }/* don't edit below this point */
        , fullhost: 'lamlaboratoriosmerck.solution.weborama.fr'
        , site: 1976
        , conversion_page: 3
        }
        try { adperfTracker.track(adperftrackobj); } catch (err) { }
    </script>
    <link href="assets/css/core-default.css" rel="stylesheet" type="text/css" />
    <style>
        @media only screen and (max-width : 992px) {
            .ht {
                margin-top: 10vh !important;
            }
        }

        @media only screen and (min-width : 992px) {
            #regRapi {
                padding-right: 80px;
            }
        }

        .brdLeft {
            border-left: 1px solid #3071b9 !important;
        }            
    </style>
</head>
<body class="fixed-left">
    <!--#include file="modal.asp" -->
    <!--#include file="header-registro.asp" -->
    <main class="page-content">
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row" id="bg">
                        <div class="row">
                            <div class="col-md-5 acomodo" style="text-align: center;">
                                <div class="col-md-12" style="margin-left: 10px;">
                                    <a href="registro.asp">
                                        <img src="images/registro.jpg" class="img-chrome img-responsive center-block" />
                                    </a>
                                    <img class="img-chrome img-responsive center-block" style="margin-top: -4vw;" src="https://www.merck-academy.org/images/chrome.png" />
                                </div>
                            </div>
                            <div class="col-md-7 acomodo brdLeft" id="regRapi">
                                <style>
                                    .form-control {
                                        padding: 3px 12px;
                                    }
                                </style>
                                <form data-toggle="validator" role="form" id="registro" method="post" action="jq/jq_registro_medio.asp">
                                    <h1 class="text-center center-block ht">REGISTRO RÁPIDO</h1>
                                    <p class="ht pad">
                                        Explore las herramientas y conozca los beneficios de Merck Academy completando los campos del "Registro rápido", tendrá acceso a todo el contenido por 15 días o de manera permanente en caso de realizar su "Registro completo".
                                        <br />
                                        Recuerde que el "Registro completo" le trae grandes beneficios, como la posibilidad de tomar cursos que le otorgan <b>constancias</b> con validez oficial.     
                                    </p>
                                    <div class="form-group col-lg-12 col-md-12 pad" id="div-registro">
                                        <div class="col-md-6">
                                            <div class="form-group" data-animation="animated fadeInRight">
                                                <input type="text" name="nombre" class="form-control" placeholder="Nombre" required>
                                            </div>
                                            <div class="form-group" style="padding: 0px" data-animation="animated fadeInLeft">
                                                <input type="text" name="apaterno" class="form-control" placeholder="A. Paterno" required>
                                            </div>
                                            <div class="form-group" style="padding: 0px" data-animation="animated fadeInLeft">
                                                <input type="text" name="amaterno" class="form-control" placeholder="A. Materno" required>
                                            </div>
                                            <div class="form-group" style="padding: 0px" data-animation="animated fadeInRight">
                                                <input placeholder="Fecha de Nacimiento" name="fechanac" class="form-control" type="text" onfocus="(this.type='date')" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" style="padding: 0px" data-animation="animated fadeInRight">
                                                <input type="text" name="cedula" class="form-control" placeholder="Cédula profesional" required>
                                            </div>
                                            <div class="form-group" data-animation="animated fadeInRight">
                                                <select class="form-control" name="especialidad" required>
                                                    <option value="">Especialidad</option>
                                                    <%SQLESP = "select * from cat_especialidades where id_especialidad not in (4) and activo = 'SI'"
                          Cursor.Open SQLESP, StrConn, 1, 3
                          Do While Not Cursor.EOF %>
                                                    <option value="<%=Cursor("id_especialidad") %>"><%=Cursor("especialidad") %></option>
                                                    <%Cursor.MoveNext
                           Loop
                          Cursor.Close %>
                                                    <option value="0">Otra</option>
                                                </select>
                                            </div>
                                            <div class="form-group" data-animation="animated fadeInLeft">
                                                <input type="email" name="email" class="form-control" placeholder="Email" required>
                                            </div>

                                            <div class="form-group" data-animation="animated fadeInRight">
                                                <input type="password" name="contrasena" class="form-control" placeholder="Contraseña" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 center-block text-center">
                                                <input type="submit" class="btn btn-success btn-lg" text="Regístrese" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!--Validator-->
    <script src="js/validator.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>



    <script type="text/javascript">
        $(document).ready(function () {
            //$(".live-tile,.flip-list").liveTile();
            //$(".appbar").applicationBar();

            function ActualizaCP() {
                $.ajax({
                    url: "jq/jq_obtener_codigos.asp",
                    type: "POST",
                    data: "estado=" + $("#inputState").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputCP").html(opciones);
                        $('#inputCP').prop("disabled", false);
                    }
                })
                $('#inputCP').prop("disabled", false);
            }

            function ActualizaEstados() {
                $.ajax({
                    url: "jq/jq_obtener_estados.asp",
                    type: "POST",
                    data: "inputcountry=" + $("#inputcountry").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputState").html(opciones);
                        if ($('#inputState option:selected').val() == 1) {
                            $('#inputState').prop("disabled", true);
                            $('#inputCity').prop("disabled", true);
                            $('#inputCP').prop("disabled", true);

                            $("#inputCP").html("<option value='1' selected>No aplica</option>");
                            $("#inputCity").val("No aplica");

                        } else {
                            $('#inputState').prop("disabled", false);
                            $('#inputCity').prop("disabled", false);
                            $("#inputCity").val("");
                        }
                    }
                })
            }

            function LimpiaCampos() {
                $('#inputState').removeClass("parsley-error");
                $('#inputCity').removeClass("parsley-error");
                $('#inputCP').removeClass("parsley-error");
            }

            $('#inputcountry').change(function () {
                LimpiaCampos();
                ActualizaEstados();
            });

            $('#inputState').change(function () {
                ActualizaCP();
            });


            $('#inputSpeciality').change(function () {
                if ($('#inputSpeciality').val() == 0) {
                    $('#inputSpeciality').parent().append("<input type='text' class='form-control' id='inputOtherSp' name='inputOtherSp' data-parsley-error-message='Complete su especialidad. Gracias.' placeholder='Especialidad' required>")
                }
                else {
                    $('#inputOtherSp').remove();
                }


            });

        });
    </script>


    <!--Alerts-->
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.js"></script>
    <script src="assets/pages/jquery.sweet-alert.init.js"></script>

    <%
        if Request("fail") = 1 then
    %>
    <script type="text/javascript">
        $(document).ready(function () {
            swal({
                title: "Este usuario ya existe",
                text: "Este email ya está registrado ",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: 'btn-success waves-effect waves-light',
                confirmButtonText: 'Aceptar'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>

    <%
        if Request("fail") = 2 then
    %>
    <script type="text/javascript">
        $(document).ready(function () {
            swal({
                title: "El código de seguridad es incorrecto",
                text: "Por favor verifique. Gracias",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: 'btn-success waves-effect waves-light',
                confirmButtonText: 'Aceptar'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>

    <div>
        <div class="sweet-overlay" tabindex="-1" style="opacity: -0.01; display: none;"></div>
        <div class="sweet-alert hideSweetAlert" tabindex="-1" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-ouside-click="false" data-has-done-function="false" data-timer="null" style="display: none; margin-top: -190px; opacity: -0.01;">
            <div class="icon error" style="display: none;"><span class="x-mark"><span class="line left"></span><span class="line right"></span></span></div>
            <div class="icon warning" style="display: none;"><span class="body"></span><span class="dot"></span></div>
            <div class="icon info" style="display: none;"></div>
            <div class="icon success" style="display: block;">
                <span class="line tip"></span><span class="line long"></span>
                <div class="placeholder"></div>
                <div class="fix"></div>
            </div>
            <div class="icon custom" style="display: none;"></div>
            <h2>Good job!</h2>
            <p class="lead text-muted" style="display: block;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis</p>
            <p>
                <button class="cancel btn btn-lg btn-default" tabindex="2" style="display: none;">Cancel</button>
                <button class="confirm btn btn-lg btn-primary" tabindex="1" style="display: inline-block;">OK</button>
            </p>
        </div>
    </div>

</body>
</html>
