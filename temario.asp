﻿<!--#include file="data/con_ma.inc" -->
<!--#include file="data/con_ma3.inc" -->
<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US

Set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

Set CursorAux = createobject("ADODB.Recordset")
CursorAux.CursorType =1 
CursorAux.LockType = 3
    
home = Request("cur")
    
select case home
    case 3
        cat = "medGral"
        colorgral = "#f5b21d"
    case 2
        cat = "medicinaInterna"
        colorgral = "#2a59a9"
    case 4
        cat = "ortopedia"
        colorgral = "#df132e"
    case 5
        cat = "odontologia"
        colorgral = "#2f8d4e"
    case 1
        cat = "ortopedia"
        colorgral = "#6ed4ff"
end select %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App title -->
    <title>Merck Academy</title>

    <!-- App CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/iconos-merck/style.css" rel="stylesheet" />

    <link href="assets/fonts/temario-font/style.css" rel="stylesheet" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>

    <style>
        .content-page .content {
            padding: 0px 5px;
            margin-top: 0px !important;
        }

        .content-page {
            margin-left: 0px;
            overflow: hidden;
        }

        #mainContainer {
            padding: 0px 0px 0px !important;
        }

        #wrapper.enlarged .content-page {
            margin-left: 0px !important;
        }

        body {
            background: transparent;
        }

        .tb-row3 {
            border-right: 1px solid #f9c851;
        }

        .tb-head3 {
            background-color: #f9c851;
        }

        .tb-row2 {
            border-right: 1px solid #188ae2;
        }

        .tb-head2 {
            color: #fff;
            background-color: #188ae2;
        }

        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #ececea !important;
        }

        .tb-row4 {
            border-right: 1px solid #ff5b5b;
        }

        .tb-head4 {
            color: #fff;
            background-color: #ff5b5b;
        }

        .tb-row5 {
            border-right: 1px solid #10c469;
        }

        .tb-head5 {
            color: #fff;
            background-color: #10c469;
        }

        .tb-row1 {
            border-right: 1px solid #6ed4ff;
        }

        .tb-head1 {
            color: #fff;
            background-color: #6ed4ff;
        }


        .iconos-tem {
            font-size: 30px;
            /*color: <%=colorgral%>;*/
        }

        .panel-group .panel .panel-heading a[data-toggle=collapse]:before {
            content: '\f062';
        }

        .panel-group .panel .panel-heading a[data-toggle=collapse].collapsed:before {
            content: '\f063';
        }

        html {
            position: relative;
            min-height: 100%;
            background-color: transparent !important;
            background: transparent !important;
        }

        .card-box {
            background-color: transparent !important;
        }
    </style>
    <link href="css/scrollbars.css" rel="stylesheet" />

</head>


<body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <%if home = "" then %>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <a id="tabMG" href="#1" data-toggle="tab" class="mi-text-color tabcursos btn btn-block btn-warning"><i class="icon-medicinaGeneral" aria-hidden="true"></i>&nbsp;Medicina General</a>
                        </div>
                        <div class="col-md-3">
                            <a id="tabMI" href="#2" data-toggle="tab" class="md-text-color tabcursos btn btn-block btn-info"><i class="icon-medicinaInterna" aria-hidden="true"></i>&nbsp;Medicina Interna</a>
                        </div>
                        <div class="col-md-3">
                            <a id="tabOR" href="#3" data-toggle="tab" class="ort-text-color tabcursos btn btn-block btn-danger"><i class="icon-ortopedia" aria-hidden="true"></i>&nbsp;Ortopedia</a>
                        </div>
                        <div class="col-md-3">
                            <a id="tabOD" href="#4" data-toggle="tab" class="odon-text-color tabcursos btn btn-block btn-success"><i class="icon-odontologia" aria-hidden="true"></i>&nbsp;Odontología</a>
                        </div>
                    </div>
                </div>
                <%end if %>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <%if home = "" then %>
                            <% for i = 2 to 5 %>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel-group" id="accordion" role="tablist"
                                        aria-multiselectable="true">
                                        <%  m = 1
                                                 if i = 2 then
                                                    SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & i & " or id_curso2 = " & i & " or id_curso3 = " & i & ") ORDER by secuencia2"
                                                elseif i = 4 then
                                                    SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & i & " or id_curso2 = " & i & " or id_curso3 = " & i & ") ORDER by secuencia3"
                                                else
                                                    SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & i & " or id_curso2 = " & i & " or id_curso3 = " & i & ") ORDER by secuencia"
                                                end if
                                            Cursor.Open SQL, StrConn, 1, 3 
                                            Do While Not Cursor.EOF
                                                if m = 1 then
                                                    dis = "" 'in
                                                else
                                                    dis = ""
                                                end if %>
                                        <div class="panel panel-default bx-shadow-none">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse"
                                                        data-parent="#accordion" href="#contenido<%=m %>_<%=i %>"
                                                        aria-expanded="true" aria-controls="collapseOne">Módulo <%=m %>: <%=Cursor("titulo") %>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="contenido<%=m %>_<%=i %>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="tech-companies-1" class="table  table-striped">
                                                        <thead>
                                                            <tr class="tb-tr<%=i %>">
                                                                <th class="tb-head<%=i %>">Título</th>
                                                                <th class="tb-head<%=i %>" data-priority="3">Ponente/Autor</th>
                                                                <th class="tb-head<%=i %>" data-priority="1">Material</th>
                                                                <th class="tb-head<%=i %>" data-priority="3">Duración</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%SQL = "SELECT dbo.tbl_videos.id_video, dbo.tbl_videos.source, dbo.tbl_videos.disponible, dbo.tbl_videos.id_modulo, dbo.tbl_videos.id_curso, dbo.tbl_videos.id_ponente, dbo.tbl_videos.titulo, dbo.tbl_videos.duracion, "
                                                                    SQL = SQL & "dbo.tbl_videos.activo, dbo.cat_ponentes.nombre, dbo.cat_ponentes.top_ponente, dbo.tbl_videos.hash, dbo.cat_ponentes.titulo AS tituloponente FROM dbo.tbl_videos LEFT OUTER JOIN "
                                                                    SQL = SQL & "dbo.cat_ponentes ON dbo.tbl_videos.id_ponente = dbo.cat_ponentes.id_ponente WHERE (dbo.tbl_videos.activo = 'SI') AND (dbo.tbl_videos.disponible = 'SI') AND (id_modulo = " & Cursor("id_modulo") & " or id_modulo2 = " & Cursor("id_modulo") & ")"
                                                                    SQL = SQL & "  AND (id_curso = " & i & " or id_curso2 = " & i & " or id_curso3 = " & i & ") ORDER by tbl_videos.secuencia"
                                                                    CursorAux.Open SQL, StrConn, 1, 3
                                                                    Do While Not CursorAux.EOF   %>
                                                            <tr class="tb-tr<%=i %>">
                                                                <th class="tb-row<%=i %>"><span class="co-name"><%=CursorAux("titulo") %></span></th>
                                                                <td class="tb-row<%=i %>"><%=CursorAux("tituloponente") %>&nbsp;<%=CursorAux("nombre") %></td>
                                                                <td class="tb-row<%=i %>">Videolección</td>
                                                                <td><%=CursorAux("duracion") %></td>
                                                            </tr>
                                                            <%CursorAux.MoveNext
                                                                  Loop
                                                                  CursorAux.Close %>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <%Cursor.MoveNext
                                                m = m + 1
                                            Loop
                                            Cursor.Close %>
                                    </div>
                                </div>
                                <!-- end col -->

                            </div>
                            <% next %>
                            <%else %>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel-group" id="accordion" role="tablist"
                                        aria-multiselectable="true">
                                        <%  m = 1
                                                 if home = 2 then
                                                    SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & home & " or id_curso2 = " & home & " or id_curso3 = " & home & ") ORDER by secuencia2"
                                                elseif home = 4 then
                                                    SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & home & " or id_curso2 = " & home & " or id_curso3 = " & home & ") ORDER by secuencia3"
                                                else
                                                    SQL = "SELECT * FROM tbl_modulos WHERE activo = 'SI' and (id_curso = " & home & " or id_curso2 = " & home & " or id_curso3 = " & home & ") ORDER by secuencia"
                                                end if
                                            Cursor.Open SQL, StrConn, 1, 3 
                                            Do While Not Cursor.EOF
                                                if m = 1 then
                                                    dis = "" 'in
                                                else
                                                    dis = ""
                                                end if %>
                                        <div class="panel panel-default bx-shadow-none">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse"
                                                        data-parent="#accordion" href="#contenido<%=m %>_<%=home %>"
                                                        aria-expanded="true" aria-controls="collapseOne" style="font-size: 20px;"><i class="icon-<%=cat %>-mod<%=m %> iconos-tem"></i>&nbsp;Módulo <%=m %>: <%=Cursor("titulo") %>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="contenido<%=m %>_<%=home %>" class="panel-collapse collapse <%=dis %>" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="tech-companies-1" class="table  table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th class="tb-head<%=home %>">Título</th>
                                                                <th class="tb-head<%=home %>" data-priority="3">Ponente/Autor</th>
                                                                <th class="tb-head<%=home %>" data-priority="1">Material</th>
                                                                <th class="tb-head<%=home %>" data-priority="3">Duración</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%SQL = "SELECT dbo.tbl_videos.id_video, dbo.tbl_videos.source, dbo.tbl_videos.cirugia, dbo.tbl_videos.disponible, dbo.tbl_videos.id_modulo, dbo.tbl_videos.id_curso, dbo.tbl_videos.id_ponente, dbo.tbl_videos.titulo, dbo.tbl_videos.duracion, "
                                                                    SQL = SQL & "dbo.tbl_videos.activo, dbo.cat_ponentes.nombre, dbo.cat_ponentes.top_ponente, dbo.tbl_videos.hash, dbo.cat_ponentes.titulo AS tituloponente, "
                                                                    SQL = SQL & "(select COUNT(*) from tbl_top_contenido where id_tipo = tbl_videos.id_video and tipo = 'V') as top_cont "
                                                                    SQL = SQL & "FROM dbo.tbl_videos LEFT OUTER JOIN "
                                                                    SQL = SQL & "dbo.cat_ponentes ON dbo.tbl_videos.id_ponente = dbo.cat_ponentes.id_ponente WHERE (dbo.tbl_videos.activo = 'SI') AND (dbo.tbl_videos.disponible = 'SI') AND (id_modulo = " & Cursor("id_modulo") & " or id_modulo2 = " & Cursor("id_modulo") & ")"
                                                                    SQL = SQL & "  AND (id_curso = " & home & " or id_curso2 = " & home & " or id_curso3 = " & home & ") ORDER by tbl_videos.secuencia"
                                                                    'Response.Write(SQL)
                                                                    CursorAux.Open SQL, StrConn, 1, 3
                                                                    Do While Not CursorAux.EOF   %>
                                                            <tr>
                                                                <%if CursorAux("top_cont") = 0 then %>
                                                                <th class="tb-row<%=home %>"><span class="co-name"><%=CursorAux("titulo") %></span></th>
                                                                <%else %>
                                                                <td class="tb-row<%=i %>"><b style="color: #2c65b4;"><i class="fa fa-star" style="color: #2c65b4;" aria-hidden="true"></i>&nbsp;<%=CursorAux("titulo") %></b></td>
                                                                <%end if %>
                                                                <td class="tb-row<%=i %>"><%=CursorAux("tituloponente") %>&nbsp;<%=CursorAux("nombre") %></td>
                                                                <% if CursorAux("cirugia") = 1 then %>
                                                                <td class="tb-row<%=home %>">Videocirugía</td>
                                                                <% else %>
                                                                <td class="tb-row<%=home %>">Videolección</td>                                                                
                                                                <% end if %>
                                                                <td><%=CursorAux("duracion") %></td>
                                                            </tr>
                                                            <%CursorAux.MoveNext
                                                                  Loop
                                                                  CursorAux.Close %>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <%Cursor.MoveNext
                                                m = m + 1
                                            Loop
                                            Cursor.Close %>
                                    </div>
                                </div>
                                <!-- end col -->

                            </div>
                            <%end if %>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- content -->
        </div>
    </div>
    <!-- END wrapper -->



    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

</body>
</html>
