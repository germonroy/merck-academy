﻿<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
</head>
<body class="fixed-left">
    <!--#include file="top_home.asp" -->
    <main class="page-content">
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row hidden-xs">
                        <div class="col-md-12">
                            <div align="center">
                                <img src="images/logo/logoMerck.png" class="img-responsive" style="margin-top: 18px; margin-bottom: 18px;" />
                            </div>
                        </div>
                    </div>
                    <div class="row" style="background-image: url(../images/fondos/plecaBlanca.png); background-size: 100% 100%;">
                        <div class="col-md-2 hidden-xs hidden-md hidden-sm"></div>
                        <div class="col-md-8" id="texto-reglas">
                            <h2 style="text-align: center; margin-bottom: 0.7em; color: #0066b2; text-align: center; font-weight: bold;">WELCOME</h2>
                            <p style="text-align: justify; line-height: 2em">
                                El área médica de <b>MERCK y los expertos en dolor</b> le dan la más cordial bienvenida a <b>MERCK ACADEMY,</b> una plataforma virtual con lo más actual en contenidos médicos relacionados con su práctica diaria. Todo con un amplio rigor científico.<br />
                                En <b>MERCK</b> sabemos la importanccia de la actualización médica y con el afán de contribuir en el crecimiento profesional de los especialistas en tratar el dolor de la población ponemos a su disposición una serie de contenidos académicos en un sólo lugar.
                            <br />
                                Al ser miembro de <b>MERCK ACADEMY</b> podrá acceder a diversos materiales que van desde la lectura de un artículo escrito por un líder de opinión hasta ser elegido para cursar un programa completo de <b>actualización médica</b> con validez oficial. <b>Todos los contenidos están segmentados de acuerdo a su perfil profesional</b>, de este modo tendrá acceso inmediato a lo que usted necesita.<br />
                                Acceda ahora mismo a todos los contenidos que <b>MERCK ACADEMY</b> tiene para usted:
                                
                            </p>
                            <ul style="margin-bottom: 5em; line-height: 2em;">
                                <li>Noticias</li>
                                <li>Artículos</li>
                                <li>eBooks</li>
                                <li>Videos</li>
                                <li>Video-conferencias</li>
                                <li>Invitaciones a talleres presenciales</li>
                                <li>Cursos online</li>
                            </ul>

                        </div>
                        <div class="col-md-2 hidden-xs hidden-md hidden-sm"></div>
                    </div>
                    <div class="col-md-3 hidden-xs hidden-md hidden-sm"></div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!--Morris Chart-->
    <script src="assets/plugins/morris/morris.min.js"></script>
    <script src="assets/plugins/raphael/raphael-min.js"></script>

    <!-- Dashboard init -->
    <script src="assets/pages/jquery.dashboard.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <!--======= Touch Swipe =========-->
    <script src="js/jquery.touchSwipe.min.js"></script>

    <!--======= Customize =========-->
    <script src="js/responsive_bootstrap_carousel.js"></script>
    <script src="js/logout.js"></script>

</body>
</html>
