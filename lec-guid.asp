﻿<%  
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID = 1034   
%>
<!--#include file="data/con_ma3.inc" -->
<!--#include file="data/con_ma.inc" -->
<%

Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
%>
<%
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

SQL = "select * from tbl_fasciculos where id_fasciculos = " & Request("idr") &""
Cursor.Open SQL, StrConn, 1, 3

If Cursor.RecordCount > 0 then
    Title = Cursor("titulo")
end if
Cursor.Close

Select case Request("idc")
    Case 1
        icono =""
    Case 2
        icono = "medicinaInterna"
    Case 3
        icono = "medicinaGeneral"
    Case 4
        icono = "ortopedia"
    Case 5
        icono = "revisioDelMes"

  End Select  
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css" />
    <style>
        .certificate-top {
            background-color: <%=colorTop %>;
        }

        .certificate-row {
            background-color: <%=color %>;
        }
    </style>
    <link href="css/lec-guid.css" rel="stylesheet" />
    <link href="css/certificados.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.css" rel="stylesheet" />

</head>

<body class="fixed-left">

    <!--#include file="loader.asp" -->

    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        -
        <div class="content-page">

            <div class="content">
                <div class="container" id="mainContainer">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="background-color: #3266b1 !important; padding: 18px 53px;" id="panelLec">
                            <h2><span class="icon-<%=icono %>" style="margin-right: 5px; margin-top: 5px; color: white; font-size: 40px; vertical-align: middle;"></span>&nbsp;Lectura Guiada:</h2>
                            <h3>&nbsp;<%=Title %></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row" id="instructions">
                                <div class="col-md-3">
                                    <img src="images/videoteca/revisiones/<%=Request("idr") %>.png" class="img-responsive" style="max-width: 100%; text-align: -webkit-center;" class="thumb-img" alt="<%=Request("idr")%>" />
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12" style="padding-top: 35px;">
                                            <%SQL = "select * from tbl_objetivos_lecturas where id_fasciculo = " & Request("idr") 
                                              Cursor.Open SQL, StrConn, 1, 3    
                                            %>
                                            <%
                                            If Cursor.RecordCount > 0 then    
                                            %>
                                            <h3 style="color: #3266b1"><b>Instrucciones: </b><%=Cursor("introduccion") %></h3>

                                            <br />
                                            <ul style="color: #3266b1; font-size: 25px">
                                                <%Objetivos=Cursor("objetivos") 
                                            a=Split(Objetivos,"|")
                                            for each x in a
                                                %>
                                                <li>
                                                    <h3 style="color: black"><%=x %></h3>
                                                </li>
                                                <%
                                            next    
                                                %>
                                            </ul>
                                            <%End If %>
                                            <%Cursor.Close() %>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="row" id="ebook" style="display: none">
                                <div class="col-md-12">
                                    <iframe style="width: 100%; height: 70vh;" src="#" id="VCirugias" name="VCirugias" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: -webkit-right; background-color: white" id="footer">
                            <button class="btn btn-success btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" onclick="window.VCirugias.location='fasciculos/<%=Request("idr") %>/?idf=<%=Request("idr") %>';$('#instructions').hide();$('#ebook').show();$('#footer').hide();$('#footer-ebook').show();"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Comenzar</button>
                        </div>
                        <div class="panel-footer" style="text-align: -webkit-right; background-color: white; display: none" id="footer-ebook">
                            <button class="btn btn-info btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" onclick="$('#instructions').show();$('#ebook').hide();$('#footer').show();$('#footer-ebook').hide();"><i class="fa fa-arrow-circle-o-left m-r-5" aria-hidden="true"></i>&nbsp;Regresar</button>
                            <button class="btn btn-success btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" id="btnRedirect"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Continuar</button>
                        </div>
                    </div>

                </div>
                <!--#include file="footer.asp" -->
            </div>
        </div>
    </div>
    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/logout.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.js"></script>
    <script type="text/javascript">
        $("#btnRedirect").click(function () {
            swal({
                title: '¡Aviso!',
                text: "¿Está listo para comenzar la evaluación?",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Si',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> No'
            }).then(function () {
                window.location = '../lec-guid-exam.asp?idr=<%=Request("idr")%>&idc=<%=Request("idc")%>';
            });
        });

    </script>


</body>
</html>
