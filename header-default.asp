<nav class="navbar navbar-default navbar-fixed-top nav-gradient-top" id="pleca">
    <!--<div class="container">-->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <a class="navbar-brand" href="default.asp">
            <img src="assets/images/logos/1.png" class="img-responsive logoMerck" />
        </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <button class="navbar-right open-left hidden-lg hidden-md hidden-xs nav-ipad">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-cube"></span>
        </button>

        <ul class="nav navbar-nav navbar-right menu-index">
            <li id="liContact"><a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-Contact"><span class="fa fa-envelope-o" data-toggle="modal" data-target="#con-close-modal"></span>&nbsp;Contacto</a></li>
            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-soporte"><span class="fa fa-question"></span>&nbsp;Soporte</a></li>
            <li class="hidden-lg hidden-md"><a href="registro-mini.asp" class="menureg"><span class="fa fa-sign-in"></span>&nbsp;Registrarse</a></li>
            <li class="hidden-lg hidden-md"><a href="login.asp" class="menuini"><span class="fa fa-user"></span>&nbsp;Iniciar Sesi&oacute;n</a></li>
            <li class="hidden-sm hidden-xs" id="btn-header-registro" style="background-color: #14a761; font-size: 20px;"><a style="font-size: 20px;" href="registro-mini.asp" class="w-md waves-effect waves-light m-b-5 menureg"><span class="fa fa-sign-in"></span>&nbsp;Registrarse</a></li>
            <li class="hidden-sm hidden-xs" style="background-color: #0f804a; font-size: 20px;"><a style="font-size: 20px;" class="menuini" href="login.asp"><span class="fa fa-user"></span>&nbsp;Iniciar Sesi&oacute;n</a></li>
        </ul>
    </div>
    <!--</div>-->
</nav>
