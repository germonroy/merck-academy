<!--#include file="geoip/GeoIP.asp"-->
<%
Dim oGeoIP,strErrMsg
Dim strIP,strCountryName,strCountryCode

Set oGeoIP = New CountryLookup
oGeoIP.GeoIPDataBase = Server.MapPath("geoip/GeoIP.dat")
If oGeoIP.ErrNum(strErrMsg) <> 0 Then
	Response.Write(strErrMsg)
Else
	strIP = request.ServerVariables("REMOTE_ADDR")
	strCountryName = oGeoIP.lookupCountryName(strIP)
	strCountryCode = oGeoIP.lookupCountryCode(strIP)
End If
Set oGeoIP = Nothing

If strCountryCode = "EC" then
  Response.Redirect "/ec"
else
 If strCountryCode = "BR" then
   Response.Redirect "/br"
 else
   If strCountryCode = "GT" or  strCountryCode = "SV" or  strCountryCode = "HN" or   strCountryCode = "NI" or  strCountryCode = "CR" or  strCountryCode = "PA" or   strCountryCode = "DO"  then
     Response.Redirect "/centroamerica"
    else
        'if strCountryCode = "ID" or strCountryCode = "PH" or strCountryCode = "MX" then
        if strCountryCode = "PH" then
            Response.Redirect "/ph"   
            Session("country") = "Filipinas"
        end if  
        if strCountryCode = "ID" then
            Response.Redirect "/as"   
            Session("country") = "Indonesia"
        end if         
   end if
  end if
end if

%>