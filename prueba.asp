<!--#include file="data/con_ma.inc" -->

<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

     set Cursor = createobject("ADODB.Recordset")
  Cursor.CursorType =1 
  Cursor.LockType = 3


         set Cursor2 = createobject("ADODB.Recordset")
  Cursor2.CursorType =1 
  Cursor2.LockType = 3


    SQL = "select *, (select max(id_ponente) from cat_ponentes) as lastOne from cat_ponentes where activo = 'SI' and mostrar = 1 order by nombre"
    Cursor.Open SQL, StrConn, 1, 3
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css" />
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />

    <!--Flipper-->
    <link href="css/flipper-journals.css" rel="stylesheet">

    <style>
        red {
            z-index: -200000;
        }

        .flipper {
            margin-bottom: 10px;
        }
    </style>
</head>

<body class="fixed-left">

    <!--#include file="loader.asp" -->

    <!-- Begin page -->
    <div id="wrapper">

        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="float: right;">
                                <img src="images/escudos/escudo_profesores.png" class="img-responsive" style="max-width: 290px; height: auto; margin-bottom: 30px;" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12" style="margin-bottom: 5px; padding-bottom: 5px;">
                        <h4 style="color: #243e9f; margin-bottom: 5px; padding-bottom: 5px;"><strong><i class="fa fa-info-circle"></i>&nbsp;Listado de docentes y su resumen curricular.</strong></h4>
                    </div>
                </div>

                <div class="row">
                    <% 
        genero = ""
        Do While not Cursor.EOF
            if Cursor("cv") = 1 then
                    %>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div id="touch<%=Cursor("id_ponente") %>" class="flip-container" ontouchstart="this.classList.toggle('hover');">
                            <div class="flipper">
                                <div class="front">
                                    <img class="img-responsive" src="images/profesores/front_<%=Cursor("id_ponente") %>.jpg" alt="<%=Cursor("titulo") %>&nbsp;<%=Cursor("nombre") %>" style="width: 100%" />
                                </div>
                                <div class="back">
                                    <img class="img-responsive" src="images/profesores/back_<%=Cursor("id_ponente") %>.jpg" alt="CV" style="width: 100%" />

                                    <a href="javascript:void(0);" onclick="document.getElementById('fullImage').src = 'images/profesores/<%=Cursor("id_ponente") %>.jpg'; document.getElementById('tituloCV').innerHTML = '<%=Cursor("titulo") %>&nbsp;<%=Cursor("nombre") %>';" disabled data-toggle="modal" data-target="#full-width-modal-cv">
                                        <button type="button" class="btn btn-primary pull-right" style="margin-top: -40px; right: 3vh; position: absolute; z-index: 3;"><i class="icon-profesores"></i>&nbsp;Ver Currículum</button>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <%
            else
                    %>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="sin-flip-container">
                            <div class="sin-flipper">
                                <div class="front">
                                    <img class="img-responsive" src="images/profesores/front_<%=Cursor("id_ponente") %>.jpg" alt="<%=Cursor("titulo") %>&nbsp;<%=Cursor("nombre") %>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <%

            end if
        Cursor.MoveNext
        Loop
        'Cursor.Close
        Cursor.MoveFirst    
                    %>
                </div>

                <!--Modal-->
                <div id="full-width-modal-cv" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-full">
                        <div class="modal-content">
                            <div class="modal-header hidden-xs">
                                <button type="button" id="close1" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.FrameCV.location='blank.htm';">×</button>
                                <img style="width: 100%" src="images/header_revisiones.png" class="hidden-xs" />
                            </div>
                            <h4 class="modal-title hidden-xs" style="z-index: 3; color: #fff; position: absolute; top: 60px; right: 35px;" id="full-width-modalLabel"><span id="tituloCV"></span></h4>
                            <img id="fullImage" src="#" class="img-responsive center-block" />
                            <!--<iframe style="width: 100%; height: 70vh;" src="#" id="FrameCV" name="FrameCV" frameborder="0" allowfullscreen></iframe>-->
                            <div class="modal-footer">
                                <button type="button" id="close2" class="btn btn-default waves-effect" data-dismiss="modal" onclick="window.FrameCV.location='blank.htm';">Cerrar</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!--End Modal-->

            </div>
            <!-- container -->
            <!--#include file="footer.asp" -->
        </div>
        <!-- content -->
    </div>
    <!-- END wrapper -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <!--<script src="assets/js/waves.js"></script>-->
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(window).load(function(){
            $('#full-width-modal-cv').modal('show');
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>


    <script src="js/MetroJs.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
            $(".appbar").applicationBar();
            //         $(".live-tile").liveTile("goto", { index: "next", autoAniDirection: true });
        });
        /* ==============================================
        LOADER -->
        =============================================== */

        $(window).load(function () {
            $('#loader').delay(300).fadeOut('slow');
            $('#loader-container').delay(200).fadeOut('slow');
            $('body').delay(300).css({ 'overflow': 'visible' });
        })
    </script>

    <script>
        var touch = 'ontouchstart' in document.documentElement
            || (navigator.MaxTouchPoints > 0)
            || (navigator.msMaxTouchPoints > 0);

            if (touch) { // remove all :hover stylesheets
                try { // prevent exception on browsers not supporting DOM styleSheets properly
                    for (var si in document.styleSheets) {
                        var styleSheet = document.styleSheets[si];
                        if (!styleSheet.rules) continue;

                        for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
                            if (!styleSheet.rules[ri].selectorText) continue;

                            if (styleSheet.rules[ri].selectorText.match(':hover')) {
                                styleSheet.deleteRule(ri);
                            }
                        }
                    }
                } catch (ex) {}
            }

        $("#close1").click(function(){
            $(".flip-touch").removeClass("flip-touch"); 
        });

        $("#close2").click(function(){
            $(".flip-touch").removeClass("flip-touch"); 
        });


        <%
            for i = 1 to Cursor("lastOne")
                idtouch = "touch" & i
        %>
            $("#<%=idtouch %>").click(function(){

                if ($("#<%=idtouch %>").hasClass("flip-touch")) {
                    $("#<%=idtouch %>").removeClass("flip-touch"); 
                }
                else {              
                   $(".flip-touch").removeClass("flip-touch"); 
                   $("#<%=idtouch %>").delay(200).addClass("flip-touch");
                }
            });
        <%
            next
        %>

    </script>

</body>
</html>
