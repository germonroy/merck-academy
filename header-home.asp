<nav class="navbar navbar-default navbar-fixed-top nav-gradient-top" id="pleca">
    <!--<div class="container">-->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <a class="navbar-brand" href="home.asp">
            <img src="assets/images/logos/1.png" class="img-responsive logoMerck" />
        </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <button class="navbar-right open-left hidden-lg hidden-md hidden-xs nav-ipad">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-cube"></span>
        </button>

        <ul class="nav navbar-nav navbar-right menu-index">
            <%if btn_home = "SI" then %>
            <li><a href="home.asp"><span class="fa fa-home"></span>&nbsp;Home</a></li>
            <%end if %>
            <li id="liContact"><a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-Contact"><span class="fa fa-envelope-o" data-toggle="modal" data-target="#con-close-modal"></span>&nbsp;Contacto</a></li>
            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-soporte"><span class="fa fa-question"></span>&nbsp;Soporte</a></li>
            <li class="hidden-sm hidden-xs">
                <form class="navbar-form navbar-right app-search" role="form" name="search" id="search" method="post" action="jq/jq_search.asp">
                    <div class="form-group">

                        <input type="text" placeholder="Buscar..." class="form-control" name="search" id="search" style="background-color: white;">


                        <a href="javascript:void(0);" onclick="document.search.submit() ;"><i class="fa fa-search" style="margin-top: 12px;"></i></a>
                    </div>
                </form>
            </li>
        </ul>
    </div>
    <!--</div>-->
</nav>