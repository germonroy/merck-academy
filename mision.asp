<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales m�dicos le da la m�s cordial bienvenida a la plataforma digital inteligente de contenidos m�dicos dise�ados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />
</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 hidden-xs imagenGrande">
                            <img src="images/mision.jpg" class="img-responsive" style="width: 120%;" />
                        </div>
                        <div class="col-md-12 hidden-sm hidden-md hidden-lg imagenGrande">
                            <img src="images/mision_m.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <!--#include file="footer.asp" -->
                </div>
            </div>
        </div>
    </div>
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>
    <script src="js/MetroJs.js" type="text/javascript"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="js/logout.js"></script>
</body>
</html>
