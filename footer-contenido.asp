<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

%>
<link href="css/footer.css" rel="stylesheet" />
<div style="height: 80px;"></div>
<div class="cookie-pop">
    <div class="row knockout-around">
            <div align="center" class="col-xs-12 hidden-xs">
            <a href="http://www.merck.com.mx/es/index.html" target="_blank">Conozca m&aacute;s sobre
                    <img src="images/logo_merck.png" style="width: 70px;" /></a>
            / <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-terminos-condiciones">T&eacute;rminos y condiciones </a>/ 
        <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-aviso-privacidad">Aviso de privacidad</a>
                  / 
        <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-cookies">Pol&iacute;tica de cookies</a>
                <br />
            Informaci&oacute;n exclusiva para profesionales de la salud
            <br />
            Desarrollado por Inkuba Project de M&eacute;xico, S.A. de CV. Derechos reservados 2016. 

            <div class="row num-aviso">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    N&uacute;mero de aviso 153300202C1679
                </div>
            </div>
        </div>
        <div align="center" class="hidden-lg hidden-md hidden-sm col-xs-12">
            <div class="row" id="footer-movil">
                <div class="col-xs-4 text-center center-block">
                    <a href="http://www.merck.com.mx/es/index.html" target="_blank">
                        <img src="images/iconos/merck.png" style="margin-bottom: 5px; width: 40%;" />
                    </a>
                </div>
                <div class="col-xs-4 text-center center-block">
                    <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-terminos-condiciones">
                        <img src="images/terminos2.png" style="margin-bottom: 5px;     width: 130%;" />
                    </a>
                </div>
                <div class="col-xs-4 text-center center-block">
                    <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-aviso-privacidad">
                        <img src="images/legales.png" style="margin-bottom: 5px;" />
                    </a>
                </div>
                <div class="row num-aviso">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        N&uacute;mero de aviso 153300202C1679
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>
<script src="js/responsive_bootstrap_carousel.js"></script>
<script src="js/switch-leftbar.js"></script>
