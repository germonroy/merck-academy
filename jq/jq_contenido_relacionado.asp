<!--#include file="../data/con_ma.inc" -->
<%
Session.LCID = 1033   

set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

filtro = Request("filtro")
activeAUX = "active"

'Response.Write("el filtro es: " & filtro)

if filtro = "" then
'Response.Write("entro")
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images from tbl_videos where id_video = "&Request("idv")
Cursor.Open SQL, StrConn, 1, 3
%>
<div id="contenidoRelacionado" class="carousel slide contRelacionado" data-ride="carousel">
    <div id="adv_range_6_columns_carousel" class="carousel slide six_shows_one_move range_slides_carousel_wrapper" style="width: 100%" data-ride="carousel" data-interval="4000" data-pause="hover">
        <div class="carousel-inner range_slides_carousel_inner" role="listbox">
            <!--Revisiones del mes-->
            <% if isNull(Cursor("ids_fasciculos")) then
                else
                SQL2 = "select top 5 * from tbl_fasciculos where id_fasciculos in (" & Cursor("ids_fasciculos") &") and activo='SI' and disponible= 'SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
if Cursor2.RecordCount > 0 then
Do While Not Cursor2.EOF
            %>
            <div class="item <%=activeAUX %>">
                <div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">
                    <figure class="effect-honey">
                        <div class="col-md-4">
                            <a href="javascript:void(0);" class="btnscrolltop" onclick="window.contenidoFrame.location='fasciculos/<%=Cursor2("id_fasciculos") %>/?idf=<%=Cursor2("id_fasciculos") %>';document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                                <img src="images/videoteca/revisiones/<%=Cursor2("id_fasciculos") %>.png" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="work-thumbnail">
                            </a>
                        </div>
                    </figure>
                </div>
            </div>
            <% Cursor2.MoveNext
activeAUX = ""
Loop
end if
Cursor2.Close
end if%>
        </div>
        <!-- Left and right controls -->
        <a class="lec-left carousel-control" href="#adv_range_6_columns_carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="lec-right carousel-control" href="#adv_range_6_columns_carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<% Cursor.Close
end if 'Si no hay filtro %>

<%if filtro = "revision" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images from tbl_videos where id_video = "&Request("idv")
Cursor.Open SQL, StrConn, 1, 3
%>
<div id="contenidoRelacionado" class="carousel slide contRelacionado" data-ride="carousel">
    <div class="carousel fdi-Carousel slide onebyone-carosel" id="eventCarousel" data-interval="0">
        <div class="carousel-inner">
            <!--Revisiones del mes-->
            <% if isNull(Cursor("ids_fasciculos")) then
                else
                SQL2 = "select top 5 * from tbl_fasciculos where id_fasciculos in (" & Cursor("ids_fasciculos") &") and activo='SI' and disponible= 'SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
if Cursor2.RecordCount > 0 then
Do While Not Cursor2.EOF
            %>
            <div class="item fasifil <%=activeAUX %>" data-category="fasifil">
                <figure class="effect-honey">
                    <div class="col-md-4">
                        <a href="javascript:void(0);" class="btnscrolltop" onclick="window.contenidoFrame.location='fasciculos/<%=Cursor2("id_fasciculos") %>/?idf=<%=Cursor2("id_fasciculos") %>';document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                            <img src="images/videoteca/revisiones/<%=Cursor2("id_fasciculos") %>.png" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="work-thumbnail">
                        </a>
                    </div>
                </figure>
            </div>
            <% Cursor2.MoveNext
activeAUX = ""
Loop
end if
Cursor2.Close
end if%>
        </div>
    </div>
    <!-- Left and right controls -->
    <a class="lec-left carousel-control" href="#contenidoRelacionado" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="lec-right carousel-control" href="#contenidoRelacionado" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<% Cursor.Close
end if %>

<%if filtro = "ebooks" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images from tbl_videos where id_video = "&Request("idv")
Cursor.Open SQL, StrConn, 1, 3
%>
<div id="contenidoRelacionado" class="carousel slide contRelacionado" data-ride="carousel">
    <div class="carousel fdi-Carousel slide onebyone-carosel" id="eventCarousel" data-interval="0">
        <div class="carousel-inner">

            <!--Ebooks-->
            <% if isNull(Cursor("ids_libros")) then
                else
                SQL2="select top 5 * from tbl_libros_revistas where id_libro in (" & Cursor("ids_libros") &") and activo='SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
Do While Not Cursor2.EOF
            %>
            <div class="item librosfil <%=activeAUX %>" data-category="librosfil">
                <figure class="effect-honey">
                    <div class="col-md-4">
                        <a href="javascript:void(0);" class="btnscrolltop" onclick="window.contenidoFrame.location='ebooks/<%=Cursor2("id_libro") %>/?idf=<%=Cursor2("id_libro") %>';document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                            <img src="images/videoteca/ebooks/<%=Cursor2("id_libro") %>.png" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="work-thumbnail">
                        </a>
                    </div>
                </figure>
            </div>
            <% Cursor2.MoveNext
activeAUX = ""
Loop
Cursor2.Close
end if%>
        </div>
    </div>
    <!-- Left and right controls -->
    <a class="lec-left carousel-control" href="#contenidoRelacionado" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="lec-right carousel-control" href="#contenidoRelacionado" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<% Cursor.Close
end if %>

<%if filtro = "interactivos" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images from tbl_videos where id_video = "&Request("idv")
Cursor.Open SQL, StrConn, 1, 3
%>
<div id="contenidoRelacionado" class="carousel slide contRelacionado" data-ride="carousel">
    <div class="carousel fdi-Carousel slide onebyone-carosel" id="eventCarousel" data-interval="0">
        <div class="carousel-inner">

            <!--Dragables-->
            <% if isNull(Cursor("ids_dragables")) then
                else
                SQL2="select top 5 * from tbl_dragables where id_dragable in (" & Cursor("ids_dragables") &") and activo='SI' order by newid()"
Cursor2.Open SQL2, StrConn, 1, 3
Do While Not Cursor2.EOF
            %>
            <div class="item drafil <%=activeAUX %>" data-category="drafil">
                <figure class="effect-honey">
                    <div class="col-md-4">
                        <a href="javascript:void(0);" class="btnscrolltop" onclick="window.contenidoFrame.location='dragables/<%=Cursor2("id_dragable") %>/?iddrag=<%=Cursor2("id_dragable") %>';document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>'">
                            <img src="images/videoteca/interactivos/<%=Cursor2("id_dragable") %>.jpg" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="<%=Cursor2("id_dragable") %>">
                        </a>
                    </div>
                </figure>
            </div>
            <% Cursor2.MoveNext
activeAUX = ""
Loop
Cursor2.Close
end if%>
        </div>
    </div>
    <!-- Left and right controls -->
    <a class="lec-left carousel-control" href="#contenidoRelacionado" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="lec-right carousel-control" href="#contenidoRelacionado" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<% Cursor.Close
end if %>

<%if filtro = "imagenes" then
SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images from tbl_videos where id_video = "&Request("idv")
Cursor.Open SQL, StrConn, 1, 3
%>
<div id="contenidoRelacionado" class="carousel slide contRelacionado" data-ride="carousel">
    <div class="carousel fdi-Carousel slide onebyone-carosel" id="eventCarousel" data-interval="0">
        <div class="carousel-inner">
            <!--Imagenes-->
            <%  if isNull(Cursor("ids_images")) then
                else
                SQL2 = "select top 5 * from tbl_images where id_imagen in (" & Cursor("ids_images") &") and activo='SI' order by newid()"
                Cursor2.Open SQL2, StrConn, 1, 3
                Do While Not Cursor2.EOF
            %>
            <div class="item imagefil <%=activeAUX %>" data-category="imagefil">
                <figure class="effect-honey">
                    <div class="col-md-4">
                        <a href="javascript:void(0);" class="btnscrolltop" onclick="window.contenidoFrame.location='images/videoteca/bancoimagenes/fullimage/<%=Cursor2("id_imagen") %>.jpg';document.getElementById('tituloLeccion').innerHTML = '<%=Cursor2("titulo") %>';">
                            <img src="images/videoteca/bancoimagenes/<%=Cursor2("id_imagen") %>.jpg" class="img-responsive thumb-img center-block text-center" style="max-width: 60%;" alt="<%=Cursor2("id_imagen") %>">
                        </a>
                    </div>
                </figure>
            </div>
            <% Cursor2.MoveNext
            activeAUX = ""
            Loop
            Cursor2.Close
            end if%>
        </div>
    </div>
    <!-- Left and right controls -->
    <a class="lec-left carousel-control" href="#contenidoRelacionado" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="lec-right carousel-control" href="#contenidoRelacionado" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<% Cursor.Close
end if %>