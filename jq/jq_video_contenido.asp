<!--#include file="../data/con_ma2.inc" -->
<!--#include file="../data/con_ma.inc" -->
<!--#include file="../data/con_ma3.inc" -->
<!--#include file="../includes/config.asp" -->
<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US

Response.addHeader "pragma", "no-cache"
Response.CacheControl = "Private"
Response.Expires = 0
    
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3  

SQL = "SELECT dbo.tbl_videos.id_video, dbo.tbl_videos.source, dbo.tbl_videos.disponible, dbo.tbl_videos.id_modulo, dbo.tbl_videos.id_curso, dbo.tbl_videos.id_ponente, dbo.tbl_videos.titulo, dbo.tbl_videos.duracion, "
SQL = SQL & "dbo.tbl_videos.activo, dbo.cat_ponentes.nombre, dbo.tbl_videos.hash, dbo.cat_ponentes.titulo AS tituloponente, " 
SQL = SQL & "(select COUNT(*) from tbl_top_contenido where id_tipo = tbl_videos.id_video) as top_cont, (SELECT COUNT(*) from ma_15.dbo.progresos where id_usuario = "&Session("id_usuario")&" and ids_vl like '%|'+ "
SQL = SQL & " CONVERT(varchar(10), dbo.tbl_videos.id_video)+'|%') as existe FROM dbo.tbl_videos LEFT OUTER JOIN "
SQL = SQL & "dbo.cat_ponentes ON dbo.tbl_videos.id_ponente = dbo.cat_ponentes.id_ponente WHERE (dbo.tbl_videos.activo = 'SI') AND (dbo.tbl_videos.disponible = 'SI') AND (id_modulo = " & Request("tlpmod") & " or id_modulo2 = " & Request("tlpmod") & ")"
'SQL = SQL & "  AND (id_curso = " & Request("idcurso") & " or id_curso2 = " & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & " or id_curso4 = "&Request("idcurso")&") ORDER by tbl_videos.secuencia"
SQL = SQL & " ORDER by tbl_videos.secuencia"
'Response.Write(SQL)
Cursor.Open SQL, StrConn, 1, 3
Do While Not Cursor.EOF %>
<div class="row">
    <figure class="effect-honey">
        <div class="col-md-5 text-sm-center text-xs-center cut-image">
            <img src="images/videoteca/videos/<%=Cursor("id_video") %>.jpg" class="img-responsive" />
        </div>
        <div class="col-md-6">
            <%if Cursor("top_cont") = 0 then %>
            <h4 class="text-left"><%=Cursor("titulo") %></h4>
            <%else %>
            <h4 class="text-left"><strong><i class="fa fa-star" style="color: #2c65b4;" aria-hidden="true"></i>&nbsp;<%=Cursor("titulo") %></strong></h4>
            <%end if %>
            <h5 class="text-left"><b><%=Cursor("tituloponente") %>&nbsp;<%=Cursor("nombre") %></b></h5>
            <h5 class="text-left"><b>Duraci&oacute;n:&nbsp;<%=Cursor("duracion") %></b></h5>
            <input type="hidden" value="<%=Cursor("titulo") %>" name="titulo" />
            <input type="hidden" value="<%=Cursor("id_video") %>" name="idv" />
            <button class="btn btn-info btn-lg btn-rounded lec-hide btnscrolltop btn-vidlec actcont" onclick="window.contenidoFrame.location='view_cirugias.asp?idv=<%=Cursor("id_video") %>&hv=<%=Cursor("hash") %>&idu=<%=Session("id_usuario") %>&idc=<%=Request("retcurso") %>&idm=<%=Request("tlpmod") %>';">
                <i class="fa fa-television" aria-hidden="true"></i>&nbsp;Ver Video</button>
            <%if Cursor("existe") >= 1 then %>
            <span class="label label-success txt-label"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Visto</span><br />
            <%else %>
            <span class="label label-warning txt-label"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;Pendiente</span><br />
            <%end if %>
        </div>
    </figure>
</div>
<hr />
<%Cursor.MoveNext
Loop
Cursor.Close %>