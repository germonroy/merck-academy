<!--#include file="../data/con_ma2.inc" -->
<!--#include file="../data/con_ma.inc" -->
<!--#include file="../data/con_ma3.inc" -->
<!--#include file="../includes/config.asp" -->
<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US

Response.addHeader "pragma", "no-cache"
Response.CacheControl = "Private"
Response.Expires = 0
    
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3  
%>

<% if Session("completo") = "NO" then  %>
<!--<div style="position: absolute; z-index: 10; width: 96%; background-color: rgba(0,0,0,0.5); height: 96%;">
</div>-->
<% end if %>
<div class="col-md-12 card-box cont-abajo">
    <h3 class="text-black"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>&nbsp;<b>Progreso del M&oacute;dulo</b></span>
        <a href="javascript:void();" class="btn btn-<%=clase %> btn-rounded pull-right btn-hide-topics hidden-sm hidden-xs" data-toggle="tooltip" data-placement="left" title="Ocultar">
            <i class="fa fa-arrow-right" aria-hidden="true"></i>
        </a>
    </h3>
    <div class="col-md-12">
        <div class="row">
            <div class="progress">
                <% SQL = "SELECT porcentaje_vl FROM [ma_15].[dbo].progresos where id_modulo = " & Request("tlpmod") & " and id_usuario = " & Session("id_usuario") 
                    Cursor.Open SQL, StrConn, 1, 3 
                    If Cursor.RecordCount > 0 then
                    Porcentaje  = cint(Cursor("porcentaje_vl"))
                    Else
                    Porcentaje = 0
                    End If
                    Cursor.Close
                    %>
                <div class="progress-bar progress-bar-striped progress-bar-<%=claseGeneral %> active" role="progressbar" aria-valuenow="<%=Porcentaje %>" aria-valuemin="0" aria-valuemax="100" style="width: <%=Porcentaje %>%">
                    <%=Porcentaje %>%                                       
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <h3><b class="lectexto">Lecciones Pendientes</b> <span style="background-color: #3266b1; color: white; padding: 10px;"><a id="totPen"></a>/<a id="totLec"></a></span></h3>
            <div class="row" id="leccionesPendientes">
                <div class="col-md-12">
                    <div id="carLeccionesP" class="carousel slide" data-ride="carousel" data-interval="6000" data-pause="hover">
                        <div class="carousel-inner">
                            <% lecActiva = "active"
                                SQL = "SELECT dbo.tbl_videos.id_video, dbo.tbl_videos.source, dbo.tbl_videos.disponible, dbo.tbl_videos.id_modulo, dbo.tbl_videos.id_curso, dbo.tbl_videos.id_ponente, dbo.tbl_videos.titulo, dbo.tbl_videos.duracion, "
                                SQL = SQL & "dbo.tbl_videos.activo, dbo.cat_ponentes.nombre, dbo.tbl_videos.hash, dbo.cat_ponentes.titulo AS tituloponente, "
                                SQL = SQL & "(SELECT COUNT(*) from ma_15.dbo.progresos where id_modulo = " & Request("tlpmod") & " and id_usuario = " & Session("id_usuario") & " and ids_vl like '%|'+ CONVERT(varchar(10), dbo.tbl_videos.id_video)+'|%') as existe FROM dbo.tbl_videos LEFT OUTER JOIN "
                                SQL = SQL & "dbo.cat_ponentes ON dbo.tbl_videos.id_ponente = dbo.cat_ponentes.id_ponente WHERE (dbo.tbl_videos.activo = 'SI') AND (dbo.tbl_videos.disponible = 'SI') AND (id_modulo = " & Request("tlpmod") & " or id_modulo2 = " & Request("tlpmod") & ")"
                                'SQL = SQL & "  AND (id_curso = " & Request("idcurso") & " or id_curso2 = " & Request("idcurso") & " or id_curso3 = " & Request("idcurso") & ") "
                                SQL = SQL & " ORDER by tbl_videos.secuencia"
                                'Response.Write(SQL)
                                Cursor.Open SQL, StrConn, 1, 3
                                totalLecciones = 0
                                totalPendientes = 0
                                Do While Not Cursor.EOF
                                visto = ltrim(rtrim(Cursor("existe")))
                                if visto = 0 then
                                totalPendientes = totalPendientes + 1 %>
                            <div class="item <%=lecActiva %> lec-item">
                                <figure class="effect-honey">
                                    <div class="col-md-5 text-sm-center text-xs-center cut-image">
                                        <img src="images/videoteca/videos/<%=Cursor("id_video") %>.jpg" class="img-responsive" />
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="text-left"><%=Cursor("titulo") %></h4>
                                        <h5 class="text-left"><b><%=Cursor("tituloponente") %>&nbsp;<%=Cursor("nombre") %></b></h5>
                                        <h5 class="text-left"><b>Duraci&oacute;n:&nbsp;<%=Cursor("duracion") %></b></h5>
                                        <input type="hidden" value="<%=Cursor("titulo") %>" name="titulo" />
                                        <input type="hidden" value="<%=Cursor("id_video") %>" name="idv" />
                                        <button class="btn btn-info btn-lg btn-rounded btnscrolltop actcont btn-vidlec" onclick="window.contenidoFrame.location='view_cirugias.asp?idv=<%=Cursor("id_video") %>&hv=<%=Cursor("hash") %>&idu=<%=Session("id_usuario") %>&idc=<%=Request("retcurso") %>&idm=<%=Request("tlpmod") %>';">
                                            <i class="fa fa-television" aria-hidden="true"></i>&nbsp;Ver Video</button>
                                    </div> 
                                </figure>
                            </div>
                            <%lecActiva = ""   
                                end if
                                Cursor.MoveNext 
                                totalLecciones = totalLecciones + 1                                               
                                Loop
                                Cursor.Close %>
                        </div>
                        <!-- Left and right controls -->
                        <a class="lec-left carousel-control" href="#carLeccionesP" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="lec-right carousel-control" href="#carLeccionesP" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row" id="leccionesPendientesNin">
                <img class="img-responsive" src="images/ningunaleccion.jpg" style="width: 100%;" />
            </div>
        </div>
    </div>
</div>