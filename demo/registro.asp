<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
    <style>
        html {
            background-color: white;
            background: url(images/fondos/fondointerior3.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            font-family: Verdana, Geneva, sans-serif;
            margin: 0;
            color: #797979;
        }

        body {
            background-color: transparent;
        }

        #bg {
            background-image: url(images/fondos/plecaBlanca.png);
            background-size: 100% 100%;
        }

        .grad {
            background: #008bff; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(#008bff, #003d90); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#008bff, #003d90); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#008bff, #003d90); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#008bff, #003d90); /* Standard syntax */
            color: white;
            box-shadow: 0px 5px 5px #888888;
            padding: 0.2em;
            margin-top: 4em;
            margin-bottom: 4em;
        }

            .grad h4 {
                color: white;
                font-weight: bold;
                margin-left: 1.5em;
            }

        .form-control {
            border: 3px solid #E3E3E3;
        }

        /* ESTILOS NORMALES (para móviles en vertical) */
        #div-registro {
            margin-top: 5em;
            margin-bottom: 5em;
        }

        /* RESPONSIVE */

        /* Móviles en horizontal o tablets en vertical*/
        @media (min-width: 768px) {
        }

        /* Tablets en horizonal y escritorios normales */
        @media (min-width: 1024px) {
            #div-registro {
                margin-top: 3em;
                margin-bottom: 5em;
            }
        }

        /*Escritorios muy anchos*/
        @media (min-width: 1200px) {
        }
    </style>
    <script>
         function RefreshImage(valImageId) {
            var objImage = document.images[valImageId];
            if (objImage == undefined) {
                return;
            }
            var now = new Date();
            objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
        }
    </script>
</head>

<body class="fixed-left">
    <!--#include file="top_home.asp" -->
    <main class="page-content">
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row hidden-xs">
                        <div class="col-md-12">
                            <div align="center">
                                <img src="images/logo/logoMerck.png" class="img-responsive" style="margin-top: 18px; margin-bottom: 18px;" />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="bg">
                        <div class="form-group col-lg-12 col-md-12" id="div-registro">
                            <div class="col-md-4">
                                <h1>REGISTRO</h1>
                            </div>
                            <div class="col-md-8">
                                <p>Por favor, ingrese la informaci&oacute;n que se le solicita a continuaci&oacute;n. Es importante que seleccione su especialidad correctamente para asegurar que la informaci&oacute;n que le mostremos sea la adecuada a su perfil. Las casillas marcadas con * son obligatorias.</p>
                            </div>
                            <div class="row">
                                <div class="col-md-12 grad">
                                    <h4><i class="fa fa-user"></i><span>&nbsp;DATOS PERSONALES</span></h4>
                                </div>
                            </div>

                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-4">*Nombre:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-4">*Apellido paterno:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-4">*Apellido materno:<input type="text" class="form-control" id="" /></div>
                            </div>
                            <div class="col-md-2"></div>

                            <div class="row">
                                <div class="col-md-12 grad">
                                    <h4><i class="fa fa-key"></i><span>&nbsp;DATOS PARA SU CUENTA</span></h4>
                                </div>
                            </div>


                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-6">*Correo electr&oacute;nico:<input type="email" class="form-control" id="" /></div>
                                <div class="col-md-6">*Contrase&ntilde;a<input type="password" class="form-control" id="" /></div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-12"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-6">*Confirme su correo:<input type="email" class="form-control" id="" /></div>
                                <div class="col-md-6">*Confirme su contrase&ntilde;a<input type="password" class="form-control" id="" /></div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-md-12"></div>

                            <div class="row">
                                <div class="col-md-12 grad">
                                    <h4><i class="fa fa-university"></i><span>&nbsp;INFORMACIÓN ACADÉMICA Y LABORAL</span></h4>
                                </div>
                            </div>

                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-6">*Especialidad:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-6">*C&eacute;dula profesional<input type="text" class="form-control" id="" /></div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                            <br />
                            <div class="col-md-12"></div>
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-6">*Tipo de pr&aacute;ctica:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-6">*Instituci&oacute;n donde labora<input type="text" class="form-control" id="" /></div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>

                            <div class="row">
                                <div class="col-md-12 grad">
                                    <h4><i class="fa fa-envelope"></i><span>&nbsp;DATOS DE CONTACTO</span></h4>
                                </div>
                            </div>

                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-4">*Calle:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-4">*N&uacute;mero:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-4">*Colonia:<input type="text" class="form-control" id="" /></div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-12"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-3">*C&oacute;digo postal:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-3">*Estado:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-3">*Ciudad:<input type="text" class="form-control" id="" /></div>
                                <div class="col-md-3">*Pa&iacute;s:<input type="text" class="form-control" id="" /></div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-12"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-6">*C&oacute;digo postal:<input type="text" class="form-control" id="" />*Estado:<input type="email" class="form-control" id="" /></div>
                                <div class="col-md-6"></div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-12"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        *Ingrese el c&oacute;digo de seguridad:
                                        <img src="captcha.asp" hspace="2" id="imgCaptcha" class="img-responsive" onclick="RefreshImage('imgCaptcha')">
                                        <!--<a href="javascript:void(0)">
                                            <i class="fa fa-refresh" aria-hidden="true" onclick="RefreshImage('imgCaptcha')"></i>
                                        </a>-->
                                    </div>
                                    <div class="col-md-8">
                                        <span>
                                            <input type="text" class="form-control" id="capchacode" />
                                        </span>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox1" type="checkbox">
                                            <label for="checkbox1">
                                                *Acepto los terminos y condiciones (consultar)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox2" type="checkbox">
                                            <label for="checkbox2">
                                                *He le&iacute;do el aviso de privacidad (consultar)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <a href="home.asp">
                                        <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-5">Registrarse</button>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>


    <script src="js/MetroJs.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
            $(".appbar").applicationBar();
            //         $(".live-tile").liveTile("goto", { index: "next", autoAniDirection: true });
        });
    </script>
</body>
</html>
