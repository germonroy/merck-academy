<link href="css/top-index.css" rel="stylesheet" />
<nav class="navbar navbar-default navbar-fixed-top nav-gradient-top" id="pleca">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="images/logoMerck.png" class="img-responsive logoMerck hidden-lg hidden-sm hidden-md" />
            </a>
        </div>
        <div class="row hidden-lg hidden-md hidden-sm" style="z-index: -10; position: fixed; margin-top: 50px; margin-right: 70px; right: 0;">
            <div class="col-xs-8 text-right pull-right">
                <div class="col-xs-8">
                    <a href="registro.asp">
                        <img src="images/btn_reg.png" class="img-responsive btn-reg-ini" onmouseover="hoverRegistro(this);" onmouseout="unhoverRegistro(this);" />
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="#" id="CargarInformacionMovil">
                        <img src="images/btn_Iniciar_movil.png" class="img-responsive btn-reg-ini" onmouseover="hoverIniciarMovil(this);" onmouseout="unhoverIniciarMovil(this);" />
                    </a>
                </div>
            </div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right menu-index">
                <li><a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-Contact"><span class="fa fa-envelope-o" data-toggle="modal" data-target="#con-close-modal" lang="es"></span>&nbsp;Contacto</a></li>
                <li><a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-soporte"><span class="fa fa-question"></span>&nbsp;Soporte</a></li>
            </ul>
        </div>
        <div class="row hidden-xs" id="botones-regini" style="position: fixed; margin-right: 20px;">
            <div class="col-md-6 text-right" id="botones-registro-inicio">

                <a href="registro.asp">
                    <img src="images/btn_reg.png" class="img-responsive btn-reg-ini" onmouseover="hoverRegistro(this);" onmouseout="unhoverRegistro(this);" />
                </a>

                <a href="#" id="CargarInformacionDesktop">
                    <img src="images/btn_Iniciar.png" class="img-responsive btn-reg-ini" onmouseover="hoverIniciar(this);" onmouseout="unhoverIniciar(this);" />
                </a>
                <a href="javascript:void(0)">
                    <img src="images/chrome-2.png" class="img-responsive  btn-reg-ini" id="chrome" />
                </a>
            </div>
        </div>
    </div>
</nav>

