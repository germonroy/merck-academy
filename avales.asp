<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

home = Request("cur")
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck and their Professional Team welcome you to the Smart digital platform of Medical Contents designed to cover your interests and expectations." />
    <meta name="keywords" content="Merck, Merck Academy, Pain, Neurobion, Pain Experts, Courses, Medicina General, Ortopedia, Merck Online, Medicine" />
    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/avales.css" />
    <%if home <> "" then %>
    <style>
        .content-page .content {
            padding: 0px 5px;
            margin-top: 0px !important;
        }

        .content-page {
            margin-left: 0px;
            overflow: hidden;
        }

        #mainContainer {
            padding: 0px 0px 0px !important;
        }

        #wrapper.enlarged .content-page {
            margin-left: 0px !important;
        }

        <%if home <> "" then %>
         .crop-img {
            /*height: 138px;*/
            overflow: hidden;
            margin-bottom: 10px;
        }

        html {
            background: transparent !important;
        }

        <%end if %> 
        <%if home = "" then %> 
        .spaceImage {
            padding-bottom: 1vw;
        }

        @media only screen and (min-width : 992px) {
            .spaceImage {
                width: 55%;
            }
        }
        <% end if %>
    </style>
    <%end if %>
    <link href="css/scrollbars.css" rel="stylesheet" />
            <style>
                hr {
                    border-top: 2px solid #3266b1;
                }

                .strong-look {
                    font-weight: bold !important;
                    font-size: 32px !important;
                    color: #1a6cb0;
                }

                .cintillo {
                    background-color: rgba(255,255,255, 0.3) !important;
                }
            </style>
</head>
<body class="fixed-left">
    <%if home = "" then %>
    <!--#include file="loader.asp" -->
    <%end if %>
    <div id="wrapper">
        <%if home = "" then %>
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <%end if %>
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <%if home = "" then %>
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="float: right;">
                                <img src="images/escudos/escudo_avales.png" class="img-responsive" style="max-width: 290px; height: auto; margin-bottom: 30px;" />
                            </div>
                        </div>
                    </div>
                    <%end if %>
                    <%if home = "" then %>
                    <div class="row" id="">
                        <div class="cintillo">
                            <h1><span class="fa fa-check-circle iconBanner"></span>&nbsp;Puntos de certificaci&oacute;n</h1>
                            <hr />
                        </div>
                        <div class="col-md-6 text-center">
                            <img src="images/avales/av_conamege.png" class="img-responsive center-block" />
                        </div>
                        <!--<div class="col-md-4 text-center">
                            <img src="images/avales/av_cmim.png" class="img-responsive center-block" />
                            <h2>30 puntos</h2>
                        </div>-->
                        <div class="col-md-6 text-center">
                            <img src="images/avales/av_cmot.png" class="img-responsive center-block" />
                        </div>
                    </div>
                    <%end if %>

                    <%if home <> "" then
                        classAv = "style='margin-top: -2vh;'" 
                      end if %>

                    <div class="row" id="divAvales" <%=classAv %>>
                        <%if home = "" then %>
                        <div class="cintillo">
                            <h1><span class="fa icon-avales iconBanner"></span>&nbsp;Avales</h1>
                            <hr />
                        </div>
                        <%end if %>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_sccot.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_sccot.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_cncd.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_cncd.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_afeme.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_afeme.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_amot.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_amot.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_ametd.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_ametd.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_agot.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_agot.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_clemi.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_clemi.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_shetd.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_shetd.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_amig.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_amig.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_anot.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_anot.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_amis.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_amis.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_ammf.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_ammf.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 center-block text-center crop-img">
                            <%if home = "" then %>
                            <img src="images/avales/av_cmmg.png" class="img-responsive center-block spaceImage" />
                            <%else %>
                            <img src="images/avales/cortados/av_cmmg.png" class="img-responsive center-block spaceImage" />
                            <%end if %>
                        </div>
                    </div>
                </div>
                <% if home = "" then %>
                <!--#include file="footer-avales.asp" -->
                <% end if %>
            </div>
        </div>

    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script src="js/validation.js" type="text/javascript"></script>
    <script src="js/MetroJs.min.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="js/logout.js"></script>
</body>
</html>
