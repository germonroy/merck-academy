<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css" />
    <link href="css/MetroJs.css" rel="stylesheet" type="text/css" />

    <!--Flipper-->
    <link href="css/flipper-journals.css" rel="stylesheet">

    <style>
        html {
            background-color: white;
            background: url(images/fondos/fondointerior3.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            font-family: Verdana, Geneva, sans-serif;
            margin: 0;
            color: #797979;
        }

        body {
            background-color: transparent;
            padding-bottom: 0px !important;
        }

        red {
            z-index: -200000;
        }

        .flipper {
            margin-bottom: 20px;
        }


        @media only screen and (width : 768px) {
        }

        @media only screen and (width : 667px) {
        }

        .btn-group-lg > .btn, .btn-lg {
            padding: 10px 60px !important;
            font-size: 80px;
        }


        @media only screen and (max-width: 410px) {
            .btn-group-lg > .btn, .btn-lg {
                padding: 5px 40px !important;
                font-size: 60px;
            }

            descripcionMerck h2 {
                font-size: 20px;
            }
        }

        @media only screen and (max-width : 479px) {
            .btn-group-lg > .btn, .btn-lg {
                padding: 10px 40px !important;
                font-size: 60px;
            }

            .btn-warning-cross {
                background: linear-gradient(45deg, transparent 30px, #f9c851 0) bottom left;
            }

            .btn-primary-cross {
                background: linear-gradient(45deg, transparent 30px, #188ae2 0) bottom left;
            }

            .btn-danger-cross {
                background: linear-gradient(45deg, transparent 30px, #ff5b5b 0) bottom left;
            }

            .btn-success-cross {
                background: linear-gradient(45deg, transparent 30px, #10c469 0) bottom left;
            }
        }

        .btn-success-cross {
            background: linear-gradient(45deg, transparent 40px, #10c469 0) bottom left;
        }

            .btn-success-cross:hover,
            .btn-success-cross:focus,
            .btn-success-cross:active,
            .btn-success-cross.active,
            .btn-success-cross.focus,
            .btn-success-cross:active,
            .btn-success-cross:focus,
            .btn-success-cross:hover,
            .open > .dropdown-toggle.btn-success-cross {
                background: linear-gradient(45deg, transparent 40px, #0eac5c 0) bottom left;
            }


        .btn-danger-cross {
            background: linear-gradient(45deg, transparent 40px, #ff5b5b 0) bottom left;
        }

            .btn-danger-cross:active,
            .btn-danger-cross:focus,
            .btn-danger-cross:hover,
            .btn-danger-cross.active,
            .btn-danger-cross.focus,
            .btn-danger-cross:active,
            .btn-danger-cross:focus,
            .btn-danger-cross:hover,
            .open > .dropdown-toggle.btn-danger-cross {
                background: linear-gradient(45deg, transparent 40px, #ff4242 0) bottom left;
            }

        .btn-warning-cross {
            background: linear-gradient(45deg, transparent 40px, #f9c851 0) bottom left;
        }

            .btn-warning-cross:hover,
            .btn-warning-cross:focus,
            .btn-warning-cross:active,
            .btn-warning-cross.active,
            .btn-warning-cross.focus,
            .btn-warning-cross:active,
            .btn-warning-cross:focus,
            .btn-warning-cross:hover,
            .open > .dropdown-toggle.btn-warning-cross {
                background: linear-gradient(45deg, transparent 40px, #f8c038 0) bottom left;
            }

        .btn-primary-cross {
            background: linear-gradient(45deg, transparent 40px, #188ae2 0) bottom left;
        }

            .btn-primary-cross:hover,
            .btn-primary-cross:focus,
            .btn-primary-cross:active,
            .btn-primary-cross.active,
            .btn-primary-cross.focus,
            .btn-primary-cross:active,
            .btn-primary-cross:focus,
            .btn-primary-cross:hover,
            .open > .dropdown-toggle.btn-primary-cross {
                background: linear-gradient(45deg, transparent 40px, #167ccb 0) bottom left;
            }

        .iconosTemario {
            font-size: 2em;
            padding: 5px;
        }

        .grad {
            background: #008bff; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(#008bff, #003d90); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#008bff, #003d90); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#008bff, #003d90); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#008bff, #003d90); /* Standard syntax */
            color: white;
            margin-top: 2em;
            margin-bottom: 2em;
            box-shadow: 0px 5px 5px #888888;
        }

            .grad h2 {
                color: white;
                font-weight: bold;
            }

        .carousel-control {
            text-align: left;
        }

            .carousel-control.right {
                text-align: right !important;
            }

        @media only screen and (min-width : 320px) {
            #descripcionMerck {
                padding-top: 3em;
            }

            .grad {
                padding-top: 0.3em;
                padding-bottom: 0.3em;
            }

                .grad h1 {
                    margin-top: 1em;
                    margin-bottom: 1em;
                }
        }

        /* Extra Small Devices, Phones */
        @media only screen and (min-width : 480px) {
            #descripcionMerck {
                padding-top: 2em;
                padding-bottom: 2em;
            }

            .grad {
                padding-top: 0.3em;
                padding-bottom: 0.3em;
            }

                .grad h1 {
                    margin-top: 1em;
                    margin-bottom: 1em;
                }
        }

        /* Small Devices, Tablets */
        @media only screen and (min-width : 768px) {
        }

        /* Medium Devices, Desktops */
        @media only screen and (min-width : 992px) {
        }

        /* Large Devices, Wide Screens */
        @media only screen and (min-width : 1200px) {
        }

        #logoInicio {
            padding-top: 5em;
            padding-bottom: 2em;
            background: #fff; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(#fff 95%, transparent); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#fff 95%, transparent); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#fff 95%, transparent); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#fff 95%, transparent); /* Standard syntax */
            color: white;
            margin-top: 2em;
            margin-bottom: 2em;
        }

        #descripcionMerck p {
            font-size: 20px;
        }

        #descripcionMerck b, h2 {
            color: #3266b1;
        }

        .boton-contenido-iphone {
            margin-top: -13%;
            position: absolute;
            right: 10px;
            z-index: 3;
        }

        .btn-default {
            background-color: rgba(255, 255, 255, 1);
        }
    </style>
</head>

<body class="fixed-left">

    <!--#include file="loader.asp" -->

    <!-- Begin page -->
    <div id="wrapper">

        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <!--<a href="reglas.asp">-->
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="images/contenido/01cursos.jpg" alt="3" />
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="images/contenido/01cursos_vuelta.jpg" alt="4" />
                                        <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-cursosEspecialidad">
                                            <button type="button" class="btn btn-default center-block text-center boton-contenido-iphone"><i class="icon-cursoscertificacion"></i>&nbsp;Mostrar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <!--<a >-->
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="images/contenido/02revisiones.jpg" alt="3" />
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="images/contenido/02revisiones_vuelta.jpg" alt="4" />
                                        <a href="rev_mes.asp">
                                            <button type="button" class="btn btn-default center-block text-center boton-contenido-iphone"><i class="icon-revisioDelMes"></i>&nbsp;Mostrar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <!--<a href="vid_cirugias.asp">-->
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="images/contenido/03video.jpg" alt="3" />
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="images/contenido/03video_vuelta.jpg" alt="4" />
                                        <a href="vid_cirugias.asp">
                                            <button type="button" class="btn btn-default center-block text-center boton-contenido-iphone"><i class="icon-videoCirugias"></i>&nbsp;Mostrar</button>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <!--<a href="ebooks.asp">-->
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="images/contenido/04ebooks.jpg" alt="3" />
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="images/contenido/04ebooks_vuelta.jpg" alt="4" />
                                        <a href="ebooks.asp">
                                            <button type="button" class="btn btn-primary center-block text-center boton-contenido-iphone"><i class="icon-eBooks"></i>&nbsp;Mostrar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <!--<a href="journals-elsevier.asp">-->
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="images/contenido/05journals.jpg" alt="3" />
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="images/contenido/05journals_vuelta.jpg" alt="4" />
                                        <a href="journals-elsevier.asp">
                                            <button type="button" class="btn btn-primary center-block text-center boton-contenido-iphone"><i class="icon-Elsevier"></i>&nbsp;Mostrar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <!--<a href="interactivos.asp">-->
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="images/contenido/06interactivos.jpg" alt="3" />
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="images/contenido/06interactivos_vuelta.jpg" alt="4" />
                                        <a href="interactivos.asp">
                                            <button type="button" class="btn btn-primary center-block text-center boton-contenido-iphone"><i class="icon-interactivos"></i>&nbsp;Mostrar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <!--<a href="banco-imagenes.asp">-->
                                <div class="flipper">
                                    <div class="front">
                                        <img class="img-responsive" src="images/contenido/07banco.jpg" alt="3" />
                                    </div>
                                    <div class="back">
                                        <img class="img-responsive" src="images/contenido/07banco_vuelta.jpg" alt="4" />
                                        <a href="banco-imagenes.asp">
                                            <button type="button" class="btn btn-primary center-block text-center boton-contenido-iphone"><i class="icon-bancoImagenes"></i>&nbsp;Mostrar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Modal Cursos-->
                    <div id="full-width-modal-cursosEspecialidad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content p-0 b-0">
                                <div class="panel panel-color panel-primary">
                                    <div class="panel-body">
                                        <div class="row center-block text-center">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=2" data-filter=".mi" type="button" class="btn btn-primary-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-medicinaInterna"></i></a>
                                                <br />
                                                Medicina Interna
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=3" data-filter=".mg" type="button" class="btn btn-warning-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-medicinaGeneral"></i></a>
                                                <br />
                                                Medicina General

                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=4" data-filter=".orto" type="button" class="btn btn-danger-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-ortopedia"></i></a>
                                                <br />
                                                Ortopedia
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 iconosTemario">
                                                <a href="reglas.asp?idcurso=5" data-filter=".odon" type="button" class="btn btn-success-cross btn-lg waves-effect waves-light m-b-5"><i class="icon-odontologia"></i></a>
                                                <br />
                                                Odontologia
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Modal Cursos-->


                    <div class="clearfix" style="height: 40px;"></div>

                    <!--#include file="footer.asp" -->
                </div>
                <!-- container -->
            </div>
        </div>
    </div>
    <!-- content -->



    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <!-- END wrapper -->

    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>


    <script src="js/MetroJs.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
            $(".appbar").applicationBar();
            //         $(".live-tile").liveTile("goto", { index: "next", autoAniDirection: true });
        });

        /* ==============================================
LOADER -->
=============================================== */

        $(window).load(function () {
            $('#loader').delay(300).fadeOut('slow');
            $('#loader-container').delay(200).fadeOut('slow');
            $('body').delay(300).css({ 'overflow': 'visible' });
        })

    </script>
</body>
</html>
