<!--#include file="data/con_ma.inc" -->
<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3


%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales m�dicos le da la m�s cordial bienvenida a la plataforma digital inteligente de contenidos m�dicos dise�ados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#e74c3c">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <title>Merck Academy</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/menu_dark.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/simple_carousel.css" rel="stylesheet" media="all">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link href="css/portfolio_columns_video_carousel.css" rel="stylesheet" media="all">
    <!--#include file="includes/google_track.asp" -->
    <script src="assets/js/modernizr.min.js"></script>
    <html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <meta name="language" content="es" />
    <meta http-equiv="Content-Language" content="es" />
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/slick-theme.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css" />
    <link href="css/slick.css" rel="stylesheet" />
    <link href="css/slick-theme.css" rel="stylesheet" />


    <style>
        .nav-colors {
            border-bottom: none;
        }

            .nav-colors a {
                background-color: <%=color %> !important;
                border: 2px solid <%=color%> !important;
            }

            .nav-colors > .active > a,
            .nav-colors > .active > a:focus,
            .nav-colors > .active > a:hover {
                border: 2px solid <%=color %> !important;
                border-bottom-color: <%=color %> !important;
                border-top-color: <%=color %> !important;
                border-left-color: <%=color %> !important;
                border-right-color: <%=color %> !important;
                background-color: <%=color %>;
            }

        h2 {
            color: <%=color %>;
        }

        .certificate-row {
            background-color: <%=color %>;
        }

        .font-600 {
            font-size: x-large;
        }

        .progress {
            border-radius: 0px;
            height: 30px;
        }

        .progress-bar {
            box-shadow: none;
            font-size: 1.5em;
            font-weight: 600;
            line-height: 1.2em;
        }

        .nav-tabs a {
            color: #6fd7e6;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #ffffff;
        }

        .card-box {
            min-height: 100px !important;
        }

        .grad {
            background: #008bff; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(#008bff, #003d90); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#008bff, #003d90); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#008bff, #003d90); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#008bff, #003d90); /* Standard syntax */
            color: white;
            margin-top: 2em;
            margin-bottom: 2em;
            box-shadow: 0px 5px 5px #888888;
            padding-top: 0.5em;
            padding-bottom: 0.5em;
        }

            .grad h2 {
                color: white;
                font-weight: bold;
            }

        html {
            background-color: white;
            background-image: url('');
            width: 98%;
        }

        body {
            background-color: transparent;
        }

        .img-responsive {
            height: 450px !important;
            width: 350px !important;
        }

        .slick-list {
            margin-left: 25px;
        }

        .slick-dotted.slick-slider {
            margin-left: 35px;
        }

        .slick-slider {
            display: table;
            table-layout: fixed;
            width: 100%;
        }
        /*.slick-list {
            margin-left: 25px;
        }*/

        .slick-dotted.slick-slider {
            margin-left: 35px;
        }

        .slick-slider {
            display: table;
            table-layout: fixed;
            width: 100%;
        }

        .slick-prev {
            z-index: 10;
        }

        .slick-list {
            margin-left: 51px;
        }
    </style>
</head>



<body class="fixed-left">

    <div class="row">
        <div class="col-md-12 grad">
            <h2><i class="icon-videoCirugias"></i><span>&nbsp;TEMAS RELACIONADOS </span></h2>
        </div>
        <div class="hidden">
            <span id="IDVideo"></span>
        </div>
        <div class="container">
            <div class="row">

                <div class="col-md-12 form-group">
                    <%
                        WithOutThemes = 0
                        SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images  from tbl_libros_revistas where id_libro = "&Request("ID")&""
                        Cursor.Open SQL, StrConn, 1, 3
                       
                    %>
                    <%If not isNull(Cursor("ids_videos")) or not isNull(Cursor("ids_videos2")) or  not isNull(Cursor("ids_conferencias")) or not isNull(Cursor("ids_fasciculos")) or not isNull(Cursor("ids_libros")) or not isNull(Cursor("ids_dragables")) or not isNull(Cursor("ids_images")) then %>
                    <div id="filters" class="button-group hidden-sm hidden-xs" style="text-align: center">


                        <%li = "" %>
                        <a href="javascript:void(0);" data-filter=".item" style="margin-right: 45px;">
                            <img src="images/corelacion/9.png" />
                        </a>
                        <%If not isNull(Cursor("ids_videos")) then
                             
                         li =  "<a href='javascript:void(0);'  class='btn btn-primary' data-filter='.videosfil'><i class='icon-videoCirugias'></i> Video Cirugias</a>"   +  vbCrLf 
                        %>
                        <% VideosVal =True %>
                        <!--<button class="button" data-filter=".videosfil">Videos</button>-->
                        <a href="javascript:void(0);" data-filter=".videosfil" style="margin-right: 45px;">
                            <img src="images/corelacion/6.png" />
                        </a>
                        <%End If %>

                        <%If not isNull(Cursor("ids_videos2")) then   
                            
                         li =  li + "<a class='btn btn-primary' href='javascript:void(0);' data-filter='.videos2fil'><i class='icon-videoCirugias'></i> Video Lecciones</a>"   +  vbCrLf     
                            
                        %>
                        <% Videos2Val = True %>
                        <!--<button class="button" data-filter=".videos2fil">Videos2</button>-->
                        <a href="javascript:void(0);" data-filter=".videos2fil" style="margin-right: 45px;">
                            <img src="images/corelacion/7.png" />
                        </a>
                        <%End If %>

                        <%If not isNull(Cursor("ids_conferencias")) then   
                         li =  li + "<a class='btn btn-primary'  href='javascript:void(0);' data-filter='.confil'> <i class='icon-cespeciales'></i> Conferecias</a>"   +  vbCrLf         
                            
                        %>
                        <%ConfVal =  True %>
                        <!--<button class="button" data-filter=".confil">Conferencias</button>-->
                        <a href="javascript:void(0);" data-filter=".confil" style="margin-right: 45px;">
                            <img src="images/corelacion/2.png" />
                        </a>
                        <%End If %>

                        <%If not isNull(Cursor("ids_fasciculos")) then 
                            
                         li =  li + "<a  class='btn btn-primary' href='javascript:void(0);' data-filter='.fasifil'><i class='icon-revisioDelMes'></i> Revisiones del Mes</a>"   +  vbCrLf             
                        %>
                        <%FasVal = True %>
                        <!--<button class="button" data-filter=".fasifil">Fasiculos</button>-->
                        <a href="javascript:void(0);" data-filter=".fasifil" style="margin-right: 45px;">
                            <img src="images/corelacion/5.png" />
                        </a>
                        <%End If %>

                        <%If not isNull(Cursor("ids_libros")) then   
                            
                            
                        li =  li + "<a class='btn btn-primary' href='javascript:void(0);' data-filter='.librosfil'><i class='icon-eBooks'></i> Ebooks</a>"   +  vbCrLf                
                        %>
                        <%LibrosVal = True %>
                        <!-- <button class="button" data-filter=".librosfil">Libros</button>-->
                        <a href="javascript:void(0);" data-filter=".librosfil" style="margin-right: 45px;">
                            <img src="images/corelacion/3.png" />
                        </a>
                        <%End If %>

                        <%If not isNull(Cursor("ids_dragables")) then   
                            
                            
                        li =  li + "<a class='btn btn-primary' href='javascript:void(0);' data-filter='.drafil'><i class='icon-interactivos'></i> Interactivos</a>"   +  vbCrLf                  
                        %>
                        <% DraVal = True %>
                        <!--<button class="button" data-filter=".drafil">Dragables</button>-->
                        <a href="javascript:void(0);" data-filter=".drafil" style="margin-right: 45px;">
                            <img src="images/corelacion/4.png" />

                        </a>
                        <%End If %>

                        <%If not isNull(Cursor("ids_images")) then  
                            
                         li =  li + "<a  class='btn btn-primary' href='javascript:void(0);' data-filter='.imagefil'><i class='icon-bancoImagenes'></i> Imagenes</a>"   +  vbCrLf                      
                        %>
                        <%ImagesVal=True  %>

                        <!--<button class="button" data-filter=".imagefil">Imagenes</button>-->
                        <a href="javascript:void(0);" data-filter=".imagefil" style="margin-right: 45px;">
                            <img src="images/corelacion/1.png" />

                        </a>



                        <%End If %>
                    </div>
                    <div id="filters2" class="btn-group btn-group-justified hidden-lg hidden-md hidden-xs filters2">
                        <%=li %>
                    </div>
                    <div id="filters3" class="btn-group btn-group-vertical hidden-lg hidden-md  hidden-sm filters2" style="width: 100%">
                        <%=li %>
                    </div>
                    <%Else %>
                    <%WithOutThemes = 1 %>
                    <div class="row">
                        <div class="col-md-12">
                            <h1>No hay temas de inter&eacute;s para este material</h1>
                        </div>
                    </div>

                    <%End If %>
                    <%Cursor.Close %>
                </div>


            </div>
        </div>

        <div class="container" style="background-color: #a8b4c2">
            <div class="col-md-12 text-center" style="padding-top: 30px; padding-left: 0px; padding-right: 0px;">
                <div class="slider">

                    <%
                        SQL= "select ids_videos,ids_videos2,ids_conferencias,ids_fasciculos,ids_libros,ids_dragables,ids_images from tbl_libros_revistas where id_libro = "&Request("ID")&""
                       
                        Cursor.Open SQL, StrConn, 1, 3
                    %>

                    <%If Cursor.RecordCount > 0 then  %>
                    <% activeAUX = "active" %>
                    <!-- Videos -->
                    <%If VideosVal then  %>

                    <%
                            SQL2="select top 5 * from tbl_videos where id_video in (" & Cursor("ids_videos") &") and activo='SI' and disponible= 'SI'  order by newid()"
                            Cursor2.Open SQL2, StrConn, 1, 3
                    %>

                    <%If Cursor2.RecordCount> 0 then %>

                    <%Do While not Cursor2.EOF   %>
                    <div class="item <%=activeAUX %> videosfil" data-category="videosfil">
                        <!-- <div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">-->
                        <a href="javascript:void(0);" onclick="parent.window.EbooksFrame.location='view_cirugias.asp?idv=<%=Cursor2("id_video") %>&hv=<%=Cursor2("hash") %>&idu=<%=Session("id_usuario") %>&idc=0';parent.scrollIframe(); ">
                            <img src="images/videoteca/videos/<%=Cursor2("id_video") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor2("id_video") %>.<%=Cursor2("titulo") %>">
                        </a>
                        <!-- </div>-->
                    </div>
                    <%
                                if activeAUX = "active" then
                                activeAUX = ""
                                end if
                    %>

                    <%
                                Cursor2.MoveNext
                                Loop
                    %>
                    <%End If %>

                    <%
                            Cursor2.Close
                    %>

                    <% End If %>
                    <!-- Videos Fin -->



                    <!-- Videos 2 -->
                    <%If Videos2Val then  %>

                    <%
                             SQL2="select  top 5 * from tbl_videos where id_video in (" & Cursor("ids_videos2") &") and activo='SI' and disponible= 'SI'  order by newid()"
                             Cursor2.Open SQL2, StrConn, 1, 3
                    %>
                    <%If Cursor2.RecordCount> 0 then %>
                    <%Do While not Cursor2.EOF   %>
                    <div class="item <%=activeAUX %> videos2fil" data-category="videos2fil">
                        <!--<div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">-->
                        <a href="javascript:void(0);" onclick="parent.window.EbooksFrame.location='view_cirugias.asp?idv=<%=Cursor2("id_video") %>&hv=<%=Cursor2("hash") %>&idu=<%=Session("id_usuario") %>&idc=0'; parent.scrollIframe();">
                            <img src="images/videoteca/videos/<%=Cursor2("id_video") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor2("id_video") %>.<%=Cursor2("titulo") %>">
                        </a>
                        <!--</div>-->
                    </div>
                    <%
                                if activeAUX = "active" then
                                activeAUX = ""
                                end if
                    %>

                    <%
                                Cursor2.MoveNext
                                Loop
                    %>


                    <%End If %>


                    <%
                            Cursor2.Close
                    %>

                    <% End If %>
                    <!-- Videos Fin 2 -->




                    <!-- Conferencias -->
                    <%If ConfVal then  %>

                    <%
                    SQL2="select top 5  * from tbl_conferencias where id_conferencia in (" & Cursor("ids_conferencias") &") and activo='SI'  order by newid()"
                            
                    Cursor2.Open SQL2, StrConn, 1, 3
                    %>
                    <%If Cursor2.RecordCount> 0 then %>
                    <%Do While not Cursor2.EOF   %>
                    <div class="item <%=activeAUX %> confil" data-category="confil">
                        <!--<div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">-->
                        <a href="javascript:void(0);" onclick="parent.window.EbooksFrame.location='view_conferencias.asp?idv=<%=Cursor2("id_conferencia") %>';GuardarRegistro3(<%=Cursor2("id_conferencia") %>);parent.scrollIframe();">
                            <img src="images/videoteca/conferencias/<%=Cursor2("id_conferencia") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="work-thumbnail">
                        </a>
                        <!-- </div>-->
                    </div>
                    <%
                                if activeAUX = "active" then
                                activeAUX = ""
                                end if
                    %>

                    <%
                                Cursor2.MoveNext
                                Loop
                    %>


                    <%End If %>


                    <%
                            Cursor2.Close
                    %>

                    <% End If %>
                    <!-- Conferencias Fin -->



                    <!-- Fasiculos -->
                    <%If FasVal then  %>

                    <%
                             SQL2="select top 5  * from tbl_fasciculos where id_fasciculos in (" & Cursor("ids_fasciculos") &") and activo='SI' and disponible= 'SI' order by newid()"
                            Cursor2.Open SQL2, StrConn, 1, 3
                    %>

                    <%If Cursor2.RecordCount> 0 then %>
                    <%Do While not Cursor2.EOF   %>
                    <div class="item <%=activeAUX %> fasifil" data-category="fasifil">
                        <!--<div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">-->
                        <a href="javascript:void(0);" onclick="parent.window.EbooksFrame.location='fasciculos/<%=Cursor2("id_fasciculos") %>/?idf=<%=Cursor2("id_fasciculos") %>&labs=1';parent.scrollIframe();">
                            <img src="images/videoteca/revisiones/<%=Cursor2("id_fasciculos") %>.png" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="work-thumbnail">
                        </a>
                        <!--</div>-->
                    </div>
                    <%
                                if activeAUX = "active" then
                                activeAUX = ""
                                end if
                    %>

                    <%
                                Cursor2.MoveNext
                                Loop
                    %>


                    <%End If %>


                    <%
                            Cursor2.Close
                    %>

                    <% End If %>
                    <!-- Fasiculos Fin -->


                    <!-- Libros -->
                    <%If LibrosVal then  %>

                    <%
                            SQL2="select top 5 * from tbl_libros_revistas where id_libro in (" & Cursor("ids_libros") &") and activo='SI' order by newid()"
                             Cursor2.Open SQL2, StrConn, 1, 3
                    %>
                    <%If Cursor2.RecordCount> 0 then %>
                    <%Do While not Cursor2.EOF   %>
                    <div class="item <%=activeAUX %> librosfil" data-category="librosfil">
                        <!--<div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">-->
                        <a href="javascript:void(0);" onclick="parent.window.EbooksFrame.location='ebooks/<%=Cursor2("id_libro") %>/?idf=<%=Cursor2("id_libro") %>';parent.scrollIframe();">
                            <img src="images/videoteca/ebooks/<%=Cursor2("id_libro") %>.png" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="work-thumbnail" />

                        </a>
                        <!-- </div>-->
                    </div>
                    <%
                                if activeAUX = "active" then
                                activeAUX = ""
                                end if
                    %>

                    <%
                                Cursor2.MoveNext
                                Loop
                    %>


                    <%End If %>


                    <%
                            Cursor2.Close
                    %>

                    <% End If %>
                    <!-- Libros Fin -->



                    <!-- Dragables -->
                    <%If DraVal then  %>

                    <%
                            SQL2="select top 5 * from tbl_dragables where id_dragable in (" & Cursor("ids_dragables") &") and activo='SI' order by newid()"
                             Cursor2.Open SQL2, StrConn, 1, 3
                    %>
                    <%If Cursor2.RecordCount> 0 then %>
                    <%Do While not Cursor2.EOF   %>
                    <div class="item <%=activeAUX %> drafil" data-category="drafil">
                        <!-- <div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">-->
                        <a href="javascript:void(0);" onclick="parent.window.EbooksFrame.location='dragables/<%=Cursor2("id_dragable") %>/?iddrag=<%=Cursor2("id_dragable") %>';parent.scrollIframe();GuardarRegistro(<%=Cursor2("id_dragable") %>);">
                            <img src="images/videoteca/interactivos/<%=Cursor2("id_dragable") %>.jpg" class="img-responsive center-block" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor2("titulo") %>" />

                        </a>
                        <!-- </div>-->
                    </div>
                    <%
                                if activeAUX = "active" then
                                activeAUX = ""
                                end if
                    %>

                    <%
                                Cursor2.MoveNext
                                Loop
                    %>


                    <%End If %>


                    <%
                            Cursor2.Close
                    %>

                    <% End If %>
                    <!-- Dragables Fin -->


                    <!-- Imagenes -->
                    <%If ImagesVal then  %>

                    <%
                            SQL2="select top 5 * from tbl_images where id_imagen in (" & Cursor("ids_images") &") and activo='SI' order by newid()"
                             Cursor2.Open SQL2, StrConn, 1, 3
                    %>

                    <%If Cursor2.RecordCount> 0 then %>
                    <%Do While not Cursor2.EOF   %>
                    <div class="item <%=activeAUX %> imagefil" data-category="imagefil">
                        <!-- <div class="col-xs-12 col-sm-4 col-md-2 range_slides_item_image">-->
                        <a href="javascript:void(0);" onclick="parent.window.EbooksFrame.location='images/videoteca/bancoimagenes/fullimage/<%=Cursor2("id_imagen") %>.jpg';GuardarRegistro2(<%=Cursor2("id_imagen") %>);parent.scrollIframe();">
                            <img src="images/videoteca/bancoimagenes/<%=Cursor2("id_imagen") %>.jpg" class="img-responsive" style="max-width: 100%;" class="thumb-img" alt="<%=Cursor2("titulo") %>" />
                        </a>
                        <!--</div>-->
                    </div>
                    <%
                                if activeAUX = "active" then
                                activeAUX = ""
                                end if
                    %>

                    <%
                                Cursor2.MoveNext
                                Loop
                    %>


                    <%End If %>


                    <%
                            Cursor2.Close
                    %>

                    <% End If %>
                    <!-- Imagenes Fin -->




                    <%End If %>

                    <%Cursor.Close %>
                    <!-- </div>-->

                </div>


            </div>
        </div>

    </div>


    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>


    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/logout.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>




    <!--======= Touch Swipe =========-->
    <script src="js/jquery.touchSwipe.min.js"></script>

    <!--======= Customize =========-->
    <script src="js/responsive_bootstrap_carousel.js"></script>



    <!--Alerts-->
    <script src="assets/plugins/bootstrap-sweetalerts2/sweetalert2.js"></script>
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/slick.min.js"></script>

    <script type="text/javascript">
        var slide = $('.slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots: true,
            cssEase: 'linear',
            responsive: [
                  {
                      breakpoint: 1024,
                      settings: {
                          slidesToShow: 2,
                          slidesToScroll: 2,
                          infinite: true,
                          dots: true
                      }
                  },
                  {
                      breakpoint: 600,
                      settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1
                      }
                  },
                  {
                      breakpoint: 480,
                      settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1
                      }
                  }
                  // You can unslick at a given breakpoint now by adding:
                  // settings: "unslick"
                  // instead of a settings object
            ]
        });



        $('#filters').on('click', 'a', function () {
            $('.slider').slick('slickUnfilter');
            var filterValue = $(this).attr('data-filter');
            $('.slider').slick('slickFilter', '' + filterValue + '');
        });
        $('#filters2').on('click', 'a', function () {
            $('.slider').slick('slickUnfilter');
            var filterValue = $(this).attr('data-filter');
            $('.slider').slick('slickFilter', '' + filterValue + '');
        });
    </script>


    <script>
        function GuardarRegistro(id_contenido)
        {
          var table = 'dragables';

            $.post("jq/jq_savetrack_generico_dragables.asp",
                        {
                            tabla: table,
                            id:id_contenido
                        },
                        function(data, status){
                            //alert("Data: " + data + "\nStatus: " + status);
                           

                        });

        }
    </script>

    <script>
         function GuardarRegistro2(id_contenido) {
                var tabla = "galerias";
                $.ajax({
                    url: "jq/jq_savetrack_galerias.asp",
                    type: "POST",
                    data: {"tabla": tabla, "id": id_contenido },
                    async: false,
                    success: function (opciones) {
//                        alert(opciones);
                    }
                })
            }

    </script>
    <script>
        function GuardarRegistro3(id_contenido)
        {
          var table = 'conferencias';

            $.post("jq/jq_savetrack_generico.asp",
                        {
                            tabla: table,
                            id:id_contenido
                        },
                        function(data, status){
                            //alert("Data: " + data + "\nStatus: " + status);
                           

                        });

        }
    </script>
    <%If WithOutThemes  = 1 then %>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#FrameContenido', window.parent.document).height('20vh');
        });
    </script>
    <%Else %>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#FrameContenido', window.parent.document).height('90vh');
        });
    </script>
    <%End If %>
</body>
</html>
