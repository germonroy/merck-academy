﻿<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
    <link href="css/login.css" rel="stylesheet" />

    <style>
        .Tache {
            opacity: 10 !important;
        }
    </style>
    <link href="assets/css/core-default.css" rel="stylesheet" type="text/css" />
</head>

<body class="fixed-left">
    <!--#include file="modal.asp" -->
    <!--#include file="header-registro.asp" -->
    <main class="page-content">
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row" id="bg" style="margin-top: 2vw;">
                        <form data-toggle="validator" role="form" id="registro" method="post" action="jq/jq_login.asp">
                            <div class="col-md-4"></div>
                            <div class="form-group col-md-4" style="margin-top: 4em; margin-bottom: 4em;">
                                <div class="row">
                                    <div class="col-md-1" align="center"><i class="fa fa-user fa-3x" aria-hidden="true" style="color: #3266b1;"></i></div>
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Ingrese su usuario o correo electrónico">
                                            <!--    <div class="help-block with-errors">Access data were submitted to indicated address. </div>-->
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1" align="center"><i class="fa fa-lock fa-3x" aria-hidden="true" style="color: #3266b1;"></i></div>
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Ingrese su contraseña">
                                            <!--  <div class="help-block with-errors">Access data were submitted to indicated address. </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#RecoverPassword"><span data-toggle="modal" data-target="#RecoverPassword"></span>&nbsp;¿Olvidó su contraseña?</a>
                                </div>
                                <br />
                                <br />
                                <div class="row">
                                    <div class="col-md-6 col-centered">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox1" type="checkbox">
                                                <label for="checkbox1">
                                                    Recordarme
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-centered" align="center">
                                        <a href="home.asp">
                                            <button type="submit" class="btn btn-primary btn-lg" onclick="GuardarInformacion()">Ingresar</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" align="center"><a href="#">¿No es usuario?</a></div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <a href="registro.asp">
                                            <button type="button" class="btn btn-success">Regístrese ahora</button>
                                        </a>
                                    </div>
                                </div>
                                <br />
                                <div class="row" style="text-align: center;">

                                    <img src="https://www.merck-academy.org/images/chrome.png" />
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <script>
            var resizefunc = [];
    </script>
    <script type="text/javascript">

        function GuardarInformacion() {

            var guardarInformacion = document.getElementById("checkbox1").checked;

            if (guardarInformacion == true) {
                var usuario = document.getElementById("inputEmail").value;
                var pwd = document.getElementById("inputPassword").value;

                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem('user', usuario);
                    localStorage.setItem('pass', pwd);
                }
                else {
                    //alert('No se puede guardar informacion');
                }
            }
        }
    </script>
    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- Dashboard init -->
    <script src="assets/pages/jquery.dashboard.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <!--======= Touch Swipe =========-->
    <script src="js/jquery.touchSwipe.min.js"></script>

    <!--======= Customize =========-->
    <script src="js/responsive_bootstrap_carousel.js"></script>

    <!--Validator-->
    <script src="js/validator.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

    <!--Alerts-->
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.js"></script>
    <script src="assets/pages/jquery.sweet-alert.init.js"></script>

    <%
        if Request("fail") = 1 then
    %>
    <script type="text/javascript">
        $(window).load(function () {
            swal({
                title: "Fallo Inicio de Sesión",
                text: "Usuario/Contraseña incorrecta",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                confirmButtonText: 'Volver a intentar'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>


    <%
        if Request("fail") = 2 then
    %>
    <script type="text/javascript">
        $(window).load(function () {
            swal({
                title: "Fallo Inicio de Sesión",
                text: "Necesitas completar todos los campos",
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: 'btn-warning waves-effect waves-light',
                confirmButtonText: 'Volver a intentar'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>


    <%
        if Request("fail") = 3 then
    %>
    <script type="text/javascript">
        $(window).load(function () {
            swal({
                title: "Correo inexistente",
                text: "El correo que ingresaste no tiene ninguna cuenta asociada",
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: 'btn-warning waves-effect waves-light',
                confirmButtonText: 'Volver a intentar'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>




    <%
        if Request("fail") = 4 then
    %>
    <script type="text/javascript">
        $(window).load(function () {
            swal({
                title: "Recuperacion de Contraseña",
                text: "Te hemos enviado un correo con tu contraseña \n NOTA:Si no encuentras el correo en tu buzón principal, porfavor revisa tu buzón de SPAM",
                type: "success",
                showCancelButton: false,
                confirmButtonClass: 'btn-warning waves-effect waves-light',
                confirmButtonText: 'Ok'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>



    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>

    <div>
        <div class="sweet-overlay" tabindex="-1" style="opacity: -0.01; display: none;"></div>
        <div class="sweet-alert hideSweetAlert" tabindex="-1" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-ouside-click="false" data-has-done-function="false" data-timer="null" style="display: none; margin-top: -190px; opacity: -0.01;">
            <div class="icon error" style="display: none;"><span class="x-mark"><span class="line left"></span><span class="line right"></span></span></div>
            <div class="icon warning" style="display: none;"><span class="body"></span><span class="dot"></span></div>
            <div class="icon info" style="display: none;"></div>
            <div class="icon success" style="display: block;">
                <span class="line tip"></span><span class="line long"></span>
                <div class="placeholder"></div>
                <div class="fix"></div>
            </div>
            <div class="icon custom" style="display: none;"></div>
            <h2>Good job!</h2>
            <p class="lead text-muted" style="display: block;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis</p>
            <p>
                <button class="cancel btn btn-lg btn-default" tabindex="2" style="display: none;">Cancel</button>
                <button class="confirm btn btn-lg btn-primary" tabindex="1" style="display: inline-block;">OK</button>
            </p>
        </div>
    </div>
</body>
</html>
