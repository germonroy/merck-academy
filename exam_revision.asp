﻿<!--#include file="includes/config.asp" -->

<%Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID = 1033
%>
<!--#include file="data/con_ma3.inc" -->
<!--#include file="data/con_ma.inc" -->
<!--#include file="generadores/md5.asp" -->
<%

Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

%>
<%
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set CursorRes = createobject("ADODB.Recordset")
CursorRes.CursorType =1 
CursorRes.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

set Cursor3 = createobject("ADODB.Recordset")
Cursor3.CursorType =1 
Cursor3.LockType = 3
    set Cursor4 = createobject("ADODB.Recordset")
Cursor4.CursorType =1 
Cursor4.LockType = 3

SQL = "select * from tbl_modulos where id_modulo = " & Request("idm") &""
Cursor.Open SQL, StrConn, 1, 3

If Cursor.RecordCount > 0 then
    Title = Cursor("titulo")
end if
Cursor.Close

Select case Request("idc")
    Case 1
        icono =""
    Case 2
        icono = "medicinaInterna"
    Case 3
        icono = "medicinaGeneral"
    Case 4
        icono = "ortopedia"
    Case 5
        icono = "revisioDelMes"

  End Select 

%>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css" />
    <link href="css/lec-guid-exam.css" rel="stylesheet" />
    <link href="css/certificados.css" rel="stylesheet" />
    <link href="icono-lec/style.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.css" rel="stylesheet" />
    <link href="css/reponsive-text.css" rel="stylesheet" />
    <link href="css/exam-revision.css" rel="stylesheet" />
</head>

<body class="fixed-left">

    <!--#include file="loader.asp" -->

    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->

        <div class="content-page">

            <div class="content">
                <div class="container" id="mainContainer">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="background-color: #3266b1 !important; padding: 18px 53px;">
                            <div class="row">

                                <div class="col-md-10" id="panelLec">
                                    <h2 style="color: white"><span class="icon-<%=icono %>" style="margin-right: 5px; margin-top: 5px; color: white; font-size: 40px; vertical-align: middle;"></span>&nbsp;Evaluación Final:</h2>
                                    <%If Request("idm") = 1 or  Request("idm") = 2 or Request("idm") = 3 then%>
                                    <h3 style="color: white">&nbsp;Introducción Tronco Común</h3>
                                    <%Else %>
                                    <h3 style="color: white">&nbsp;<%=Title %></h3>
                                    <%End If %>
                                </div>
                                <div class="col-md-12" id="panelLec2">
                                    <h2 style="color: white"><span class="fa fa-file-text-o" style="margin-right: 5px; margin-top: 5px; color: white; font-size: 40px; vertical-align: middle;"></span>&nbsp;Calificación obtenida:&nbsp;<b><%=Session("Grade") %>%</b></h2>
                                </div>


                            </div>

                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <ol style="font-size: 1.6em; color: black; font-weight: 600;">
                                        <%SQL = "select *  from tbl_preguntas_eval where id_modulo =" & Request("idm") & " and activo = 'SI' order by NEWID()"
                                       Cursor.Open SQL, StrConn, 1, 3
                                        %>
                                        <%If Cursor.RecordCount > 0 then %>
                                        <%TotalPreguntas = Cursor.RecordCount %>
                                        <%Contador = 1 %>
                                        <%Do While not Cursor.EOF      %>
                                        <%TotalRel = 0 %>


                                        <li>
                                            <h2 style="color: black"><%=Cursor("pregunta") %></h2>
                                            <div class="row">
                                                <div class="col-md-9" style="border-right: 1px solid rgba(0, 0, 0, 0.31);">
                                                    <form id="PREG<%=Contador %>">
                                                        <%Select Case Cursor("tipo_pregunta") %>
                                                        <%Case "VF" %>
                                                        <%ResIncorrectaVF = False %>
                                                        <input type="hidden" name="HID<%=Contador %>" id="HID<%=Contador %>" value="VF" />
                                                        <input type="hidden" name="HIDPREG<%=Contador %>" id="HIDPREG<%=Contador %>" value="<%=Cursor("id_pregunta") %>" />
                                                        <input type="hidden" name="HIDVALOR<%=Contador %>" id="HIDVALOR<%=Contador %>" value="<%=Cursor("porcentaje") %>" />
                                                        <%SQL = "select * from tbl_respuestas_eval_vf  where id_pregunta = " & Cursor("id_pregunta") & "and activo='SI 'order by NEWID()" %>
                                                        <%TotalReactivosVF = 0 %>
                                                        <%TotalCorrectasVF = 0 %>
                                                        <%Cursor2.Open SQL, StrConn, 1, 3 %>
                                                        <%If Cursor2.RecordCount > 0 then  %>
                                                        <%TotalReactivosVF = Cursor2.RecordCount %>
                                                        <input type="hidden" name="HIDNUMPREG<%=Contador %>" id="HIDNUMPREG<%=Contador %>" value="<%=Cursor2.RecordCount%>" />
                                                        <%Cont2= 1 %>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                                                                        <div class="row">
                                                                            <div class="form-group col-md-4 col-sm-4 col-xs-4">
                                                                                <h3><b>Verdadero</b></h3>
                                                                            </div>

                                                                            <div class="form-group col-md-4 col-sm-4 col-xs-4">
                                                                                <h3><b>Falso</b></h3>
                                                                            </div>

                                                                            <div class="form-group col-md-4 col-sm-4 col-xs-4 text-lg-center text-md-center">
                                                                                <h3><b>Correctas</b></h3>
                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%Do While not Cursor2.EOF%>
                                                        <%SQL = "select * from log_respuesta_eval a inner join log_evaluacion_eval   b on a.id_evaluacion = b.id_evaluacion where  a.id_pregunta = " & Cursor("id_pregunta") &"  and a.id_respuesta= " & Cursor2("id_respuesta") & " and b.id_usuario = "&Session("id_usuario")&" and b.id_modulo = " & Request("idm") & " and b.id_curso = " & Request("idc") &" and b.hash= '" & Session("HUExam") &"'" %>
                                                        <%CursorRes.Open SQL, StrConn2, 1, 3 %>
                                                        <%If CursorRes.RecordCount > 0  then%>
                                                        <%SelectResponseVF = CursorRes("respuesta") %>
                                                        <%End If %>
                                                        <%CursorRes.Close %>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-6 text-sm-center text-xs-center text-lg-left text-md-left hidden-lg hidden-md" style="font-size: 24px; margin-bottom: 22px; font-weight: 100">
                                                                    <%=Cursor2("respuesta") %>
                                                                </div>
                                                                <div class="row hidden-md hidden-lg">
                                                                    <div class="form-group col-md-4 col-sm-4 col-xs-4 ">
                                                                        <h3><b>Verdadero</b></h3>
                                                                    </div>

                                                                    <div class="form-group col-md-4 col-sm-4 col-xs-4">
                                                                        <h3><b>Falso</b></h3>
                                                                    </div>

                                                                    <div class="form-group col-md-4 col-sm-4 col-xs-4 text-lg-center text-md-center text-center">
                                                                        <h3><b>Correcta</b></h3>
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6 text-sm-center text-xs-center text-lg-left text-md-left hidden-sm hidden-xs" style="font-size: 24px; margin-bottom: 22px; font-weight: 100">
                                                                        <%=Cursor2("respuesta") %>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <%Select Case SelectResponseVF  %>
                                                                            <%Case "F" %>
                                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                                <div class="onoffswitch">
                                                                                    <input type="radio" name="RESVF<%=Cont2 %>-<%=Contador %>" data-contador="<%=Cursor2("id_respuesta") %>" class="onoffswitch-checkbox" id="RESVF-V<%=Cont2 %>-<%=Contador %>" required disabled />
                                                                                    <label class="onoffswitch-label" for="RESVF-V<%=Cont2 %>-<%=Contador %>">
                                                                                        <span class="onoffswitch-inner-v"></span>
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label>

                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-6">

                                                                                <div class="onoffswitch">
                                                                                    <input type="radio" name="RESVF<%=Cont2 %>-<%=Contador %>" data-contador="<%=Cursor2("id_respuesta") %>" class="onoffswitch-checkbox" id="RESVF-F<%=Cont2 %>-<%=Contador %>" disabled checked />
                                                                                    <label class="onoffswitch-label" for="RESVF-F<%=Cont2 %>-<%=Contador %>">
                                                                                        <span class="onoffswitch-inner-f"></span>
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <%Case "V" %>
                                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                                <div class="onoffswitch">
                                                                                    <input type="radio" name="RESVF<%=Cont2 %>-<%=Contador %>" data-contador="<%=Cursor2("id_respuesta") %>" class="onoffswitch-checkbox" id="RESVF-V<%=Cont2 %>-<%=Contador %>" required disabled checked />
                                                                                    <label class="onoffswitch-label" for="RESVF-V<%=Cont2 %>-<%=Contador %>">
                                                                                        <span class="onoffswitch-inner-v"></span>
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label>

                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-6">

                                                                                <div class="onoffswitch">
                                                                                    <input type="radio" name="RESVF<%=Cont2 %>-<%=Contador %>" data-contador="<%=Cursor2("id_respuesta") %>" class="onoffswitch-checkbox" id="RESVF-F<%=Cont2 %>-<%=Contador %>" disabled />
                                                                                    <label class="onoffswitch-label" for="RESVF-F<%=Cont2 %>-<%=Contador %>">
                                                                                        <span class="onoffswitch-inner-f"></span>
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <%End Select %>
                                                                            <div class="col-md-4 col-sm-4 col-xs-4 text-lg-center text-md-center text-center">
                                                                                <%If Cursor2("correcta") = SelectResponseVF  then  %>
                                                                                <%TotalCorrectasVF = TotalCorrectasVF +1  %>
                                                                                <i class="fa fa-check-circle-o" style="font-size: 40px; color: #10c469"></i>
                                                                                <%Else %>
                                                                                <%ResIncorrectaVF= true %>
                                                                                <i class="fa fa-ban" style="font-size: 40px; color: #ff5b5b"></i>
                                                                                <%End If %>
                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <%
                                                        Cursor2.MoveNext
                                                        Cont2 = Cont2+1
                                                        Loop 
                                                        
                                                        %>
                                                        <%End If %>
                                                        <%Cursor2.Close()  %>







                                                        <%Case "MUL" %>
                                                        <%CorrectaMul =False %>
                                                        <input type="hidden" name="HID<%=Contador %>" id="HID<%=Contador %>" value="MUL" />
                                                        <input type="hidden" name="HIDPREG<%=Contador %>" id="HIDPREG<%=Contador %>" value="<%=Cursor("id_pregunta") %>" />
                                                        <input type="hidden" name="HIDVALOR<%=Contador %>" id="HIDVALOR<%=Contador %>" value="<%=Cursor("porcentaje") %>" />
                                                        <%SQl = "select * from tbl_respuestas_eval_mul where id_pregunta = " & Cursor("id_pregunta") & "and activo='SI 'order by NEWID()" %>
                                                        <%Cursor2.Open SQL, StrConn, 1, 3 %>
                                                        <%If Cursor2.RecordCount > 0 then  %>
                                                        <input type="hidden" name="HIDNUMPREG<%=Contador %>" id="HIDNUMPREG<%=Contador %>" value="<%=Cursor2.RecordCount%>" />


                                                        <%Do While not Cursor2.EOF      %>
                                                        <%SQL = "select * from log_respuesta_eval a inner join log_evaluacion_eval  b on a.id_evaluacion = b.id_evaluacion where  a.id_pregunta = " & Cursor("id_pregunta") &"  and b.id_usuario = "&Session("id_usuario")&" and b.id_modulo = " & Request("idm") & " and b.id_curso = " & Request("idc") &" and b.hash= '" & Session("HUExam") &"'" %>
                                                        <%CursorRes.Open SQL, StrConn2, 1, 3 %>
                                                        <%If CursorRes.RecordCount > 0  then%>
                                                        <%SelectResponse = CursorRes("respuesta") %>
                                                        <%End If %>
                                                        <%CursorRes.Close %>

                                                        <div class="form-inline col-lg-4 col-md-6 col-sm-12 col-xs-12" style="font-size: 20px; display: inline; min-height: 50px">
                                                            <%If cint(SelectResponse) = Cursor2("id_respuesta") then  %>
                                                            <div class="form-group col-md-1  col-sm-2  col-xs-2 respuesta-mul  ">
                                                                <input type="radio" class="option-input radio" id="RESMUL<%=Cursor2("id_respuesta") %>" value="<%=Cursor2("id_respuesta") %>" name="RESMULPREG<%=Contador %>" checked="true" disabled style="margin: 0px !important; top: 0px !important;" />
                                                            </div>
                                                            <div class="form-group col-md-9  col-sm-10  col-xs-10">
                                                                <label for="RESMUL<%=Cursor2("id_respuesta") %>" style="font-weight: 100 !important; display: inline"><%=Cursor2("respuesta") %></label>
                                                            </div>
                                                            <%Else %>
                                                            <div class="form-group col-md-1  col-sm-2  col-xs-2 respuesta-mul  ">
                                                                <input type="radio" class="option-input radio" id="RESMUL<%=Cursor2("id_respuesta") %>" value="<%=Cursor2("id_respuesta") %>" name="RESMULPREG<%=Contador %>" disabled style="margin: 0px !important; top: 0px !important;" />
                                                            </div>
                                                            <div class="form-group col-md-9  col-sm-10  col-xs-10">
                                                                <label for="RESMUL<%=Cursor2("id_respuesta") %>" style="font-weight: 100 !important; display: inline"><%=Cursor2("respuesta") %></label>
                                                            </div>
                                                            <%End If %>
                                                        </div>





                                                        <%
                                                        Cursor2.MoveNext
                                                        Loop 
                                                       
                                                        %>

                                                        <%End If %>
                                                        <% Cursor2.Close()  %>






                                                        <%Case "REL" %>
                                                        <%TotalRel = Cursor("porcentaje") %>
                                                        <%Columna1 = "" %>
                                                        <%Correcta = "" %>
                                                        <%Columna2 = "" %>
                                                        <%ResIncorrectaREL = False %>
                                                        <input type="hidden" name="HID<%=Contador %>" id="HID<%=Contador %>" value="REL" />
                                                        <input type="hidden" name="HIDPREG<%=Contador %>" id="HIDPREG<%=Contador %>" value="<%=Cursor("id_pregunta") %>" />
                                                        <input type="hidden" name="HIDVALOR<%=Contador %>" id="HIDVALOR<%=Contador %>" value="<%=Cursor("porcentaje") %>" />
                                                        <input type="hidden" name="HIDNUMRELACIONADO<%=Contador %>" id="HIDNUMRELACIONADO<%=Contador %>" value="0" />
                                                        <%SQl = "select * from tbl_respuestas_eval_rel where id_pregunta = " & Cursor("id_pregunta") & "and activo='SI 'order by correcta" %>
                                                        <%TotalReactivosRel = 0 %>
                                                        <%TotalCorrectasRel= 0 %>
                                                        <%Cursor2.Open SQL, StrConn, 1, 3 %>
                                                        <%If Cursor2.RecordCount > 0 then  %>
                                                        <%TotalReactivosRel = Cursor2.RecordCount %>
                                                        <input type="hidden" name="HIDNUMPREG<%=Contador %>" id="HIDNUMPREG<%=Contador %>" value="<%=Cursor2.RecordCount%>" />
                                                        <%TotalReactivo = cint(TotalRel)/Cursor2.RecordCount %>
                                                        <div class="col-md-5 col-sm-6 col-xs-6">
                                                            <%Do While not Cursor2.EOF      %>

                                                            <%SQL = "select * from log_respuesta_eval a inner join log_evaluacion_eval  b on a.id_evaluacion = b.id_evaluacion where  a.id_pregunta = " & Cursor("id_pregunta") &"  and a.id_respuesta= " & Cursor2("id_respuesta") & " and b.id_usuario = "&Session("id_usuario")&" and b.id_modulo = " & Request("idm") & " and b.id_curso = " & Request("idc") &"  and b.hash= '" & Session("HUExam") &"'" %>
                                                            <%CursorRes.Open SQL, StrConn2, 1, 3 %>

                                                            <%If CursorRes.RecordCount > 0  then%>
                                                            <%SelectResponseREL = cint(CursorRes("id_respuesta")) %>
                                                            <%End If %>
                                                            <%CursorRes.Close %>
                                                            <%RelCorrecta=Cursor2("correcta") %>


                                                            <div class="col-md-12">
                                                                <div class="drag" id="COLONE<%=Cursor2("id_respuesta") %>" data-respuesta="<%=Cursor2("id_respuesta") %>" data-correcta="<%=Cursor2("correcta") %>" data-total="<%=TotalReactivo %>" data-pregunta="<%=Cursor("id_pregunta") %>" style="z-index: 1000">
                                                                    <h2><%=Cursor2("respuesta") %></h2>
                                                                </div>


                                                            </div>

                                                            <%
                                                            Cursor2.MoveNext
                                                            Loop 
                                                            End If %>
                                                            <% Cursor2.Close()  %>
                                                        </div>


                                                        <div class="col-md-5 col-sm-6 col-xs-6">
                                                            <%SQl = "select *  from tbl_subrespuestas_eval_rel  where id_pregunta in("&Cursor("id_pregunta")&") and activo='SI 'order by id_subrespuesta_eval" %>
                                                            <%Cursor3.Open SQL, StrConn, 1, 3 %>
                                                            <%If Cursor3.RecordCount > 0 then  %>
                                                            <%Do While not Cursor3.EOF      %>
                                                            <div class="col-md-12">
                                                                <div class="drop" id="drop-<%=Cursor3("id_subrespuesta_eval") %>" data-relacion="<%=Cursor3("id_subrespuesta_eval") %>" data-numpreg="<%=Contador %>">
                                                                    <h2><%=Cursor3("Respuesta") %></h2>
                                                                </div>
                                                            </div>
                                                            <%
                                                        Cursor3.MoveNext
                                                        Loop  
                                                            %>
                                                            <%End If %>
                                                            <%Cursor3.Close %>
                                                        </div>
                                                        <div class="col-md-2 col-sm-12 col-xs-12 text-center">
                                                            <div class="form-group" style="text-align: center">
                                                                <h3><b>Correctas</b></h3>
                                                            </div>
                                                            <%SQL = "select * from tbl_respuestas_eval_rel where id_pregunta = " & Cursor("id_pregunta") & "and activo='SI 'order by correcta" %>

                                                            <%Cursor2.Open SQL, StrConn, 1, 3 %>
                                                            <%If Cursor2.RecordCount > 0 then  %>
                                                            <%Do While not Cursor2.EOF      %>

                                                            <%SQL = "select * from log_respuesta_eval a inner join log_evaluacion_eval  b on a.id_evaluacion = b.id_evaluacion where  a.id_pregunta = " & Cursor("id_pregunta") &"  and a.id_respuesta= " & Cursor2("id_respuesta") & " and b.id_usuario = "&Session("id_usuario")&" and b.id_modulo = " & Request("idm") & " and b.id_curso = " & Request("idc") &"" %>
                                                            <%CursorRes.Open SQL, StrConn2, 1, 3 %>
                                                            <%If CursorRes.RecordCount > 0  then%>
                                                            <%SelectResponseREL = cint(CursorRes("respuesta")) %>
                                                            <%End If %>
                                                            <%CursorRes.Close %>
                                                            <%RelCorrecta=Cursor2("correcta") %>
                                                            <%If RelCorrecta = SelectResponseREL then %>
                                                            <%TotalCorrectasRel = TotalCorrectasRel +1  %>
                                                            <div class="col-md-12">
                                                                <i class="fa fa-check-circle-o" style="font-size: 40px; color: #10c469"></i>
                                                            </div>

                                                            <%Else %>
                                                            <%ResIncorrectaREL = True %>
                                                            <div class="col-md-12">
                                                                <i class="fa fa-ban" style="font-size: 40px; color: #ff5b5b"></i>
                                                            </div>

                                                            <%End If %>
                                                            <%
                                                            Cursor2.MoveNext
                                                            Loop 
                                                            End If %>
                                                            <% Cursor2.Close()  %>
                                                        </div>



                                                        <%End Select %>
                                                    </form>


                                                </div>

                                                <div class="col-md-3 col-sm-12 col-xs-12" style="text-align: center">
                                                    <%Select Case Cursor("tipo_pregunta") %>
                                                    <%Case "VF" %>
                                                    <%If ResIncorrectaVF then %>
                                                    <div class="row">
                                                        <div class="label label-danger">Incorrecta</div>
                                                    </div>
                                                    <div class="row">
                                                        <h1 style="margin-top: 10px; color: #ff5b5b"><%=Round(((Cursor("porcentaje")/TotalReactivosVF)*TotalCorrectasVF),2)%>%</h1>
                                                    </div>
                                                    <%Else %>
                                                    <div class="row">
                                                        <div class="label label-success">Correcta</div>
                                                    </div>
                                                    <div class="row">
                                                        <h1 style="margin-top: 10px; color: #10c469"><%=Round(((Cursor("porcentaje")/TotalReactivosVF)*TotalCorrectasVF),2)%>%</h1>
                                                    </div>

                                                    <%End If %>


                                                    <%Case "MUL" %>
                                                    <%SQl = "select * from tbl_respuestas_eval_mul where id_respuesta = " & cint(SelectResponse) %>
                                                    <%CursorRes.Open SQL, StrConn, 1, 3 %>
                                                    <%If CursorRes.RecordCount > 0  then%>
                                                    <%If CursorRes("correcta") = "SI"  then   %>
                                                    <div class="row">
                                                        <div class="label label-success">Correcta</div>
                                                    </div>
                                                    <div class="row">
                                                        <h1 style="margin-top: 10px; color: #10c469"><%=Round(Cursor("porcentaje"),2)%>%</h1>
                                                    </div>
                                                    <%Else %>
                                                    <div class="row">
                                                        <div class="label label-danger">Incorrecta</div>
                                                    </div>
                                                    <div class="row">
                                                        <h1 style="margin-top: 10px; color: #ff5b5b">0%</h1>
                                                    </div>
                                                    <%End if %>
                                                    <%End If %>
                                                    <%CursorRes.Close %>




                                                    <%Case "REL" %>
                                                    <%If ResIncorrectaREL then %>
                                                    <div class="row">
                                                        <div class="label label-danger">Incorrecta</div>
                                                    </div>
                                                    <div class="row">
                                                        <h1 style="margin-top: 10px; color: #ff5b5b"><%=Round(((Cursor("porcentaje")/TotalReactivosRel)*TotalCorrectasRel),2)%>%</h1>
                                                    </div>
                                                    <%Else %>
                                                    <div class="row">
                                                        <div class="label label-success">Correcta</div>
                                                    </div>
                                                    <div class="row">
                                                        <h1 style="margin-top: 10px; color: #10c469"><%=Round(((Cursor("porcentaje")/TotalReactivosRel)*TotalCorrectasRel),2)%>%</h1>
                                                    </div>
                                                    <%End If %>



                                                    <%End Select %>
                                                </div>

                                            </div>
                                        </li>

                                        <hr />
                                        <%                      
                                        Cursor.MoveNext
                                        Contador = Contador +1 
                                        Loop 
                                   
                                        %>
                                        <%End If %>
                                        <%Cursor.Close()%>
                                    </ol>

                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: -webkit-right; background-color: white" id="footer">
                            <%If Session("MOD"& Request("idm") &"INT")  = 2 and Session("Grade") < 80 then %>
                            <button class="btn btn-success btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg del-info"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Regresar</button>
                            <%Else %>
                            <%If Request("idm")  = 1 then %>
                            <button class="btn btn-success btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" onclick=" window.location =  'view_cont_cert.asp?tlpmod=<%=Request("idm")%>&idcurso=<%=Request("idm")%>&retcurso=<%=Request("idc")%>&swal=3'"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Regresar</button>

                            <%Else %>
                            <button class="btn btn-success btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" onclick=" window.location =  'view_cont_cert.asp?tlpmod=<%=Request("idm")%>&idcurso=<%=Request("idc")%>&retcurso=<%=Request("idc")%>&swal=3'"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Regresar</button>

                            <%End If %>
                            
                            <%End If %>
                        </div>


                    </div>

                </div>
                <!--#include file="footer.asp" -->
            </div>
        </div>
    </div>
    <script>
            var resizefunc = [];
    </script>
    <script src="js/block-back.js"></script>
    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/logout.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.js"></script>
    <script src="js/jquery-1.7.1.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript">
        $(".del-info").click(function () {
            var modulo =  <%=Request("idm")%>;
            var formData = {
                'id_usuario': <%=Session("id_usuario")%>,
                'id_modulo' :  <%=Request("idm")%>,
            };
            // process the form
            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: 'jq/reset_user.asp', // the url where we want to POST
                data: formData, // our data object
                //dataType: 'json', // what type of data do we expect back from the server
                encode: true
            }).done(function (data) {
                if (data > 0)
                {
                    if (modulo == 1)
                    {
                        window.location =  'view_cont_cert.asp?tlpmod=<%=Request("idm")%>&idcurso=<%=Request("idm")%>&retcurso=<%=Request("idc")%>&swal=2'
                    }
                    else{
                        window.location =  'view_cont_cert.asp?tlpmod=<%=Request("idm")%>&idcurso=<%=Request("idc")%>&retcurso=<%=Request("idc")%>&swal=2'
                    }

                }
             });

        });
    </script>



</body>
</html>
