﻿<!--#include file="data/con_ma2.inc" -->
<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID = 1034
%>


<%
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3
%>


<%
Response.Buffer = TRUE
Response.ContentType = "application/vnd.ms-excel"
archivo = "UsuariosNuevos.xls"
Response.AddHeader "Content-Disposition", "attachment; filename=" & archivo
%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<table border="0">
    <tr>
        <td border="0"><b>Usuarios dados de alta  en la semana  del <%=date %> - <%= DateAdd("d", -7, date()) %> </b></td>
    </tr>
</table>
<table border="1">
    <thead style="background: yellow;">
        <tr id="trExport">
            <th bgcolor="#FFFF99">ID Usuario</th>
            <th bgcolor="#FFFF99">Nombre</th>
            <th bgcolor="#FFFF99">Apellido Paterno</th>
            <th bgcolor="#FFFF99">Apellido Materno</th>
            <th bgcolor="#FFFF99">Genero</th>
            <th bgcolor="#FFFF99">Fecha Nacimiento</th>
            <th bgcolor="#FFFF99">Email</th>
            <th bgcolor="#FFFF99">Password</th>
            <th bgcolor="#FFFF99">Lada</th>
            <th bgcolor="#FFFF99">Telefono</th>
            <th bgcolor="#FFFF99">Lada celular</th>
            <th bgcolor="#FFFF99">Celular</th>
            <th bgcolor="#FFFF99">Pais</th>
            <th bgcolor="#FFFF99">Cedula</th>
            <th bgcolor="#FFFF99">Fecha Registro</th>
        </tr>
    </thead>
    <tbody>
        <%
            
       SQL = "select id_usuario,nombre,apaterno,amaterno,genero,fecha_nacimiento,email,pass,lada,telefono,lada_cel,celular,b.pais,cedula,fecha_registro from ma_15.dbo.tbl_usuarios a inner join ma_15.dbo.cat_paises b on a.id_pais = b.id_pais where fecha_registro  > dateadd(Day,-7,GETDATE()) and a.id_patrocinador = 1"
            
        %>

        <%
        Cursor.Open SQL, StrConn, 1, 3
        Do While not Cursor.EOF    
        %>
        <tr>
            <td><%=Cursor("id_usuario") %></td>
            <td><%=Cursor("nombre")%></td>
            <td><%=Cursor("apaterno")%></td>
            <td><%=Cursor("amaterno") %></td>
            <td><%=Cursor("genero")%></td>
            <td><%=Cursor("fecha_nacimiento")%></td>
            <td><%=Cursor("email") %></td>
            <td><%=Cursor("pass")%></td>
            <td><%=Cursor("lada")%></td>
            <td><%=Cursor("telefono") %></td>
            <td><%=Cursor("lada_cel")%></td>
            <td><%=Cursor("celular")%></td>
            <td><%=Cursor("pais") %></td>
            <td><%=Cursor("cedula")%></td>
            <td><%=Cursor("fecha_registro")%></td>

        </tr>
        <%
        Cursor.MoveNext
        Loop
        Cursor.Close   
        %>
    </tbody>
</table>
