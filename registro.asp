<!--#include file="data/con_ma.inc" -->
<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header.asp" -->
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
    <link href="css/registro.css" rel="stylesheet" />
    <script>
         function RefreshImage(valImageId) {
            var objImage = document.images[valImageId];
            if (objImage == undefined) {
                return;
            }
            var now = new Date();
            objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
        }
    </script>
    <script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1029986&mt_adid=157482&v1=&v2=&v3=&s1=&s2=&s3='></script>
    <!-- CONVERSION TAG -->
    <script type="text/javascript" src="https://cstatic.weborama.fr/js/advertiserv2/adperf_conversion.js"></script>
    <script type="text/javascript">
        var adperftrackobj = {
            client: ""      /* set your client id here */
        , amount: "0.0"  /* set the total price here */
        , invoice_id: "" /* set your invoice id here */
        , quantity: 0    /* set the number of items purchased */
        , is_client: 0   /* set to 1 if the client is a new client */
        , optional_parameters: {
            "N1": "0" /* to set */
            , "N2": "0" /* to set */
            , "N3": "0" /* to set */
            /* to set free parameter follow this pattern : */
            /*      ,"customer_type" : "abc" */
        }

            /* don't edit below this point */
        , fullhost: 'lamlaboratoriosmerck.solution.weborama.fr'
        , site: 1976
        , conversion_page: 3
        }
        try { adperfTracker.track(adperftrackobj); } catch (err) { }
    </script>
    <link href="assets/css/core-default.css" rel="stylesheet" type="text/css" />
    <style>
        hr {
            border-top: 2px solid #3266b1;
        }

        .strong-look {
            font-weight: bold !important;
            font-size: 32px !important;
            margin-left: 15px;
        }
    </style>
</head>
<body class="fixed-left">
    <!--#include file="modal.asp" -->
    <!--#include file="header-registro.asp" -->
    <main class="page-content">
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row" id="bg">
                        <!--<form data-toggle="validator" role="form" id="registro" method="post" action="jq/jq_registro.asp">
                            <div class="form-group col-lg-12 col-md-12" id="div-registro">
                                <div class="col-md-4">
                                    <h1>REGISTRATION FORM</h1>
                                </div>
                                <div class="col-md-8">
                                    <p>Please, enter the information below. Selecting your field of specialty correctly is the utmost importance, in order for us to show you the most appropriate information according to your profile. Required fields are indicated with *. </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-user"></i><span>&nbsp;PERSONAL DATA</span></h4>
                                    </div>
                                </div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">
                                                *Name
                                                <br />
                                                <b style="color: red;">(This information will appear in your academic certificate, so you must verify this information is correct.)</b></label>
                                            <input type="text" class="form-control" id="inputName" name="inputName" placeholder="Name" data-parsley-error-message="Complete your name. Thanks. " required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputApaterno" class="control-label">
                                                *Surname
                                                <br />
                                                <b style="color: red;">(This information will appear in your academic certificate, so you must verify this information is correct.)</b>
                                            </label>
                                            <input type="text" class="form-control" id="inputApaterno" name="inputApaterno" placeholder="Surname" data-parsley-error-message="Complete your surname. Thanks." required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputAmaterno" class="control-label">
                                                *Second Surname
                                                <br />
                                                <b style="color: red;">(This information will appear in your academic certificate, so you must verify this information is correct.)</b>
                                            </label>
                                            <input type="text" class="form-control" id="inputAmaterno" name="inputAmaterno" placeholder="Second Surname" data-parsley-error-message="Complete your second surname. Thanks." required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSex">Genre</label>
                                            <select class="form-control form-control-sm" style="font-size: 12px; height: 40px;" id="inputSex" name="inputSex">
                                                <option>Select</option>
                                                <option value="F">Female</option>
                                                <option value="M">Male</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputBirth">*Date of Birth</label>
                                            <input class="form-control" type="date" style="font-size: 12px; height: 40px;" id="inputBirth" name="inputBirth" data-parsley-error-message="Complete date of birth. Thanks." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-key"></i><span>&nbsp;ACCOUNT INFORMATION</span></h4>
                                    </div>
                                </div>


                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*E-mail</label>
                                            <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" data-parsley-error-message="Complete your e-mail. Thanks. " data-parsley-type="email" required>
                                            
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label">*Password</label>
                                            <input type="password" data-minlength="6" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password"  data-parsley-error-message="Complete your password. Thanks." required>
                                            <div class="help-block">At least 6 characters</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Confirm E-mail</label>
                                            <input type="email" class="form-control" id="inputEmailConfirm" name="inputEmailConfirm" data-match="#inputEmail" data-parsley-error-message="Confirm e-mail. Thanks." data-parsley-type="email" placeholder="Confirm e-mail" required>
                                            
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Confirm Password</label>
                                            <input type="password" class="form-control" id="inputPasswordConfirm" name="inputPasswordConfirm" data-match="#inputPassword" data-parsley-error-message="Confirm password. Thanks. "  placeholder="Confirm password" required>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-12"></div>

                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-university"></i><span>&nbsp;WORK HISTORY AND ACADEMIC QUALIFICATIONS</span></h4>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSpeciality">*Specialty</label>
                                            <select class="form-control form-control-sm" id="inputSpeciality" name="inputSpeciality" style="font-size: 12px; height: 40px;" data-parsley-error-message="Select specialty. Thanks. " required>
                                                <option value="">Select one option</option>
                                                <%
                                                    set CursorEspecialidades = createobject("ADODB.Recordset")
                                                    CursorEspecialidades.CursorType =1 
                                                    CursorEspecialidades.LockType = 3

                                                    SQLEspecialidades = "select * from cat_especialidades where activo = 'SI' order by id_especialidad"
                                                    CursorEspecialidades.Open SQLEspecialidades, StrConn, 1, 3

                                                    Do While not CursorEspecialidades.EOF 
                                                %>
                                                <option value="<%=CursorEspecialidades("id_especialidad") %>"><%=CursorEspecialidades("especialidad_en") %></option>
                                                <%
                                                    CursorEspecialidades.MoveNext
                                                    Loop
                                                  '  CursorEspecialidades.Close
                                                    CursorEspecialidades.MoveFirst    
                                                %>
                                                <option value="0">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputProfessionalID" class="control-label">*Professional practice license</label>
                                            <input type="text" class="form-control" style="height: 40px;" id="inputProfessionalID" name="inputProfessionalID" placeholder="Professional practice license" data-parsley-error-message="Professional practice license. Thanks. " required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>
                                <br />
                                <div class="col-md-12"></div>
                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSpeciality">*Type of practice</label>
                                            <select class="form-control form-control-sm" id="inputPractice" name="inputPractice" style="font-size: 12px; height: 40px;" data-parsley-error-message="Select type of practice. Thanks. " required>
                                                <option value="">Select one option</option>
                                                <option value="Pública">Public</option>
                                                <option value="Privada">Private</option>
                                                <option value="Ambas">Both</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputProfessionalID" class="control-label">*Institution where you work</label>
                                            <input type="text" class="form-control" style="height: 40px;" id="inputInstitution" name="inputInstitution" placeholder="Institution" data-parsley-error-message="Institution where you work. Thanks." required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>

                                <div class="row">
                                    <div class="col-md-12 grad">
                                        <h4><i class="fa fa-envelope"></i><span>&nbsp;Contacto INFORMATION</span></h4>
                                    </div>
                                </div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-4">
                                        <label for="inputStreet" class="control-label">Street</label>
                                        <input type="text" class="form-control" id="inputStreet" name="inputStreet" placeholder="Street" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputNumber" class="control-label">Number</label>
                                        <input type="text" class="form-control" id="inputNumber" name="inputNumber" placeholder="Number" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputStreet2" class="control-label">Neighborhood</label>
                                        <input type="text" class="form-control" id="inputStreet2" name="inputStreet2" placeholder="Neighborhood" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">

                                    <div class="col-md-3">
                                        <%
                                            set CursorPaises = createobject("ADODB.Recordset")
                                            CursorPaises.CursorType =1 
                                            CursorPaises.LockType = 3

                                            SQLPaises = "select * from cat_paises where activo = 'si'"
                                            CursorPaises.Open SQLPaises, StrConn, 1, 3

                                        %>
                                        <label for="inputcountry" class="control-label">Country</label>
                                        <select name="inputcountry" id="inputcountry" class="form-control form-control-sm" style="font-size: 12px; height: 40px;" data-parsley-error-message="Select country. Thanks. " required>
                                            <option value="">*Select</option>
                                            <%
                                            Do While not CursorPaises.EOF 
                                            %>
                                            <option value="<%=CursorPaises("id_pais") %>"><%=CursorPaises("pais") %></option>
                                            <%
                                            CursorPaises.MoveNext
                                            Loop
                                          '  CursorPaises.Close
                                            CursorPaises.MoveFirst    
                                            %>
                                        </select>
                                    </div>


                               
                                    <div class="col-md-3">
                                        <label for="inputState" class="control-label">*State</label>
                                        <select class="form-control form-control-sm" id="inputState" name="inputState" style="font-size: 12px; height: 40px;" data-parsley-error-message="Select state. Thanks. " required disabled>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputCity" class="control-label">*City</label>
                                        <input type="text" class="form-control" id="inputCity" name="inputCity" style="font-size: 12px; height: 40px;" placeholder="City" data-parsley-error-message="City. Thanks. " required disabled>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputCP" class="control-label">*Zip Code</label>
                                        <select class="form-control form-control-sm" id="inputCP" name="inputCP" style="font-size: 12px; height: 40px;" data-parsley-error-message="Select/Write zip code. Thanks. " required disabled>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="col-md-12">
                                            <label for="inputLadaTel" class="control-label">Telephone Number</label>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" id="inputLadaTel" name="inputLadaTel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="International Dialing Code">
                                        </div>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" id="inputTelefono" name="inputTelefono" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Telephone Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="col-md-12">
                                            <label for="inputLadaTel" class="control-label">Mobile Phone</label>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" id="inputLadaCel" name="inputLadaCel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="International Dialing Code"> 
                                        </div>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" id="inputCelular" name="inputCelular" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Mobile Phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            *Security code:
                                        <img src="captcha.asp" hspace="2" id="imgCaptcha" class="img-responsive" onclick="RefreshImage('imgCaptcha')" />
                                        </div>
                                        <div class="col-md-8">
                                            <span>
                                                <input type="text" class="form-control" id="captchacode" name="captchacode" style="font-size: 12px; height: 40px;" placeholder="Code" data-parsley-error-message="Enter security code. Thanks." required>
                                            </span>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox1" type="checkbox" required data-parsley-error-message="You must accept our Termns and Conditions. Thanks.">
                                                <label for="checkbox1">
                                                    *I accept your Términos y condiciones   <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-terminos-condiciones">(view)</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary" >
                                                <input id="checkbox2" type="checkbox" required data-parsley-error-message="You must agree our Privacy Agreement. Thanks.">
                                                <label for="checkbox2">
                                                    *I have read your Privacy Agreement  <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-aviso-privacidad">(view)</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Sign Up</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </form>-->
                        <form data-toggle="validator" role="form" id="registro" method="post" action="jq/jq_registro.asp">
                            <div class="form-group col-lg-12 col-md-12" id="div-registro">
                                <div class="col-md-12 text-center">
                                    <h1 style="font-size:50px; font-weight: bold;">REGISTRO</h1>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8 text-center">
                                    <p>Por favor, ingrese la informaci&oacute;n que se le solicita a continuaci&oacute;n. Es importante que seleccione su especialidad correctamente para asegurar que la informaci&oacute;n que le mostremos sea la adecuada a su perfil. Las casillas marcadas con * son obligatorias.</p>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="strong-look"><i class="fa fa-user strong-look"></i><span>&nbsp;DATOS PERSONALES</span></h4>
                                        <hr />
                                    </div>
                                </div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">
                                                *Nombre
                                                <br />
                                                <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b></label>
                                            <input type="text" class="form-control" id="inputName" name="inputName" placeholder="Nombre" data-parsley-error-message="Complete su nombre. Gracias." required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputApaterno" class="control-label">
                                                *Apellido Paterno
                                                <br />
                                                <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b>
                                            </label>
                                            <input type="text" class="form-control" id="inputApaterno" name="inputApaterno" placeholder="Apellido Paterno" data-parsley-error-message="Complete su apellido paterno. Gracias." required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="inputAmaterno" class="control-label">
                                                *Apellido Materno
                                                <br />
                                                <b style="color: red;">(Este dato aparecerá en su constancia académica por lo que le pedimos se asegure que sea correcto)</b>
                                            </label>
                                            <input type="text" class="form-control" id="inputAmaterno" name="inputAmaterno" placeholder="Apellido Materno" data-parsley-error-message="Complete su apellido materno. Gracias." required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSex">Sexo</label>
                                            <select class="form-control form-control-sm" style="font-size: 12px; height: 40px;" id="inputSex" name="inputSex">
                                                <option>Seleccione</option>
                                                <option value="F">Femenino</option>
                                                <option value="M">Masculino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputBirth">*Fecha de nacimiento</label>
                                            <input class="form-control" type="date" style="font-size: 12px; height: 40px;" id="inputBirth" name="inputBirth" data-parsley-error-message="Complete su fecha de nacimiento. Gracias." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="strong-look"><i class="fa fa-key"></i><span>&nbsp;DATOS PARA SU CUENTA</span></h4>
                                        <hr />
                                    </div>
                                </div>


                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Correo electrónico</label>
                                            <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" data-parsley-error-message="Complete su correo electrónico. Gracias." data-parsley-type="email" required>
                                            <div class="help-block with-errors"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label">*Contraseña</label>
                                            <input type="password" data-minlength="6" class="form-control" id="inputPassword" name="inputPassword" placeholder="Contraseña" data-parsley-error-message="Escriba su contraseña. Gracias." required>
                                            <div class="help-block">Mínimo 6 caracteres</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Confirme su correo electrónico</label>
                                            <input type="email" class="form-control" id="inputEmailConfirm" name="inputEmailConfirm" data-match="#inputEmail" data-parsley-error-message="Confirme su correo electrónico. Gracias." data-parsley-type="email" placeholder="Confirmar correo" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">*Confirme su contraseña</label>
                                            <input type="password" class="form-control" id="inputPasswordConfirm" name="inputPasswordConfirm" data-match="#inputPassword" data-parsley-error-message="Confirme su contraseña. Gracias." placeholder="Confirmar" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-12"></div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="strong-look"><i class="fa fa-university"></i><span>&nbsp;INFORMACIÓN ACADÉMICA Y LABORAL</span></h4>
                                        <hr />
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSpeciality">*Especialidad</label>
                                            <select class="form-control form-control-sm" id="inputSpeciality" name="inputSpeciality" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su especialidad. Gracias." required>
                                                <option value="">Seleccione</option>
                                                <%
                                                    set CursorEspecialidades = createobject("ADODB.Recordset")
                                                    CursorEspecialidades.CursorType =1 
                                                    CursorEspecialidades.LockType = 3

                                                    SQLEspecialidades = "select * from cat_especialidades where id_especialidad not in ('4') and activo = 'SI' order by id_especialidad"
                                                    CursorEspecialidades.Open SQLEspecialidades, StrConn, 1, 3

                                                    Do While not CursorEspecialidades.EOF 
                                                %>
                                                <option value="<%=CursorEspecialidades("id_especialidad") %>"><%=CursorEspecialidades("especialidad") %></option>
                                                <%
                                                    CursorEspecialidades.MoveNext
                                                    Loop
                                                  '  CursorEspecialidades.Close
                                                    CursorEspecialidades.MoveFirst    
                                                %>
                                                <option value="0">Otra</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputProfessionalID" class="control-label">*Cédula profesional</label>
                                            <input type="text" class="form-control" style="height: 40px;" id="inputProfessionalID" name="inputProfessionalID" placeholder="Cédula profesional" data-parsley-error-message="Complete su cédula profesional. Gracias." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>
                                <br />
                                <div class="col-md-12"></div>
                                <div class="col-md-2 col-lg-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSpeciality">*Tipo de práctica</label>
                                            <select class="form-control form-control-sm" id="inputPractice" name="inputPractice" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su tipo de práctica. Gracias." required>
                                                <option value="">Seleccione</option>
                                                <option value="Pública">Pública</option>
                                                <option value="Privada">Privada</option>
                                                <option value="Ambas">Ambas</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputProfessionalID" class="control-label">*Institución donde labora</label>
                                            <input type="text" class="form-control" style="height: 40px;" id="inputInstitution" name="inputInstitution" placeholder="Institución" data-parsley-error-message="Complete su institución laboral. Gracias." required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="strong-look"><i class="fa fa-envelope"></i><span>&nbsp;DATOS DE CONTACTO</span></h4>
                                        <hr />
                                    </div>
                                </div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-4">
                                        <label for="inputStreet" class="control-label">Calle</label>
                                        <input type="text" class="form-control" id="inputStreet" name="inputStreet" placeholder="Calle" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputNumber" class="control-label">Número</label>
                                        <input type="text" class="form-control" id="inputNumber" name="inputNumber" placeholder="Número" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputStreet2" class="control-label">Colonia</label>
                                        <input type="text" class="form-control" id="inputStreet2" name="inputStreet2" placeholder="Colonia" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-3">
                                        <%
                                            set CursorPaises = createobject("ADODB.Recordset")
                                            CursorPaises.CursorType =1 
                                            CursorPaises.LockType = 3

                                            SQLPaises = "select * from cat_paises where activo = 'si'"
                                            CursorPaises.Open SQLPaises, StrConn, 1, 3

                                        %>
                                        <label for="inputCountry" class="control-label">País</label>
                                        <select name="inputcountry" id="inputcountry" name="inputcountry" class="form-control form-control-sm" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su país. Gracias." required>
                                            <option value="">*Seleccione</option>
                                            <%
                                            Do While not CursorPaises.EOF 
                                            %>
                                            <option value="<%=CursorPaises("id_pais") %>"><%=CursorPaises("pais") %></option>
                                            <%
                                            CursorPaises.MoveNext
                                            Loop
                                          '  CursorPaises.Close
                                            CursorPaises.MoveFirst    
                                            %>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputState" class="control-label">*Estado</label>
                                        <select class="form-control form-control-sm" id="inputState" name="inputState" style="font-size: 12px; height: 40px;" data-parsley-error-message="Seleccione su estado. Gracias." required disabled>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputCity" class="control-label">*Ciudad</label>
                                        <input type="text" class="form-control" id="inputCity" name="inputCity" style="font-size: 12px; height: 40px;" placeholder="Ciudad" data-parsley-error-message="Complete su ciudad. Gracias." required disabled>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputCP" class="control-label">*Código Postal</label>
                                        <select class="form-control form-control-sm" id="inputCP" name="inputCP" style="font-size: 12px; height: 40px;" data-parsley-error-message="Escriba su código postal. Gracias." required disabled>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="col-md-12">
                                            <label for="inputLadaTel" class="control-label">Teléfono</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="inputLadaTel" name="inputLadaTel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Lada">
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="inputTelefono" name="inputTelefono" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Teléfono">
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="col-md-12">
                                            <label for="inputLadaTel" class="control-label">Celular</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" id="inputLadaCel" name="inputLadaCel" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Lada">
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="inputCelular" name="inputCelular" data-parsley-type="digits" style="font-size: 12px; height: 40px;" placeholder="Teléfono">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-12"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            *Ingrese el c&oacute;digo de seguridad:
                                        <img src="captcha.asp" hspace="2" id="imgCaptcha" class="img-responsive" onclick="RefreshImage('imgCaptcha')" />
                                        </div>
                                        <div class="col-md-8">
                                            <span>
                                                <input type="text" class="form-control" id="captchacode" name="captchacode" style="font-size: 12px; height: 40px;" placeholder="Código" data-parsley-error-message="Escriba el código de seguridad. Gracias." required>
                                            </span>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox1" type="checkbox" data-parsley-error-message="Debe aceptar los términos y condiciones. Gracias" required>
                                                <label for="checkbox1">
                                                    *Acepto los términos y condiciones <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-terminos-condiciones">(consultar)</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox2" type="checkbox" data-parsley-error-message="Debe leer el aviso de privacidad. Gracias" required>
                                                <label for="checkbox2">
                                                    *He le&iacute;do el aviso de privacidad <a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-aviso-privacidad">(consultar)</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Registrarse</button>
                                            <!--<%= Session("COD") %>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--#include file="footer.asp" -->
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!--Validator-->
    <script src="js/validator.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>



    <script type="text/javascript">
        $(document).ready(function () {
            //$(".live-tile,.flip-list").liveTile();
            //$(".appbar").applicationBar();

            function ActualizaCP() {
                $.ajax({
                    url: "jq/jq_obtener_codigos.asp",
                    type: "POST",
                    data: "estado=" + $("#inputState").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputCP").html(opciones);
                        $('#inputCP').prop("disabled", false);
                    }
                })
                $('#inputCP').prop("disabled", false);
            }

            function ActualizaEstados() {
                $.ajax({
                    url: "jq/jq_obtener_estados.asp",
                    type: "POST",
                    data: "inputcountry=" + $("#inputcountry").val(),
                    async: false,
                    success: function (opciones) {
                        $("#inputState").html(opciones);
                        if ($('#inputState option:selected').val() == 1) {
                            $('#inputState').prop("disabled", true);
                            $('#inputCity').prop("disabled", true);
                            $('#inputCP').prop("disabled", true);

                            $("#inputCP").html("<option value='1' selected>No aplica</option>");
                            $("#inputCity").val("No aplica");

                        } else {
                            $('#inputState').prop("disabled", false);
                            $('#inputCity').prop("disabled", false);
                            $("#inputCity").val("");
                        }
                    }
                })
            }

            function LimpiaCampos() {
                $('#inputState').removeClass("parsley-error");
                $('#inputCity').removeClass("parsley-error");
                $('#inputCP').removeClass("parsley-error");
            }

            $('#inputcountry').change(function () {
                LimpiaCampos();
                ActualizaEstados();
            });

            $('#inputState').change(function () {
                ActualizaCP();
            });


            $('#inputSpeciality').change(function () {
                if ($('#inputSpeciality').val() == 0) {
                    $('#inputSpeciality').parent().append("<input type='text' class='form-control' id='inputOtherSp' name='inputOtherSp' data-parsley-error-message='Complete su especialidad. Gracias.' placeholder='Especialidad' required>")
                }
                else {
                    $('#inputOtherSp').remove();
                }


            });

        });
    </script>


    <!--Alerts-->
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.js"></script>
    <script src="assets/pages/jquery.sweet-alert.init.js"></script>

    <%
        if Request("fail") = 1 then
    %>
    <script type="text/javascript">
        $(document).ready(function () {
            swal({
                title: "Este usuario ya existe",
                text: "Este email ya está registrado ",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: 'btn-success waves-effect waves-light',
                confirmButtonText: 'Aceptar'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>

    <%
        if Request("fail") = 2 then
    %>
    <script type="text/javascript">
        $(document).ready(function () {
            swal({
                title: "El código de seguridad es incorrecto",
                text: "Por favor verifique. Gracias",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: 'btn-success waves-effect waves-light',
                confirmButtonText: 'Aceptar'
            });
        });

        document.querySelector('.msg').onclick = function () {
            swal("Here's a message !");
        };

    </script>
    <%
        end if
    %>

    <div>
        <div class="sweet-overlay" tabindex="-1" style="opacity: -0.01; display: none;"></div>
        <div class="sweet-alert hideSweetAlert" tabindex="-1" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-ouside-click="false" data-has-done-function="false" data-timer="null" style="display: none; margin-top: -190px; opacity: -0.01;">
            <div class="icon error" style="display: none;"><span class="x-mark"><span class="line left"></span><span class="line right"></span></span></div>
            <div class="icon warning" style="display: none;"><span class="body"></span><span class="dot"></span></div>
            <div class="icon info" style="display: none;"></div>
            <div class="icon success" style="display: block;">
                <span class="line tip"></span><span class="line long"></span>
                <div class="placeholder"></div>
                <div class="fix"></div>
            </div>
            <div class="icon custom" style="display: none;"></div>
            <h2>Good job!</h2>
            <p class="lead text-muted" style="display: block;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis</p>
            <p>
                <button class="cancel btn btn-lg btn-default" tabindex="2" style="display: none;">Cancel</button>
                <button class="confirm btn btn-lg btn-primary" tabindex="1" style="display: inline-block;">OK</button>
            </p>
        </div>
    </div>

</body>
</html>
