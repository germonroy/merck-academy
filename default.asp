﻿<!--#include file="data/con_ma.inc" -->
<!--#include file="getcountry.asp" -->
<%
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US

Set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3
    
Dim my_num,max,min
max=0
min=4
Randomize
alnum = int((max-min+1)*rnd+min)
%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <meta name="author" content="Inkuba Project México" />
    <title>Merck Academy</title>
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#e74c3c">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core-default.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/menu-default.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/special-classes.css" rel="stylesheet" />
    <link href="assets/iconos-merck/style.css" rel="stylesheet" />
    <link href="assets/css/theme.min.css" rel="stylesheet" />
    <link href="assets/css/plugin.min.css" rel="stylesheet" />
    <link href="assets/css/plugin1.min.css" rel="stylesheet" />
    <link href="assets/css/text-align.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    <link href="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.css" rel="stylesheet" />
    <!--#include file="includes/google_track.asp" -->

    <script src="assets/js/modernizr.min.js"></script>

    <script src="assets/js/jquery.min.js"></script>

    <script src="assets/js/bootstrap.min.js"></script>

    <script src="assets/js/detect.js"></script>

    <script src="assets/js/fastclick.js"></script>

    <script src="assets/js/jquery.blockUI.js"></script>

    <!--<script src="assets/js/waves.js"></script>-->

    <script src="assets/js/jquery.nicescroll.js"></script>

    <script src="assets/js/jquery.slimscroll.js"></script>

    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- KNOB JS -->
    <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->

    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->

    <script src="assets/js/jquery.core.js"></script>

    <script src="assets/js/jquery.app.js"></script>

    <link href="css/customized.css" rel="stylesheet" />
    <style>
        .runway
        {
            font-size: 14px;
            position: absolute;
            z-index: 1;
            top: 30%;
            right: 24vw;
        }
        .widthFrame
        {
            /*width: 720px;
            height: 480px;*/
            width: 885px;
            height: 500px;
        }
        .widthFrame2
        {
            width: 100%;
            height: 100%;
        }
        hr
        {
            border-top: 2px solid #3266b1;
        }
        .hg-card
        {
            background-color: rgba(255,255,255, 0.3) !important;
        }
        .btn-grey
        {
            background-color: #e4e4e4 !important;
            border: 1px solid #d0d0d0 !important;
            color: #000 !important;
        }
        .panel-group .panel-heading
        {
            padding: 5px 26px;
        }
        .wrapper-video
        {
            margin-top: -73px;
        }
        .range_slides_carousel_inner
        {
            border: none !important;
        }
        .range_slides_item_image img
        {
            border-left: none !important;
            border-right: none !important;
        }
        .range_slides_item_image img
        {
            border-bottom: none !important;
            border-top: none !important;
        }
        .js_frm_signup > form > div > input
        {
            height: 33px !important;
        }
        #miniRegistro
        {
            max-height: 425px;
            margin-top: 14px;
        }
        .strong-look
        {
            font-weight: bold !important;
            font-size: 32px !important;
        }
        /* Small Devices, Tablets */@media only screen and (min-width : 768px)
        {
            .hg-card
            {
                padding: 2em;
                margin: 1em;
                min-height: 45vw;
                background-color: rgba(255,255,255, 0.3) !important;
            }
        }
        /* Medium Devices, Desktops */@media only screen and (max-width : 992px)
        {
            .hg-card
            {
                margin-top: 55px;
            }
        }
        /* Small Devices, Tablets */@media only screen and (max-width : 768px)
        {
            #adv_range_3_columns_carousel
            {
                max-height: 265px;
            }
            .widthFrame
            {
                width: 380px;
            }
        }
        /* Extra Small Devices, Phones */@media only screen and (max-width : 480px)
        {
            .wrapper-video
            {
                margin-top: 10px;
            }
            .fa-hand-o-right
            {
                margin-top: -46%;
                font-size: 30px !important;
            }
            .fa-hand-o-left
            {
                margin-top: -46%;
                font-size: 30px !important;
                margin-left: -20% !important;
            }
            #adv_range_3_columns_carousel
            {
                max-height: 160px;
            }
            .h3, h3
            {
                font-size: 18px;
                line-height: 27px;
            }
            #btn-registro-ahora
            {
                font-size: 14px;
                margin-top: 22px;
                left: -21%;
            }
            #temarios
            {
                margin-top: -10px !important;
            }
            .avalesbann
            {
                max-height: 150px;
            }
            #lg-legales
            {
                max-width: 70%;
                margin-left: 7%;
            }
            #lg-terminos
            {
                margin-left: -13%;
            }
            .articles-img
            {
                width: 100%;
            }
        }
        /* Custom, iPhone Retina */@media only screen and (max-width : 320px)
        {
        }
        .carousel-ind-right
        {
            position: absolute;
            top: 100%;
            left: 5%;
            color: #1a6cb0;
        }
        .carousel-ind-left
        {
            position: absolute;
            margin-top: -28px;
            left: 5%;
            color: #1a6cb0;
            top: 0 !important;
        }
        .carousel-ind-right:hover
        {
            color: #1a6cb0;
        }
        .carousel-ind-left:hover
        {
            color: #1a6cb0;
        }
        .card-box
        {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19) !important;
        }
        .novedades-img
        {
            height: 170px;
            overflow: hidden;
        }
        .novedades-img img
        {
            margin-top: -50px;
        }
        .scroll-top-wrapper
        {
            position: fixed;
            opacity: 0;
            visibility: hidden;
            overflow: hidden;
            text-align: center;
            z-index: 99999999;
            background-color: #777777;
            color: #eeeeee;
            width: 50px;
            height: 48px;
            line-height: 48px;
            right: 30px;
            bottom: 30px;
            padding-top: 2px;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            border-bottom-left-radius: 10px;
            -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -ms-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
        }
        .scroll-top-wrapper:hover
        {
            background-color: #888888;
        }
        .scroll-top-wrapper.show
        {
            visibility: visible;
            cursor: pointer;
            opacity: 1.0;
        }
        .scroll-top-wrapper i.fa
        {
            line-height: inherit;
        }
        #detallesTem
        {
            height: 73vh;
            width: 100%;
        }
        .combo-player-slides
        {
            margin-top: -73px;
        }
        #combo-player-video
        {
            margin-top: -73px;
        }
    </style>
    <link href="css/video_carousel.css" rel="stylesheet" media="all">

    <script>
        $('#video_carousel').bind('slid', function() {
            // Get currently selected item
            var item = $('#video_carousel .carousel-inner .item.active');

            // Deactivate all nav links
            $('#video_carousel-nav a').removeClass('active');

            // Index is 1-based, use this to activate the nav link based on slide
            var index = item.index() + 1;
            $('#video_carousel-nav a:nth-child(' + index + ')').addClass('active');
        });
    </script>

    <link href="css/hover-min.css" rel="stylesheet" />
    <link href="css/figure_set1.css" rel="stylesheet" />
    <style>
        .panel-group .panel .panel-heading a[data-toggle=collapse]:before
        {
            content: '\f062';
        }
        .panel-group .panel .panel-heading a[data-toggle=collapse].collapsed:before
        {
            content: '\f063';
        }
        .range_slides_carousel_wrapper
        {
            background: transparent !important;
        }
        .myTeaser
        {
            max-height: 28vw;
            margin-top: 73px;
        }
        @media only screen and (width : 1920px )
        {
            .myTeaser
            {
                /*max-height: 50vh;*/
            }
        }
        /* Small Devices, Tablets */@media only screen and (max-width : 767px)
        {
            .myTeaser
            {
                min-height: 365px;
                margin-top: -30px;
            }
            .regminibtn
            {
                margin-top: 25px;
                right: 23%;
            }
            .titulocarben
            {
                margin-top: -8vw;
                text-shadow: 1px 1px #0c0c0c;
            }
        }
        /*iPad*//*@media only screen and (width : 768px) {
            .myTeaser {
                margin-top: 75px;
                max-height: 300px !important;
            }
        }*/</style>
    <style>
        .titulocarben
        {
            margin-top: -1vw;
        }
        .vertical .carousel-inner
        {
            height: 100%;
            left: 0;
        }
        .carousel.vertical .item
        {
            -webkit-transition: 0.6s ease-in-out top;
            -moz-transition: 0.6s ease-in-out top;
            -ms-transition: 0.6s ease-in-out top;
            -o-transition: 0.6s ease-in-out top;
            transition: 0.6s ease-in-out top;
            left: 0;
        }
        .carousel.vertical .active
        {
            top: 0;
            left: 0;
        }
        .carousel.vertical .next
        {
            top: 400px;
            left: 0;
        }
        .carousel.vertical .prev
        {
            top: -400px;
            left: 0;
        }
        .carousel.vertical .next.left, .carousel.vertical .prev.right
        {
            top: 0;
            left: 0;
        }
        .carousel.vertical .active.left
        {
            top: -400px;
            left: 0;
        }
        .carousel.vertical .active.right
        {
            top: 400px;
            left: 0;
        }
        .carousel.vertical .item
        {
            left: 0;
        }
        .carousel.vertical .carousel-control
        {
            width: 100%;
            bottom: inherit;
            top: inherit;
            left: 0;
        }
        .carousel.vertical .carousel-control.left
        {
            top: 0;
            left: 0;
        }
        .intro-color
        {
            background-color: #6ed4ff !important;
        }
        @media (max-width: 770px) and (min-width: 760px)
        {
            .video_carousel_caption_text
            {
                top: 8% !important;
            }
        }
        @media only screen and (min-device-width: 1024px) and (max-device-width: 1366px) and (-webkit-min-device-pixel-ratio: 1.5)
        {
            .video_carousel_caption_text h1
            {
                font-size: 20px;
            }
            .video_carousel_caption_text p
            {
                font-size: 15px;
            }
            .menureg, .menuini
            {
                color: #fff !important;
            }
            .video_carousel_colored_button, .video_carousel_colored_button:visited
            {
                outline: 0;
                margin: 0%;
                width: auto;
                height: 44px;
                border: none;
                cursor: pointer;
                font-size: 13px;
                font-weight: 700;
                text-align: left;
                text-shadow: none;
                line-height: 44px;
                padding: 1px 25px;
            }
        }
    </style>
</head>
<body>
    <!--#include file="modal.asp" -->
    <!--#include file="header-default.asp" -->
    <section id="home" style="margin-top: 93px">
        <!--Banner-->
        <!--<div id="video_carousel" class="carousel video_carousel_fade animate_text video_carousel_wrapper" data-ride="carousel" data-interval="18000" data-pause="hover">-->
        <div id="video_carousel" class="carousel video_carousel_fade animate_text video_carousel_wrapper" data-ride="carousel" data-interval="false">
            <!--<ol class="carousel-indicators">
                <li data-target="#video_carousel" data-slide-to="0" class="active"></li>
                <li data-target="#video_carousel" data-slide-to="1"></li>
            </ol>-->

            <!-- Indicators -->
            <ol class="carousel-indicators carousel-indicators--thumbnails hidden-xs hidden-sm">
                <% u = 1
                idsTeasers = Array()
                SQL = "select top 5 teaser_rel " &_
                    "from tbl_top_contenido where tipo = 'V' and teaser_rel is not null and localizacion = 'B' and activo = 'SI' "&_
                    " order by newid()"
                Cursor.Open SQL, StrConn, 1, 3
                Do While Not Cursor.EOF
                   teasersM = teasersM & Cursor("teaser_rel")
                    if u < 5 then
                        teasersM = teasersM & ","
                    end if
                %>
                <li data-target="#video_carousel" data-slide-to="<%=u %>" class="active">
                    <div class="thumbnail-carousel">
                        <img src="images/teasers/cursos/<%=Cursor("teaser_rel") %>.jpg" class="img-responsive">
                    </div>
                </li>
                <% valueT = Cursor("teaser_rel")
                Foo = AddItem(idsTeasers,valueT)
                Cursor.MoveNext
                u = u + 1
                Loop
                Cursor.Close 

                Function AddItem(arr, val)
                    ReDim Preserve arr(UBound(arr) + 1)
                    arr(UBound(arr)) = val
                    AddItem = arr
                End Function%>
            </ol>

            <!--======= Wrapper For Slides =======-->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="margin-right: 7px;">
                    <img src="images/teasersland/lg/poster_welcome.jpg" class="img-responsive img-banner hidden-xs hidden-sm" />
                    <img src="images/teasersland/sm/poster_welcome.jpg" class="img-responsive img-banner hidden-lg hidden-md" style="width: 100%" />
                    <div class="carousel-caption video_carousel_caption">
                        <div class="video_carousel_caption_text">
                            <h1 class="titulocarben" data-animation="animated fadeInDown">¡Bienvenido!</h1>
                            <p data-animation="animated fadeInUp" style="margin-top: 10px;">
                                La plataforma digital de educación médica más avanzada, se renueva pensando en sus necesidades.
                                <br />
                                Merck Academy, los expertos en el dolor.
                            </p>
                            <a href="javascript:void(0)" class="video_carousel_colored_button changefr" data-target="#video_carousel" data-slide-to="6" data-animation="animated fadeInLeft">Ver video</a>
                            <input class="hidden" name="idv" value="26" />
                        </div>
                        <!--<img src="assets/images/ficha02.jpg" class="img-responsive img-banner" />-->
                    </div>
                </div>

                <% 'claseBanner = "active"
                    claseBanner = ""
                    '"and id_top in (3,6,9,12,15,21,24) "&_
                SQL = "select top 5 *, (select titulo from tbl_videos where id_video = tbl_top_contenido.id_tipo) as titulo, " &_
                    "(select duracion from tbl_videos where id_video = tbl_top_contenido.id_tipo) as duracion, " &_
                    "(select (titulo + ' ' + nombre) from cat_ponentes where id_ponente in (select top 1 id_ponente from " &_
                    " tbl_videos where tbl_videos.id_video = tbl_top_contenido.id_tipo)) as ponente " &_
                    "from tbl_top_contenido where tipo = 'V' and teaser_rel is not null and localizacion = 'B' and activo = 'SI' "&_
                    " and teaser_rel in ("&teasersM&")" &_
                    " order by case teaser_rel WHEN "&idsTeasers(0)&" THEN 1 WHEN "&idsTeasers(1)&" THEN 2 WHEN "&idsTeasers(2)&" THEN 3 " &_
                    " WHEN "&idsTeasers(3)&" THEN 4 ELSE 5 END"
                'Response.Write(SQL)
                Cursor.Open SQL, StrConn, 1, 3
                Do While Not Cursor.EOF %>
                <div class="item <%=claseBanner %> " style="margin-right: 7px;">
                    <img src="images/teasersland/lg/<%=Cursor("id_top") %>.jpg" class="img-responsive img-banner hidden-xs hidden-sm" />
                    <img src="images/teasersland/sm/<%=Cursor("id_top") %>.jpg" class="img-responsive img-banner hidden-lg hidden-md" style="width: 100%" />
                    <div class="carousel-caption video_carousel_caption">
                        <div class="video_carousel_caption_text">
                            <h1 class="titulocarben" data-animation="animated fadeInDown"><%=Cursor("titulo") %></h1>
                            <p data-animation="animated fadeInUp" style="margin-top: 10px;">
                                <%=Cursor("texto") %>
                                <br />
                                Impartido por: <%=Cursor("ponente") %>.
                                <br />
                                Duración: <%=Cursor("duracion") %>
                            </p>
                            <a href="javascript:void(0)" class="video_carousel_colored_button changefr" data-target="#video_carousel" data-slide-to="6" data-animation="animated fadeInLeft">Ver video</a>
                            <input class="hidden" name="idv" value="<%=Cursor("teaser_rel") %>" />
                        </div>
                        <!--<img src="assets/images/ficha02.jpg" class="img-responsive img-banner" />-->
                    </div>
                </div>
                <%Cursor.MoveNext
                  claseBanner=""
                  Loop
                  Cursor.Close %>

                <!--Teasers-->
                <div class="item wrapper-video" style="background-color: transparent;">
                    <!--<img src="images/carousel_video_img.png" alt="carousel video img" />-->
                    <img src="images/teasersland/lg/poster_welcome.jpg" class="img-responsive img-banner hidden-xs hidden-sm" style="margin-top: 73px;" />
                    <img src="images/teasersland/sm/poster_welcome.jpg" class="img-responsive img-banner hidden-lg hidden-md" style="width: 100%" />
                    <div class="carousel-caption video_carousel_caption" style="background-color: transparent;">
                        <div class="video_carousel_caption_text">
                        </div>
                        <iframe id="frameVid" class="myTeaser widthFrame" frameborder="0" src="blank.htm"></iframe>
                    </div>
                </div>

            </div>

            <!--======= Left Button =========-->
            <a class="left carousel-control video_carousel_control_left" href="#video_carousel" role="button" data-slide="prev">
                <span class="fa fa-hand-o-left video_carousel_control_icons" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <!--======= Right Button =========-->
            <a class="right carousel-control video_carousel_control_right" href="#video_carousel" role="button" data-slide="next">
                <span class="fa fa-hand-o-right video_carousel_control_icons" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
        <!--Fin Banner-->

        <!--Registro-->
        <div id="miniRegistro" class="js_frm_signup js_frm_signup_white pull-right text-md-center hidden-sm hidden-xs hidden-md animate" style="text-align: center" data-animate="bounceInRight" data-duration="1.0s" data-delay="0.75s" data-offset="100">
            <h3 style="font-size: 26px;" data-animation="animated fadeInLeft" class="">REGISTRO RÁPIDO</h3>
            <form role="form" action="jq/jq_registro_medio.asp">
                <div class="form-group" data-animation="animated fadeInRight">
                    <input type="text" name="nombre" class="form-control" placeholder="Nombre" required>
                </div>
                <div class="form-group col-md-6" style="padding: 0px" data-animation="animated fadeInLeft">
                    <input type="text" name="apaterno" class="form-control" placeholder="A. Paterno" required>
                </div>
                <div class="form-group col-md-6" style="padding: 0px" data-animation="animated fadeInLeft">
                    <input type="text" name="amaterno" class="form-control" placeholder="A. Materno" required>
                </div>
                <div class="form-group col-md-6" style="padding: 0px" data-animation="animated fadeInRight">
                    <input placeholder="Fecha de Nacimiento" name="fechanac" class="form-control" type="text" onfocus="(this.type='date')" required>
                </div>
                <div class="form-group col-md-6" style="padding: 0px" data-animation="animated fadeInRight">
                    <input type="text" name="cedula" class="form-control" placeholder="Cédula profesional" required>
                </div>
                <div class="form-group" data-animation="animated fadeInLeft">
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group" data-animation="animated fadeInRight">
                    <input type="password" name="contrasena" class="form-control" placeholder="Contraseña" required>
                </div>
                <div class="form-group" data-animation="animated fadeInRight">
                    <select class="form-control" name="especialidad" required>
                        <option value="">Especialidad</option>
                        <%SQLESP = "select * from cat_especialidades where id_especialidad not in (4) and activo = 'SI'"
                          Cursor.Open SQLESP, StrConn, 1, 3
                          Do While Not Cursor.EOF %>
                        <option value="<%=Cursor("id_especialidad") %>"><%=Cursor("especialidad") %></option>
                        <%Cursor.MoveNext
                           Loop
                          Cursor.Close %>
                        <option value="0">Otra</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" data-animation="animated fadeInRight">Enviar Datos</button>
            </form>
        </div>
        <!--Fin Registro-->

    </section>
    <section id="banner" class="m-t-20">
        <div class="content">
            <div class="row">
                <div class="col-md-12 center-block text-center">
                    <a href="registro-mini.asp">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" style="min-height: inherit !important;">
                                <% max=4
                                    min=1
                                    Randomize
                                    my_num=int((max-min+1)*rnd+min)                                    
                                     for i = 1 to 3
                                    if my_num = i then
                                        claseC = "active"
                                    else
                                        claseC = ""
                                    end if %>
                                <div class="item <%=claseC %>">
                                    <img src="images/bannerpromo/banner0<%=i %>.jpg" class="img-responsive center-block" />
                                </div>
                                <%next %>
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" style="top: 40%; background-image: none; color: #1a6cb0;" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" style="top: 40%; background-image: none; color: #1a6cb0;" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section id="temarios" class="m-t-20">
        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="row hg-card">
                        <div class="col-md-12 ">
                            <h3 style="font-size: 26px;"><i class="fa fa-newspaper-o strong-look" aria-hidden="true"></i><span class="strong-look" style="font-size: 26px;">&nbsp;NOVEDADES</span></h3>
                            <hr />
                        </div>
                        <div class="col-md-12 text-left" style="margin-top: 14px;">
                            <div id="carousel-example-generic" class="carousel slide vertical" data-ride="carousel" data-interval="false">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner vertical" role="listbox">
                                    <% claseNov = "active"
                                SQL = "select *, (select titulo from tbl_videos where id_video = tbl_top_contenido.id_tipo) as tituloVideo, " &_
                                      "(select titulo from tbl_fasciculos where id_fasciculos = tbl_top_contenido.id_tipo) as tituloRevision " &_
                                      " from tbl_top_contenido where localizacion = 'N' and tipo = 'V' and activo = 'SI' order by newid()"
                                Cursor.Open SQL, StrConn, 1, 3
                                Do While Not Cursor.EOF %>
                                    <div class="item <%=claseNov %>" style="margin-right: 7px;">
                                        <div class="col-md-12 card-box text-left hvr-grow ">
                                            <figure class="effect-honey">
                                                <div class="col-md-6 novedades-img">
                                                    <%select case Cursor("tipo")
                                                        case "V"
                                                            textoNov = "Videolección"
                                                            textoTit = Cursor("tituloVideo")
                                                    %>
                                                    <img src="images/videoteca/videos/<%=Cursor("id_tipo") %>.jpg" alt="<%=Cursor("id_tipo") %>" class="img-responsive articles-img" />
                                                    <%case "R"
                                                            textoNov = "Revisión"
                                                            textoTit = Cursor("tituloRevision") %>
                                                    <img src="images/videoteca/revisiones/<%=Cursor("id_tipo") %>.png" alt="<%=Cursor("id_tipo") %>" class="img-responsive articles-img" style="margin-top: 0px;" />
                                                    <%case "L"
                                                            textoNov = "Lectura guiada"
                                                            textoTit = Cursor("tituloRevision") %>
                                                    <img src="images/videoteca/revisiones/<%=Cursor("id_tipo") %>.png" alt="<%=Cursor("id_tipo") %>" class="img-responsive articles-img" style="margin-top: 0px;" />
                                                    <%end select %>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="text-dark"><b><%=textoNov %>:&nbsp;<%=textoTit %></b></h4>
                                                    <h5 class="text-dark"><%=Cursor("texto") %></h5>
                                                    <a href="login.asp" class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">Ver más</a>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>

                                    <% Cursor.MoveNext
                                claseNov = ""
                                Loop                                
                                Cursor.Close %>
                                </div>

                                <!-- Controls -->
                                <a class="carousel-ind-left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-ind-right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row hg-card">
                        <div class="col-md-12">
                            <h3><i class="fa fa-list strong-look" aria-hidden="true"></i><span class="strong-look" style="font-size: 26px;">&nbsp;TEMARIO</span></h3>
                            <hr />
                        </div>
                        <div class="col-md-12">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default bx-shadow-none intro-color">
                                    <div class="panel-heading intro-color" role="tab" id="headingFour">
                                        <h4 class="text-white">
                                            <a class="collapsed text-white" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree"><i class="icon-cursoscertificacion" aria-hidden="true"></i>&nbsp;<b style="font-size: 20px;">Curso Introductorio</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body text-white">
                                            <div class="col-md-8 border">
                                                <%SQL = "select * from tbl_modulos where (id_curso  = 1 or id_curso2 = 1 or id_curso3 = 1 ) and activo = 'SI' ORDER by secuencia" %>
                                                <%Cursor.Open SQL, StrConn, 1, 3  %>
                                                <% 
                                            Contador = 1
                                            Do While not Cursor.EOF                                   
                                                %>
                                                <p><b>MÓDULO</b> <%=Contador %>. <%=Cursor("titulo") %></p>
                                                <%
                                            Cursor.MoveNext
                                             Contador = Contador + 1
                                            Loop 
                                                %>
                                                <%Cursor.Close %>
                                            </div>
                                            <div class="col-md-4 text-center">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h2 class="text-white m-t-75" style="margin-left: -10px;">TOTAL DE HORAS:&nbsp;<b>01:46</b></h2>
                                                        <button class="btn btn-lg btn-grey in-dt" data-toggle="modal" data-target="#detalleTemario"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;Ver detalle</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default bx-shadow-none mi-color">
                                    <div class="panel-heading  mi-color" role="tab" id="headingTwo">
                                        <h4 class="text-white">
                                            <a class="collapsed text-white" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="icon-medicinaGeneral" aria-hidden="true"></i>&nbsp;<b style="font-size: 20px;">Medicina General</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body text-white">
                                            <div class="col-md-8 border">
                                                <%SQL = "select * from tbl_modulos where (id_curso  = 3 or id_curso2 = 3 or id_curso3 = 3 )  and activo = 'SI' ORDER by secuencia" %>
                                                <%Cursor.Open SQL, StrConn, 1, 3  %>
                                                <% 
                                            Contador = 1
                                            Do While not Cursor.EOF                                   
                                                %>
                                                <p><b>MÓDULO</b> <%=Contador %>. <%=Cursor("titulo") %></p>
                                                <%
                                            Cursor.MoveNext
                                             Contador = Contador + 1
                                            Loop 
                                                %>
                                                <%Cursor.Close %>
                                            </div>
                                            <div class="col-md-4 text-center">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h2 class="text-white m-t-75" style="margin-left: -10px;">TOTAL DE HORAS:&nbsp;<b>18:06</b></h2>
                                                        <button class="btn btn-lg btn-grey mg-dt" data-toggle="modal" data-target="#detalleTemario"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;Ver detalle</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default bx-shadow-none md-color">
                                    <div class="panel-heading md-color parallax" role="tab" id="headingOne">
                                        <h4 class="text-white">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="text-white"><i class="icon-medicinaInterna" aria-hidden="true"></i>&nbsp;<b style="font-size: 20px;">Medicina Interna</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body text-white">
                                            <div class="col-md-8 border">
                                                <%SQL = "select * from tbl_modulos where (id_curso  = 2 or id_curso2 = 2 or id_curso3 = 2 ) and activo = 'SI' ORDER by secuencia2" %>
                                                <%Cursor.Open SQL, StrConn, 1, 3  %>
                                                <% 
                                            Contador = 1
                                            Do While not Cursor.EOF                                   
                                                %>
                                                <p><b>MÓDULO</b> <%=Contador %>. <%=Cursor("titulo") %></p>
                                                <%
                                            Cursor.MoveNext
                                             Contador = Contador + 1
                                            Loop 
                                                %>
                                                <%Cursor.Close %>
                                            </div>
                                            <div class="col-md-4 text-center">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h2 class="text-white m-t-75" style="margin-left: -10px;">TOTAL DE HORAS:&nbsp;<b>16:19</b></h2>
                                                        <button class="btn btn-lg btn-grey mi-dt" data-toggle="modal" data-target="#detalleTemario"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;Ver detalle</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default bx-shadow-none ort-color">
                                    <div class="panel-heading ort-color " role="tab" id="headingThree">
                                        <h4 class="text-white">
                                            <a class="collapsed text-white" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="icon-ortopedia" aria-hidden="true"></i>&nbsp;<b style="font-size: 20px;">Ortopedia</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body text-white">
                                            <div class="col-md-8 border">
                                                <%SQL = "select * from tbl_modulos where (id_curso  = 4 or id_curso2 = 4 or id_curso3 = 4 )  and activo = 'SI' ORDER by secuencia3" %>
                                                <%Cursor.Open SQL, StrConn, 1, 3  %>
                                                <% 
                                            Contador = 1
                                            Do While not Cursor.EOF                                   
                                                %>
                                                <p><b>MÓDULO</b> <%=Contador %>. <%=Cursor("titulo") %></p>
                                                <%
                                            Cursor.MoveNext
                                             Contador = Contador + 1
                                            Loop 
                                                %>
                                                <%Cursor.Close %>
                                            </div>
                                            <div class="col-md-4 text-center">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h2 class="text-white m-t-75" style="margin-left: -10px;">TOTAL DE HORAS:&nbsp;<b>31:39</b></h2>
                                                        <button class="btn btn-lg btn-grey or-dt" data-toggle="modal" data-target="#detalleTemario"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;Ver detalle</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Detalle Temario -->
    <div id="detalleTemario" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 id="dtFrame" class="modal-title">
                        Temario</h4>
                </div>
                <div class="modal-body">
                    <iframe id="detallesTem" name="contenidoFrame" src="temario.asp?cur=2" frameborder="0"
                        allowfullscreen=""></iframe>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <h4 style="color: #2c65b4;">
                                <i class="fa fa-star" style="color: #2c65b4;" aria-hidden="true"></i>&nbsp;Contenido
                                destacado</h4>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">
                                Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--#include file="footer.asp" -->
    <div class="scroll-top-wrapper ">
        <span class="scroll-top-inner"><i class="fa fa-2x fa-arrow-circle-up"></i></span>
    </div>

    <script>
        var resizefunc = [];
    </script>

    <script src="assets/js/scrolla.jquery.min.js"></script>

    <script src="js/responsive-carousel-video.js"></script>

    <script src="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.js"></script>

    <script type="text/javascript">

        function limpiarFrame() {
            $('#frameVid').removeClass('widthFrame')
            $('#frameVid').addClass('widthFrame2')
        }

        function enterCur(x) {
            $('#miniRegistro').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            });
        };

        function saleCur(x) {
            $('#miniRegistro').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            });
        };

        $(document.body).on('click', '.thumbnail-carousel', function(e) {
            e.preventDefault();
            anim = "bounceOutRight";
            $('#miniRegistro').removeClass('bounceInRight')
            $('#miniRegistro').removeClass('animate')
            enterCur(anim);
            limpiarFrame();
        });

        $(document.body).on('click', '.changefr', function(e) {
            //e.preventDefault();
            var myvid = $(this).parent().find('input:hidden[name=idv]').val();
            url = "view_teasers.asp?idv=" + myvid;
            //alert(url);
            $('#frameVid').attr('src', url);
            $('#teaserFra').removeClass('hidden');
            limpiarFrame();
        });

        $('#video_carousel').bind('slide.bs.carousel', function(e) {
            currentIndex = $('div.active').index() + 1;
            //alert(currentIndex);
            if (currentIndex == 7) {
                //alert("debe de cambiar");
                url = "blank.htm";
                $('#frameVid').attr('src', url);
            }
            limpiarFrame();
        });

        $(document.body).on('click', '.carousel-control', function(e) {
            e.preventDefault();
            anim = "bounceOutRight";
            $('#miniRegistro').removeClass('bounceInRight')
            $('#miniRegistro').removeClass('animate')
            enterCur(anim);
        });

        $(document.body).on('click', '.btn-registro', function(e) {
            e.preventDefault();
            anim = "bounceInRight";
            $('#miniRegistro').removeClass('bounceOutRight')
            $('#miniRegistro').removeClass('animate')
            enterCur(anim);
        });

        $(document.body).on('click', '.mi-dt', function(e) {
            $('#dtFrame').html('<i class="icon-medicinaInterna" aria-hidden="true"></i>&nbsp;Temario Curso Medicina Interna')
            $('#detallesTem').attr('src', "temario.asp?cur=2")
        });

        $(document.body).on('click', '.mg-dt', function(e) {
            $('#dtFrame').html('<i class="icon-medicinaGeneral" aria-hidden="true"></i>&nbsp;Temario Curso Medicina General')
            $('#detallesTem').attr('src', "temario.asp?cur=3")
        });

        $(document.body).on('click', '.or-dt', function(e) {
            $('#dtFrame').html('<i class="icon-ortopedia" aria-hidden="true"></i>&nbsp;Temario Curso Ortopedia')
            $('#detallesTem').attr('src', "temario.asp?cur=4")
        });

        $(document.body).on('click', '.od-dt', function(e) {
            $('#dtFrame').html('<i class="icon-odontologia" aria-hidden="true"></i>&nbsp;Temario Curso Odontología')
            $('#detallesTem').attr('src', "temario.asp?cur=5")
        });

        $(document.body).on('click', '.in-dt', function(e) {
            $('#dtFrame').html('<i class="icon-medicinaGeneral" aria-hidden="true"></i>&nbsp;Temario Curso Introductorio')
            $('#detallesTem').attr('src', "temario.asp?cur=1")
        });

        $(document.body).on('mouseover', '#btn-header-registro', function(e) {
            e.preventDefault();
            anim = "bounceInRight";
            $('#miniRegistro').removeClass('bounceOutRight')
            $('#miniRegistro').removeClass('animate')
            enterCur(anim);
        });


        $(document).ready(function() {
            $(function() {
                $(document).on('scroll', function() {
                    if ($(window).scrollTop() > 100) {
                        $('.scroll-top-wrapper').addClass('show');
                    } else {
                        $('.scroll-top-wrapper').removeClass('show');
                    }
                });
                $('.scroll-top-wrapper').on('click', scrollToTop);
            });

            function scrollToTop() {
                verticalOffset = typeof (verticalOffset) != 'undefined' ? verticalOffset : 0;
                element = $('body');
                offset = element.offset();
                offsetTop = offset.top;
                $('html, body').animate({ scrollTop: offsetTop }, 500, 'linear');
            }
        });
    </script>

    <script>
        $('.carousel .vertical .item').each(function() {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i = 1; i < 2; i++) {
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
            }
        });
    </script>

    <%If Request("fail") = "email" then  %>

    <script type="text/javascript">
        $(window).load(function() {
            swal(
              'Error',
              'El email ya fue registrado. Intente nuevamente',
              'error'
            )
        });
    </script>

    <%End If %>

</body>
</html>
