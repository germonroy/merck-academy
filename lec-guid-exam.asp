﻿<!--#include file="includes/config.asp" -->
<%Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID = 1033
%>
<!--#include file="data/con_ma3.inc" -->
<!--#include file="data/con_ma.inc" -->
<!--#include file="generadores/md5.asp" -->
<%

Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

%>
<%
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3

set Cursor2 = createobject("ADODB.Recordset")
Cursor2.CursorType =1 
Cursor2.LockType = 3

set Cursor3 = createobject("ADODB.Recordset")
Cursor3.CursorType =1 
Cursor3.LockType = 3
    set Cursor4 = createobject("ADODB.Recordset")
Cursor4.CursorType =1 
Cursor4.LockType = 3

SQL = "select * from tbl_fasciculos where id_fasciculos = " & Request("idr") &""
Cursor.Open SQL, StrConn, 1, 3

If Cursor.RecordCount > 0 then
    Title = Cursor("titulo")
end if
Cursor.Close

Select case Request("idc")
    Case 1
        icono =""
    Case 2
        icono = "medicinaInterna"
    Case 3
        icono = "medicinaGeneral"
    Case 4
        icono = "ortopedia"
    Case 5
        icono = "revisioDelMes"

  End Select 
    
strH = Session("id_user") & "." & Session("TheName") & "-" & now() & "-urrOOuqe.MA15-" &Session("email")
HU = md5(strH)
Session("HUExam") = HU
%>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />

    <!--#include file="header_int.asp" -->
    <link rel="stylesheet" href="assets/css/font_ma.css" />
    <link href="css/lec-guid-exam.css" rel="stylesheet" />
    <link href="css/certificados.css" rel="stylesheet" />
    <link href="icono-lec/style.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.css" rel="stylesheet" />

</head>

<body class="fixed-left">

    <!--#include file="loader.asp" -->

    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->

        <div class="content-page">

            <div class="content">
                <div class="container" id="mainContainer">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="background-color: #3266b1 !important; padding: 18px 53px;">
                            <div class="row">
                                <%SQL = "select *  from tbl_preguntas_lec where id_fasciculos =" & Request("idr") & " and activo = 'SI' and tipo_pregunta = 'REL' order by NEWID()"
                                       Cursor.Open SQL, StrConn, 1, 3
                                %>
                                <%If Cursor.RecordCount > 0 then  %>
                                <div class="col-md-10" id="panelLec">
                                    <h2 style="color: white"><span class="icon-<%=icono %>" style="margin-right: 5px; margin-top: 5px; color: white; font-size: 40px; vertical-align: middle;"></span>&nbsp;Lectura Guiada:</h2>
                                    <h3 style="color: white">&nbsp;<%=Title %></h3>
                                </div>

                                <div class="col-md-2">
                                    <img src="images/iconos/drag.png" class="img-responsive" style="max-height: 85px;" />
                                </div>
                                <%Else %>
                                <div class="col-md-12" id="panelLec">
                                    <h2 style="color: white"><span class="icon-<%=icono %>" style="margin-right: 5px; margin-top: 5px; color: white; font-size: 40px; vertical-align: middle;"></span>&nbsp;Lectura Guiada:</h2>
                                    <h3 style="color: white">&nbsp;<%=Title %></h3>
                                </div>
                                <%End If %>
                                <%Cursor.Close() %>
                            </div>

                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <ol style="font-size: 1.4em; color: black; font-weight: 600;">
                                        <%SQL = "select *  from tbl_preguntas_lec where id_fasciculos =" & Request("idr") & " and activo = 'SI' order by NEWID()"
                                       Cursor.Open SQL, StrConn, 1, 3
                                        %>
                                        <%If Cursor.RecordCount > 0 then %>
                                        <%TotalPreguntas = Cursor.RecordCount %>
                                        <%Contador = 1 %>
                                        <%Do While not Cursor.EOF      %>
                                        <%TotalRel = 0 %>

                                        <li>
                                            <h3 style="color: black"><%=Cursor("pregunta") %></h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form id="PREG<%=Contador %>">
                                                        <%Select Case Cursor("tipo_pregunta") %>
                                                        <%Case "VF" %>
                                                        <input type="hidden" name="HID<%=Contador %>" id="HID<%=Contador %>" value="VF" />
                                                        <input type="hidden" name="HIDPREG<%=Contador %>" id="HIDPREG<%=Contador %>" value="<%=Cursor("id_pregunta") %>" />
                                                        <input type="hidden" name="HIDVALOR<%=Contador %>" id="HIDVALOR<%=Contador %>" value="<%=Cursor("porcentaje") %>" />
                                                        <%SQL = "select * from tbl_respuestas_lec_vf  where id_pregunta = " & Cursor("id_pregunta") & "and activo='SI 'order by NEWID()" %>
                                                        <%Cursor2.Open SQL, StrConn, 1, 3 %>
                                                        <%If Cursor2.RecordCount > 0 then  %>
                                                        <input type="hidden" name="HIDNUMPREG<%=Contador %>" id="HIDNUMPREG<%=Contador %>" value="<%=Cursor2.RecordCount%>" />
                                                        <%Cont2= 1 %>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <h3><b>Verdadero</b></h3>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <h3><b>Falso</b></h3>
                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%Do While not Cursor2.EOF%>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-6" style="font-size: 20px; margin-bottom: 22px; font-weight: 100">
                                                                        <%=Cursor2("respuesta") %>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="onoffswitch">
                                                                                    <input type="radio" name="RESVF<%=Cont2 %>-<%=Contador %>" data-contador="<%=Cursor2("id_respuesta") %>" class="onoffswitch-checkbox" id="RESVF-V<%=Cont2 %>-<%=Contador %>" required />
                                                                                    <label class="onoffswitch-label" for="RESVF-V<%=Cont2 %>-<%=Contador %>">
                                                                                        <span class="onoffswitch-inner-v"></span>
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label>

                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">

                                                                                <div class="onoffswitch">
                                                                                    <input type="radio" name="RESVF<%=Cont2 %>-<%=Contador %>" data-contador="<%=Cursor2("id_respuesta") %>" class="onoffswitch-checkbox" id="RESVF-F<%=Cont2 %>-<%=Contador %>" />
                                                                                    <label class="onoffswitch-label" for="RESVF-F<%=Cont2 %>-<%=Contador %>">
                                                                                        <span class="onoffswitch-inner-f"></span>
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <%
                                                        Cursor2.MoveNext
                                                        Cont2 = Cont2+1
                                                        Loop 
                                                        
                                                        %>
                                                        <%End If %>
                                                        <%Cursor2.Close()  %>







                                                        <%Case "MUL" %>
                                                        <input type="hidden" name="HID<%=Contador %>" id="HID<%=Contador %>" value="MUL" />
                                                        <input type="hidden" name="HIDPREG<%=Contador %>" id="HIDPREG<%=Contador %>" value="<%=Cursor("id_pregunta") %>" />
                                                        <input type="hidden" name="HIDVALOR<%=Contador %>" id="HIDVALOR<%=Contador %>" value="<%=Cursor("porcentaje") %>" />
                                                        <%SQl = "select * from tbl_respuestas_lec_mul where id_pregunta = " & Cursor("id_pregunta") & "and activo='SI 'order by NEWID()" %>
                                                        <%Cursor2.Open SQL, StrConn, 1, 3 %>
                                                        <%If Cursor2.RecordCount > 0 then  %>
                                                        <input type="hidden" name="HIDNUMPREG<%=Contador %>" id="HIDNUMPREG<%=Contador %>" value="<%=Cursor2.RecordCount%>" />


                                                        <%Do While not Cursor2.EOF      %>
                                                        <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12" style="font-size: 20px; display: inline; min-height: 50px">
                                                            <div class="col-lg-1 col-md-2  col-sm-2  col-xs-2 ">
                                                                <input type="radio" class="option-input radio" id="RESMUL<%=Cursor2("id_respuesta") %>" value="<%=Cursor2("id_respuesta") %>" name="RESMULPREG<%=Contador %>" required style="margin: 0px !important; top: 0px !important;" />
                                                            </div>

                                                            <div class="col-lg-11 col-md-10  col-sm-10  col-xs-10 ">
                                                                <label for="RESMUL<%=Cursor2("id_respuesta") %>" style="font-weight: 100 !important; margin-left: 12px; display: inline"><%=Cursor2("respuesta") %></label>
                                                            </div>
                                                        </div>





                                                        <%
                                                        Cursor2.MoveNext
                                                        Loop 
                                                       
                                                        %>

                                                        <%End If %>
                                                        <% Cursor2.Close()  %>






                                                        <%Case "REL" %>
                                                        <%TotalRel = Cursor("porcentaje") %>
                                                        <%Columna1 = "" %>
                                                        <%Correcta = "" %>
                                                        <%Columna2 = "" %>
                                                        <input type="hidden" name="HID<%=Contador %>" id="HID<%=Contador %>" value="REL" />
                                                        <input type="hidden" name="HIDPREG<%=Contador %>" id="HIDPREG<%=Contador %>" value="<%=Cursor("id_pregunta") %>" />
                                                        <input type="hidden" name="HIDVALOR<%=Contador %>" id="HIDVALOR<%=Contador %>" value="<%=Cursor("porcentaje") %>" />
                                                        <input type="hidden" name="HIDNUMRELACIONADO<%=Contador %>" id="HIDNUMRELACIONADO<%=Contador %>" value="0" />

                                                        <%SQl = "select * from tbl_respuestas_lec_rel where id_pregunta = " & Cursor("id_pregunta") & "and activo='SI 'order by NEWID()" %>

                                                        <%Cursor2.Open SQL, StrConn, 1, 3 %>
                                                        <%If Cursor2.RecordCount > 0 then  %>
                                                        <input type="hidden" name="HIDNUMPREG<%=Contador %>" id="HIDNUMPREG<%=Contador %>" value="<%=Cursor2.RecordCount%>" />
                                                        <%TotalReactivo = cint(TotalRel)/Cursor2.RecordCount %>
                                                        <div class="col-md-6">
                                                            <%Cont2 = 1 %>
                                                            <%Do While not Cursor2.EOF      %>
                                                            <input type="hidden" id="REL-<%=Contador%>-<%=Cont2 %>" name="REL-<%=Contador%>-<%=Cont2 %>" value="0" />
                                                            <input type="hidden" id="COREL-<%=Contador%>-<%=Cont2 %>" name="COREL-<%=Contador%>-<%=Cont2 %>" value="0" />
                                                            <div class="col-md-12">
                                                                <div class="drag" id="COLONE<%=Cursor2("id_respuesta") %>" data-respuesta="<%=Cursor2("id_respuesta") %>" data-correcta="<%=Cursor2("correcta") %>" data-total="<%=TotalReactivo %>" data-pregunta="<%=Cursor("id_pregunta") %>" data-contador="<%=Cont2 %>">
                                                                    <h2><%=Cursor2("respuesta") %></h2>
                                                                </div>


                                                            </div>
                                                            <%
                                                        Cursor2.MoveNext
                                                        Cont2 = Cont2+1
                                                        Loop 
                                                            %>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <%SQl = "select *  from tbl_subrespuestas_lec_rel  where id_pregunta in("&Cursor("id_pregunta")&") and activo='SI 'order by NEWID()" %>
                                                            <%Cursor3.Open SQL, StrConn, 1, 3 %>
                                                            <%If Cursor3.RecordCount > 0 then  %>
                                                            <%Do While not Cursor3.EOF      %>
                                                            <div class="col-md-12">
                                                                <div class="drop" id="drop-<%=Cursor3("id_subrespuesta_lec") %>" data-relacion="<%=Cursor3("id_subrespuesta_lec") %>" data-numpreg="<%=Contador %>">
                                                                    <h2><%=Cursor3("Respuesta") %></h2>
                                                                </div>
                                                            </div>
                                                            <%
                                                        Cursor3.MoveNext
                                                        Loop  
                                                            %>
                                                            <%End If %>
                                                            <%Cursor3.Close %>
                                                        </div>



                                                        <%End If %>
                                                        <% Cursor2.Close()  %>

                                                        <%End Select %>
                                                    </form>


                                                </div>
                                            </div>
                                        </li>

                                        <hr />
                                        <%                      
                                        Cursor.MoveNext
                                        Contador = Contador +1 
                                        Loop 
                                       
                                        %>
                                        <%End If %>
                                        <% Cursor.Close()  %>
                                    </ol>

                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: -webkit-right; background-color: white" id="footer">
                            <button class="btn btn-success btn-rounded btn-lg w-md waves-effect waves-light m-b-5 w-lg" id="btnEnviarFormulario"><i class="fa fa-check-circle m-r-5" aria-hidden="true"></i>&nbsp;Enviar</button>
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>
    <script>
            var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script src="js/logout.js"></script>
    <script src="js/loader.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-sweetalerts2/sweetalert2.min.js"></script>

    <script type="text/javascript">
        $("#btnRedirect").click(function () {
            swal({
                title: 'Alto',
                text: "¿Esta seguro continuar con su evaluación?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:  '<i class="fa fa-thumbs-up"></i> Si!',
                cancelButtonText:  '<i class="fa fa-thumbs-down"></i>No'
            }).then(function () {

            });
        });

    </script>
    <script type="text/javascript">
      
        var respuesta= "";

        var totalMul = 0;
        var totalRel = 0;
        var totalVF = 0 ;
        var totalEvaluacion = 0;


        var totalPreguntasVF = 0;
        var totalReactivo = 0;
        var totalCorrectas = 0;
        var totalPreguntasRel = 0;
        var badExamen = false;
        var totalRelacion = 0;
        var elmnt,correcta,cocorrecta;

       

        $("#btnEnviarFormulario").click(function () {

            
            swal({
                title: '¡Aviso!',
                text: "¿Está seguro de enviar su evaluación?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: ' <i class="fa fa-paper-plane" aria-hidden="true"></i> Si',
                cancelButtonText: '<i class="fa fa-times" aria-hidden="true"></i> No',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            $(window).unbind('beforeunload');
                            badExamen = false;
                            totPreguntas = <%=TotalPreguntas%>;
                            totalMul = 0;
                            totalVF = 0;
                            totalCorrectas = 0;
                            var i;
                            Loop:
                                for(i =1;i<= totPreguntas;i++)
                                {
                                    tipoPregunta = document.getElementById("HID"+i).value;

                                    switch(tipoPregunta) {
                                        case "MUL":
                                            respuesta =  $('input[name="RESMULPREG'+i+'"]:checked').val();        
                                            if(!respuesta){
                                                respuesta = "0"
                                                badExamen = true;
                                            }
                                            else{
                                                var formData = {
                                                    'answer': respuesta,
                                                    'question':document.getElementById("HIDPREG"+i).value
                                                };
                                                $.ajax({
                                                    async: false,
                                                    type: 'POST',
                                                    url: 'jq/jq_check_mul_answers.asp', 
                                                    data: formData, 
                                                    encode: true
                                                }).done(function (data) {
                                                    totalMul = parseFloat(data) + parseFloat(totalMul);

                                                });
                                            }
                                            break;
                                        case "VF":
                                            totalPreguntasVF = 0;
                                            totalReactivo = 0;
                                            totalCorrectas = 0;
                            
                                            totalReactivo = $('input[name="HIDVALOR'+i+'"]').val(); 
                                            totalPreguntasVF = $('input[name="HIDNUMPREG'+i+'"]').val();
                                            Loop2:
                                                for (var j=1; j <= totalPreguntasVF; j++) 
                                                {           
                                                    if($('input[id="RESVF-V'+j+'-'+i+'"]').is(':checked') ||$('input[id="RESVF-F'+j+'-'+i+'"]').is(':checked') )   
                                                    {
                               
                                                        if($('input[id="RESVF-V'+j+'-'+i+'"]').is(':checked')) { 
                                                            respuesta = "V"
                                                        }else if($('input[id="RESVF-F'+j+'-'+i+'"]').is(':checked')){
                                                            respuesta = "F"
                                                        }

                                                        var formData = {
                                                            'answer': respuesta,
                                                            'question': $('input[name="RESVF'+j+'-'+i+'"]').data("contador")
                                                        };
                                                        $.ajax({
                                                            async: false,
                                                            type: 'POST',
                                                            url: 'jq/jq_check_vf_answers.asp', 
                                                            data: formData, 
                                                            encode: true
                                                        }).done(function (data) {
                                                            if (data == "1")
                                                            {
                                                                totalCorrectas = totalCorrectas +1;
                                                            }
                                                        });
                                                    }  
                                                    else{
                                                        badExamen = true;
                                                        break Loop2;
                                                    }                  
                               
                               
                                  
                                                }
                            
                            
                            
                                            totalPreguntasVF = parseFloat((totalCorrectas * 100 )) / totalPreguntasVF;
                                            totalVF = parseFloat(totalVF) + parseFloat(((totalPreguntasVF*parseFloat(totalReactivo))/100))
                                            break;
                                        case "REL":
                                            totalPreguntasRel= 0;
                                            totalPreguntasRel = $('input[name="HIDNUMPREG'+i+'"]').val();
                                            totalPreguntasYaRel =  $("#HIDNUMRELACIONADO"+i).val();
                                            if(totalPreguntasYaRel != totalPreguntasRel)
                                            {
                                                badExamen = true;
                                            }

                                            break;


                                    }
                                    if(badExamen)
                                    {
                                        break Loop;
                                    }
                                }

                            if(badExamen)
                            {
                                swal(
                                   'Aviso!',
                                   'Tiene que contestar todas las preguntas',
                                   'error'
                                 )
                            }
                            else{
                                for(var i =1;i<= totPreguntas;i++)
                                {
                                    tipoPregunta = document.getElementById("HID"+i).value;
                                    switch(tipoPregunta) {
                                        case "MUL":
                                            respuesta =  $('input[name="RESMULPREG'+i+'"]:checked').val();        
                                            LogAnswers(document.getElementById("HIDPREG"+i).value,respuesta,respuesta,'MUL');
                                            console.log("KEEP MUL")
                                            break;
                                        case "VF":
                                            totalPreguntasVF = $('input[name="HIDNUMPREG'+i+'"]').val();
                                            for (var j=1; j <= totalPreguntasVF; j++) 
                                            {           
                                                if($('input[id="RESVF-V'+j+'-'+i+'"]').is(':checked')) { 
                                                    respuesta = "V"
                                                }else if($('input[id="RESVF-F'+j+'-'+i+'"]').is(':checked')){
                                                    respuesta = "F"
                                                }
                                                console.log("KEEP VF")
                                                LogAnswers(document.getElementById("HIDPREG"+i).value,$('input[id="RESVF-V'+j+'-'+i+'"]').data("contador"),respuesta,'VF');
                                            }
                                
                              
                                            break;
                                        case "REL":
                                            totalPreguntasRel = $('input[name="HIDNUMPREG'+i+'"]').val();
                                            for (var j=1;j<= totalPreguntasRel;j++)
                                            {
                                                correcta = $("#REL-"+i+"-"+j+"").val();
                                                cocorrecta = $("#COREL-"+i+"-"+j+"").val();
                                                console.log("KEEP REL")
                                                LogAnswers(document.getElementById("HIDPREG"+i).value,correcta, cocorrecta, 'REL');
                                            }
                                           
                                            break;
                                    }
                                }


                                totalEvaluacion = parseFloat(totalMul) + parseFloat(totalRel) + parseFloat(totalVF);


                                LogExam(<%=Request("idr")%>,<%=Request("idc")%>,Math.round(totalEvaluacion));
                                swal({
                                    title: 'Prueba Finalizada',
                                    text: "Para esta lectura guiada  su calificación obtenida fue de " + Math.round(totalEvaluacion) +"%",
                                    type: 'success',
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: ' <i class="fa fa-thumbs-up"></i> OK!',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false,
                                    allowEnterKey: false,
                                }).then(function () {
                                    window.location =  'certificados.asp?idcurso=<%=Request("idc")%>'
                                });
                            }

                        },1000);
                    })
                }
            }).then(function () {
                

            })


        });


    </script>
    <script src="js/jquery-1.7.1.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript">
        $(".drag").draggable({
            revert : function(event, ui) {

                $(this).data("ui-draggable").originalPosition = {
                    top : 0,
                    left : 0
                };
                return !event;
            }
        });
        $(".drag").data("select", false);
    </script>
    <script type="text/javascript">
        $(".drop").droppable({
            drop: function(event, ui) {
                if (!ui.draggable.data("select")) {
                    //ui.draggable.data("select", true);
                    var correcta = $(this).data("relacion");
                    var numPreg =  $(this).data("numpreg");

                    $(this).removeClass("border").removeClass("over");
                    var dropped = ui.draggable;
                    var numRelacion = ui.draggable.data("contador");
                    var droppedOn = $(this);
                    $(dropped).detach().css({ top: 0, left: 0, width: "100%" }).appendTo(droppedOn);
                    if (correcta == ui.draggable.data("correcta")) {
                        totalRel = parseFloat(totalRel) + parseFloat(ui.draggable.data("total"))

                    }
                    var relacionado = $("#HIDNUMRELACIONADO"+numPreg).val();
                    $("#HIDNUMRELACIONADO"+numPreg).val(parseFloat(relacionado)+1);

                    $("#REL-"+numPreg+"-"+numRelacion+"").val(correcta);
                    $("#COREL-"+numPreg+"-"+numRelacion+"").val(ui.draggable.data("correcta"));
                }
            }
           ,
            over: function(event, elem) {
                $(this).addClass("over");
                console.log("over");
            }
               ,
            out: function(event, elem) {
                $(this).removeClass("over");
            }
        });
    </script>

    <script type="text/javascript">
        function LogAnswers(id_pregunta,id_respuesta,respuesta,tipo_pregunta)
        {
            var formData = {
                'id_pregunta': id_pregunta,
                'id_respuesta':id_respuesta,
                'respuesta':respuesta,
                'tipo_pregunta':tipo_pregunta,
                't':'lec'
                
            };
            $.ajax({
                async: false,
                type: 'POST',
                url: 'jq/log.asp', 
                data: formData, 
                encode: true
            }).done(function (data) {
               
            });
          
        }
        function LogExam(id_fasciculo,id_curso,totalEvaluacion)
        {
            var formData = {
                'id_fasciculo': id_fasciculo,
                'id_curso':id_curso,
                'calificacion':totalEvaluacion,
                't':'lec'
                
            };
            $.ajax({
                async: false,
                type: 'POST',
                url: 'jq/log_exam.asp', 
                data: formData, 
                encode: true
            }).done(function (data) {

            });
        }
    </script>

</body>
</html>
