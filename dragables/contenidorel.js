function my_function() {
    var anim = "zoomIn";
    testAnim(anim);
}

function testAnim(x) {
    $('#contenido-rel').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $(this).removeClass();
    });
};