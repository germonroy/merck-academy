<div id="contenido-rel" class="hidden">
    <div class="contenido-rel-container">
        <h3 class="strong-look"><span class="strong-look fa fa-check-circle iconBanner"></span>&nbsp;Temas Relacionados</h3>
        <hr style="border-top: 2px solid #3266b1;">
        <div class="row">
            <div class="col-md-12 form-group">
                <div id="filters" class="button-group hidden-sm hidden-xs" style="text-align: center">
                    <a class="btn btn-default btn-bordred cr-All" href="../../carousel-int-rel-4.asp?id=2" target="contenidoFrame">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-th fa-stack-1x fa-inverse"></i>
                        </span>Todos
                    </a>

                    <a class="btn btn-default btn-bordred cr-Revisiones" href="../../carousel-int-rel-4.asp?id=2&filtro=revision" target="contenidoFrame">
                        <span class="fa-stack fa-lg ">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-file-o fa-stack-1x fa-inverse"></i>
                        </span>Revisiones del mes
                    </a>

                    <a class="btn btn-default btn-bordred cr-Ebooks" href="../../carousel-int-rel-4.asp?id=2&filtro=ebooks" target="contenidoFrame">
                        <span class="fa-stack fa-lg ">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-book fa-stack-1x fa-inverse"></i>
                        </span>Ebooks
                    </a>

                    <a class="btn btn-default btn-bordred cr-Inter" href="../../carousel-int-rel-4.asp?id=2&filtro=interactivos" target="contenidoFrame">
                        <span class="fa-stack fa-lg ">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-pencil fa-stack-1x fa-inverse" style="margin-top: 10px;"></i>
                        </span>Interactivos
                    </a>
                    <a class="btn btn-default btn-bordred cr-Images" href="../../carousel-int-rel-4.asp?id=2&filtro=imagenes" target="contenidoFrame">
                        <span class="fa-stack fa-lg ">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-file-image-o fa-stack-1x fa-inverse"></i>
                        </span>Banco de im�genes
                    </a>

                </div>
                <div id="filters3" class="btn-group btn-group-vertical hidden-lg hidden-md filters2" style="width: 100%">
                    <a class='btn btn-primary cr-All' href='javascript:void(0);document.getElementById("contenidoFrame").src ="../../carousel-int-rel-4.asp?id=2;"'><i class='fa fa-th'></i>&nbsp;Todos</a>
                    <a class='btn btn-primary cr-Revisiones' href='javascript:void(0);'><i class='icon-revisioDelMes'></i>&nbsp;Revisi�n del mes</a>
                    <a class='btn btn-primary cr-Ebooks' href='javascript:void(0);'><i class='icon-eBooks'></i>&nbsp;Ebooks</a>
                    <a class='btn btn-primary cr-Inter hidden-sm hidden-xs' href='javascript:void(0);'><i class='icon-interactivos'></i>&nbsp;Interactivos</a>
                    <a class='btn btn-primary cr-Images' href='javascript:void(0);'><i class='icon-bancoImagenes'></i>&nbsp;Im�genes</a>
                </div>
            </div>
        </div>
        <div class="embed-responsive embed-responsive-16by9 lecframe ">
            <iframe id="contenidoFrame" class="embed-responsive-item lecframe" name="contenidoFrame" src="http://labs.ma.webtube.com.mx/carousel-int-rel-4.asp?id=5" frameborder="0" allowfullscreen="" style="width: 100%"></iframe>
        </div>
    </div>
</div>