﻿<!--#include file="data/con_ma2.inc" -->
<!--#include file="generadores/md5.asp" -->
<%
Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"

Session.CodePage = 65001
Response.charset ="utf-8"

     set Cursor = createobject("ADODB.Recordset")
  Cursor.CursorType =1 
  Cursor.LockType = 3

  SQL = "SELECT  * FROM views_servier WHERE id = 0 "
  Cursor.Open SQL, StrConn ,1 ,3

    Cursor.AddNew
    Cursor("id_usuario") = Session("id_usuario")
    Cursor("fecha") = date()
    Cursor("hora") = time()
    Cursor.Update
    Cursor.Close
    StrConn.Close
    Set Cursor = Nothing
    Set StrConn = Nothing


   F1 = year(date())

   If day(date()) < 10 then
     D = "0" & day(date())
   else
     D = day(date())
   end if 
    
   If month(date()) < 10 then
     M2 = "0" & month(date())
   else
     M2 = month(date())
   end if  

    F1 = F1 & M2 & D 
   
    strH = "merckAcademy-" & F1
    Token = md5(strH)
    'URL = "http://articlesservice.es/merckAcademy/inicio.php?email=" & Session("id_usuario") & "&cupon=TESTMLGFQ1&paramkeyhttps=" & Token
    'URL = "http://articlesservice.es/merckAcademy/inicio.php?email=" & Session("id_usuario") & "&cupon=MERCKY0M2H2&paramkeyhttps=" & Token
    'Response.Redirect (URL)
    'Response.Write(Token)

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Merck y su equipo de profesionales médicos le da la más cordial bienvenida a la plataforma digital inteligente de contenidos médicos diseñados para cumplir con sus intereses y expectativas." />
    <meta name="keywords" content="merck, merck academy, dolor, neurobion, expertos en dolor, cursos, medicina interna, odontologia, medicina general, ortopedia, merck online, medicina" />
    <!--#include file="header_int.asp" -->
    <link href="css/flipper-journals.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/font_ma.css">
    <link href="css/journals-elsevier.css" rel="stylesheet" />
</head>
<body class="fixed-left">
    <!--#include file="loader.asp" -->
    <div id="wrapper">
        <!--#include file="top_int.asp" -->
        <!--#include file="sidebar.asp" -->
        <div class="content-page">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="float: right;">
                                <img src="images/escudos/escudo_journals.png" class="img-responsive " style="max-width: 290px; height: auto; margin-bottom: 30px;" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="margin-bottom: 5px; padding-bottom: 5px;">
                            <h4 style="color: #243e9f;"><strong><i class="fa fa-info-circle"></i>Acceso gratuito a 3 reconocidas publicaciones a nivel mundial especializadas en dolor.</strong></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div id="touch1" class="flip-container">
                                <div class="flipper">
                                    <div class="front" id="front1">
                                        <%
Set xmlDOM = Server.CreateObject("MSXML2.DOMDocument")
xmlDOM.async = False
xmlDOM.setProperty "ServerHTTPRequest", True
xmlDOM.Load("http://articlesservice.es/informes/informes.php?informe=6&contrato=LATAMdolor&clave=Elsevier2015&format=xml")
 
Set itemList = XMLDom.SelectNodes("respuesta/revistas/revista")
Dim a
a= 0
For Each itemAttrib In itemList
   if a= 0 then 
   urlportadafull = itemAttrib.SelectSingleNode("urlportadafull").text
   volumen = itemAttrib.SelectSingleNode("volumen").text
   titulo = itemAttrib.SelectSingleNode("titulo").text
   numero = itemAttrib.SelectSingleNode("numero").text

                                        %>
                                        <img src="<%=urlportadafull%>" class="img-responsive img-portada" />
                                        <%
   Exit For
    end if
                                                         a= a+ 1
Next
 
Set xmlDOM = Nothing
Set itemList = Nothing
                                        %>
                                    </div>
                                    <div class="back" id="back1" style="height: 100%;">
                                        <div id="thumbnail1" style="height: 100%;" class="thumbnail" style="padding: 0">
                                            <div>
                                                <img class="img-responsive center-block" src="images/elservier/logo_elsevier.jpg" />
                                            </div>
                                            <div class="caption text-center">
                                                <h4><%=titulo %></h4>
                                                <h5>Vol.&nbsp;<%=volumen %>&nbsp;<%=numero %></h5>
                                            </div>
                                            <div class="modal-footer" style="text-align: justify">
                                                <p class="hidden-lg hidden-md hidden-sm">Es una revista médica bimestral que abarca la medicina de cuidados intensivos.</p>
                                                <p class="hidden-xs">Es una revista médica bimestral que abarca la medicina de cuidados intensivos. Es el Diario Oficial de la Federación Mundial de Sociedades de Medicina Intensiva y Cuidados Intensivos y la Sociedad para enfermedades agudas complejas.</p>
                                                <p align="center"><a href="http://articlesservice.es/merckAcademy/inicio.php?email=<%=Session("id_usuario") %>&cupon=MERCKY0M2H2&paramkeyhttps=<%=Token %>&issn=15265900" class="btn btn-primary btn-sm" role="button">Leer</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div id="touch2" class="flip-container">
                                <div class="flipper">
                                    <div class="front" id="front2">
                                        <%
Set xmlDOM = Server.CreateObject("MSXML2.DOMDocument")
xmlDOM.async = False
xmlDOM.setProperty "ServerHTTPRequest", True
xmlDOM.Load("http://articlesservice.es/informes/informes.php?informe=6&contrato=LATAMdolor&clave=Elsevier2015&format=xml")
 
Set itemList = XMLDom.SelectNodes("respuesta/revistas/revista")
a= 0
For Each itemAttrib In itemList
   if a= 1 then 
   urlportadafull = itemAttrib.SelectSingleNode("urlportadafull").text
   volumen = itemAttrib.SelectSingleNode("volumen").text
   titulo = itemAttrib.SelectSingleNode("titulo").text
   numero = itemAttrib.SelectSingleNode("numero").text

                                        %>
                                        <img src="<%=urlportadafull%>" class="img-responsive img-portada" />
                                        <%
   Exit For
    end if
                                                         a= a+ 1
Next
 
Set xmlDOM = Nothing
Set itemList = Nothing
                                        %>
                                    </div>
                                    <div class="back" id="back2" style="height: 100%;">
                                        <div class="thumbnail" style="padding: 0; height: 100%;">
                                            <div>
                                                <img class="img-responsive center-block" src="images/elservier/logo_elsevier.jpg" />
                                            </div>
                                            <div class="caption text-center">
                                                <h4><%=titulo %></h4>
                                                <h5>Vol.&nbsp;<%=volumen %>&nbsp;<%=numero %></h5>
                                            </div>
                                            <div class="modal-footer" style="text-align: justify">
                                                <p class="hidden-lg hidden-md hidden-sm">Es una revista mensual que cubre la investigación clínica del alivio de la enfermedad</p>
                                                <p class="hidden-xs">Es una revista mensual que cubre la investigación clínica relacionada con el alivio de la carga de enfermedad en los pacientes que sufren de enfermedades graves o potencialmente mortales.</p>
                                                <p align="center"><a href="http://articlesservice.es/merckAcademy/inicio.php?email=<%=Session("id_usuario") %>&cupon=MERCKY0M2H2&paramkeyhttps=<%=Token %>&issn=08853924" class="btn btn-primary btn-sm" role="button">Leer</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div id="touch3" class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <%
Set xmlDOM = Server.CreateObject("MSXML2.DOMDocument")
xmlDOM.async = False
xmlDOM.setProperty "ServerHTTPRequest", True
xmlDOM.Load("http://articlesservice.es/informes/informes.php?informe=6&contrato=LATAMdolor&clave=Elsevier2015&format=xml")
 
Set itemList = XMLDom.SelectNodes("respuesta/revistas/revista")
a= 0
For Each itemAttrib In itemList
   if a= 2 then 
   urlportadafull = itemAttrib.SelectSingleNode("urlportadafull").text
   volumen = itemAttrib.SelectSingleNode("volumen").text
   titulo = itemAttrib.SelectSingleNode("titulo").text
   numero = itemAttrib.SelectSingleNode("numero").text

                                        %>
                                        <img src="<%=urlportadafull%>" class="img-responsive img-portada" />
                                        <%
   Exit For
    end if
                                                         a= a+ 1
Next
 
Set xmlDOM = Nothing
Set itemList = Nothing
                                        %>
                                    </div>
                                    <div class="back text-center" style="height: 100%;">
                                        <div class="thumbnail" style="padding: 0; height: 100%;">
                                            <div>
                                                <img class="img-responsive center-block" src="images/elservier/logo_elsevier.jpg" />
                                            </div>
                                            <div class="caption text-center">
                                                <h4><%=titulo %></h4>
                                                <h5>Vol.&nbsp;<%=volumen %>&nbsp;<%=numero %></h5>
                                            </div>
                                            <div class="modal-footer" style="text-align: justify">
                                                <p class="hidden-lg hidden-md hidden-sm">Es una revista médica mensual publicada por Elsevier en nombre de la Sociedad Americana del Dolor.</p>
                                                <p class="hidden-xs">Es una revista médica mensual publicada por Elsevier en nombre de la Sociedad Americana del Dolor. Abarca la investigación y revisiones en el dolor, incluyendo anestesiología y cuidados paliativos además de temas de educación y política relacionados al dolor.</p>
                                                <p align="center"><a href="http://articlesservice.es/merckAcademy/inicio.php?email=<%=Session("id_usuario") %>&cupon=MERCKY0M2H2&paramkeyhttps=<%=Token %>&issn=08839441" class="btn btn-primary btn-sm" role="button">Leer</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--#include file="footer.asp" -->
        </div>
    </div>
    <script>
            var resizefunc = [];
    </script>
    <script>
        var right = document.getElementById('front1').style.height;
        var left = document.getElementById('back1').style.height;
        if(left>right)
        {
            document.getElementById('back1').style.height=left;
            document.getElementById('thumbnail1').style.minHeight=left;
        }
        else
        {
            document.getElementById('front1').style.height=right;
            document.getElementById('thumbnail1').style.minHeight=right;
        }
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- Counter Up  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    <!-- KNOB JS -->
    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script src="js/logout.js"></script>
    <script src="js/loader.js"></script>
    <script>
        // Add no-touch class to body for mobile touch events and toggle hover class on elements that need it
	if ("mouseup" in document.documentElement) {
		document.documentElement.className += " touch";
	}
	// Add and remove no-hover class to <li>'s for mobile hover events
	jQuery('.touch .flip-container').each(function() {
		var div = jQuery(this);
		
		div.hover(function() {
			div.removeClass('no-hover');
		});
		
		jQuery('*').not(div).bind('click', function() {
			div.addClass('no-hover');
		});
		
	});
    </script>
    <script>
        var touch = 'ontouchstart' in document.documentElement
            || (navigator.MaxTouchPoints > 0)
            || (navigator.msMaxTouchPoints > 0);

            if (touch) { // remove all :hover stylesheets
                try { // prevent exception on browsers not supporting DOM styleSheets properly
                    for (var si in document.styleSheets) {
                        var styleSheet = document.styleSheets[si];
                        if (!styleSheet.rules) continue;

                        for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
                            if (!styleSheet.rules[ri].selectorText) continue;

                            if (styleSheet.rules[ri].selectorText.match(':hover')) {
                                styleSheet.deleteRule(ri);
                            }
                        }
                    }
                } catch (ex) {}
            }

        <%
            for i = 1 to 3 
                idtouch = "touch" & i
        %>
            $("#<%=idtouch %>").click(function(){

                if ($("#<%=idtouch %>").hasClass("flip-touch")) {
                    $("#<%=idtouch %>").removeClass("flip-touch"); 
                }
                else {              
                   $(".flip-touch").removeClass("flip-touch"); 
                   $("#<%=idtouch %>").delay(200).addClass("flip-touch");
                }
            });
        <%
            next
        %>
    </script>

</body>
</html>
