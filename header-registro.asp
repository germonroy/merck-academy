<nav class="navbar navbar-default navbar-fixed-top nav-gradient-top" id="pleca">
    <!--<div class="container">-->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <a class="navbar-brand" href="default.asp">
            <img src="assets/images/logos/1.png" class="img-responsive logoMerck" />
        </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <button class="navbar-right open-left hidden-lg hidden-md hidden-xs nav-ipad">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-cube"></span>
        </button>

        <ul class="nav navbar-nav navbar-right menu-index">
            <li id="liContact"><a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-Contact"><span class="fa fa-envelope-o" data-toggle="modal" data-target="#con-close-modal"></span>&nbsp;Contacto</a></li>
            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#full-width-modal-soporte"><span class="fa fa-question"></span>&nbsp;Soporte</a></li>
        </ul>
    </div>
    <!--</div>-->
</nav>
