<%
    Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"
%>
<link href="css/top_home.css" rel="stylesheet" />
<nav class="navbar navbar-default navbar-fixed-top nav-gradient-top" id="pleca">
    <div class="container">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-sm hidden-md" href="home.asp">
                <img src="images/logoMerck.png" class="img-responsive logoMerck" />
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <button class="navbar-right open-left hidden-lg hidden-md hidden-xs nav-ipad">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-cube"></span>
            </button>
            <ul class="nav navbar-nav navbar-right menu-index">
                <li><a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-Contact"><span class="fa fa-envelope-o" data-toggle="modal" data-target="#con-close-modal"></span>&nbsp;Contacto</a></li>
                <li><a href="javascript:void(0);" disabled data-toggle="modal" data-target="#full-width-modal-soporte"><span class="fa fa-question"></span>&nbsp;Soporte</a></li>
            </ul>
            <button id="btn-downmenu" type="button" style="position: absolute; margin-top: 40px; left: 50%;" class="btn btn-default btn-side-blanco navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">^</button>
        </div>
    </div>
</nav>
<!--#include file="con-sop-tut.asp" -->
