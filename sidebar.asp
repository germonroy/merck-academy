<%
if Session("TheName") = "" then
    Response.Redirect "../login.asp"
end if

Response.Expires = 15
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "private"    

%>
<style>
    .sidebar-inner::-webkit-scrollbar {
        width: 1em;
    }

    #style-3::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }
    #style-3::-webkit-scrollbar {
        width: 6px !important;
    }
</style>

<link href="css/sidebar.css" rel="stylesheet" />
<div id="switch" class="switch-leftbar closed-switch">
    <div class="row hidden-lg hidden-md hidden-sm" data-backdrop="static" data-keyboard="false">
        <button id="swbtn" data-target="barra-lateral-si" data-backdrop="static" data-keyboard="false" type="button" class="btn-default btn-side-blanco collapsed open-left btn-switch">></button>
    </div>
</div>
<div class="left side-menu" style="overflow-x: auto;">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 904px; background-color: #fff;">
        <div class="sidebar-inner" id="style-3" style="overflow: inherit; width: auto; height: 904px; background-color: #fff;">
            <div class="user-box" style="padding: 65px 0px 20px 0px;">
                <%
                flagbtn = 1
                if flagbtn = 1 then
                    clase = "btn-open-menu"
                    flagbtn = 0 
                end if
                %>
                <div class="user-img">
                    <%if Ltrim(Rtrim(Session("urlimagen"))) = "" then
                    if Session("cargo") = "Bienvenida " then %>
                    <img src="assets/images/users/Avatarmujer.jpg" class="img-circle img-thumbnail img-responsive" alt="<%=Session("TheName")%>">
                    <%else %>
                    <img src="assets/images/users/AvatarHombre.jpg" alt="<%=Session("TheName")%>" class="img-circle img-thumbnail img-responsive">
                    <%  end if
                else %>
                    <img src="/uploadfolder/<%=Session("urlimagen") %>" alt="<%=Session("TheName")%>" class="img-circle img-thumbnail img-responsive">
                    <%end if %>
                    <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
                </div>
                <div>
                    <h5>
                        <a href="#"><%=Session("TheName")%></a>
                    </h5>
                    <ul class="list-inline">
                        <%if Session("completo") = "SI" then %>
                        <li>
                            <a href="mi-cuenta.asp">
                                <i class="zmdi zmdi-settings" style="font-size: 1.5em;"></i>
                            </a>
                        </li>
                        <%end if %>

                        <%if Session("completo") = "NO" then %>
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Debe realizar el registro completo.">
                                <i class="zmdi zmdi-settings" style="font-size: 1.5em;"></i>
                            </a>
                        </li>
                        <%end if %>


                        <li>
                            <a href="jq/jq_logout.asp" class="text-custom" id="logbtnout">
                                <i class="zmdi zmdi-power" style="font-size: 1.5em; color: red;"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="sidebar-menu" onload="btnSideMenu">
                <ul>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="icon-escudo"></i><span>Acerca de </span><span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="mision.asp"><span class="icon-mission" style="margin-right: 5px; font-size: 16px;"></span>Misi&oacute;n</a></li>
                            <li><a href="vision.asp"><span class="fa fa-eye" style="margin-right: 5px; font-size: 16px;"></span>Visi&oacute;n</a></li>
                            <li><a href="valores.asp"><span class="icon-vision" style="margin-right: 5px; font-size: 16px;"></span>Valores</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="avales.asp" class="waves-effect"><i class="icon-avales"></i><span>Avales </span></a>
                    </li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="icon-cursoscertificacion "></i><span>Cursos de &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <span style="margin-left:38px;"> certificaci&oacute;n</span> </span><span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="reglas.asp?idcurso=3" style="color: #feca0d;"><span class="icon-medicinaGeneral" style="margin-right: 5px; font-size: 16px; color: #feca0d;"></span>Medicina General</a></li>
                            <li><a href="reglas.asp?idcurso=2" style="color: #0066b2;"><span class="icon-medicinaInterna" style="margin-right: 5px; font-size: 16px;"></span>Medicina Interna</a></li>
                            <li><a href="reglas.asp?idcurso=4" style="color: #ec1a3b;"><span class="icon-ortopedia" style="margin-right: 5px; font-size: 16px;"></span>Ortopedia</a></li>
                            <!--<li><a href="reglas.asp?idcurso=5" style="color: #009a5a;"><span class="icon-odontologia" style="margin-right: 5px; font-size: 16px;"></span>Odontolog&iacute;a</a></li>-->
                        </ul>
                    </li>
                    <li>
                        <a href="vid_cirugias.asp" class="waves-effect"><i class="icon-videoCirugias"></i><span>Video cirug&iacute;as </span></a>
                    </li>
                    <li>
                        <a href="conf_esp.asp" class="waves-effect"><i class="icon-cespeciales"></i><span>Conf. especiales </span></a>
                    </li>
                    <li>
                        <a href="rev_mes.asp" class="waves-effect "><i class="icon-revisioDelMes"></i><span>Revisi&oacute;n del mes </span></a>
                    </li>
                    <!--<li>
                        <a href="journals-elsevier.asp" class="waves-effect"><i class="icon-Elsevier"></i><span>Journals Elsevier </span></a>
                    </li>-->
                    <li>
                        <a href="ebooks.asp" class="waves-effect"><i class="icon-eBooks"></i><span>eBooks </span></a>
                    </li>
                    <li class="hidden-sm hidden-xs">
                        <a href="interactivos.asp" class="waves-effect"><i class="icon-interactivos"></i><span>Interactivos </span></a>
                    </li>
                    <li>
                        <a href="banco-imagenes.asp" class="waves-effect"><i class="icon-bancoImagenes"></i><span>Banco de im&aacute;genes </span></a>
                    </li>
                    <li>
                        <a href="profesores.asp" class="waves-effect"><i class="icon-profesores"></i><span>Profesores </span></a>
                    </li>
                    <style>
                        .alert-gary {
                            color: #1E2122;
                            background-color: rgba(126, 126, 126, 0.3);
                            border-color: rgba(30, 33, 34, 0.4);
                        }
                    </style>
                    <li>
                        <div class="alert alert-gary alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Para visualizar correctamente el contenido de este sitio, se recomienda utilizar <strong>Chrome</strong>
                        </div>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            
        </div>

    </div>
</div>
