<!--#include file="md5.asp"-->
<!--#include file="../data/con_ma.inc" -->


<%
Function PwdAleatorio ( Longitud, Repetir )
Dim vPass(), I, J ' nuestro vector y dos contadores
Dim vNumeros()	  ' vector para guardar lo que llevamos
Dim n, bRep		  
Dim vCaracteres	  ' vector donde est�n los posibles caract.

vCaracteres = Array("a","b", "c", "d", "e", "f", "g", "h", _
"j", "k", "m", "n", "p", "q", "r", "s", "t", _
"u", "v", "w", "x", "y", "z", "A","B", "C", "D", "E", "F", _ 
"G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", _
"S", "T", "U", "V", "W", "X", "Y", "Z", "2", "3", "4", _
"5", "6", "7", "8", "9")

'Establezco la longitud del vector
Redim vPass(Longitud-1)
'Y del vector auxiliar que guarda los caracteres ya escogidos
Redim vNumeros(Longitud-1)
I = 0
'Inicializo los n�s aleatorios
Randomize
'Hasta que encuentre todos los caracteres
do until I = Longitud
	'Hallo un n�mero aleatorio entre 0 y el m�ximo indice
	' del vector de caracteres.
	n = int(rnd*Ubound(vCaracteres))
	'Si no puedo repetir...
	if not Repetir then
		bRep = False
		'Busco el numero entre los ya elegidos
		for J = 0 to UBound(vNumeros)
			if n = vNumeros(J) then
			'Si esta, indico que ya estaba
				bRep = True
			end if
		next
		'Si ya estaba, tengo que repetir este caracter
		'as� que resto 1 a I y volvemos sobre la misma
		'posici�n.
		if bRep then 
			I = I - 1
		else
			vNumeros(I) = n
			vPass(I) = vCaracteres(n)	
		end if
	else
	'Me da igual que est� o no repetido
		vNumeros(I) = n
		vPass(I) = vCaracteres(n)
	end if
'Siguiente car�cter!
I = I + 1
loop

'Devuelvo la cadena. Join une los elementos de un vector
'utilizando como separador el segundo par�metro: en este
'caso, nada -&gt; "".
PwdAleatorio = Join(vPass, "")

End Function 'PwdAleatorio
A = 1
set Cursor = createobject("ADODB.Recordset")
Cursor.CursorType =1 
Cursor.LockType = 3 

Do While A <= 4 ''''CANTIDAD DE CODIGOS
  
  TipoCodigo = 1 '1 = Registro Unico, 2 = Registro Multiple

  If TipoCodigo = 1 then
    Gift = "MERCKACADEMY-" & Ucase(PwdAleatorio(5,2)) '''Definir Prefijo
  else
    Gift = "" & "-" & "" ''' Definir Codigo Completo
  end if

  SQL = "SELECT * FROM tbl_codigos WHERE codigo = '" & Gift & "'"
  Cursor.Open SQL, StrConn, 1, 3
  
  If Cursor.RecordCount = 0 then
   Cursor.AddNew
   Cursor("codigo") = Gift
   Cursor("id_patrocinador") = 2 ''1 = Merck, 2 Inkuba
   Cursor("id_tp_codigo") = TipoCodigo
   Cursor("fecha_creado") = date()
   Cursor("hash") = md5(Gift)
   Cursor("cantidad_utilizado") = 0
   Cursor("utilizado") = 0

   Cursor.Update
   A = A + 1
  end if

  Cursor.Close
  Gift = ""
Loop    
Response.Write "Proceso Terminado!!"




%>
